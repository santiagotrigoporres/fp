/*
Ejercicio sin resolver I.7

Solicitar el número ->
  printf("¿Número? ");
  scanf("%d",&numero);
Comprobar todos los números entre 1 y 10.000 ->
  for(int i=1;i<=10000;i++){
    ...
  }
En cada iteración, separar las cifras del número y calcular su suma y producto ->
  Inicializar las variables ->
    suma=0;
    producto=1;
  Inicializar una variable auxiliar de la que se extraerán dígitos por la derecha ->
    auxiliar=i;
  Mientras la variable tenga dígitos, repetir el proceso ->
    while(auxiliar>0){
      ...
    }
  En cada iteración,obtener el módulo de 10 (cifra de la derecha) y dividir la variable por 10 ->
    cifra=auxiliar%10;
    suma=suma+cifra;
    producto=producto*cifra;
    auxiliar=auxiliar/10;
  Comprobar si se cumplen las condiciones y mostrar el resultado, en su caso ->
    if((numero%suma)==0){
      printf("Suma de las cifras (%d) de %d divisora del número\n",suma,i);
    }
    if(producto!=0&&(producto%numero)==0){
      printf("Producto de las cifras (%d) de %d múltiplo del número\n",producto,i);
    }
*/

#include <stdio.h>

int main(){

  int producto,suma; /* Producto y suma de las cifras */
  int auxiliar; /* Variable auxiliar */
  int numero; /* Número solicitado al usuario */
  int cifra; /* Última cifra obtenida */

  /* Solicitar el número */
  printf("\250N\243mero? ");
  scanf("%d",&numero);

  /* Comprobar todos los números entre 1 y 10.000 */
  for(int i=1;i<=10000;i++){

    /* En cada iteración, separar las cifras del número y calcular su suma y producto */
    suma=0;
    producto=1;

    auxiliar=i;

    while(auxiliar>0){

      /* En cada iteración,obtener el módulo de 10 (cifra de la derecha) y dividir la variable por 10 */
      cifra=auxiliar%10;
      suma=suma+cifra;
      producto=producto*cifra;
      auxiliar=auxiliar/10;

    }

    /* Comprobar si se cumplen las condiciones y mostrar el resultado, en su caso */
    if((numero%suma)==0){
      printf("Suma de las cifras (%d) de %d divisora del n\243mero\n",suma,i);
    }

    if(producto!=0&&(producto%numero)==0){ /*** ERROR: se cuentan los productos nulos (con una cifra igual a 0) ***/
      printf("Producto de las cifras (%d) de %d m\243ltiplo del n\243mero\n",producto,i);
    }
  }

  return 0;

}
