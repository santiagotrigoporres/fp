/*
Ejercicio sin resolver I.1

Solicitar el número ->
  printf("¿Número? ");
  scanf("%d",&numero);
Imprimir la cabecera de la tabla ->
  printf("Tabla de multiplicar por %d\n",numero);
  printf("---------------------------\n");
Imprimir la tabla de multiplicar (de 1 a 10) de dicho número ->
  for(int i=1;i<=10;i++){
    printf("%6d x %2d = %7d\n",numero,i,numero*i);
  }
*/

#include <stdio.h>

int main(){

  int numero;  /* Número solicitado al usuario */

  /* Solicitar el número */
  printf("\250N\243mero? ");
  scanf("%d",&numero);

  /* Imprimir la cabecera de la tabla */
  printf("Tabla de multiplicar por %d\n",numero);
  printf("---------------------------\n");

  /* Imprimir la tabla de multiplicar (de 1 a 10) de dicho número */
  for(int i=1;i<=10;i++){
    printf("%6d x %2d = %7d\n",numero,i,numero*i);
  }

  return 0;

}
