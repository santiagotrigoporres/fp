/*
Ejercicio sin resolver I.2

Solicitar los números ->
  Solicitar el primer número ->
    printf("¿Primer número? ");
    scanf("%d",&primer_numero);
  Solicitar el segundo número ->
    printf("¿Segundo número? ");
    scanf("%d",&segundo_numero);
Calcular el máximo común divisor ->
  Inicializar las variables auxiliares de división ->
    if(primer_numero>=segundo_numero){
      dividendo=primer_numero;
      divisor=segundo_numero;
    }
    else{
      dividendo=segundo_numero;
      divisor=primer_numero;
    }
  Aplicar el algoritmo de Euclides ->
    Comprobar que el resto de la división no sea 0 ->
    while(dividendo%divisor!=0){
      ...
    }
    En cada iteración, hacer que el dividendo sea el anterior divisor y que éste sea el resto de la división ->
      auxiliar=divisor;
      divisor=dividendo%divisor;
      dividendo=auxiliar;
    Obtener el máximo común divisor, que es igual al último divisor ->
      maximo_comun_divisor=divisor;
Imprimir el resultado ->
  printf("El máximo común divisor de %d y %d es: %d\n",primer_numero,segundo_numero,maximo_comun_divisor);
*/

#include <stdio.h>

int main(){

  int primer_numero,segundo_numero;  /* Números solicitados al usuario */
  int auxiliar,dividendo,divisor; /* Variables utilizadas en la aplicación del algoritmo de Euclides */
  int maximo_comun_divisor; /* Resultado del cálculo */

  /* Solicitar los números */
  printf("\250Primer n\243mero? ");
  scanf("%d",&primer_numero);
  printf("\250Segundo n\243mero? ");
  scanf("%d",&segundo_numero);

  /* Inicializar las variables auxiliares de división */
  if(primer_numero>=segundo_numero){

    dividendo=primer_numero;
    divisor=segundo_numero;

  }

  else{

    dividendo=segundo_numero;
    divisor=primer_numero;

  }

  /* Aplicar el algoritmo de Euclides */
  while(dividendo%divisor!=0){

    /* En cada iteración, hacer que el dividendo sea el anterior divisor y que éste sea el resto de la división */
    auxiliar=divisor;
    divisor=dividendo%divisor;
    dividendo=auxiliar;

  }

  /* Obtener el máximo común divisor, que es igual al último divisor */
  maximo_comun_divisor=divisor;

  /* Imprimir el resultado */
  printf("El m\240ximo com\243n divisor de %d y %d es %d\n",primer_numero,segundo_numero,maximo_comun_divisor);

  return 0;

}
