/*
Ejercicio sin resolver I.6

Inicializar las variables ->
  caracter=' ';
  caracteres=0;
  vocales=0;
  aes=0;
  es=0;
  ies=0;
  oes=0;
  ues=0;
Comprobar cada vez que no se ha introducido un punto ->
  while(caracter!=terminador){
    ...
  }
En cada iteración, leer el siguiente carácter ->
  scanf("%c",&caracter);
  fflush(stdin);
Actualizar los contadores que procedan ->
  caracteres++;
  if(caracter=='a'||caracter=='e'||caracter=='i'||caracter=='o'||caracter=='u'||caracter=='A'||caracter=='E'||caracter=='I'||caracter=='O'||caracter=='U'){
    vocales++;
    if(caracter=='a'||caracter=='A'){
      aes++;
    }
    else if(caracter=='e'||caracter=='E'){
      es++;
    }
    else if(caracter=='i'||caracter=='I'){
      ies++;
    }
    else if(caracter=='o'||caracter=='O'){
      oes++;
    }
    else{
      ues++;
    }
  }
Imprimir los resultados ->
  printf("%d caracteres (incluyendo el punto)\n",caracteres);
  printf("  de los que %d vocales\n",vocales);
  printf("    de las que %d aes\n",aes);
  printf("    de las que %d es\n",es);
  printf("    de las que %d íes\n",ies);
  printf("    de las que %d oes\n",oes);
  printf("    de las que %d úes\n",ues);
*/

#include <stdio.h>

int main(){

  const char terminador='.'; /* Carácter terminador */

  int aes,es,ies,oes,ues; /* Contador de vocales específicas */
  int caracteres; /* Contador de caracteres */
  char caracter; /* Carácter leído del teclado */
  int vocales; /* Contador de vocales */

  /* Inicializar las variables; */
  caracter=' ';
  caracteres=0;
  vocales=0;
  aes=0;
  es=0;
  ies=0;
  oes=0;
  ues=0;

  /* Comprobar cada vez que no se ha introducido un punto */
  while(caracter!=terminador){

    /* En cada iteración, leer el siguiente carácter */
    scanf("%c",&caracter);
    fflush(stdin);

    /* Actualizar los contadores que procedan */
    caracteres++;

    if(caracter=='a'||caracter=='e'||caracter=='i'||caracter=='o'||caracter=='u'||caracter=='A'||caracter=='E'||caracter=='I'||caracter=='O'||caracter=='U'){

      vocales++;

      if(caracter=='a'||caracter=='A'){
        aes++;
      }
      else if(caracter=='e'||caracter=='E'){
        es++;
      }
      else if(caracter=='i'||caracter=='I'){
        ies++;
      }
      else if(caracter=='o'||caracter=='O'){
        oes++;
      }
      else{
        ues++;
      }
    }
  }

  /* Imprimir los resultados */
  printf("%d caracteres (incluyendo el punto)\n",caracteres);
  printf("  de los que %d vocales\n",vocales);
  printf("    de las que %d aes\n",aes);
  printf("    de las que %d es\n",es);
  printf("    de las que %d \241es\n",ies);
  printf("    de las que %d oes\n",oes);
  printf("    de las que %d \243es\n",ues);

  return 0;

}
