/*
Ejercicio sin resolver I.4

Solicitar el error tolerado ->
  printf("¿Error tolerado? ");
  scanf("%f",&error_tolerado);
Comprobar que el error tolerado sea positivo y no supere un máximo ->
  if(error_tolerado>=0&&error_tolerado<=error_tolerado_maximo){
    ...
  }
Inicializar las variables (resultado obtenido, error y número de iteración) ->
  numero_e=1;
  error=e_referencia-numero_e;
  iteracion=1;
Repetir el cálculo mientras el error sea superior al permitido ->
  Comprobar la diferencia entre el error y el permitido ->
    while(error>error_tolerado){
      ...
    }
  En cada iteración, añadir la inversa del factorial del número de iteración ->
    Calcular el factorial ->
      factorial=1;
      for(int i=2;i<=iteracion;i++){
        factorial=factorial*i;
      }
    Sumar el inverso del factorial ->
      numero_e=numero_e+1/float(factorial);
    Actualizar el error (no debería hacer falta comprobar si el resultado se ha excedido de la referencia) ->
      error=e_referencia-numero_e;
    Incrementar el número de iteración ->
      iteracion++;
Imprimir el resultado obtenido y el número de iteraciones ->
  printf("Valor de e calculado: %10.8f tras %d iteraciones\n",numero_e,iteracion);
*/

#include <stdio.h>

int main(){

  const float e_referencia=2.7182818284590452353602874713527; /* Valor de referencia del número e */
  const float error_tolerado_maximo=0.1; /* Límite máximo del error tolerado */

  float error_tolerado; /* Error tolerado solicitado al usuario */
  float numero_e; /* Último resultado obtenido */
  int iteracion; /* Número de iteración */
  int factorial; /* Factorial del número de iteración */
  float error; /* Error del último resultado */

  /* Solicitar el error tolerado */
  printf("\250Error tolerado? ");
  scanf("%f",&error_tolerado);

  /* Comprobar que el error tolerado sea positivo y no supere un máximo */
  if(error_tolerado>=0&&error_tolerado<=error_tolerado_maximo){

    /* Inicializar las variables (resultado obtenido, error y número de iteración) */
    numero_e=1;
    error=e_referencia-numero_e;
    iteracion=1;

    /* Repetir el cálculo mientras el error sea superior al permitido */
    while(error>error_tolerado){

      /* Calcular el factorial */
      factorial=1;

      for(int i=2;i<=iteracion;i++){
        factorial=factorial*i;
      }

      /* Sumar el inverso del factorial */
      numero_e=numero_e+1/float(factorial);

      /* Actualizar el error (no debería hacer falta comprobar si el resultado se ha excedido de la referencia) */
      error=e_referencia-numero_e;

      /* Incrementar el número de iteración */
      iteracion++;

    }

    /* Imprimir el resultado obtenido y el número de iteraciones */
    printf("Valor de e calculado: %10.8f tras %d iteraciones\n",numero_e,iteracion);

  }

  return 0;

}
