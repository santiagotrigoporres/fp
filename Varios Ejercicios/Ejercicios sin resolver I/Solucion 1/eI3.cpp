/*
Ejercicio sin resolver I.3

Solicitar el lado ->
  printf("¿Lado? ");
  scanf("%d",&lado);
Comprobar que el lado esté en un rango determinado ->
  if(lado>=lado_minimo&&lado<=lado_maximo){
    ...
  }
Dibujar el rombo ->
  Dibujar las filas de la parte superior y el vértice ->
    Repetir el proceso tantas veces como filas deban dibujarse ->
      for(int i=1;i<=lado;i++){
        ...
      }
    En cada iteración, dibujar una fila ->
      Escribir los espacios en blanco a la izquierda ->
        for(int j=0;j<lado-i;j++){
          printf(" ");
        }
      Escribir los asteriscos ->
        for(int j=0;j<i;j++){
          printf("*");
          if(j+1<i){
            printf(" ");
          }
        }
      Saltar a la línea ->
        printf("\n");
  Dibujar las filas de la parte inferior ->
    Repetir el proceso tantas veces como filas deban dibujarse ->
      for(int i=lado-1;i>=1;i--){
        ...
      }
    En cada iteración, dibujar una fila ->
      Escribir los espacios en blanco a la izquierda ->
        for(int j=0;j<lado-i;j++){
          printf(" ");
        }
      Escribir los asteriscos ->
        for(int j=0;j<i;j++){
          printf("*");
          if(j+1<i){
            printf(" ");
          }
        }
      Saltar a la línea ->
        printf("\n");
*/

#include <stdio.h>

int main(){

  const int lado_maximo=30; /* Límite superior del lado */
  const int lado_minimo=1; /* Límite inferior del lado */

  int lado; /* Lado solicitado al usuario */

  /* Solicitar el lado */
  printf("\250Lado? ");
  scanf("%d",&lado);

  /* Comprobar que el lado esté en un rango determinado */
  if(lado>=lado_minimo&&lado<=lado_maximo){

    /* Dibujar las filas de la parte superior y el vértice */
    for(int i=1;i<=lado;i++){ /*** ERROR: <= ***/

      /* Escribir los espacios en blanco a la izquierda */
      for(int j=0;j<lado-i;j++){
        printf(" ");
      }

      /* Escribir los asteriscos */
      for(int j=0;j<i;j++){

        printf("*");

        if(j+1<i){
          printf(" ");
        }
      }

      /* Saltar a la línea */
      printf("\n");

    }

    /* Dibujar las filas de la parte inferior */
    for(int i=lado-1;i>=1;i--){

      /* Escribir los espacios en blanco a la izquierda */
      for(int j=0;j<lado-i;j++){
        printf(" ");
      }

      /* Escribir los asteriscos */
      for(int j=0;j<i;j++){

        printf("*");

        if(j+1<i){
          printf(" ");
        }
      }

      /* Saltar a la línea */
      printf("\n");

    }
  }

  return 0;

}
