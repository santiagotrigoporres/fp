/*
Ejercicio sin resolver I.5

Solicitar los lados del triángulo ->
  printf("¿Primer lado? ");
  scanf("%f",&primer_lado);
  printf("¿Segundo lado? ");
  scanf("%f",&segundo_lado);
  printf("¿Tercer lado? ");
  scanf("%f",&tercer_lado);
Comprobar que los lados sean positivos ->
  if(primer_lado>0&&segundo_lado>0&&tercer_lado>0){
    ...
  }
Ordenar los lados de menor a mayor ->
  Ordenar los dos primeros ->
    if(primer_lado>segundo_lado){
      auxiliar=segundo_lado;
      segundo_lado=primer_lado;
      primer_lado=auxiliar;
    }
  Ordenar los tres ->
    Ordenar el primero con el tercero ->
      if(primer_lado>tercer_lado){
        auxiliar=tercer_lado;
        tercer_lado=segundo_lado;
        segundo_lado=primer_lado;
        primer_lado=auxiliar;
      }
    Ordenar el segundo con el tercero ->
      else if(segundo_lado>tercer_lado){
        auxiliar=tercer_lado;
        tercer_lado=segundo_lado;
        segundo_lado=auxiliar;
      }
Comprobar el tipo de triángulo e imprimir el resultado ->
  if(primer_lado+segundo_lado<tercer_lado){
    printf("Los lados no forman triángulo\n");
  }
  else if(primer_lado==segundo_lado&&segundo_lado==tercer_lado){
    printf("Los lados forman un triángulo equilátero\n");
  }
  else{

    if(primer_lado==segundo_lado||segundo_lado==tercer_lado){
      printf("Los lados forman un triángulo isósceles\n");
    }
    else{
      printf("Los lados forman un triángulo escaleno\n");
    }

    if(primer_lado*primer_lado+segundo_lado*segundo_lado==tercer_lado*tercer_lado){
      printf("Además, el triángulo es rectángulo\n");
    }
  }
*/

#include <stdio.h>

int main(){

  int primer_lado,segundo_lado,tercer_lado; /* Lados solicitados al usuario */
  int auxiliar; /* Variable auxiliar utilizada para ordenar los lados */

  /* Solicitar los lados del triángulo */
  printf("\250Primer lado? ");
  scanf("%d",&primer_lado);
  printf("\250Segundo lado? ");
  scanf("%d",&segundo_lado);
  printf("\250Tercer lado? ");
  scanf("%d",&tercer_lado);

  /* Comprobar que los lados sean positivos */
  if(primer_lado>0&&segundo_lado>0&&tercer_lado>0){

    /* Ordenar los lados de menor a mayor */
    if(primer_lado>segundo_lado){

      auxiliar=segundo_lado;
      segundo_lado=primer_lado;
      primer_lado=auxiliar;

    }

    if(primer_lado>tercer_lado){

      auxiliar=tercer_lado;
      tercer_lado=segundo_lado;
      segundo_lado=primer_lado;
      primer_lado=auxiliar;

    }

    else if(segundo_lado>tercer_lado){

      auxiliar=tercer_lado;
      tercer_lado=segundo_lado;
      segundo_lado=auxiliar;

    }

    /* Comprobar el tipo de triángulo e imprimir el resultado */
    if(primer_lado+segundo_lado<tercer_lado){
      printf("Los lados no forman tri\240ngulo\n");
    }

    else if(primer_lado==segundo_lado&&segundo_lado==tercer_lado){
      printf("Los lados forman un tri\240ngulo equil\240tero\n");
    }

    else{

      if(primer_lado==segundo_lado||segundo_lado==tercer_lado){
        printf("Los lados forman un tri\240ngulo is\242sceles\n");
      }
      else{
        printf("Los lados forman un tri\240ngulo escaleno\n");
      }

      if(primer_lado*primer_lado+segundo_lado*segundo_lado==tercer_lado*tercer_lado){
        printf("Adem\240s, el tri\240ngulo es rect\240ngulo\n");
      }
    }
  }

  return 0;

}
