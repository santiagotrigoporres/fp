/*
Ejercicio sin resolver I.8

Solicitar los datos del préstamo ->
  printf("¿Capital? ");
  scanf("%f",&capital);
  printf("¿Tipo de interés (%%)? ");
  scanf("%f",&interes);
  printf("¿Años? ");
  scanf("%d",&annos);
Comprobar que los datos sean correctos ->
  if(capital>0&&interes>=0&&annos>0){
    ...
  }
Calcular y mostrar la anualidad ->
  anualidad=capital*(pow(1+interes/100.0,annos)*interes/100.0)/(pow(1+interes/100.0,annos)-1.0);
  printf("Anualidad: %10.02f\n",anualidad);
  printf("Año   Intereses   Amortización   Saldo vivo\n");
  printf("-------------------------------------------\n");
Para cada año, calcular y mostrar la parte de intereses, la parte de amortización y el saldo vivo ->
  Inicializar la variable ->
    saldo_vivo=capital;
  Repetir el proceso tantas veces como años tenga el préstamo ->
    for(int i=1;i<=annos;i++){
      ...
    }
  Realizar los cálculos pertinentes ->
    parte_intereses=saldo_vivo*interes/100;
    parte_capital=anualidad-parte_intereses;
    saldo_vivo=saldo_vivo-parte_capital;
  Mostrar el resultado ->
    printf("%6d%12.02f%15.02f%10.02f\n",i,parte_intereses,parte_capital,saldo_vivo);
*/

#include <math.h>
#include <stdio.h>

int main(){

  float parte_capital,parte_intereses,saldo_vivo; /* Datos del préstamo mostrados para cada año */
  float anualidad; /* Anualidad calculada a partir de los datos del usuario */
  float interes; /* Tipo de interés (en tanto por ciento) solicitado al usuario */
  float capital; /* Capital solicitado al usuario */
  int annos; /* Número de años de pago solicitado al usuario */

  /* Solicitar los datos del préstamo */
  printf("\250Capital? ");
  scanf("%f",&capital);
  printf("\250Tipo de inter\202s (%%)? "); /*** ERROR: %% ***/
  scanf("%f",&interes);
  printf("\250A\244os? ");
  scanf("%d",&annos);

  /* Comprobar que los datos sean correctos */
  if(capital>0&&interes>0&&annos>0){ /*** ERROR: >=0 (la fórmula falla) ***/

    /* Calcular y mostrar la anualidad */
    anualidad=capital*(pow(1+interes/100.0,annos)*interes/100.0)/(pow(1+interes/100.0,annos)-1.0); /*** ERROR: anualidad no declarada ***/
    printf("Anualidad: %10.02f\n",anualidad);
    printf("A\244o   Intereses   Amortizaci\242n   Saldo vivo\n");
    printf("-------------------------------------------\n");

    /* Para cada año, calcular y mostrar la parte de intereses, la parte de amortización y el saldo vivo */
    saldo_vivo=capital;

    for(int i=1;i<=annos;i++){ /*** ERROR: int ***/

      parte_intereses=saldo_vivo*interes/100;
      parte_capital=anualidad-parte_intereses;
      saldo_vivo=saldo_vivo-parte_capital;

      printf("%6d%12.02f%15.02f%10.02f\n",i,parte_intereses,parte_capital,saldo_vivo);

    }
  }

  return 0;

}
