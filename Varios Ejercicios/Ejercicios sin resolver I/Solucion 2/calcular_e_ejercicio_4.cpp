/*Calcula el n�mero e mediante desarrollo en serie con un error menor
que el introducido como dato.*/
#include <stdio.h>
float error=0.0;    /*dato a introducir*/
int factorial_i=1;  /*factorial*/
float valor =1.0;   /*variable donde se va guardan el valor de e*/
float fraccion=1.0; /*fracci�n resultante de 1/i!. Se sigue calculando mientras
                    fraccion>error */
int iterador =1;    /*variable auxiliar para calcular el factorial*/
int i=1;            /*variable auxiliar para calcular el factorial y fraccion*/
int main (){

  printf ("Error ? ");
  scanf ("%f",&error);

  while (fraccion>error){   /*condici�n para seguir generando fraccion*/
    while (iterador <= i){  /*condici�n hasta d�nde se genera factorial*/
      factorial_i = factorial_i*iterador; /*factorial generado*/
      iterador++;
      }
    fraccion =1.0/float(factorial_i);   /*fraccion generada*/

    /*printf ("fraccion = %3.22f\n", fraccion);*/

    valor = valor + fraccion;   /*generaci�n progresiva del valor de e*/
    i++;
    }

  printf ("Valor de e = %19.22f", valor);
  }

