#include <stdio.h>

int numero =0;
int main (){
  printf ("Numero? ");
  scanf ("%d", &numero);

  printf ("Tabla de multiplicar por %d\n", numero);
  printf ("============================\n");
  printf ("\t%d x  1 = %2d\n", numero, numero*1);
  printf ("\t%d x  2 = %2d\n", numero, numero*2);
  printf ("\t%d x  3 = %2d\n", numero, numero*3);
  printf ("\t%d x  4 = %2d\n", numero, numero*4);
  printf ("\t%d x  5 = %2d\n", numero, numero*5);
  printf ("\t%d x  6 = %2d\n", numero, numero*6);
  printf ("\t%d x  7 = %2d\n", numero, numero*7);
  printf ("\t%d x  8 = %2d\n", numero, numero*8);
  printf ("\t%d x  9 = %2d\n", numero, numero*9);
  printf ("\t%d x 10 = %2d\n", numero, numero*10);


  }
