/*Analiza un texto terminado en un punto (.) y contabiliza:
-n�mero total de caracteres.
-n�mero total de vocales utilizadas.
-total de veces que se emplea la "a" may�scula o min�scula.
-total de veces que se emplea la "e" may�scula o min�scula.
-total de veces que se emplea la "i" may�scula o min�scula.
-total de veces que se emplea la "o" may�scula o min�scula.
-total de veces que se emplea la "u" may�scula o min�scula.
*/

#include <stdio.h>
#include <ctype.h>
int main (){
  char caracter;
  int numCaracteres=0;
  int numVocales=0;
  int numAs=0;
  int numEs=0;
  int numIs=0;
  int numOs=0;
  int numUs=0;

  printf ("Introducir texto.\n");
  while (caracter!='.'){
    scanf ("%c",&caracter);
    caracter = tolower(caracter);

    if (caracter=='a'){
      numVocales++;
      numAs++;
      }else if (caracter=='e'){
      numVocales++;
      numEs++;
      }else if (caracter=='i'){
      numVocales++;
      numIs++;
      }else if (caracter=='o'){
      numVocales++;
      numOs++;
      }else if (caracter=='u'){
      numVocales++;
      numUs++;
      }
    numCaracteres++;
    }
  printf ("El n�mero de caracteres es: %d\n", numCaracteres);
  printf ("El n�mero de vocales es: %d\n", numVocales);
  printf ("El n�mero de as es: %d\n", numAs);
  printf ("El n�mero de es es: %d\n", numEs);
  printf ("El n�mero de is es: %d\n", numIs);
  printf ("El n�mero de os es: %d\n", numOs);
  printf ("El n�mero de us es: %d\n", numUs);
  }
