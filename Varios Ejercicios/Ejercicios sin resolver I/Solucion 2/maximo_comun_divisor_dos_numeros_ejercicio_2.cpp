/*Calcula el m�ximo com�n divisor de dos enteros.*/
#include <stdio.h>

int primerNumero=0;     /*primer n�mero a introducir*/
int segundoNumero=0;    /*segundo n�mero a introducir*/
int maxComunDivisor=0;  /*variable para albergar resultado*/


int main (){
  printf ("Primer Numero? ");
  scanf ("%d", &primerNumero);
  printf ("Segundo Numero? ");
  scanf ("%d", &segundoNumero);

  if (primerNumero>segundoNumero){    /*Comprobamos si el primer n�mero es mayor que
                                      el segundo.*/
    for (int i=1; i<=segundoNumero; i++){ /*Se contempla la posibilidad de que el segundo
                                          n�mero sea divisor del primero*/
      if ((primerNumero%i==0)&&(segundoNumero%i==0)){ /*Comprobamos que sea divisor
                                                      de ambos n�meros*/
        maxComunDivisor =i;
        }
      }
    }else {                                   /*primer n�mero menor que el segundo.*/
      for (int i=1; i<=primerNumero; i++){    /*idem: posibilidad de que el primero sea
                                              divisor del segundo*/
        if ((primerNumero%i==0)&&(segundoNumero%i==0)){ /*idem.*/
          maxComunDivisor =i;
          }
        }
      }
  printf ("El m�ximo com�n divisor es:  %d\n", maxComunDivisor);

  }
