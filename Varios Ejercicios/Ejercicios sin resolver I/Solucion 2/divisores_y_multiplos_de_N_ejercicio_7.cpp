/*
Introducido un n�mero N como dato, se escriben todos los n�meros del 1 al 10000
que cumplen las reglas siguientes:
-La suma de sus cifras ha de ser un divisor de N.
-El producto de sus cifras ha de ser un m�ltiplo de N.
*/

#include <stdio.h>

int main (){
  int numero;
  printf ("Introduzca n�mero:\n");
  scanf ("%d", &numero);

  /*Los n�meros entre el 1 y el 10000 se forman como iteraciones de cuatro cifras
  adjuntas*/
  for (int i=0; i<10; i++){
    for (int j=0; j<10; j++){
      for (int k=0; k<10; k++){
        for (int l=1; l<10; l++){
          if (i==0 && j==0 && k==0 ){l++;} /*El primer n�mero es el 2*/
          if (numero>(i+j+k+l) && numero%(i+j+k+l)==0){ /*Comprobamos que n�mero sea
                                                        mayor que sus posibles divisores*/
             printf ("Divisores de %d: %d%d%d%d\n", numero, i,j,k,l);
           }
          if ((i*j*k*l)>numero && (i*j*k*l)%numero==0){ /*Comprobamos que los posibles
                                                        m�ltiples sean mayores que n�mero*/

             printf ("M�ltiplos de %d: %d%d%d%d\n", numero, i,j,k,l);

            }
          }
        }
      }
    }

  }
