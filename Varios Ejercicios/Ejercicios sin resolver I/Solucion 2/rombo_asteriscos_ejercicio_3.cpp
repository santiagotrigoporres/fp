/*Escribe un rombo sim�trico de asteriscos, tomando como
dato el n�mero de asteriscos que tiene el lado*/

#include <stdio.h>

int lado=0;
int ancho;
int main (){

  printf ("Lado? ");
  scanf ("%d",&lado);
  printf ("\n");
  for (int i=lado; i>=1; i--){  /*L�neas del tri�ngulo superior.*/
    ancho=lado-1;
    for (int j=i-1; j>=0; j--){ /*Espacios de punta a base del tri�ngulo superior.*/
      printf (" ");
      }
    for (int k=i-2; k<ancho; k++){  /*Asteriscos tri�ngulo superior.*/
      printf ("* ");                /*Al emplear este patr�n de impresi�n, conseguimos
                                    todo el tri�ngulo superior del rombo.*/
      }
    printf ("\n\n");
    }
  for (int l=lado-1; l>=1; l--){    /*L�neas del tri�ngulo inferior.*/
    for (int m=l-2; m<ancho; m++){  /*Espacios de base a punta del tri�ngulo inferior.*/
      printf (" ");
      }
    for (int n=l; n>=1; n--){       /*Asteriscos tri�ngulo inferior.*/
      printf ("* ");                /*Al emplear este patr�n de impresi�n, conseguimos
                                    todo el tri�ngulo inferior del rombo.*/
      }
    printf ("\n\n");
    }

  }
