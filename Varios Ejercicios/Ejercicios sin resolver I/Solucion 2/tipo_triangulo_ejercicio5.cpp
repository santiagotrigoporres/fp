/*Lee la longitud de los tres lados de un tri�ngulo y analiza qu� tipo
de tri�ngulo es.
Los resultados posibles son:
-No forman tri�ngulo (un lado mayor es mayor que la suma de los otros dos).
-Tri�ngulo equil�tero (tres lados iguales).
-Tri�ngulo is�sceles (dos lados iguales).
-Tri�ngulo escaleno (tres lados distintos).
-Tri�ngulo rect�ngulo (sus lados cumplen el teorema de pit�goras).
*/

#include <stdio.h>

int main (){

  int lado1, lado2, lado3;

  printf ("introduzca longitud del primer lado.\n");
  scanf ("%d",&lado1);
  printf ("introduzca longitud del segundo lado.\n");
  scanf ("%d",&lado2);
  printf ("introduzca longitud del tercer lado.\n");
  scanf ("%d",&lado3);

  if (lado1+lado2<lado3 || lado1+lado3<lado2 || lado3+lado2<lado1){
    printf ("No es tri�ngulo.\n");

    }else if (lado1==lado2 && lado2==lado3){
      printf ("Es un tri�ngulo equil�tero.\n");

      } else if ((lado1*lado1+lado2*lado2 ==lado3*lado3)||
                  (lado3*lado3+lado2*lado2 ==lado1*lado1)||
                  (lado1*lado1+lado3*lado3 ==lado2*lado2)){
                    printf ("Es un tri�ngulo rect�ngulo.\n");
        } else if ((lado1==lado2 && lado2!=lado3) ||
                  (lado2==lado3 && lado2!=lado1) ||
                  (lado1==lado3 && lado2!=lado3)){
          printf ("Es un tri�ngulo is�sceles.\n");
        }else if ((lado1!=lado2 && lado2!=lado3)){
          printf ("Es un tri�ngulo escaleno.\n");
        }


  }
