/*A partir del capital, el porcentaje de inter�s anual y los a�os de amortizaci�n
de un cr�dito, calcula la anualidad fija a pagar a lo largo de los a�os.*/


#include <stdio.h>

int main (){
  int annos;                /*variable a introducir*/
  float capital, interes;  /*variables a introducir*/
  float intereses_parciales;  /*Intereses por cada anulidad*/
  float amortizacion =0;      /*Amortizaci�n por cada anualidad*/
  float anualidad;            /*anualidad = intereses_parciales+amortizaci�n*/
  float amortizado =0;        /*capital amortizado tras cada anualidad (cumulativo)*/
  float parcial=1;            /*c�lculo parcial*/

  printf ("Capital:?");
  scanf("%f",&capital);
  fflush (stdin);

  printf ("A�os:?");
  scanf("%d",&annos);
  fflush (stdin);

  printf ("Inter�s:?");
  scanf("%f",&interes);


  for (int i=1; i<=annos; i++){     /*c�culo de (1+I/100)^A*/
    parcial = parcial*(1.0+interes/100.0);
    }

  anualidad = capital * (parcial*interes/100.0)/(parcial-1); /*c�lculo anualidad*/

  printf ("Anualidad: %9.0f\n\n", anualidad);   /*Impresi�n anualidad*/

  printf ("A�o\t\tIntereses\t\tAmortizaci�n\n");

  for ( int i=1; i<=annos; i++){
    intereses_parciales = (capital-amortizado)*interes/100.0;  /*C�lculo intereses parciales*/
    amortizacion =anualidad-(capital-amortizado)*interes/100.0;/*C�lculo de cada amortizaci�n*/
    amortizado = amortizado +amortizacion;                /*C�lculo de capital amortizado*/

    printf ("%d\t\t", i);
    printf ("%9.2f\t\t", intereses_parciales);
    printf ("%9.2f\n", amortizacion);
    }
}
