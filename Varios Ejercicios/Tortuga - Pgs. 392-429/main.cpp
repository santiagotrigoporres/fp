/**************************************
* Programa: DibujarC2
*
* Descripcion:
*Este programa lee como dato el orden de una curva C, y a
*continuación la dibuja en forma de texto en pantalla
*****************************************************/

#include <stdio.h>
#include "curva.h"
#include "tortuga.h"
#include "papel.h"

int main() {
  int orden;
  TipoTortuga tt;

  printf( "Orden: " ) ;
  scanf( "%d" , &orden );
  PonerEnBlanco();
  /* posición inicial de conveniencia */
  tt.Poner( 8, 3, Este );
  CurvaC( orden, tt );
  Imprimir() ;
}
