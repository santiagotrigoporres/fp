/***************************************************** ***********
* M�dulo: Curva


*Este m�dulo encapsula un �nico ejemplar de dato abstracto
*de tipo PAPEL, capaz de almacenar un dibujo formado
*por trazos en una cuadricula
****************************************************************/
#include <stdio.h>
#include "papel.h"

/*---------- Elementos privados ----------*/

const int ANCHO = 32; /* cuadricula en */
const int ALTO = 19; /* la pantalla */

typedef char MatrizMarcas[ANCHO][ALTO];

static MatrizMarcas marcasH;
static MatrizMarcas marcasV;
static bool Dentro( int x, int y ) {
  return (x >= 0 && x < ANCHO && y >= 0 && y < ALTO);
}

/*-------- -- Elementos p�blicos ----------*/

void PonerEnBlanco() {
  for (int x=0; x<ANCHO; x++) {
    for (int y=0; y<ALTO; y++) {
      marcasH[x][y]= ' ';
      marcasV[x][y]= ' ';
    }
  }
}

void MarcarHorizontal( int x, int y ) {
  if (Dentro( x, y )) {
    marcasH[x][y] = '_';
  }
}

void MarcarVertical(int x, int y ) {
  if (Dentro( x, y )) {
    marcasV[x][y] = '|';
  }
}

void Imprimir() {
  for (int y=ALTO-1; y>=0; y-- ) {
    for (int x=0; x<ANCHO; x++) {
      printf( "%c%c", marcasV[x][y] , marcasH[x][y]);
    }
    printf( "\n" );
  }
}
