/****************************************************************
* Interfaz de m�dulo: Papel

*Este m�dulo encapsula un �nico ejemplar de dato abstracto
*de tipo PAPEL, capaz de almacenar un dibujo formado
*por trazos en una cuadricula
****************************************************************/
#pragma once

void PonerEnBlanco( );
void MarcarHorizontal( int x, int y );
void MarcarVertical( int x, int y );
void Imprimir();
