/****************************************************************
* Interfaz de m�dulo: Tortuga

*Este m�dulo define el tipo abstracto TORTUGA, capaz de
*ir trazando una trayectoria mediante avances y giros
****************************************************************/
#pragma once

typedef enum TipoRumbo { Este, Norte, Oeste, Sur };

typedef struct TipoTortuga {
  void Poner( int x, int y, TipoRumbo rumbo );
  void Avanzar();
  void GirarDerecha();
  void GirarIzquierda();
private:
  int xx, yy;
  TipoRumbo sentido;
};
