/****************************************************************
* M�dulo: Tortuga

*Este m�dulo define el tipo abstracto TORTUGA, capaz de
*ir trazando una trayectoria mediante avances y giros
****************************************************************/
#include "tortuga.h"
#include "papel.h"

void TipoTortuga::Poner( int x, int y, TipoRumbo rumbo) {
xx = x;
yy = y;
sentido = rumbo;
}

void TipoTortuga::Avanzar() {
  switch (sentido) {
  case Norte:
    MarcarVertical( xx, yy );
    yy++ ;
    break;
  case Sur:
    yy--;
    MarcarVertical( xx, yy );
    break;
  case Este:
    MarcarHorizontal( xx, yy );
    xx++;
    break;
  case Oeste:
    xx--;
    MarcarHorizontal( xx, yy );
    break;
  }
}

void TipoTortuga:: GirarDerecha() {
  sentido = TipoRumbo( (int(sentido)-1+4) % 4 );
  /* +4 evita rumbo negativo */
}

void TipoTortuga::GirarIzquierda() {
  sentido = TipoRumbo( (int(sentido)+1) % 4 );
}
