/****************************************************************
* M�dulo: Tortuga

*Este m�dulo contiene la funcion que genera una Curva-C
****************************************************************/
#include "curva.h"

void CurvaC ( int orden, TipoTortuga & t){
  if (orden == 0){
      t.Avanzar();
  } else if (orden > 0){
    CurvaC( orden - 1, t);
    t.GirarDerecha();
    CurvaC ( orden - 1, t);
    t.GirarIzquierda();
  }
}
