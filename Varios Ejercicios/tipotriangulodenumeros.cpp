1 /**************************************************************
2 * Ejemplo 5.4.2; escribir un tri�ngulo de d�gitos. P�gina 114
3 *
4 * Programa: TrianguloDeNumeros.cpp
5 *
6 * Descripci�n:
7 * Este programa escribe un tri�ngulo de d�gitos.
8 * la altura se lee como dato y debe ser menor de 10.
9 *
10 ***************************************************************/
11
12 #include <stdio.h>
13 int main() {
14
15 const int centro = 35; /** Posici�n del eje del tri�ngulo */
16 const int inicial = 1; /** D�gito inicial: superior y laterales */
17 int nivel;
18
19
20 /** --Leer los datos --*/ {
21 printf(" �Altura del tri�ngulo ? ");
22 scanf("%d", &nivel);
23
24 /** esta sentencia la he incluido yo para detectar error al
25 introducir el n�mero, que ha de ser del 1 al 9- */
26
27 if (nivel>9 || nivel<1) {
28 printf("\n"); /** l�nea en blanco */
29 printf(" ERROR. EL N�MERO HA DE SER ENTRE 1 Y 9. \n" );
30 return 0;
31 }
32 }
33
34
35 /**-- Una iteraci�n por cada l�nea del tri�ngulo */
36 for(int altura = inicial; altura <= nivel; altura++) {
37
38 /**--Paso 1�: situar primer n�mero de cada l�nea --*/ {
39 for (int indice=1; indice <= centro-altura; indice++) {
40 printf(" ");
41 }
42 printf("%1d", inicial);
43 }
44
45
46 /**--Paso 2�; Primer mitad de la l�nea del tri�ngulo
47 Escribir n�mero consecutivos has altura --*/ {
48 for (int indice=inicial+1; indice<=altura; indice++) {
49 printf("%1d", indice);
50 }
51 }
52
53 /**-- Paso 3�: Segunda mitad de la l�nea del tri�ngulo
54 escribir n�meros decrecientes hasta inicial --*/ {
55 for (int indice=altura-1; indice>=inicial; indice--) {
56 printf("%1d", indice);
57 }
58 }
59
60
61 /**-- Paso 4�: Saltar a una nueva linea --*/ {
62 printf("\n");
63 }
64 }
65 }
66