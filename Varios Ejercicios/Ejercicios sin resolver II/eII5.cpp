/*
Ejercicio sin resolver II.5

Leer la cadena, el número de vocales diferentes y el número de consonantes diferentes
Inicializar las variables
Recorrer la cadena
  Si se está dentro de una palabra...
    Si el carácter es una letra, indicar que la palabra incluye dicha letra
    Si el carácter es un separador, indicar que ya no se está en una palabra y comprobar la variedad de letras
  Si no se está dentro de una palabra...
    Si el carácter no es un separador, incrementar el número de palabras e inicializar las variables de letras
Si se ha terminado la cadena dentro de una palabra, comprobar la variedad de letras
Imprimir los resultados
*/

#include <ctype.h>
#include <stdio.h>
#include <string.h>

int main(){

  const int numero_separadores=7;
  const int longitud_cadena=100;
  const int numero_letras=26;

  typedef char separadores_t[numero_separadores];
  typedef char cadena_t[longitud_cadena+1];
  typedef bool letras_t[numero_letras];

  const separadores_t separadores={' ','\t','\r','.',',',':',';'};

  int palabras,palabras_vocales,palabras_consonantes;
  int vocales_distintas,consonantes_distintas;
  int numero_vocales,numero_consonantes;
  letras_t letras;
  cadena_t cadena;
  bool separador;
  char caracter;
  bool palabra;
  int longitud;
  int indice;

  /* Leer la cadena, el número de vocales diferentes y el número de consonantes diferentes */
  printf("\250Frase? ");
  scanf("%s",cadena);
  fflush(stdin);
  printf("\250Número de vocales distintas? ");
  scanf("%d",&numero_vocales);
  fflush(stdin);
  printf("\250Número de consonantes distintas? ");
  scanf("%d",&numero_consonantes);

  /* Inicializar las variables */
  palabras=0;
  palabras_vocales=0;
  palabras_consonantes=0;
  palabra=false;
  longitud=strlen(cadena);

  /* Recorrer la cadena */
  for(int i=0;i<longitud;i++){

    caracter=tolower(cadena[i]);

    /* Determinar si el carácter es un separador */
    indice=0;
    separador=false;

    while(!separador&&indice<numero_separadores){

      if(separadores[indice]==caracter){
        separador=true;
      }

      indice++;

    }

    /* Si se está dentro de una palabra... */
    if(palabra){

      /* Si el carácter es una letra, indicar que la palabra incluye dicha letra */
      if(!separador){

        if(caracter>='a'&&caracter<='z'){
          letras[int(caracter-'a')]=true;
        }
      }

      /* Si el carácter es un separador, indicar que ya no se está en una palabra y comprobar la variedad de letras */
      else{

        palabra=false;

        vocales_distintas=0;
        consonantes_distintas=0;

        for(int j=0;j<numero_letras;j++){

          if(letras[j]){

            if(j==int('a'-'a')||j==int('e'-'a')||j==int('i'-'a')||j==int('o'-'a')||j==int('u'-'a')){
              vocales_distintas++;
            }
            else{
              consonantes_distintas++;
            }
          }
        }

        if(vocales_distintas>=numero_vocales){
          palabras_vocales++;
        }

        if(consonantes_distintas>=numero_consonantes){
          palabras_consonantes++;
        }
      }
    }

    /* Si no se está dentro de una palabra... */
    else{
      /* Si el carácter no es un separador, incrementar el número de palabras e inicializar las variables de letras */
      if(!separador){

        palabras++;
        palabra=true;

        for(int j=0;j<numero_letras;j++){
          letras[j]=false;
        }

        /* Contar la primera letra de la palabra */
        if(caracter>='a'&&caracter<='z'){ /*** ERROR: omisión ***/
          letras[int(caracter-'a')]=true;
        }
      }
    }
  }

  /* Si se ha terminado la cadena dentro de una palabra, comprobar la variedad de letras */
  if(palabra){

    vocales_distintas=0;
    consonantes_distintas=0;

    for(int i=0;i<numero_letras;i++){

      if(letras[i]){

        if(i==int('a'-'a')||i==int('e'-'a')||i==int('i'-'a')||i==int('o'-'a')||i==int('u'-'a')){
          vocales_distintas++;
        }
        else{
          consonantes_distintas++;
        }
      }
    }

    if(vocales_distintas>=numero_vocales){
      palabras_vocales++;
    }

    if(consonantes_distintas>=numero_consonantes){
      palabras_consonantes++;
    }
  }

  /* Imprimir los resultados */
  printf("%d palabras (%d con %d vocales o m\240s y %d con %d consonantes o m\240s)\n",palabras,palabras_vocales,numero_vocales,palabras_consonantes,numero_consonantes);

  return 0;

}
