/*
Ejercicio sin resolver II.4

Calcular el número de días transcurridos desde la fecha de referencia hasta la fecha seleccionada
  Calcular el número de días de los años completos
  Calcular el número de meses de los meses completos del año en curso
  Calcular el número de días del mes
  Sumar el número de día de la semana de la fecha de referencia al total
Devolver el total módulo 7

Solicitar el mes y el año
Si el mes y el año son correctos, escribir el calendario del mes
  Obtener el día de la semana del primer día del mes
  Obtener el número de días del mes
  Escribir el encabezado
  Escribir los números de los días
    Escribir tantos espacios como días de la semana falten para el primero
    Inicializar el contador de día de la semana
    Para cada día hasta el último del mes...
      Escribir el número e incrementar el día de la semana
      Si el día de la semana es el último, saltar a la línea siguiente y reiniciar el contador de día de la semana
*/

#include <stdio.h>

const int anno_referencia=1601;
const int mes_febrero=2;
const int meses=12;

typedef enum dia_semana_t{Lunes,Martes,Miercoles,Jueves,Viernes,Sabado,Domingo};
typedef int dias_meses_t[meses];

const dias_meses_t dias_meses={31,28,31,30,31,30,31,31,30,31,30,31};

bool ObtenerBisiesto(int anno){

  return !(anno%4)&&(anno%100||!(anno%400));

}

dia_semana_t CalcularDiaSemana(int dia,int mes,int anno){

  const dia_semana_t dia_1_enero_referencia=Lunes;
  const int dias_anno_no_bisiesto=365; /*** ERROR: 366 ***/
  const int dias_anno_bisiesto=366; /*** ERROR: 365 ***/

  int suma;

  suma=0;

  /* Calcular el número de días de los años completos */
  for(int i=anno_referencia;i<anno;i++){

    if(!ObtenerBisiesto(i)){
      suma=suma+dias_anno_no_bisiesto;
    }
    else{
      suma=suma+dias_anno_bisiesto;
    }
  }

  /* Calcular el número de meses de los meses completos del año en curso */
  for(int i=1;i<mes;i++){

    suma=suma+dias_meses[i-1];

    if(i==mes_febrero&&ObtenerBisiesto(anno)){
      suma++;
    }
  }

  /* Calcular el número de días del mes */
  for(int i=1;i<dia;i++){
    suma++;
  }

  /* Sumar el número de día de la semana de la fecha de referencia al total */
  suma=suma+int(dia_1_enero_referencia);

  /* Devolver el total módulo 7 */
  return dia_semana_t(suma%7);

}

void EscribirMesAnno(int mes,int anno){

  switch(mes){
    case 1:
      printf("Enero");
      break;
    case 2:
      printf("Febrero");
      break;
    case 3:
      printf("Marzo");
      break;
    case 4:
      printf("Abril");
      break;
    case 5:
      printf("Mayo");
      break;
    case 6:
      printf("Junio");
      break;
    case 7:
      printf("Julio");
      break;
    case 8:
      printf("Agosto");
      break;
    case 9:
      printf("Septiembre");
      break;
    case 10:
      printf("Octubre");
      break;
    case 11:
      printf("Noviembre");
      break;
    case 12:
      printf("Diciembre");
      break;
    default:
      ;
  }

  printf(" %04d\n",anno);

}

int main(){

  const int anno_minimo=anno_referencia;
  const int mes_maximo=meses;
  const int dias_semana=7;
  const int mes_minimo=1;

  dia_semana_t dia_semana_comienzo;
  int dia_semana; /*** ERROR: dia_semana_t ***/
  int anno,mes;
  int dias_mes;

  /* Solicitar el mes y el año */
  printf("\250A\244o? ");
  scanf("%d",&anno);
  printf("\250Mes? ");
  scanf("%d",&mes);

  /* Si el mes y el año son correctos, escribir el calendario del mes */
  if(anno>=anno_minimo&&mes>=mes_minimo&&mes<=mes_maximo){

    /* Obtener el día de la semana del primer día del mes */
    dia_semana_comienzo=CalcularDiaSemana(1,mes,anno);

    /* Obtener el número de días del mes */
    dias_mes=dias_meses[mes-1];

    if(mes==mes_febrero&&ObtenerBisiesto(anno)){
      dias_mes++;
    }

    /* Escribir el encabezado */
    printf("          ");
    EscribirMesAnno(mes,anno);
    printf("LU   MA   MI   JU   VI   SA   DO\n");

    /* Escribir tantos espacios como días de la semana falten para el primero */
    for(int i=int(Lunes);i<int(dia_semana_comienzo);i++){
      printf("     ");
    }

    /* Inicializar el contador de día de la semana */
    dia_semana=int(dia_semana_comienzo);

    /* Para cada día hasta el último del mes... */
    for(int i=1;i<=dias_mes;i++){

      /* Escribir el número e incrementar el día de la semana */
      printf("%2d",i);
      dia_semana++;

      /* Si el día de la semana es el último, saltar a la línea siguiente y reiniciar el contador de día de la semana */
      if(dia_semana>=dias_semana){ /*** ERROR: > en lugar de >= ***/

        printf("\n");
        dia_semana=0;

      }

      else{
        printf("   ");
      }
    }

    if(dia_semana){
      printf("\n");
    }
  }

  return 0;

}
