/*
Ejercicio sin resolver II.2

Original
--------
Inicializar el valor
Buscar la letra
  Si se encuentra, obtener el valor situado en la misma posición en el array de valores

Cambio
------
Inicializar las variables
Buscar la letra
  Si la encuentra, copiar los datos
  Incrementar el índice

Inicializar las variables
Recorrer la cadena mientras no haya ningún error
  Si se ha encontrado una letra válida, procesarla
    Si hay una letra anterior, procesarla
      Si la anterior letra es de tipo 1 y es inferior a la actual en una proporción de 5 o 10, restarla dos veces
      Si la anterior letra es de tipo 5 y es de menor valor a la actual, activar el error
    Si la letra es de tipo 1, aplicar las reglas correspondientes
      Si la anterior letra es la misma, incrementar el contador de números de tipo 1 y comprobar que no sea superior o igual a 3 (si lo es, activar el error)
      Si la anterior letra es distinta, reiniciar el contador de números de tipo 1
    Si la letra es de tipo 5, aplicar las reglas correspondientes
      Si la anterior letra es la misma, activar el error
      Reiniciar el contador de números de tipo 1
    Almacenar la letra en la variable auxiliar
    Sumar el valor de la letra
  Si no se ha encontrado una letra válida, indicarlo en la variable correspondiente
*/

#include <stdio.h>
#include <string.h>

const int longitud_cadena=100; /* Longitud máxima de la cadena */

typedef char cadena_t[longitud_cadena];

typedef enum tipo_numero_t{
  tipo1,
  tipo5
};

typedef struct letra_t{
  char numero;
  int valor;
  tipo_numero_t tipo;
};

bool BuscarLetra(char letra,letra_t &resultado_datos){

  const int letras=7; /* Número de letras permitidas */

  typedef letra_t letras_t[letras];

  const letras_t datos={ /* Datos sobre las letras */
    {'I',1,tipo1},
    {'V',5,tipo5},
    {'X',10,tipo1},
    {'L',50,tipo5},
    {'C',100,tipo1},
    {'D',500,tipo5},
    {'M',1000,tipo1}
  };

  bool resultado; /* Letra encontrada */
  int indice; /* Índice de búsqueda */

  /* Inicializar las variables */
  resultado=false;
  indice=0;

  /* Buscar la letra */
  while(indice<letras&&!resultado){

    /* Si la encuentra, copiar los datos */
    if(datos[indice].numero==letra){

      resultado_datos=datos[indice];
      resultado=true;

    }

    /* Incrementar el índice */
    indice++;

  }

  return resultado;

}

int ObtenerValorCadena(const cadena_t cadena){

  letra_t datos_letra_anterior;
  letra_t datos_letra;
  int contador_tipo_1;
  int longitud;
  int indice;
  int numero;
  bool error;

  /* Inicializar las variables */
  error=false;
  contador_tipo_1=0;
  numero=0;
  indice=0;

  longitud=strlen(cadena);

  /* Recorrer la cadena mientras no haya ningún error */
  while(indice<longitud&&!error){

    /* Si se ha encontrado una letra válida, procesarla */
    if(BuscarLetra(cadena[indice],datos_letra)){

      /* Si hay una letra anterior, procesarla */
      if(indice>0){

        /* Si la anterior letra es de tipo 1 y es inferior a la actual en una proporción de 5 o 10, restarla dos veces */
        if(datos_letra_anterior.tipo==tipo1&&(datos_letra_anterior.valor==datos_letra.valor/5||datos_letra_anterior.valor==datos_letra.valor/10)){
          numero=numero-2*datos_letra_anterior.valor;
        }

        /* Si la anterior letra es de tipo 5 y es de menor valor a la actual, activar el error */
        else if(datos_letra_anterior.tipo==tipo5&&datos_letra_anterior.valor<datos_letra.valor){
          error=true;
        }
      }

      /* Si la letra es de tipo 1, aplicar las reglas correspondientes */
      if(datos_letra.tipo==tipo1){

        /* Si la anterior letra es la misma, incrementar el contador de números de tipo 1 y comprobar que no sea superior o igual a 3 (si lo es, activar el error) */
        if(indice>0&&datos_letra_anterior.tipo==tipo1&&datos_letra_anterior.numero==datos_letra.numero){

          contador_tipo_1++;

          if(contador_tipo_1>=3){
            error=true;
          }
        }
        /* Si la anterior letra es distinta, reiniciar el contador de números de tipo 1 */
        else{
          contador_tipo_1=0;
        }
      }

      /* Si la letra es de tipo 5, aplicar las reglas correspondientes */
      else if(datos_letra.tipo==tipo5){

        /* Si la anterior letra es la misma, activar el error */
        if(indice>0&&datos_letra_anterior.tipo==tipo5&&datos_letra_anterior.numero==datos_letra.numero){
          error=true;
        }

        /* Reiniciar el contador de números de tipo 1 */
        contador_tipo_1=0;

      }

      /* Almacenar la letra en la variable auxiliar */
      datos_letra_anterior=datos_letra;

      /* Sumar el valor de la letra */
      numero=numero+datos_letra.valor;

      indice++; /*** ERROR: omisión ***/

    }

    /* Si no se ha encontrado una letra válida, indicarlo en la variable correspondiente */
    else{
      error=true;
    }
  }

  return numero;

}

int main(){

  letra_t datos_letra; /* Datos sobre la letra */
  cadena_t cadena; /* Cadena solicitada */
  char letra; /* Letra solicitada */
  int valor; /* Valor numérico de una letra */

  /* Solicitar la letra */
  printf("\250Letra? ");
  scanf("%c",&letra);

  /* Buscar los datos de la letra */
  if(BuscarLetra(letra,datos_letra)){
    valor=datos_letra.valor;
  }
  else{
    valor=0;
  }

  /* Imprimir el valor entero correspondiente */
  printf("Valor entero: %d\n",valor);

  /* Solicitar la cadena */
  printf("\250Cadena? ");
  scanf("%s",cadena);

  /* Imprimir el valor entero correspondiente */
  printf("Valor entero: %d\n",ObtenerValorCadena(cadena));

  return 0;

}
