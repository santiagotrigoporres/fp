/*
Ejercicio sin resolver II.6

Si es un coche peque�o, comprobar las plazas peque�as
Si todav�a no se ha encontrado plaza (esto ocurre siempre que sea grande), comprobar las plazas grandes
Devolver el �xito de la operaci�n y, en su caso, el tipo y n�mero de plaza
*/

#include <conio.h>
#include <ctype.h>
#include <stdio.h>

const int numero_plazas_pequennas=15;
const int numero_plazas_grandes=10;

typedef bool plazas_pequennas_t[numero_plazas_pequennas];
typedef bool plazas_grandes_t[numero_plazas_grandes];
typedef enum tipo_plaza_t{PlazaPequenna,PlazaGrande};

bool IntroducirCoche(plazas_pequennas_t plazas_pequennas,plazas_grandes_t plazas_grandes,tipo_plaza_t &tipo,int &numero){

  bool resultado;
  int indice;

  resultado=false;

  if(tipo==PlazaPequenna){

    indice=0;

    while(indice<numero_plazas_pequennas&&plazas_pequennas[indice]){
      indice++;
    }

    if(indice<numero_plazas_pequennas){

      resultado=true;
      numero=indice; /* No hace falta indicar que la plaza es peque�a */

      plazas_pequennas[numero]=true;

    }
  }

  if(!resultado){

    indice=0;

    while(indice<numero_plazas_grandes&&plazas_grandes[indice]){
      indice++;
    }

    if(indice<numero_plazas_grandes){

      resultado=true;
      tipo=PlazaGrande;
      numero=indice;

      plazas_grandes[numero]=true;

    }
  }

  return resultado;

}

bool LeerPlaza(tipo_plaza_t &tipo,int &numero){

  char caracter;

  scanf("%c %d",&caracter,&numero);
  caracter=tolower(caracter);

  if(caracter=='p'&&numero>=1&&numero<=numero_plazas_pequennas){

    tipo=PlazaPequenna;
    numero--; /*** ERROR: omisi�n ***/

    return true;

  }

  else if(caracter=='g'&&numero>=1&&numero<=numero_plazas_grandes){

    tipo=PlazaGrande;
    numero--; /*** ERROR: omisi�n ***/

    return true;

  }

  else{
    return false;
  }
}

tipo_plaza_t LeerTipoPlaza(){

  char caracter;

  do{

    caracter=tolower(char(getch()));
    fflush(stdin);

  } while(caracter!='p'&&caracter!='g');

  if(caracter=='p'){

    printf("Peque\244a\n");
    return PlazaPequenna;

  }
  else{

    printf("Grande\n");
    return PlazaGrande;

  }
}

void MostrarPlazas(const plazas_pequennas_t plazas_pequennas,const plazas_grandes_t plazas_grandes){

  for(int i=1;i<=numero_plazas_grandes;i++){
    printf("+--------");
  }
  printf("+\n");

  for(int i=1;i<=numero_plazas_grandes;i++){
    printf("|  G %2d  ",i);
  }
  printf("|\n");

  for(int i=1;i<=numero_plazas_grandes;i++){
    if(plazas_grandes[i-1]){
      printf("|  [**]  ");
    }
    else{
      printf("|  [  ]  ");
    }
  }
  printf("|\n");

  for(int i=1;i<=numero_plazas_grandes;i++){
    printf("+--------");
  }
  printf("+\n");

  for(int i=1;i<=numero_plazas_pequennas;i++){
    printf("+-----");
  }
  printf("+\n");

  for(int i=1;i<=numero_plazas_pequennas;i++){
    printf("| P %2d",i);
  }
  printf("|\n");

  for(int i=1;i<=numero_plazas_pequennas;i++){
    if(plazas_pequennas[i-1]){
      printf("| [*] ");
    }
    else{
      printf("| [ ] ");
    }
  }
  printf("|\n");

  for(int i=1;i<=numero_plazas_pequennas;i++){
    printf("+-----");
  }
  printf("+\n");

}

bool SacarCoche(plazas_pequennas_t plazas_pequennas,plazas_grandes_t plazas_grandes,tipo_plaza_t tipo,int numero){

  if(tipo==PlazaPequenna&&plazas_pequennas[numero]){

    plazas_pequennas[numero]=false;
    return true;

  }

  else if(tipo==PlazaGrande&&plazas_grandes[numero]){

    plazas_grandes[numero]=false;
    return true;

  }

  else{
    return false;
  }
}

int main(){

  plazas_pequennas_t plazas_pequennas;
  plazas_grandes_t plazas_grandes;
  tipo_plaza_t tipo_plaza;
  int plaza_numero;
  char opcion;

  for(int i=0;i<numero_plazas_pequennas;i++){
    plazas_pequennas[i]=false;
  }

  for(int i=0;i<numero_plazas_grandes;i++){
    plazas_grandes[i]=false;
  }

  do{

    printf("1. Entrada\n");
    printf("2. Salida\n");
    printf("3. Situaci\242n del aparcamiento\n");
    printf("4. Salir del programa\n\n");
    printf("\250Opci\242n? ");
    opcion=getch();
    fflush(stdin);

    switch(opcion){
      case '1':
        printf("Entrada\n");
        printf("\250Tama\244o del coche? (P/G) ");
        tipo_plaza=LeerTipoPlaza();

        if(IntroducirCoche(plazas_pequennas,plazas_grandes,tipo_plaza,plaza_numero)){

          if(tipo_plaza==PlazaPequenna){
            printf("Plaza asignada: P %d.\n",plaza_numero+1);
          }
          else{
            printf("Plaza asignada: G %d.\n",plaza_numero+1);
          }
        }

        else{
          printf("No hay sitio libre.\n");
        }

        printf("Pulse una tecla para continuar.\n");
        getch();
        break;
      case '2':
        printf("Salida\n");
        printf("\250Plaza? ");

        if(LeerPlaza(tipo_plaza,plaza_numero)){

          if(SacarCoche(plazas_pequennas,plazas_grandes,tipo_plaza,plaza_numero)){
            printf("Plaza libre.\n");
          }
          else{
            printf("La plaza no estaba ocupada.\n");
          }
        }

        else{
          printf("Plaza inv\240lida.\n");
        }

        printf("Pulse una tecla para continuar.\n");
        getch();
        break;
      case '3':
        printf("3. Situaci\242n del aparcamiento\n");
        MostrarPlazas(plazas_pequennas,plazas_grandes);
        printf("Pulse una tecla para continuar.\n");
        getch();
        break;
      case '4':
        printf("4. Salir del programa\n");
        break;
      default:
        ;

    }

    printf("\n");

  } while(opcion!='4');

  return 0;

}
