/*
Ejercicio sin resolver II.1

Inicializar la suma de divisores
Comprobar cada divisor hasta el número menos uno
  Si el módulo del número con el índice es cero, es divisor; entonces, añadir el número a la suma
Comparar la suma con el número
*/

#include <stdio.h>

bool ComprobarPerfecto(int numero){

  int divisor; /* Divisor potencial del número */
  int suma; /* Suma de los divisores */

  /* Inicializar las variables */
  suma=0;
  divisor=1;

  /* Comprobar cada divisor hasta el número menos uno, siempre que la suma sea inferior o igual al número*/
  while(divisor<=numero-1&&suma<=numero){

    /* Si el módulo del número con el índice es cero, es divisor; entonces, añadir el número a la suma */
    if(!(numero%divisor)){
      suma=suma+divisor;
    }

    /* Incrementar el divisor */
    divisor++;

  }

  /* Comparar la suma con el número */
  return suma==numero;

}

int main(){

  const int columnas=4; /* Número de columnas por fila */

  int columna; /* Número de columna */
  int limite; /* Límite solicitado al usuario */

  /* Solicitar el límite */
  printf("\250L\241mite? ");
  scanf("%d",&limite);

  /* Inicializar la columna */
  columna=0;

  /* Mostrar el listado de números perfectos hasta el límite */
  for(int i=1;i<=limite;i++){

    if(ComprobarPerfecto(i)){

      printf("%10d",i);

      columna++;

      if(columna>=columnas){

        printf("\n");
        columna=0;

      }
    }
  }

  return 0;

}
