/*
Ejercicio sin resolver II.3

Para cada primera letra...
  Para cada segunda letra...
    Para cada tercera letra...
      Para cada cuarta letra...
        Escribir las cuatro letras correspondientes
*/

#include <stdio.h>

int main(){

  const int numero_letras=4; /* Número de letras */

  typedef char cadena_t[numero_letras];

  const cadena_t letras={'A','B','C','D'}; /* Letras utilizadas */

  for(int i=0;i<numero_letras;i++){
    for(int j=0;j<numero_letras;j++){
      for(int k=0;k<numero_letras;k++){
        for(int l=0;l<numero_letras;l++){
          printf("%c%c%c%c\n",letras[i],letras[j],letras[k],letras[l]);
        }
      }
    }
  }

  return 0;

}
