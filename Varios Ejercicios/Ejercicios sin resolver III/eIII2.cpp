/*
Ejercicio sin resolver III.2
*/

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

const int numero_dimensiones=2;
const int numero_puntos=10;

typedef float punto_t[numero_dimensiones];
typedef punto_t puntos_t[numero_puntos];

float CalcularDistancia(const punto_t punto){

  float suma_cuadrados;

  suma_cuadrados=0.0;

  for(int i=0;i<numero_dimensiones;i++){
    suma_cuadrados=suma_cuadrados+powf(punto[i],2);
  }

  return sqrtf(suma_cuadrados);

}

bool CompararPuntos(const punto_t primero,const punto_t segundo){

  return CalcularDistancia(primero)>CalcularDistancia(segundo);

}

void CopiarPunto(punto_t destino,const punto_t origen){

  for(int i=0;i<numero_dimensiones;i++){
    destino[i]=origen[i];
  }

}

void ImprimirPunto(const punto_t punto,int numero){

  printf("Punto %2d: ",numero+1);

  for(int i=0;i<numero_dimensiones-1;i++){
    printf("%8.02f,",punto[i]);
  }

  printf("%8.02f (distancia al origen: %8.02f)\n",punto[numero_dimensiones-1],CalcularDistancia(punto));

}

int main(){

  const int puntos_aleatorios_amplitud=200; /* Coordenadas aleatorias entre -100.0 y 100.0 */
  const int puntos_aleatorios_minimo=-100;
  const bool solicitar_puntos=false;

  punto_t auxiliar;
  puntos_t puntos;
  int indice;

  if(solicitar_puntos){
    for(int i=0;i<numero_puntos;i++){

      printf("\250Punto %d? (x,y) ",i+1);
      scanf("%f,%f",&puntos[i][0],&puntos[i][1]); /* Para dos dimensiones */

    }
  }

  else{

    srand(time(NULL));

    for(int i=0;i<numero_puntos;i++){
      for(int j=0;j<numero_dimensiones;j++){
        puntos[i][j]=float((100*puntos_aleatorios_minimo)+rand()%(100*puntos_aleatorios_amplitud))/100.0;
      }
    }
  }

  printf("Antes de ordenar\n");
  printf("----------------\n");

  for(int i=0;i<numero_puntos;i++){
    ImprimirPunto(puntos[i],i);
  }

  for(int i=1;i<numero_puntos;i++){

    indice=i;
    CopiarPunto(auxiliar,puntos[i]);

    while(indice>0&&CompararPuntos(puntos[indice-1],auxiliar)){

      CopiarPunto(puntos[indice],puntos[indice-1]);
      indice--;

    }

    CopiarPunto(puntos[indice],auxiliar);

  }

  printf("\nDespu\202s de ordenar\n");
  printf("------------------\n");

  for(int i=0;i<numero_puntos;i++){
    ImprimirPunto(puntos[i],i);
  }

  return 0;

}
