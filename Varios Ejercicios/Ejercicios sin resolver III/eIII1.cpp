/*
Ejercicio sin resolver III.1
*/

#include <math.h>
#include <stdio.h>

typedef struct moneda_t{
  float valor; /*** ERROR: int ***/
  int cantidad;
};

const int monedas_tipos=5;

typedef moneda_t monedas_t[monedas_tipos]; /*** ERROR: dentro de main() ***/

float CalcularDinero(const monedas_t monedas){

  float resultado;

  resultado=0.0;

  for(int i=0;i<monedas_tipos;i++){
    resultado=resultado+monedas[i].valor*float(monedas[i].cantidad);
  }

  return resultado;

}

void SacarDinero(monedas_t monedas,float importe){

  float restante;
  int cantidad;
  int moneda;

  restante=importe;
  moneda=0;

  printf("Entrega:");

  while(moneda<monedas_tipos&&restante>0.0){

    cantidad=int(ceilf(100.0*restante/monedas[moneda].valor)/100.0); /* Para arreglar problemas de paso a entero */

    if(cantidad>monedas[moneda].cantidad){
      cantidad=monedas[moneda].cantidad;
    }

    if(cantidad>0){
      printf(" %dx%4.2f",cantidad,monedas[moneda].valor);
    }

    monedas[moneda].cantidad=monedas[moneda].cantidad-cantidad;
    restante=restante-monedas[moneda].valor*float(cantidad); /*** ERROR: restante-... (omisi�n) ***/

    moneda++;

  }

  if(restante>=0.01){ /* Puede ocurrir si se pide una cantidad con dos decimales */
    printf(" (Resto: %4.2f)",restante);
  }

  printf("\n");

}

int main(){

  typedef float monedas_valores_t[monedas_tipos];

  const int monedas_inicial=100;
  const monedas_valores_t valores={2.00,1.00,0.50,0.20,0.10}; /* Orden de extracci�n */

  monedas_t monedas;
  float importe;
  float dinero;

  for(int i=0;i<monedas_tipos;i++){

    monedas[i].valor=valores[i];
    monedas[i].cantidad=monedas_inicial;

  }

  dinero=CalcularDinero(monedas);

  do{

    printf("Monedas:");

    for(int i=0;i<monedas_tipos;i++){
      printf(" %dx%4.2f",monedas[i].cantidad,monedas[i].valor);
    }

    printf("\n\250Importe? (M\240ximo: %6.2f) ",dinero);
    scanf("%f",&importe);

    if(importe>0){

      if(dinero>=importe){

        SacarDinero(monedas,importe);
        dinero=CalcularDinero(monedas);

      }

      else{
        printf("No hay dinero suficiente\n");
      }

      printf("\n");

    }
  } while(importe>0);

  return 0;

}
