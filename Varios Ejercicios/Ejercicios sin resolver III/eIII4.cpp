/*
Ejercicio sin resolver III.4
*/

#include <stdio.h>
#include "eIII4.h"

void lista_t::Iniciar(){

  _Lista=NULL;

}

void lista_t::Insertar(int numero){

  nodo_puntero_t auxiliar,anterior;
  nodo_puntero_t nuevo;

  anterior=NULL;
  auxiliar=_Lista;

  while(auxiliar&&auxiliar->numero<numero){

    anterior=auxiliar;
    auxiliar=auxiliar->siguiente;

  }

  nuevo=new nodo_t;

  nuevo->numero=numero;
  nuevo->siguiente=auxiliar;

  if(anterior!=NULL){
    anterior->siguiente=nuevo;
  }
  else{
    _Lista=nuevo;
  }
}

bool lista_t::ListaVacia(){

  return _Lista==NULL;

}

int lista_t::NumeroElementos(){

  nodo_puntero_t auxiliar;
  int numero;

  auxiliar=_Lista;
  numero=0;

  while(auxiliar){

    auxiliar=auxiliar->siguiente;
    numero++;

  }

  return numero;

}

bool lista_t::RetirarPrimero(int &numero){

  nodo_puntero_t siguiente;
  bool resultado;

  if(_Lista){

    resultado=true;
    numero=_Lista->numero;

    siguiente=_Lista->siguiente;
    delete _Lista;
    _Lista=siguiente;

  }

  else{
    resultado=false;
  }

  return resultado;

}

int main(){

  lista_t lista;
  int numero;

  lista.Iniciar();

  do{

    if(!lista.ListaVacia()){
      printf("(%d n\243meros en la lista) ",lista.NumeroElementos());
    }
    else{
      printf("(Lista vac\241a) ");
    }

    printf("\250N\243mero? ");
    scanf("%d",&numero);

    if(numero!=0){
      lista.Insertar(numero);
    }

  } while(numero!=0);

  if(!lista.ListaVacia()){

    printf("\nLista:");

    while(lista.NumeroElementos()){
      if(lista.RetirarPrimero(numero)){
        printf(" %d",numero);
      }
    }

    printf("\n");

  }

  return 0;

}
