/*
Ejercicio sin resolver III.3
*/

#include <ctype.h>
#include <stdio.h>
#include <string.h>

const int longitud_maxima_texto=1000;
/* const int palabras_maximo=(longitud_maxima+1)/2; */

typedef char texto_t[longitud_maxima_texto+1];
typedef char palabra_t[longitud_maxima_texto+1];

typedef struct nodo_palabra_t{
  palabra_t palabra;
  nodo_palabra_t *siguiente; /*** ERROR: palabra_t ***/
};

typedef nodo_palabra_t* puntero_nodo_palabra_t;

void AnnadirPalabra(puntero_nodo_palabra_t &palabras,texto_t texto,int comienzo,int final){

  puntero_nodo_palabra_t anterior,indice;
  puntero_nodo_palabra_t nuevo;

  nuevo=new nodo_palabra_t;

  strncpy(nuevo->palabra,texto+comienzo,final-comienzo);
  nuevo->palabra[final-comienzo]='\0';
  nuevo->siguiente=NULL;

  indice=palabras;
  anterior=palabras;

  while(indice!=NULL){

    anterior=indice;
    indice=indice->siguiente;

  }

  if(anterior!=NULL){
    anterior->siguiente=nuevo;
  }
  else{
    palabras=nuevo;
  }
}

int ExtraerPalabras(puntero_nodo_palabra_t &palabras,texto_t texto){

  const char separador=' ';

  int comienzo_palabra;
  int numero_palabras;
  int longitud;

  longitud=strlen(texto);

  numero_palabras=0; /*** ERROR: 1 ***/
  comienzo_palabra=0;

  for(int i=1;i<longitud;i++){ /* No contar el primer car�cter */
    if(texto[i]==separador){

      AnnadirPalabra(palabras,texto,comienzo_palabra,i);
      comienzo_palabra=i+1;
      numero_palabras++;

    }
  }

  if(comienzo_palabra<longitud-1){

    AnnadirPalabra(palabras,texto,comienzo_palabra,longitud);
    numero_palabras++;

  }

  return numero_palabras;

}

int main(){

  const char terminador='.';

  int longitud_maxima,longitud_minima,numero_vocales_maximo,numero_consonantes_maximo;
  puntero_nodo_palabra_t larga,corta,mas_vocales,mas_consonantes;
  puntero_nodo_palabra_t siguiente,indice_palabra;
  int longitud,numero_vocales,numero_consonantes;
  puntero_nodo_palabra_t palabras;
  int numero_palabras;
  texto_t texto;
  int indice;

  printf("\250Texto?\n");
  fgets(texto,longitud_maxima_texto,stdin);
  printf("\n");

  indice=strlen(texto)-1; /*** ERROR: longitud_maxima_texto ***/

  while(indice>0&&texto[indice]!=terminador){
    indice--;
  }

  texto[indice]='\0';

  palabras=NULL;
  numero_palabras=ExtraerPalabras(palabras,texto);

  indice_palabra=palabras;

  larga=palabras;
  corta=palabras;
  mas_vocales=palabras;
  mas_consonantes=palabras;

  numero_vocales_maximo=0;
  numero_consonantes_maximo=0;
  longitud_maxima=0;
  longitud_minima=longitud_maxima_texto;

  for(int i=0;i<numero_palabras;i++){

    longitud=strlen(indice_palabra->palabra);

    numero_vocales=0;

    for(int j=0;j<longitud;j++){
      switch(tolower(indice_palabra->palabra[j])){
        case 'a':
        case 'e':
        case 'i':
        case 'o':
        case 'u':
        case '�':
        case '�':
        case '�':
        case '�':
        case '�':
        case '�':
        case '�':
        case '�':
        case '�':
        case '�':
          numero_vocales++;
          break;
        default:
          ;
      }
    }

    numero_consonantes=longitud-numero_vocales;

    if(longitud>longitud_maxima){

      larga=indice_palabra;
      longitud_maxima=longitud;

    }

    if(longitud<longitud_minima){

      corta=indice_palabra;
      longitud_minima=longitud;

    }

    if(numero_vocales>numero_vocales_maximo){

      mas_vocales=indice_palabra;
      numero_vocales_maximo=numero_vocales;

    }

    if(numero_consonantes>numero_consonantes_maximo){

      mas_consonantes=indice_palabra;
      numero_consonantes_maximo=numero_consonantes;

    }

    printf("Palabra %d: \"%s\"\n",i+1,indice_palabra->palabra);
    indice_palabra=indice_palabra->siguiente;

  }

  printf("\n");

  if(larga){
    printf("Palabra m\240s larga (%d caracteres): \"%s\"\n",longitud_maxima,larga->palabra);
  }
  if(corta){
    printf("Palabra m\240s corta (%d caracteres): \"%s\"\n",longitud_minima,corta->palabra);
  }
  if(mas_vocales){
    printf("Palabra con m\240s vocales (%d): \"%s\"\n",numero_vocales_maximo,mas_vocales->palabra);
  }
  if(mas_consonantes){
    printf("Palabra con m\240s consonantes (%d): \"%s\"\n",numero_consonantes_maximo,mas_consonantes->palabra);
  }

  indice_palabra=palabras;

  for(int i=0;i<numero_palabras;i++){

    siguiente=indice_palabra->siguiente;
    delete indice_palabra;

    indice_palabra=siguiente;

  }

  return 0;

}
