/*
Ejercicio sin resolver III.4
*/

typedef struct nodo_t{
  int numero;
  nodo_t *siguiente;
};

typedef nodo_t* nodo_puntero_t;

typedef struct lista_t{
    void Iniciar();
    void Insertar(int numero);
    bool ListaVacia();
    int NumeroElementos();
    bool RetirarPrimero(int &numero);
  private:
    nodo_puntero_t _Lista;
};
