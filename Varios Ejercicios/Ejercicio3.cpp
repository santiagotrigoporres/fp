#include <stdio.h>

  void LeerDatos (int & lado){
    /*Leer lado del rombo*/
  printf( "Numero de asteriscos en el lado?");
  scanf( "%d", &lado);
  }

    /*Imprimir el vertice superior*/
  void VerticeSuperior(int & lado){
    if (lado > 0 ){
      for (int k = 1; k <= lado-1; k++){
        printf( " ");
      }
      printf( "*\n");
    }
  }

    /*Parte superior del rombo*/
  void ParteSuperior(int & lado){
    for (int k = 2; k <= lado - 1; k++){
      for (int j = 1; j <= lado - k; j++){
        printf( " ");
      }
      printf( "*");
    for (int j = 1; j<=k-1; j++){
      printf( "**");
    }
    printf ("\n");
    }
  }

    /*Imprimir el borde central*/
  void BordeCentral(int & lado){
    if (lado > 1){
      printf ("*");
      for (int k = 1; k <= lado - 1; k++){
        printf( "**");
      }
    printf ("\n");
    }
  }

  void ParteInferior(int & lado){
   for (int k = lado - 1; k >= 1; k--){
    for(int j = 1; j <= lado - k; j++){
      printf(" ");
      }
    printf("*");
    for (int j = 1; j <= k-1; j++){
      printf ("**");
    }
    printf("\n");
  }
}

  int main(){
    int lado = 0;

    LeerDatos (lado);
    VerticeSuperior(lado);
    ParteSuperior(lado);
    BordeCentral(lado);
    ParteInferior(lado);
}







