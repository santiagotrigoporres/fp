/*Programa que cuenta los caracteres y las vocales de un texto introducido
y que termina con un punto(.)*/

#include <stdio.h>

/*Procedimiento para la lectura*/
void LeerDatos(){
  printf ("Introduce un texto que termine con un punto(.): \n");
}

/*Procedimiento que realiza las acciones necesarias*/
void LeerDatos1(int & cA, int & cE, int & cI, int & cO, int & cU, int & cCaracteres, int & espacio ){
char texto = ' ';


  while (texto != '.'){         /*Analiza caracter a caracter y realiza las acciones detallas a continuación*/
    scanf ("%c", &texto);       /*hasta que se encuentra con un punto(.), entoces finaliza y muestra los*/
                               /*resultados*/
  if (texto != '.'){
      cCaracteres = cCaracteres + 1;
  }
  if ( texto == ' '){
      espacio = espacio + 1;
  }
  if ((texto == 'a')|| (texto == 'A')){
      cA = cA + 1;
  }
  if ((texto == 'e')|| (texto == 'E')){
      cE = cE + 1;
  }
  if ((texto == 'i')|| (texto == 'I')){
      cI = cI + 1;
  }
  if ((texto == 'o')|| (texto == 'O')){
      cO = cO + 1;
  }
  if ((texto == 'u')|| (texto == 'U')){
      cU = cU + 1;

      }
    }
  }
/*Programa principal*/
int main(){

int cA = 0;
int cE = 0;
int cI = 0;
int cO = 0;
int cU = 0;
int cCaracteres = 0;
int espacio = 0;

LeerDatos();
LeerDatos1(cA, cE, cI, cO, cU, cCaracteres, espacio);

printf ("En el texto hay %d caracteres incluyendo el espacio\n", cCaracteres);
printf ("En el texto hay %d caracteres excluyendo el espacio\n", cCaracteres - espacio);
printf ("En el texto hay %d vocales a\n", cA);
printf ("En el texto hay %d vocales e\n", cE);
printf ("En el texto hay %d vocales i\n", cI);
printf ("En el texto hay %d vocales o\n", cO);
printf ("En el texto hay %d vocales u\n", cU);
printf ("En el texto hay un total de %d vocales\n\n", cA + cE + cI + cO + cU);

}
