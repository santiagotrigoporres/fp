/* Informa de que tipo de triangulo es tomando las medidas de los lados */

#include <stdio.h>

int main() {
  int lado1, lado2, lado3, hipotenusa;

  printf("Introduce el tamano de los lados: ");
  scanf("%d %d %d", &lado1, &lado2, &lado3);

  if ( lado1 > lado2+lado3 || lado2 > lado1+lado3 || lado3 > lado1+lado2 ) {
    printf("No forman triangulo");
  } else if (lado1==lado2 && lado2==lado3) {
    printf("Es un triangulo equilatero");
  } else {
    if (lado1==lado2 || lado1==lado3 || lado2==lado3) {
    printf("Es un triangulo isosceles");
    } else {
      printf("Es un triangulo escaleno");
    }
    /* calcular si es triangulo recto con la teorema de pitagoras, ej: 3 4 5 */
    if (lado1>lado2 && lado1>lado3) {
      if (lado1*lado1 == lado2*lado2 + lado3*lado3) {
        printf("\nAdemas es un triangulo rectangulo");
      }
    } else if (lado2>lado1 && lado2>lado3) {
      if (lado2*lado2 == lado1*lado1 + lado3*lado3) {
        printf("\nAdemas es un triangulo rectangulo");
      }
    } else {
      if (lado3*lado3 == lado1*lado1 + lado2*lado2) {
        printf("\nAdemas es un triangulo rectangulo");
      }
    }
  }
}
