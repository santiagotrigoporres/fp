#include <stdio.h>

  void LeerDatos(int & ladoA, int & ladoB, int & ladoC){
    int aux;

  printf ( "Introduce los 3 lados del triangulo (Ejemplo: 3 4 5): " );
  scanf ( "%d %d %d", &ladoA, &ladoB, &ladoC );
  }

  void OrdenarNumeros(int & ladoA, int & ladoB, int & ladoC){
    int aux;
     /*Primero se ordenan los datos de menor a mayor. Primer paso ordenar los dos primeros datos*/
  if (ladoA > ladoB){
    aux = ladoA;
    ladoA = ladoB;
    ladoB = aux;
  }
  /*Segundo paso ordenar el tercer dato*/
  if (ladoC < ladoA){
    aux = ladoC;
    ladoC = ladoB;
    ladoB = ladoA;
    ladoA = aux;
  }else if (ladoC < ladoB){
    aux = ladoB;
    ladoB = ladoC;
    ladoC = aux;
  }
  }
int main() {

   int ladoA, ladoB, ladoC, aux;

   LeerDatos(ladoA, ladoB, ladoC);
   OrdenarNumeros(ladoA, ladoB, ladoC);

   if (ladoC > ladoA + ladoB){
    printf ("No es un triangulo!!!"); /*No forman ningun triangulo*/
  }else if (ladoA == ladoB && ladoA == ladoC){    /*Es un triangulo equilatero*/
    printf ("Es un triangulo equilatero");
  }else if (ladoA == ladoB || ladoA == ladoC || ladoB == ladoC){ /*Es un triangulo isosceles*/
    printf ("Es un triangulo isosceles");
  }else if ((ladoA * ladoA) + (ladoB * ladoB) == (ladoC) * (ladoC)){ /*Es un triangulo rectangulo*/
    printf ("Es un triangulo rectangulo");
  }else if (ladoA != ladoB && ladoA != ladoC && ladoB != ladoC){ /*Es un triangulo escaleno*/
    printf ("Es un triangulo escaleno");

  }

}


