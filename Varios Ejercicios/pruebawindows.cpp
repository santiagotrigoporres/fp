/*************************************************************
*    Programa para probar algunas funciones de la API de Windows
*
*    Descripci�n: Se prueba cambiar el color de la consola alternadolo con
                  cambiar el color del sistema,
*                 la funci�n que detecta un caracter pulsado
*                 y el MessageBox (peque�a ventana emergente)
*
**************************************************************/

#include <stdio.h> /* printf(), sprintf()*/
#include <windows.h> /* system(), GetAsyncKeyState() ,SetConsoleTextAttribute(), GetStdHandle(), MessageBox()*/

typedef char TipoString[10];

/*============================================
Funci�n para dar formato al color del sistema
==============================================
/*Esta funci�n convierte un valor int en char teniendo en cuenta que hay 15 colores del sistema
y del 1 al 9 son el n�mero correspondiente y de 10 a 15 es una letra de la a a la f

Valores color: 0 = Negro, 1= Azul, 2 = Verde, 3 = Aguamarina, 4 = Rojo, 5 = Purpura, 6 = Amarillo,
   7 = Blanco, 8 = Gris, 9 = Azul claro A (10) = Verde claro B (11)= Aguamarina claro, C (12)= Rojo claro,
  D (13)= Purpura claro, E (14) = Amarillo claro, F (15) = Blanco brillante */

char ColorCMD(int color) {
  char colorCMD;
  if (color >= 10) {
    colorCMD = char(color+87);/* a partir de 9 el valor es un caracter a-f*/
  } else {
    colorCMD = char(color+48); /* Hasta 9 el valor es un n�mero que coincide con su valor*/
  }
  return colorCMD;
}

/*============================================
Parte principal del programa
==============================================*/

int main() {
  /* En este programa vamos a crear una pantalla donde se impriman 15 l�neas de texto
   cada una con el texto de un color y el color de fondo se vaya cambiando al pulsar cualquier tecla
   excepto ESC que valdr� para salir del programa*/

  TipoString cadenaColor;
  int msgboxID = IDNO;
  int colorTexto=0;
  int colorFondo=0;
  char colorPantalla;


  do {
    /*Determinamos el valor de la variable colorPantalla. La idea es que no debe coincider
     el color de sistema del fondo y el texto porque entonces no funciona.
     En este caso determinamos que sea 7 excepto cuando este color lo tiene tambien el fondo*/

    if (colorFondo == 7) {
      colorPantalla = '0';
    } else {
      colorPantalla = '7';
    }

    sprintf(cadenaColor,"color %c%c",ColorCMD(colorFondo),colorPantalla);/*Damos valor a la vaiable
                tipo cadena que pasaremos como argumento a system() para cambiar el color del sistema */

    if (colorTexto == 0) {
      printf("\nCambio: %s\n",cadenaColor);/* Informamos del cambio de color del sistema */
      /* El color del sistema de cambia de forma que coincida con el color de fondo de la consola*/
      system(cadenaColor);/*Seleccionamos el color de sistema*/
    }
    /*Selecionamos el color de la consola mediante la API de Windows*/
    SetConsoleTextAttribute(GetStdHandle(STD_OUTPUT_HANDLE), (16*colorFondo)+colorTexto);/*Funcion para cambiar el color*/
    /* Imprimimos mensaje*/
    printf("\nColor:%2d * 16 + %2d - Presiona ESC para salir e INTRO para continuar.\n",colorFondo,colorTexto);

    if (GetAsyncKeyState(VK_ESCAPE)) { /*Determina si hemos pulsado ESC*/
        system("CLS");/*Limpiamos pantalla*/

      /*-- Mostramos el MessageBox -- */

          msgboxID = MessageBox(
                 NULL, /*identificador de la ventana propietaria*/
                 "�Desea salir del programa?",/*Mensage que aparecer�*/
                 "Prueba de ventana",/*T�tulo de la ventana*/
                 MB_YESNO + MB_ICONQUESTION /*Botones + Icono (usamos c�digos pero tienen valor entero)*/
               );
               /* La respuesta es un entero que se puede poner el valor tal cual
               o un c�digo de retorno que es lo que hemos puesto*/

      if (msgboxID == IDYES) { /*Si apretamos el bot�n s� el valor ser�a 7*/
        system("color 07"); /* Ponemos fondo negro con texto blanco*/
        exit(0);//Salimos
      }
      /*-- Ajustamos los cambios de color ---*/
    }
    if (colorTexto==15) {
      printf("\n");/* Un salto de l�nea antes del mensaje de pause*/
      system("PAUSE");/* Cuando se han mostrado los 15 colores en el texto se produce una pausa*/
      system("CLS");/*      Al pulsar una tecla se limpia la pantalla, */
      colorTexto = 0; /*    se pone el contador de colorTexto a 0      */
      colorFondo++; /*      y el contador de colorFondo avanza 1       */
    } else {
      colorTexto++; /* Mientras tanto el contador de colorTexto avanza 1 */
    }
    if (colorFondo == 16) {/* Cuando se han mostrado los 15 colores en el fondo */
      colorFondo = 0; /* El contado colorFondo se pone a 0 */
    }
  } while (!((GetAsyncKeyState(VK_ESCAPE))&&(msgboxID == IDYES)));
/*Las iteraciones tendr�n lugar hasta que se pulse ESC y se selecione si en el MessageBox que aparecer�*/

}
