/************************************
* M�dulo de definici�n: parking
************************************/

typedef int TipoVector[5];
typedef struct TipoParking {
	TipoVector disponibles;
	int plantas;
	void configurarParking( int np, int nd);
	void entrarCoche( int planta);
	void salirCoche( int planta);
     	void imprimirParking();
}

