#include "SimpleGPS.h"

/* Inicia todo el tipo abstracto a los valores iniciales (Posici�n 3D a cero y todos los sat�lites
   a distancia 0 y calidad mala */
void SimpleGPS::IniciarGPS() {
  posicion.latitud = 0.0;
  posicion.longitud = 0.0;
  posicion.altitud = 0.0;
  for (int i = 0; i < MaxSatelites; i++) {
    listaSatelites[i].distancia = 0.0;
    listaSatelites[i].calidad = mala;
  }
}

/* Devuelve cierto si se puede triangular la posici�n (existen al menos 3 sat�lites con calidad buena)
   y calcular la altura con un cuarto satelite (calidad buena) */
bool SimpleGPS::EsCalculoPosible() {
  int contadorSatelites = 0;            /* Busca 4 sat�lites con calidad buena */
  bool encontrados = false;             /* Si exiten al menos 4 sat�lites con calidad buena ser� true */
  int indice = 0;                       /* Para recorrer el vector */
  while (!encontrados && (indice < MaxSatelites)) {
    if (listaSatelites[indice].calidad == buena) {
      contadorSatelites++;
      if (contadorSatelites == 4) {
        encontrados = true;
      }
    }
    indice++;
  }
  return encontrados;
}

/* Toma los valores de tres sat�lites con calidad buena y llama al procedimiento Triangular para
   determinar la altitud y la longitud en un momento dado */
void SimpleGPS::CalcularPosicion() {
  float distanciaSat1 = 0.0;              /* Distancia del primer sat�lite */
  float distanciaSat2 = 0.0;              /* Distancia del segundo sat�lite */
  float distanciaSat3 = 0.0;              /* Distancia del tercer sat�lite */
  int asignar = 1;                        /* Para saber a qu� variable "distancia" debo asignar el valor */
  bool encontrados = false;               /* Se volver� true cuando se hayan encontrado 3 sat�lites con calidad buena */
  int indice = 0;                         /* Para recorrer el vector */
  while (!encontrados && (indice < MaxSatelites)) {
    if (listaSatelites[indice].calidad == buena) {
      switch (asignar) {
        case 1:
          distanciaSat1 = listaSatelites[indice].distancia;
          asignar++;
          break;
        case 2:
          distanciaSat2 = listaSatelites[indice].distancia;
          asignar++;
          break;
        case 3:
          distanciaSat3 = listaSatelites[indice].distancia;
          encontrados = true;
          break;
      }
    }
    indice++;
  }
  /* -- Se han encontrado 3 sat�lites con calidad buena -- */
  if (encontrados) {
    /* Se llama a la funci�n triangular de "calculos.h" con los valores de los tres sat�lites encontrados. Suponemos que
       dicha funci�n devolver� como par�matros por referencia la latitud y la longitud */
    Triangular(distanciaSat1,distanciaSat2,distanciaSat3,posicion.latitud,posicion.longitud);
  }
}
