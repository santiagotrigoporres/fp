#pragma once
#include "calculos.h"

/* Definici�n de constantes */
const int MaxSatelites = 24;

/* Tipo enumerado para la calidad de la se�al */
typedef enum CalidadSenal { buena, regular, mala };

/* Tipo sat�lite */
typedef struct TipoSatelite {
  float distancia;          /* Distancia en metros del sat�lite al receptor */
  CalidadSenal calidad;     /* Calidad de la se�al del sat�lite */
};

/* Tipo posici�n 3D */
typedef struct TipoPosicion3D {
  float latitud;
  float longitud;
  float altitud;
};

/* Tipo vector para almacenar los datos de los sat�lites */
typedef TipoSatelite ArraySatelites[MaxSatelites];

/* TAD */
typedef struct SimpleGPS {
  /* Inicia todo el tipo abstracto a los valores iniciales (Posici�n 3D a cero y todos los sat�lites
     a distancia 0 y calidad mala */
     void IniciarGPS();
     /* Devuelve cierto si se puede triangular la posici�n (existen al menos 3 sat�lites con calidad buena)
        y calcular la altura con un cuarto satelite (calidad buena) */
     bool EsCalculoPosible();
     /* Toma los valores de tres sat�lites con calidad buena y llama al procedimiento Triangular para
        determinar la altitud y la longitud en un momento dado */
     void CalcularPosicion();
     private:
      ArraySatelites listaSatelites;
      TipoPosicion3D posicion;
};



