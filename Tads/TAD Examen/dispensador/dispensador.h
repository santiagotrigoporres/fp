/********************************************************************
*  Interfaz de modulo: dispensador
*
*	Este modulo define el tipo abstracto dispensador, que ...
*
*********************************************************************/

#pragma once

typedef struct TipoProducto {
	int codigo;
	int unidadesDisponibles;
	float precioUnidad;
}

typedef TipoProducto TipoVectorProducto[10];

typedef struct TipoDispensador {
	void IniciarDispensador();
	void CargarProducto(int codigo, int cantidad, int precio);
	void ComprarProducto(int codigo);

	private:
		TipoVectorProducto vectorProducto;
		
		float recaudacion;
};
	