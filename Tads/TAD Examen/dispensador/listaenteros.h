/********************************************************************
*  Interfaz de modulo: listaenteros
*
*	Este modulo define el tipo abstracto listaenteros, que ...
*
*********************************************************************/

#pragma once

typedef int TipoVector[10];

typedef struct TipoListaEnteros {

	void EliminarRepetidos();
	void DarLaVuelta();

	private:
		TipoVector vector;
		int tamano;
		bool NoEsta(int valor, TipoVector v, int tam);
};
	