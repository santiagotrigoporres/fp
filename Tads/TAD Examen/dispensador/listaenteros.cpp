/********************************************************************
*  Modulo: listaenteros
*
*	Este modulo define el tipo abstracto listaenteros, que ...
*
*********************************************************************/

#include <stdio.h>
#include "listaenteros.h"

bool TipoListaEnteros::NoEsta(int valor, TipoVector v, int tam) {
	bool ne=true;
	for(int i=0; i<tam; i++) {
		if (v[i]==valor) {
			ne=false;
		}
	}
	return ne;
}

void TipoListaEnteros::EliminarRepetidos() {
	TipoVector vectorNuevo;
	int tamanoNuevo=0;
	int i=1;
	if (tamano>0) {
		
		tamanoNuevo++;
		vectorNuevo[0]=vector[0];
		while(i<tamano) {
			if(NoEsta(vector[i], vectorNuevo, tamanoNuevo)) {
				vectorNuevo[tamanoNuevo]=vector[i];
				tamanoNuevo++;
			} 

		}
		tamano=tamanoNuevo;
		for(int i=1; i<tamano; i++) {
			vector[i]=vectorNuevo[i];
		} 

	}
}

void TipoListaEnteros::DarLaVuelta() {
	if (tamano>0) {
		int aux;
		int i=0;
		while (i<=(tamano/2-1)){
			aux=vector[i];
			vector[i]=vector[tamano-1-i];
			vector[tamano-1-i]=aux;
			i++;
		}

	}
}

	