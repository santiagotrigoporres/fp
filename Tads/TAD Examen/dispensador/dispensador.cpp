/********************************************************************
*  Modulo: dispensador
*
*	Este modulo define el tipo abstracto dispensador, que ...
*
*********************************************************************/

#include <stdio.h>
#include "dispensador.h"

void TipoDispensador::IniciarDispensador() {
	for(int i=0; i<10; i++) {
		vectorProducto[i].codigo=0;
		vectorProducto[i].unidadesDisponibles=0;
	}
	recaudacion=0.;
}

void TipoDispensador::CargarProducto(int codigo, int cantidad, int precio)
{
	int i=0;
	bool encontradoProductoConEspacio=false;
	int posProducto=-1;
	bool encontradoHueco=false;
	int posHueco=-1;
		
	/*-- Busqueda producto --*/
	while ((i<10)&&(!encontradoProductoConEspacio)){
		if ((vectorProducto[i].codigo==codigo)&&((20-vectorProducto[i].unidadesDisponibles)>=cantidad)) {
			posProducto=i;
			encontradoProductoConEspacio=true;
		}
		i++;
	}
	if(!encontradoProductoConEspacio) {
		/*-- Busqueda hueco --*/
		i=0;
		while ((i<10)&&(!encontradoHueco)) {
			if (vectorProducto[i].codigo==0) {
				posHueco=i;
				encontradoHueco=true;
			}
			i++;
		) 
		if (encontradoHueco) {
			vectorProducto[posHueco].codigo=codigo;
			vectorProducto[posHueco].unidadesDisponibles=cantidad;
			vectorProducto[posHueco].precioUnidad=precio;
		} else {
			printf("Imposible cargar producto\n");
		}
	} else {
		vectorProducto[posProducto].unidadesDisponibles=vectorProducto[posProducto].unidadesDisponibles+cantidad;
	}
}

void TipoDispendador::ComprarProducto(int codigo) {
	/*-- Busqueda producto --*/
	int i=0;
	int posProducto=-1;
	bool encontradoProducto=false;
	while ((i<10)&&(!encontradoProducto)){
		if (vectorProducto[i].codigo!=codigo)	{
			posProducto=i;
			encontradoProducto=true;
		}
		i++;
	}
	if (!encontradoProducto) {
		printf("Producto no disponible\n");
	} else {
		vectorProducto[posProducto].unidadesDisponibles--;
		recaudacion=recaudacion+vectorProducto[i].precioUnidad;
		if (vectorProducto[i].unidadesDisponibles==0) {
			vectorProducto[i].codigo=0
		}
	}

}

	