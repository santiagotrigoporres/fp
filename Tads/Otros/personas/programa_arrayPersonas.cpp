/* Si ejecutamos el programa de prueba indicado el resultado debe ser este:
*** Operacion mayores de ***
Detectada persona con edad mayor o igual que 40 con datos:
Nombre y apellidos: Evo Morales Bolivia
Estado civil: divorciado
Edad: 66
Detectada persona con edad mayor o igual que 40 con datos:
Nombre y apellidos: Barack Obama EEUU
Estado civil: casado
Edad: 44
Operacion ContEstadoCivil
El numero de personas solteras es 1
Operacion ContEstadoCivil
El numero de personas casadas es 2
Operacion ContEstadoCivil
El numero de personas divorciadas es 1
Operacion guardar nuevo
Nuevos datos almacenados en posicion 4
Operacion guardar nuevo
Nuevos datos almacenados en posicion 5
Operacion ContEstadoCivil
El numero de personas casadas es 4
Pulsar cualquier tecla para continuar.
Nota: los arrays los rellenamos con edad cero en sus 50 items y estado civil indefinido. Los datos que se
consideran v�lidos son todos los datos hasta el �ltimo dato de edad distinto de cero.*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "arrayPersonas.h"


int main () {
  TipoArrayPersonas array1;
  /*Inicializamos las edades a cero y estados civiles a indefinido */
  for (int i = 0; i <= upperBoundArray; i++) {
    array1.edad[i]=0; array1.estadoCivil[i] = indefinido;
  }
  /* array1.nombre[0] = "Juan Gonzalez"; No permitido hemos de usar strcpy funci�n de string.h*/
  strcpy (array1.nombre[0], "Juan"); strcpy (array1.primerApellido[0], "Gonzalez"); strcpy
  (array1.segundoApellido[0], "Hernandez");
  array1.estadoCivil[0] = casado; array1.edad[0] = 23;
  strcpy (array1.nombre[1], "Evo"); strcpy (array1.primerApellido[1], "Morales"); strcpy
  (array1.segundoApellido[1], "Bolivia");
  array1.estadoCivil[1] = divorciado; array1.edad[1] = 66;
  strcpy (array1.nombre[2], "Barack"); strcpy (array1.primerApellido[2], "Obama"); strcpy
  (array1.segundoApellido[2], "EEUU");
  array1.estadoCivil[2] = casado; array1.edad[2] = 44;
  strcpy (array1.nombre[3], "Nicolas"); strcpy (array1.primerApellido[3], "Maduro"); strcpy
  (array1.segundoApellido[3], "Venezuela");
  array1.estadoCivil[3] = soltero; array1.edad[3] = 15;
  array1.MayoresDe(40);
  printf ("El numero de personas solteras es %d \n", array1.ContEstadoCivil(soltero));
  printf ("El numero de personas casadas es %d \n", array1.ContEstadoCivil(casado));
  printf ("El numero de personas divorciadas es %d \n", array1.ContEstadoCivil(divorciado));
  array1.GuardarNuevo ("Pedro", "Perez", "Suarez", casado, 41);
  array1.GuardarNuevo ("Cristina", "Fernandez", "Argentina", casado, 37);
  printf ("El numero de personas casadas es %d \n", array1.ContEstadoCivil(casado));
  return 0;
}
