/*Realizar en C/C++ un TAD, con fichero de interfaz y de implementaci�n, capaz de contener los siguientes
datos de 50 personas: Nombre, Primer Apellido, Segundo Apellido, Estado civil (soltero, casado, divorciado) y
edad. El TAD dispondr� de los siguientes subprogramas: MayoresDe, que recibe como entrada un n�mero
natural e imprime por pantalla los datos de aquellas personas cuya edad sea mayor o igual que la solicitada;
ContEstadoCivil, que recibe como entrada un estado civil y devuelve el n�mero de personas que tienen ese
estado civil; GuardarNuevo, que recibe los datos de una nueva persona y los almacena en el TAD.*/

#pragma once

const int upperBoundArray = 49;
typedef enum TipoEstadoCivil {soltero, casado, divorciado, indefinido};
typedef char TipoCadena[100]; /*Cadena de texto*/
typedef TipoCadena ArrayCadenas[upperBoundArray] ;
typedef TipoEstadoCivil ArrayEstadosCiv[upperBoundArray] ;
typedef int ArrayEdades[upperBoundArray] ;
typedef struct TipoArrayPersonas {
  /*Datos en el TAD*/
  ArrayCadenas nombre;
  ArrayCadenas primerApellido;
  ArrayCadenas segundoApellido;
  ArrayEstadosCiv estadoCivil;
  ArrayEdades edad;

  /*Operaciones en el TAD: MayoresDe, ContEstadoCivil y GuardarNuevo*/
  void MayoresDe (int edadParaListado);
  int ContEstadoCivil (TipoEstadoCivil estadoRecibido);
  void GuardarNuevo (TipoCadena nombreRecibido, TipoCadena primerApRecibido, TipoCadena
                     segApellidoRecibido, TipoEstadoCivil estadoRecibido, int edadRecibida);
  /* Operaciones auxiliares: no definimos ninguna*/
}; /*Cierre del struct con ; */
