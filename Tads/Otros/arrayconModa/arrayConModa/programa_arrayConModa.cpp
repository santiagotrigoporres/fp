/* Si ejecutamos el programa de prueba indicado el resultado debe ser este:
Comienzo del programa
La moda en el array 1 es 87
La moda en el array 2 es 0
La moda en el array 3 es 76
La moda en el array 4 es 12
Pulsar cualquier tecla para continuar.
Nota: todos los arrays los rellenamos con 50 ceros. Los datos que se consideran v�lidos son todos los datos
hasta el �ltimo dato distinto de cero. En el caso del array 2 el �ltimo dato distinto de cero es el de �ndice 12
con lo que se considera que los datos v�lidos son array2(0) con valor 0, array2(1) con valor cero, array2(2)
con valor cero � as� hasta array2(12) con valor 25. Por tanto se analiza cu�l es la moda teniendo 11 valores
cero y un valor 25, resultando la moda 0.
Nota: este ejercicio tiene su complicaci�n. Quiz�s pueda simplificarse un poco buscando otras alternativas
distintas a la propuesta (y no comentando tanto, nosotros hemos tratado de comentar bastante para que
quedara claro el por qu� de las cosas). La cuesti�n es �da tiempo a resolverlo completo en el examen
pensando que el tiempo disponible para el ejercicio de programaci�n puede ser de 1 hora � 1 hora y 30
minutos? La forma de saberlo es hacerlo y comprobarlo. Si no da tiempo, lo m�s adecuado ser�a dejar
algunas partes como operaciones en las que se comenta qu� es lo que devuelven pero no se incluye el
c�digo.
Por ejemplo: lastValidIndex = obtenerUltimoIndiceValido(); /* Con esto obtenemos el �ltimo valor del array
distinto de cero */
/* Si escribimos esto, dejamos indicado que ejecutamos una operaci�n pero no incluimos su c�digo.*/

#include <stdio.h>
#include <stdlib.h>
#include "arrayConModa.h"
/* www.aprenderaprogramar.com/foros/index.php?topic=401.0*/
int main () {
  TipoArrayConModa array1; TipoArrayConModa array2; TipoArrayConModa array3; TipoArrayConModa array4;
  printf("Comienzo del programa\n");
  /* Inicializaci�n del vector todos los valores a cero*/
  for (int i = 0; i <= upperBoundArray; i++) {
    array1.setValor(i, 0);
    array2.setValor(i, 0);
    array3.setValor(i, 0);
    array4.setValor(i, 0);
  } /*Cierre del for*/
  /* Valores que introducimos manualmente para prueba */
  array1.setValor(0, -33); array1.setValor(1, 44); array1.setValor(2, 87);
  array1.setValor(3, 87); array1.setValor(5, 87); array1.setValor(6, 44);
  array2.setValor(12, 25);
  array3.setValor(0, -331); array3.setValor(1, 434); array3.setValor(2, 287);
  array3.setValor(3, 787); array3.setValor(4, 182); array3.setValor(5, 76);
  array3.setValor(6, -33); array3.setValor(7, 44); array3.setValor(8, 87);
  array3.setValor(9, 76); array3.setValor(10, 76); array3.setValor(11, 44);
  array3.setValor(12, -33); array3.setValor(13, 76); array3.setValor(14, 87);
  array3.setValor(15, 76); array3.setValor(16, 87); array3.setValor(17, 44);
  array4.setValor(0, 11); array4.setValor(1, 12); array4.setValor(3, 12);
  printf ("La moda en el array 1 es %d\n", array1.calcularModa());
  printf ("La moda en el array 2 es %d\n", array2.calcularModa());
  printf ("La moda en el array 3 es %d\n", array3.calcularModa());
  printf ("La moda en el array 4 es %d\n", array4.calcularModa());
}
