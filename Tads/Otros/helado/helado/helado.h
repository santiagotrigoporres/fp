/* ENUNCIADO
Una helader�a ofrece cucuruchos con bolas de 25 gramos de tres sabores distintos a escoger entre
chocolate, vainilla, fresa, lim�n, pistacho y menta. Construya un Tipo Abstracto de Datos (TAD) que
almacene la cantidad de helado disponible de cada sabor e implemente las siguientes operaciones:
-IncrementarHelado: a�ade una cantidad de helado de un determinado sabor.
-EsPosibleCucurucho: recibe como entrada 3 sabores e indica si hay helado suficiente para confeccionar el
cucurucho.
-CucuruchosDisponibles: imprime en pantalla qu� tipos de cucuruchos pueden confeccionarse con los
helados disponibles.
SOLUCI�N PROPUESTA
Nota: es interesante ver c�mo se construye el bucle de tipos de cucuruchos que pueden confeccionarse con
los helados disponibles. Corresponde a combinaciones sin repetici�n.*/

/*INTERFAZ DEL TAD*/
#pragma once

typedef enum TipoError {errorGenerado};
typedef enum TipoSabor {desconocido, chocolate, vainilla, fresa, limon, pistacho, menta};
typedef float TipoCantidadDisponible[7];
typedef char TipoCadena[15];
typedef TipoCadena TipoNombresHelados[7];
typedef struct TipoHelado {
  TipoCantidadDisponible cantidadDisponible;
  void IncrementarHelado (TipoSabor sabor, float cantidad);
  bool EsPosibleCucurucho (TipoSabor sabor1, TipoSabor sabor2, TipoSabor sabor3);
  void CucuruchosDisponibles ();
};
