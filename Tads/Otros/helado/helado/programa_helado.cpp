/* EJEMPLO DE RESPUESTA DEL PROGRAMA
Comienzo del programa
Helado sabor 1 (chocolate) incrementado en 250.00 gramos
Helado sabor 1 (chocolate) incrementado en 250.00 gramos
Helado sabor 1 (chocolate) incrementado en 250.00 gramos
Helado sabor 5 (pistacho) incrementado en 250.00 gramos
Helado sabor 4 (limon) incrementado en 2000.00 gramos
Helado sabor 6 (menta) incrementado en 600.00 gramos
No es posible fabricar helado de chocolate-fresa-limon
Es posible fabricar helado de limon-pistacho-menta
No es posible fabricar helado de vainilla-fresa-pistacho
Helado sabor chocolate disponible 750.00 gramos permite preparar 30.00 bolas
Helado sabor vainilla no disponible
Helado sabor fresa no disponible
Helado sabor limon disponible 2000.00 gramos permite preparar 80.00 bolas
Helado sabor pistacho disponible 250.00 gramos permite preparar 10.00 bolas
Helado sabor menta disponible 600.00 gramos permite preparar 24.00 bolas
Combinacion disponible: chocolate - limon - pistacho
Combinacion disponible: chocolate - limon - menta
Combinacion disponible: chocolate - pistacho - menta
Combinacion disponible: limon - pistacho - menta
Pulsar cualquier tecla para continuar. */

#include <stdio.h>
#include <stdlib.h>
#include "helado.h"

int main() {
TipoHelado heladeria;
printf ("Comienzo del programa\n");
/*Inicializamos la cantidad disponible de cada tipo de helado a cero*/
for (int i=1; i<=6; i++){
heladeria.cantidadDisponible[i]=0;
}
heladeria.IncrementarHelado (chocolate, 250);
heladeria.IncrementarHelado (chocolate, 250);
heladeria.IncrementarHelado (chocolate, 250);
heladeria.IncrementarHelado (pistacho, 250);
heladeria.IncrementarHelado (limon, 2000);
heladeria.IncrementarHelado (menta, 600);
if (heladeria.EsPosibleCucurucho(chocolate, fresa, limon)){
printf ("Es posible fabricar helado de chocolate-fresa-limon\n");
} else {
printf ("No es posible fabricar helado de chocolate-fresa-limon\n");
}
if (heladeria.EsPosibleCucurucho(limon, pistacho, menta)){
printf ("Es posible fabricar helado de limon-pistacho-menta\n");
} else {
printf ("No es posible fabricar helado de limon-pistacho-menta\n");
}
if (heladeria.EsPosibleCucurucho(vainilla, fresa, pistacho)){
printf ("Es posible fabricar helado de vainilla-fresa-pistacho\n");
} else {
printf ("No es posible fabricar helado de vainilla-fresa-pistacho\n");
}
heladeria.CucuruchosDisponibles();
}
