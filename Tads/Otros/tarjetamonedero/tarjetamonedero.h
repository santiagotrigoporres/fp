/* Realizar en C+- un TAD para gestionar una tarjeta monedero. La informaci�n de la tarjeta es el saldo y las 10
�ltimas operaciones. La informaci�n de cada operaci�n es fecha (d�a-mes-a�o), cantidad (float), y tipo de
operaci�n (cargar, sacar, consultar). Las operaciones a realizar son:
1. Cargar una cantidad.
2. Sacar una cierta cantidad si hay saldo disponible.
3. Consultar el saldo y todas las �ltimas operaciones.*/

#pragma once
#include "gestordefechas.h"

const int MaxRegOperaciones = 5;
typedef enum TipoError {errorindefinido, saldoinsuficiente};
typedef enum TipoClaseOperacion {Indefinida, Cargar, Sacar, Consultar};
typedef struct TipoDatosOperacion {
  TipoFecha fechaOperacion;
  float cantidadOperacion;
  TipoClaseOperacion claseOperacion;
  float saldoTrasOperacion;
};
typedef TipoDatosOperacion TipoVectorDatosOperacion [MaxRegOperaciones];

typedef struct TipoDatosTarjetaMonedero {
  float saldo;
  TipoVectorDatosOperacion Operacion;
  void CargarCantidad(TipoFecha fecha, float cantidad);
  void SacarCantidad(TipoFecha fecha, float cantidad);
  void Consultar();
  void ReordenarRegistros();
};
