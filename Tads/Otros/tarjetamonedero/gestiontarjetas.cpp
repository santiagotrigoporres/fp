#include <stdio.h>
#include <stdlib.h>
#include "tarjetamonedero.h"
#include "gestordefechas.h"

int main () {
  TipoDatosTarjetaMonedero tarjeta1;
  try {
    printf ("Comienza el programa\n");
    for (int i=1; i<=10; i++) {
      tarjeta1.CargarCantidad(fechaHoy, 22.50);
      fechaHoy.dia = i;
      if (i==2) {
        tarjeta1.Consultar();
      }
    }
    for (int i=1; i<=10; i++) {
      fechaHoy.dia = i+20;
      tarjeta1.SacarCantidad(fechaHoy, 5.0);
      if (i==3) {
        tarjeta1.Consultar();
      }
    }
    tarjeta1.Consultar();
  } catch (TipoError e) {
    printf("se ha producido un error");
  }
}
