/*****************************************************************************************************************************
* REalizar el tipo abstracto de datos PrecioOro para guardar el precio de la onza de oro
* en 5 monedas diferentes (Dolar, euro, yen, libra, yuan). La Operacion nuevoprecio guarda el nuevo precio en una determinada
* moneda. La operacion CambioOro entre dos monedas devuelve los cambios unitarios de cada moneda en funcion
* de la otra tomando como base el precio del oro. La Operacion ListarPrecios escribe en
* pantalla la tabla de precios de la onza de oro en cada moneda.
*****************************************************************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include "precio_oro.h"

int main(){

TipoVectorOro oro;
printf("comienzo del programa\n");
oro.moneda[1].nombreMoneda=dolares;
oro.moneda[2].nombreMoneda=euros;
oro.moneda[3].nombreMoneda=yenes;
oro.moneda[4].nombreMoneda=libras;
oro.moneda[5].nombreMoneda=yuanes;

for(int i=1;i<=numeroMonedas; i++){
oro.moneda[i].precioOroMoneda = float(i*7+2);
printf(" Valor de la onza en la moneda %d es %.2f uds\n", i, oro.moneda[i].precioOroMoneda);
}

oro.ListarPrecios();
oro.NuevoPrecio(dolares, 13.85);
oro.NuevoPrecio(yuanes, 23.66);
oro.ListarPrecios();

oro.CambioOro(dolares, yuanes);
printf(" El cambio de dolares respecto a yuanes es de %.2f yuanes por dolar\n", cambioMoneda1);
printf(" El cambio de yuanes respecto a dolares es de %.2f dolares por yuan\n", cambioMoneda2);

oro.CambioOro(dolares, libras);
printf(" El cambio de dolares respecto a libras es de %.2f libras por dolar\n", cambioMoneda1);
printf(" El cambio de yuanes respecto a libras es de %.2f dolares por libra\n", cambioMoneda2);
}
