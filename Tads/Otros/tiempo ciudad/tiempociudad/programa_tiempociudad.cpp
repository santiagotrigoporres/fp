/* EJEMPLO DE RESPUESTA DEL PROGRAMA
Comienzo del programa
Creada nueva ciudad: Lima
Creada nueva ciudad: Madrid
Creada nueva ciudad: Sevilla
Creada nueva ciudad: Valencia
Nuevos datos registrados para Lima
Momento 0 : temperatura 12.00 y estado 0
Nuevos datos registrados para Lima
Momento 1 : temperatura 22.50 y estado 1
Nuevos datos registrados para Lima
Momento 2 : temperatura 5.50 y estado 1
Nuevos datos registrados para Lima
Momento 3 : temperatura 3.00 y estado 2
Ciudad : Lima
Por la mannana situacion : sol temperatura 12.00 grados
Por la tarde situacion : nubes temperatura 22.50 grados
Por la noche situacion : nubes temperatura 5.50 grados
Por la madrugada situacion : lluvia temperatura 3.00 grados
Nuevos datos registrados para Lima
Momento 2 : temperatura 15.50 y estado 3

Ciudad : Lima
Por la mannana situacion : sol temperatura 12.00 grados
Por la tarde situacion : nubes temperatura 22.50 grados
Por la noche situacion : nieve temperatura 15.50 grados
Por la madrugada situacion : lluvia temperatura 3.00 grados
Ciudad : Madrid
Por la mannana situacion : sol temperatura 0.00 grados
Por la tarde situacion : sol temperatura 0.00 grados
Por la noche situacion : sol temperatura 0.00 grados
Por la madrugada situacion : sol temperatura 0.00 grados
Nuevos datos registrados para Madrid
Momento 0 : temperatura 22.00 y estado 2
Ciudad : Madrid
Por la mannana situacion : lluvia temperatura 22.00 grados
Por la tarde situacion : sol temperatura 0.00 grados
Por la noche situacion : sol temperatura 0.00 grados
Por la madrugada situacion : sol temperatura 0.00 grados
Pulsar cualquier tecla para continuar.*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tiempociudad.h"

int main () {
  TipoTiempoCiudad tiempoLima;
  TipoTiempoCiudad tiempoMadrid;
  TipoTiempoCiudad tiempoSevilla;
  TipoTiempoCiudad tiempoValencia;
  printf("Comienzo del programa\n");
  tiempoLima.NuevaCiudad ("Lima");
  tiempoMadrid.NuevaCiudad ("Madrid");
  tiempoSevilla.NuevaCiudad ("Sevilla");
  tiempoValencia.NuevaCiudad ("Valencia");
  tiempoLima.NuevoTiempo ("Lima", mannana, 12.0, sol);
  tiempoLima.NuevoTiempo ("Lima", tarde, 22.5, nubes);
  tiempoLima.NuevoTiempo ("Lima", noche, 5.5, nubes);
  tiempoLima.NuevoTiempo ("Lima", madrugada, 3.0, lluvia);
  tiempoLima.ListaTiempo ("Lima");
  tiempoLima.NuevoTiempo ("Lima", noche, 15.5, nieve);
  tiempoLima.ListaTiempo ("Lima");
  tiempoMadrid.ListaTiempo ("Madrid");
  tiempoMadrid.NuevoTiempo ("Madrid", mannana, 22.0, lluvia);
  tiempoMadrid.ListaTiempo ("Madrid");
}
