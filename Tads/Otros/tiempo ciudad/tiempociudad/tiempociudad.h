/* Realizar en C+- el tipo abstracto de datos TiempoCiudad para guardar las temperaturas y el estado del
tiempo en una ciudad en 4 momentos diferentes. La operaci�n NuevaCiudad guarda el NombreCiudad
pasado como argumento e inicializa las temperaturas y los estados. La operaci�n NuevoTiempo con los
argumentos NombreCiudad, TipoMomento (enumerado: ma�ana, tarde, noche, madrugada), Temperatura
y Estado (enumerado: sol, nubes, lluvia, nieve) guarda el nuevo tiempo de la ciudad para ese momento. La
operaci�n ListaTiempo escribe en pantalla la ciudad y las temperaturas y estado en los distintos momentos
para el NombreCiudad pasado como argumento.*/

/*INTERFAZ DEL TIPO ABSTRACTO TIEMPO CIUDAD*/

#pragma once

typedef char TipoCadena [100]; /*Cadena de texto*/
typedef enum TipoError {ErrorGenerado}; /*Para uso con throw*/
typedef enum TipoMomento {mannana, tarde, noche, madrugada};
typedef enum TipoEstado {sol, nubes, lluvia, nieve};
typedef TipoEstado TipoVectorEstados [4];
typedef float TipoVectorTemperaturas [4];

typedef struct TipoTiempoCiudad {
  TipoVectorTemperaturas temperatura;
  TipoVectorEstados estado;
  TipoCadena NombreCiudad;
  void NuevoTiempo (TipoCadena nombredeciudad, TipoMomento momento, float temperaturamomento, TipoEstado estadomomento);
  void NuevaCiudad (TipoCadena nombredeciudad);
  void ListaTiempo (TipoCadena nombredeciudad);
};
