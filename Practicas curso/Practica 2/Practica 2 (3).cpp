#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>

int main ()
{
  int lado = 0;
  int fila = 0;
  int print = 0;

  printf ("Lado del Rombo?");
  scanf ("%d", &lado);
  printf("\r\n");

  if ((lado<1) || (lado > 20))
  {
    printf("El lado del triangulo debe ser entre 1 y 20");
  }
  else
  {
    if ( 0 <= lado && lado <21) /* Valor de lado entre 0 y 20*/
    {
      /* Triangulo Superior*/
      for (int fila=1 ; fila<=lado ; fila++)
      {
        /* Imprimir espacios*/
         for (int i=fila ; i<lado ; i++)
        {
          printf(" ");
        }
        /* Imprimir simbolos*/
        for (int j=0 ; j<fila*2-1 ; j++)
        {
          /* Esquina superior izquierda*/
          if (j < (fila*2)/2)
          {
            if((j%4) == 0)
            {
              printf("@");
            }
            else if((j%4) == 1)
            {
              printf(".");
            }
            else if((j%4) == 2)
            {
              printf("o");
            }
            else if((j%4) == 3)
            {
              printf(".");
            }
          }
          /* Esquina superior derecha*/
          else
          {
            print = (fila*2-1) - j;
            if((print%4) == 0)
            {
              printf(".");
            }
            else if((print%4) == 1)
            {
              printf("@");
            }
            else if((print%4) == 2)
            {
              printf(".");
            }
            else if((print%4) == 3)
            {
              printf("o");
            }
            print++;
          }
        }
        printf ("\n");
      }
      /* Triangulo Inferior */
      for (int fila=1 ; fila<lado ; fila++)
      {
        /* Imprimir espacios*/
         for (int i=0; i<fila ; i++)
        {
          printf(" ");
        }
        /* Imprimir simbolos */
        for (int j=0 ; j<(lado-fila)*2-1 ; j++)
        {
          /* Esquina inferior izquierda*/
          if (j < ((lado-fila)*2)/2)
          {
            if((j%4) == 0)
            {
              printf("@");
            }
            else if((j%4) == 1)
            {
              printf(".");
            }
            else if((j%4) == 2)
            {
              printf("o");
            }
            else if((j%4) == 3)
            {
              printf(".");
            }
          }
           /* Esquina inferior derecha*/
          else
          {
            print = ((lado-fila)*2-1) - j;
            if((print%4) == 0)
            {
              printf(".");
            }
            else if((print%4) == 1)
            {
              printf("@");
            }
            else if((print%4) == 2)
            {
              printf(".");
            }
            else if((print%4) == 3)
            {
              printf("o");
            }
            print++;
          }
        }
        printf ("\n");
      }

    }
  }
}
