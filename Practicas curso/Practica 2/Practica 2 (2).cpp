#include <stdio.h>

int main(){

  int resto, lado, posicion;

/*Introducir datos*/
  printf("\xa8Lado del Rombo?");
  scanf("%d", &lado);
  printf("\n");

/*Bucle indefinido de entrada de datos*/
  if ((lado >= 1) && (lado <= 20)){

/*Parte superior izquierda (lado -1 para que no imprima
la linea central*/
    for(int altura = 1; altura <= lado-1; altura++){
      for (int indice = 1; indice <= lado-altura; indice++){
        printf(" ");
      }
/*Posicion = 4 para que empiece con caracter '@'*/
      posicion = 4;
      for (int indice = 1; indice <= altura; indice++){
      resto = posicion%4;
        if(resto == 0){
        printf("@");
        }else if ((resto == 1)||(resto == 3)){
        printf(".");
        }else if (resto == 2){
        printf("o");
        }
        posicion++;
      }
/*Parte superior derecha*/
      posicion = posicion-2;
        for (int indice = altura-1; indice>=1; indice --){
          resto = posicion%4;
          if(resto == 0){
          printf("@");
          }else if ((resto == 1)||(resto == 3)){
          printf(".");
          }else if (resto == 2){
          printf("o");
          }
          posicion--;
        }
          printf("\n");
  }

/*Parte inferior izquierda*/
    for(int altura = lado; altura >= 1; altura--){
      for (int indice = lado-altura; indice >= 1; indice--){
        printf(" ");
      }
      posicion = 4;
        for (int indice = 1; indice <= altura; indice++){
          resto = posicion%4;
          if(resto == 0){
          printf("@");
          }else if ((resto == 1)||(resto == 3)){
          printf(".");
          }else if (resto == 2){
          printf("o");
          }
          posicion++;
        }
/*Parte inferior derecha*/
      posicion = posicion-2;
        for (int indice = altura-1; indice >= 1; indice --){
          resto = posicion%4;
          if(resto == 0){
          printf("@");
          }else if ((resto == 1)||(resto == 3)){
          printf(".");
          }else if (resto == 2){
          printf("o");
          }
          posicion--;
        }
          printf("\n");
  }
}
}
