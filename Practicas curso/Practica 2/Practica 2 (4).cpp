/*================================================
          DIRECTIVA DE COMPILACIÓN
==================================================*/
#include <stdio.h>

/*================================================
        PARTE EJECUTABLE DEL PROGRAMA
==================================================*/

/* Variables Globales */
int caracter,arroba;

void ImprimirEspaciosBlanco(int numLados, int numfila) {
  for (int i=1; i<=numLados-numfila; i++) {
    printf(" ");
  }
}

/* Paso por referencia las variables para tener sus valores en la mitad derecha*/
void ImprimirMitadIzquierda( int &arroba, int &caracter, int numfila) {

  /*Iniciamos las variables que se utilizarán para ver que caracter le toca*/{
    caracter=1;
    arroba=1;
  }
  for(int i=0; i<=numfila-1; i++){
    if ((caracter % 2)==0) {
      printf(".");
    } else {
      if (arroba==1) {
        printf("@");
        arroba=0;
      } else {
        printf("o");
        arroba=1;
      }
    }
    caracter++;
  }
  if ((numfila % 2)==0) {
    if (arroba==1) {
      arroba=0;
    } else {
      arroba=1;
    }
  }
}

/* Aqui no hace falta pasar las variables por referencia ya que se vuelven a reniciar*/
void ImprimirMitadDerecha( int arroba, int caracter, int numfila) {
  for(int i=0; i<=numfila-2; i++){
    if ((caracter %2) == 0 ) {
      printf(".");
    } else {
      if (arroba==1) {
        printf("@");
        arroba=0;
      } else {
        printf("o");
        arroba=1;
      }
    }
    caracter++;
  }
  printf("\n");
}

void ImprimirTrianguloSuperior(int numLados) {
  for (int i=1; i<=numLados; i++) {
    ImprimirEspaciosBlanco(numLados,i);
    ImprimirMitadIzquierda(arroba,caracter,i);
    ImprimirMitadDerecha(arroba,caracter,i);
  }
}

void ImprimirTrianguloInferior(int numLados) {
  for (int i=numLados-1; i>=1; i--) {
    ImprimirEspaciosBlanco(numLados,i);
    ImprimirMitadIzquierda(arroba,caracter,i);
    ImprimirMitadDerecha(arroba,caracter,i);
  }
}

int main() {
  int numLados=0;
  printf("\xa8Lado del Rombo?");
  scanf("%d",&numLados);
  printf("%\n");
  if (numLados>0 && numLados<21) {
    ImprimirTrianguloSuperior(numLados);
    ImprimirTrianguloInferior(numLados);
  }
}
