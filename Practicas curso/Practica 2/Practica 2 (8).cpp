#include <stdio.h>
int main()
{
	int lado,cont1,cont2,cont3,cont4,control,control2;   //cont = contador
	char c1[4];
	char c2[4];
	c1[0]='@';
	c1[1]='.';
	c1[2]='o';
	c1[3]='.';

	printf(" �lado del rombo? ");
	scanf("%d",&lado);

/*=====================================================================================================================
											   TRIANGULO SUPERIOR
=====================================================================================================================*/

/*************************************** TRIANGULO SUPERIOR IZQUIERDO **************************************************/

   for (cont1=1;cont1<=lado ;cont1++ )    //este for sirve para el n� de filas
   {
	   for (cont2=1;cont2<=lado-cont1 ;cont2++) /*if lado = 10, la primera vez har� 9 espacios. la segunda vez 8,
	                                             y as� sucesivamente*/
		{
			printf(" ");
		}

		control=0;                          //con esta variable(control) controlo los caracteres que tienen q salir
		for (cont3=0;cont3<cont1 ;cont3++ )//este for sirve para escribir los caracteres del triangulo superior izquierdo
		{

			printf("%c",c1[control]);
			control++;
			if (control==4)
			{
				control=0;  /*cuando control == 4, quiere decir que ha llegado al final de la posici�n de memoria de mi
							 array as� que comienzo desde el principio(0) del array, porque s�lo tengo 4
							 caracteres  que se repiten, de este modo consigo repetir @.�.@.� ... */
			}
		}

 /*************************************** TRIANGULO SUPERIOR DERECHO **************************************************/

		control2=control-2;   /* -2 porque trabajar� con la simetria correspondiente del triangulo superior derecho con
		                       respecto al triangulo superior izquierdo, entonces retrocedo al caracter anterior al
							   ultimo introducido correspondiente al triangulo superior izquierdo para empezar a colocar
							   los caracteres de forma inversa, gracias a control2--.*/
		for (cont4=0;cont4<cont1-1 ;cont4++)//este for sirve para escribir los caracteres del triangulo superior derecho
		{
			if (control2==-2)
			{
				control2=2;
			}
			if (control2==-1 )
			{
				control2=3;
			}
			printf("%c",c1[control2]);
			control2--;
		}
		printf("\n");

   }

/*=====================================================================================================================
											 TRIANGULO INFERIOR
=====================================================================================================================*/

/*************************************** TRIANGULO INFERIOR IZQUIERDO **************************************************/

// hare lo mismo que con los triangulos anteriores, a exepci�n de que los espacios ir�n de forma inversa

 for (cont1=1;cont1<lado ;cont1++ )    //este for sirve para el n� de filas
   {
	   for (cont2=1;cont2<=lado-(lado-cont1) ;cont2++)
		{
			printf(" ");
		}

		control=0;                          //con esta variable(control) controlo los caracteres que tienen q salir
		for (cont3=0;cont3<lado-cont1 ;cont3++ )//este for sirve para escribir los caracteres del triangulo superior izquierdo
		{

			printf("%c",c1[control]);
			control++;
			if (control==4)
			{
				control=0;  /*cuando control == 4, quiere decir que ha llegado al final de la posici�n de memoria de mi
							 array as� que comienzo desde el principio(0) del array, porque s�lo tengo 4
							 caracteres  que se repiten, de este modo consigo repetir @.�.@.� ... */
			}
		}


 /*************************************** TRIANGULO INFERIOR DERECHO **************************************************/

		control2=control-2;   /* -2 porque trabajar� con la simetria correspondiente del triangulo superior derecho con
		                       respecto al triangulo superior izquierdo, entonces retrocedo al caracter anterior al
							   ultimo introducido correspondiente al triangulo superior izquierdo para empezar a colocar
							   los caracteres de forma inversa, gracias a control2--.*/
		for (cont4=0;cont4<lado-cont1-1;cont4++)//este for sirve para escribir los caracteres del triangulo superior derecho
		{
			if (control2==-2)
			{
				control2=2;
			}
			if (control2==-1 )
			{
				control2=3;
			}
			printf("%c",c1[control2]);
			control2--;
		}
		printf("\n");

}

}

