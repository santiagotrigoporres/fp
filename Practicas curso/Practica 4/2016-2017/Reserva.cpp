/***************************************************************************
* M�dulo GestionReservaSala
*
*   Este m�dulo controla la gesti�n de reservas
*
* CURSO:  2016/2017
*****************************************************************************/

#include <stdio.h>
#include <string.h>
#include "Reserva.h"
#include "CalendarioMes.h"

//crear e inicializar un TAD TipoReserva
void TipoReserva::CrearReserva(TipoFecha fecha,TipoNombre nombre, int horaInicio, int duracion)
{
  int auxHoraInicio;
  Fecha=fecha;
  strcpy(Nombre,nombre);
  Duracion=duracion;
  HoraInicio=horaInicio;
  //crear vector de horas reservadas
  auxHoraInicio=horaInicio;
  //inicializar a cero el vector
  for(int i = 0; i<12;i++)
  {
    HorasReservadas[i]=0;
  }
  for(int i = 0; i<duracion;i++)
  {
    HorasReservadas[i]=auxHoraInicio;
    auxHoraInicio++;
  }
  Activa=true;
}
void TipoReserva::imprimirReserva()
{
  printf("%d a %d reservada por: %s\n",HoraInicio,HoraInicio+Duracion,Nombre);
}

//crear e inicializar un TAD TipoFecha
void TipoFecha::crearFecha(int d, int m, int y)
{
  //Saber el tipo de dia con la fecha introducida
  diaTipo=TipoDia(diaFecha(d,m,y));
  mesTipo=TipoMes(m-1);
  mes=m;
  dia=d;
  anno=y;
}

