/***************************************************************************
*Interfaz de M�dulo GestionReservaSala
*
*   Este m�dulo controla la gesti�n de reservas
*
* CURSO:  2016/2017
*****************************************************************************/

//evitar Duplicidad de c�digo en compilaci�n
#pragma once
#include "Reserva.h"

const int maxReservas=21;
typedef TipoReserva Reservas[maxReservas];

char pintarMenuPrincipal();
typedef struct GestionReservaSala{
    int contadorMesActual;
    int contadorMesMasUno;
    int contadorMesMasDos;
    int contadorMesMasTres;
    Reservas reservasMesActual;
    TipoMes MesActualTipo;
    int mesActual;
    Reservas reservasMesMasUno;
    TipoMes MesMasUnoTipo;
    int mesMasUno;
    Reservas reservasMesMasDos;
    TipoMes MesMasDosTipo;
    int mesMasDos;
    Reservas reservasMesMasTres;
    TipoMes MesMasTresTipo;
    int mesMasTres;
    bool NuevaReserva();
    void AnularReserva();
    void PrintReservasDia();
    void PrintReservasMes();
    bool Inicializar();
    bool comprobarHorasReservadas(TipoMes m,int d, int stTime, int durTime);
    int LeerDatosReserva();
    void BorrarRegistro(TipoReserva &r);
    void imprimirReservasDia();
    void comprobarReservasMesDia(int _dia,int _mes);
    void mostrarMesReservas(int mes, int agno);
    bool comprobarReservasMesDiaDatos(int _dia, int _mes);
    void imprimirReservasDiaPorFecha(int day, int month, int year);
};
