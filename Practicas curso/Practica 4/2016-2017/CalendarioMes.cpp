/***************************************************************************
*
*      MODULO CALENDARIOMES             PR�CTICA 4
*
*         Este m�dulo controla la gesti�n de calendario
*
* CURSO:  2016/2017
*****************************************************************************/
//evitar Duplicidad de c�digo en compilaci�n
#include <stdio.h>
#include <time.h>
#include <windows.h>
#include "Reserva.h"

typedef char cadena[18];
typedef tm* Mitipotime;

//calcular el Mes actual (int)
//calcular el numero del mes actual
int MesActual()
{
  const int annoInicio=1970;
  time_t seconds;
  float horas;
  float dias;
  float annos;
  int annoActual;
  float annoActualDecimal;
  int mes;
  float mesDecimal;
  seconds = time(NULL);
  horas = seconds/3600;
  dias = horas/24;
  annos = dias/365;
  annoActualDecimal=annos+annoInicio;
  annoActual=annos+annoInicio;
  mesDecimal=annoActualDecimal-float(annoActual);
  mes=mesDecimal*12;
  return mes-1;
}


//obtener datos fecha actual
//i=0 --> dia actual
//i=1 --> Mes actual
//i=2 --> A�o actual
//i=3 --> hora actual
int fechaHora(int i)
{
 cadena fecha;
 time_t tiempo;
 Mitipotime s_tiempo;

 int mes;
 int ano;
 int dia;
 int hora;

 tiempo = time(0);
 s_tiempo = localtime(&tiempo);


 ano=s_tiempo->tm_year + 1900;
 mes=s_tiempo->tm_mon + 1;
 dia=s_tiempo->tm_mday;
 hora=s_tiempo->tm_hour;

 if (i==0)
 {
   return dia;
 }
 else if (i==1)
 {
   return mes;
 }
 else if (i==2)
 {
   return ano;
 }
 else if (i==3)
 {
   return hora;
 }
 else
 {
   return -1;
 }

}


//calcula con la variable mes, (1,2 o 3), el mes actual mas la variable mes
//se utiliza para saber los 3 meses siguientes al mes actual
  TipoMes MesObjetivo(int mes)
{
  int EsteMes;
  int mesObjetivo;
  EsteMes=fechaHora(1);
  if((EsteMes+mes)>12)
  {
    mesObjetivo=(mes+EsteMes)-12;
  }
  else
  {
    mesObjetivo=EsteMes;
  }
  return TipoMes(mesObjetivo-1);
}

//si el a�o es Bisiesto
bool agnoBisiesto(int anno)
{
  return !(anno%4) && (anno%100 || !(anno%400));
}

//Calcula los dias que tiene un mes en concreto
int diaFinal(int mes, int agno)
{
    typedef int Vector[12];
    Vector dias = {31,28 + agnoBisiesto(agno),31,30,31,30,31,31,30,31,30,31};
    return dias[mes-1];
}

//Comprueba que la fecha introducida es v�lida para el sistema con los criterios de la pr�ctica
bool comprobarFecha(int dia,int mes, int anno)
{
  bool resultado=false;
  int diasMes;
  TipoMes EsteMes;
  TipoMes EsteMes1;
  TipoMes EsteMes2;
  TipoMes EsteMes3;
  TipoDia DiaTipo;
  DiaTipo=TipoDia(diaFecha(dia,mes,anno));
  EsteMes = TipoMes(fechaHora(1)-1);
  EsteMes1=MesObjetivo(1);
  EsteMes2=MesObjetivo(2);
  EsteMes3=MesObjetivo(3);
  diasMes=diaFinal(mes,anno);
  if (TipoMes(mes-1)!=EsteMes && TipoMes(mes-1)!=EsteMes1 && TipoMes(mes-1)!=EsteMes2 && TipoMes(mes-1)!=EsteMes3 )
  {
    resultado=false;
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
    printf("\nEl sistema solo puede reservar dias de este mes o de los 3 siguientes.");
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
  }
  else if ((EsteMes>TipoMes(mes-1))&&(anno<=fechaHora(2)))
  {
      resultado=false;
      SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
      printf("\nLa Fecha Introducida es anterior a la fecha de Hoy");
      SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);

  }
  else if (dia>diasMes)
  {
    resultado=false;
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
    printf("\nEl dia introducido no existe en el mes que se quiere reservar");
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);

  }
  //comprobar si no es fin de semana
  else if (DiaTipo==Sabado || DiaTipo == Domingo)
  {
    resultado=false;
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
    printf("\nEl dia introducido es Fin de semana.");
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);

  }//comprobar a�o
  else if (!(fechaHora(2)<=anno)||(fechaHora(2)>=anno+1))
  {
    resultado=false;
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
    printf("\nEl anio introducido es erroneo.");
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);

  }else
  {
      resultado=true;
  }
  //comprobar dia
  if(fechaHora(1)==mes)
  {
    if(!(fechaHora(0)<=dia))
    {
      resultado=false;
      SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
      printf("\nEl dia es anterior a la fecha actual!");
      SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
    }
  }

  return resultado;
}

//calcular el dia por doomsay(dia de la semana segun fecha)
int diaFecha(int dia, int mes, int agno)
{

  int result1,result2,result3,result4,result5;
  typedef int Vector[12];
  Vector regular={0,3,3,6,1,4,6,2,5,0,3,5};
  Vector bisiesto={0,3,4,0,2,5,0,3,6,1,4,6};
  if (agnoBisiesto(agno))
  {
    mes=bisiesto[mes-1];
  }else
  {
    mes=regular[mes-1];
  }
      result1=(agno-1)%7;
      result2=(agno-1)/4;
      result3=(3*(((agno-1)/100)+1))/4;
      result4=(result2-result3)%7;
      result5=dia%7;
      dia=(result1+result4+mes+result5)%7;
      switch (dia)
      {
        case 0:
        dia=6;//domingo
        break;
        case 1:
        dia=0;//lunes
        break;
        case 2:
        dia=1;//martes
        break;
        case 3:
        dia=2;//miercoles
        break;
        case 4:
        dia=3;//jueves
        break;
        case 5:
        dia=4;//viernes
        break;
        case 6:
        dia=5;//sabado
        break;
      }
    return dia;
}

//imprimir cabecera de mes
void imprimirMes(int mes, int agno)
{
  switch (mes)
  {
    case 1:
    printf("ENERO                  %d\n",agno);
    break;
    case 2:
    printf("FEBRERO                %d\n",agno);
    break;
    case 3:
    printf("MARZO                  %d\n",agno);
    break;
    case 4:
    printf("ABRIL                  %d\n",agno);
    break;
    case 5:
    printf("MAYO                   %d\n",agno);
    break;
    case 6:
    printf("JUNIO                  %d\n",agno);
    break;
    case 7:
    printf("JULIO                  %d\n",agno);
    break;
    case 8:
    printf("AGOSTO                 %d\n",agno);
    break;
    case 9:
    printf("SEPTIEMBRE             %d\n",agno);
    break;
    case 10:
    printf("OCTUBRE                %d\n",agno);
    break;
    case 11:
    printf("NOVIEMBRE              %d\n",agno);
    break;
    case 12:
    printf("DICIEMBRE              %d\n",agno);
    break;

  }
}





