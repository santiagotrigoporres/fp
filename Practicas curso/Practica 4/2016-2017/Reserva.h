/***************************************************************************
*Interfaz de M�dulo Reserva
*
*   Este m�dulo es una abstracci� de la figura de una reserva
*
* CURSO:  2016/2017
*****************************************************************************/

//evitar Duplicidad de c�digo en compilaci�n
#pragma once

const int maxNombre=21;
const int maxHorasReservadas=12;

typedef char TipoNombre[maxNombre];

typedef int TipoHorasReservadas[maxHorasReservadas];
//typedef int HorasReservadas[maxHorasReservadas]; Borrar

typedef enum TipoMes {
Enero, Febrero, Marzo, Abril, Mayo,
Junio, Julio, Agosto, Septiembre,
Octubre, Noviembre, Diciembre
};

typedef enum TipoDia {
  Lunes, Martes, Miercoles,
  Jueves, Viernes, Sabado,
  Domingo
  };

int diaFecha(int dia, int mes, int agno);

typedef struct TipoFecha {
int dia;
int mes;
TipoMes mesTipo;
int anno;
TipoDia diaTipo;
void crearFecha(int d, int m, int y);
};



typedef struct TipoReserva{
  TipoFecha Fecha;
  TipoNombre Nombre;
  int HoraInicio;
  int Duracion;
  int Indice;
  bool Activa;
  TipoHorasReservadas HorasReservadas;
  void CrearReserva(TipoFecha fecha,TipoNombre nombre, int horaInicio, int duracion);
  void imprimirReserva();
  };

