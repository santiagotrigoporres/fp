/***************************************************************************
* MAIN
*
*   Programa principal
*
* CURSO:  2016/2017
*****************************************************************************/

#include <stdio.h>
#include <string.h>
#include "GestionReservaSala.h"

//Metodo creado para comprobar datos

void datos(GestionReservaSala &g)
{
  int contadorHoras;
  int contadorReservas;
  g.reservasMesActual[0].Activa=true;
  g.reservasMesActual[0].Fecha.crearFecha(3,2,2017);
  g.reservasMesActual[0].Duracion=1;
  g.reservasMesActual[0].HoraInicio=8;
  g.reservasMesActual[0].HorasReservadas[0]=8;
  //strncpy(g.reservasMesActual[0].Nombre,"Juan Ibero");
  strcpy(g.reservasMesActual[0].Nombre,"Juan Ibero");
  g.contadorMesActual++;

  g.reservasMesActual[1].Activa=true;
  g.reservasMesActual[1].Fecha.crearFecha(3,2,2017);
  g.reservasMesActual[1].Duracion=1;
  g.reservasMesActual[1].HoraInicio=9;
  g.reservasMesActual[1].HorasReservadas[0]=9;
  //strncpy(g.reservasMesActual[0].Nombre,"Juan Ibero");
  strcpy(g.reservasMesActual[1].Nombre,"Juan Palomo");
  g.contadorMesActual++;

  g.reservasMesActual[2].Activa=true;
  g.reservasMesActual[2].Fecha.crearFecha(3,2,2017);
  g.reservasMesActual[2].Duracion=1;
  g.reservasMesActual[2].HoraInicio=10;
  g.reservasMesActual[2].HorasReservadas[0]=10;
  //strncpy(g.reservasMesActual[0].Nombre,"Juan Ibero");
  strcpy(g.reservasMesActual[2].Nombre,"Maria Ruiz");
  g.contadorMesActual++;

  g.reservasMesActual[3].Activa=true;
  g.reservasMesActual[3].Fecha.crearFecha(7,2,2017);
  g.reservasMesActual[3].Duracion=12;
  g.reservasMesActual[3].HoraInicio=8;
  contadorHoras=0;
  contadorHoras=8;
  for (int i =0; i<12;i++)
  {
    g.reservasMesActual[3].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  //strncpy(g.reservasMesActual[0].Nombre,"Juan Ibero");
  strcpy(g.reservasMesActual[3].Nombre,"Gema Martinez");
  g.contadorMesActual++;

  g.reservasMesActual[4].Activa=true;
  g.reservasMesActual[4].Fecha.crearFecha(16,2,2017);
  g.reservasMesActual[4].Duracion=3;
  g.reservasMesActual[4].HoraInicio=10;
  g.reservasMesActual[4].HorasReservadas[0]=10;
  g.reservasMesActual[4].HorasReservadas[1]=11;
  g.reservasMesActual[4].HorasReservadas[2]=12;
  //strncpy(g.reservasMesActual[0].Nombre,"Juan Ibero");
  strcpy(g.reservasMesActual[4].Nombre,"Darth Vader");
  g.contadorMesActual++;

  g.reservasMesActual[5].Activa=true;
  g.reservasMesActual[5].Fecha.crearFecha(16,2,2017);
  g.reservasMesActual[5].Duracion=1;
  g.reservasMesActual[5].HoraInicio=14;
  g.reservasMesActual[5].HorasReservadas[0]=14;
  //strncpy(g.reservasMesActual[0].Nombre,"Juan Ibero");
  strcpy(g.reservasMesActual[5].Nombre,"Pepe Ripodas");
  g.contadorMesActual++;

  g.reservasMesActual[6].Activa=true;
  g.reservasMesActual[6].Fecha.crearFecha(16,2,2017);
  g.reservasMesActual[6].Duracion=1;
  g.reservasMesActual[6].HoraInicio=16;
  g.reservasMesActual[6].HorasReservadas[0]=16;
  //strncpy(g.reservasMesActual[0].Nombre,"Juan Ibero");
  strcpy(g.reservasMesActual[6].Nombre,"Javier Lopez");
  g.contadorMesActual++;

  g.reservasMesActual[7].Activa=true;
  g.reservasMesActual[7].Fecha.crearFecha(22,2,2017);
  g.reservasMesActual[7].Duracion=1;
  g.reservasMesActual[7].HoraInicio=16;
  g.reservasMesActual[7].HorasReservadas[0]=16;
  //strncpy(g.reservasMesActual[0].Nombre,"Juan Ibero");
  strcpy(g.reservasMesActual[7].Nombre,"Javier Mario");
  g.contadorMesActual++;

  g.reservasMesActual[8].Activa=true;
  g.reservasMesActual[8].Fecha.crearFecha(22,2,2017);
  g.reservasMesActual[8].Duracion=1;
  g.reservasMesActual[8].HoraInicio=10;
  g.reservasMesActual[8].HorasReservadas[0]=10;
  //strncpy(g.reservasMesActual[0].Nombre,"Juan Ibero");
  strcpy(g.reservasMesActual[8].Nombre,"Bob Esponja");
  g.contadorMesActual++;

  g.reservasMesActual[9].Activa=true;
  g.reservasMesActual[9].Fecha.crearFecha(22,2,2017);
  g.reservasMesActual[9].Duracion=1;
  g.reservasMesActual[9].HoraInicio=11;
  g.reservasMesActual[9].HorasReservadas[0]=11;
  //strncpy(g.reservasMesActual[0].Nombre,"Juan Ibero");
  strcpy(g.reservasMesActual[9].Nombre,"Javier Perez");
  g.contadorMesActual++;

  //mes mas tres
  contadorReservas=0;
  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(20,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=12;
  g.reservasMesMasTres[contadorReservas].HoraInicio=8;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"perico Sanchez");
  g.contadorMesMasTres++;
  contadorReservas++;


  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(21,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=12;
  g.reservasMesMasTres[contadorReservas].HoraInicio=8;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"perico Sanchez");
  g.contadorMesMasTres++;
  contadorReservas++;

  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(22,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=6;
  g.reservasMesMasTres[contadorReservas].HoraInicio=8;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"perico Sanchez");
  g.contadorMesMasTres++;
  contadorReservas++;

  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(23,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=12;
  g.reservasMesMasTres[contadorReservas].HoraInicio=8;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"perico Sanchez");
  g.contadorMesMasTres++;
  contadorReservas++;

  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(24,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=4;
  g.reservasMesMasTres[contadorReservas].HoraInicio=8;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"perico Sanchez");
  g.contadorMesMasTres++;
  contadorReservas++;



  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(13,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=1;
  g.reservasMesMasTres[contadorReservas].HoraInicio=8;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"Raquel Indart");
  g.contadorMesMasTres++;
  contadorReservas++;
  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(13,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=1;
  g.reservasMesMasTres[contadorReservas].HoraInicio=9;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"Juan Ibero");
  g.contadorMesMasTres++;
  contadorReservas++;

  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(13,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=1;
  g.reservasMesMasTres[contadorReservas].HoraInicio=10;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"Satanas Garcia");
  g.contadorMesMasTres++;
  contadorReservas++;



  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(13,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=1;
  g.reservasMesMasTres[contadorReservas].HoraInicio=11;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"maria Lopez");
  g.contadorMesMasTres++;
  contadorReservas++;

  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(13,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=1;
  g.reservasMesMasTres[contadorReservas].HoraInicio=12;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"Paco Placeta");
  g.contadorMesMasTres++;
  contadorReservas++;


  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(13,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=1;
  g.reservasMesMasTres[contadorReservas].HoraInicio=13;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"Mikel Arregui");
  g.contadorMesMasTres++;
  contadorReservas++;

  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(13,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=1;
  g.reservasMesMasTres[contadorReservas].HoraInicio=14;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"Angel Ibero");
  g.contadorMesMasTres++;
  contadorReservas++;


  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(13,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=1;
  g.reservasMesMasTres[contadorReservas].HoraInicio=19;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"Jose Ibero");
  g.contadorMesMasTres++;
  contadorReservas++;


  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(13,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=1;
  g.reservasMesMasTres[contadorReservas].HoraInicio=15;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"MariCarmen Indart");
  g.contadorMesMasTres++;
  contadorReservas++;


  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(13,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=1;
  g.reservasMesMasTres[contadorReservas].HoraInicio=16;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"Jesus indart");
  g.contadorMesMasTres++;
  contadorReservas++;

  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(13,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=1;
  g.reservasMesMasTres[contadorReservas].HoraInicio=17;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"Perico Palotes");
  g.contadorMesMasTres++;
  contadorReservas++;

  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(13,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=1;
  g.reservasMesMasTres[contadorReservas].HoraInicio=18;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"Jose arregui");
  g.contadorMesMasTres++;
  contadorReservas++;


  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(14,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=1;
  g.reservasMesMasTres[contadorReservas].HoraInicio=8;
  contadorHoras=0;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"Marisa Ruiz");
  g.contadorMesMasTres++;
  contadorReservas++;

  g.reservasMesMasTres[contadorReservas].Activa=true;
  g.reservasMesMasTres[contadorReservas].Fecha.crearFecha(14,3,2017);
  g.reservasMesMasTres[contadorReservas].Duracion=2;
  g.reservasMesMasTres[contadorReservas].HoraInicio=10;
  contadorHoras=0;
  g.contadorMesMasTres=18;
  contadorHoras=g.reservasMesMasTres[contadorReservas].HoraInicio;
  for (int i =0; i<g.reservasMesMasTres[contadorReservas].Duracion;i++)
  {
    g.reservasMesMasTres[contadorReservas].HorasReservadas[i]=contadorHoras;
    contadorHoras++;
  }
  strcpy(g.reservasMesMasTres[contadorReservas].Nombre,"Lionel Messi");
  g.contadorMesMasTres++;
  contadorReservas++;

}



int main()
{
  GestionReservaSala g;
  char opcionMenu;
  bool repetirMenu;
  repetirMenu=true;
  g.Inicializar();
  datos(g); //descomentar esta linea del main para forzar la introduccion de datos por codigo
  while(repetirMenu)
  {

    opcionMenu=pintarMenuPrincipal();
    switch(opcionMenu)
    {
      case 'N':
      g.NuevaReserva();
      break;
      case 'A':
      g.AnularReserva();
      break;
      case 'D':
      g.imprimirReservasDia();
      break;
      case 'M':
      g.PrintReservasMes();
      break;
      case 'S':
      repetirMenu=false;
      break;
    }
  }
}
