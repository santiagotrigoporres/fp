/***************************************************************************
*M�dulo GestionReservaSala
*
*   Este m�dulo controla la gesti�n de reservas
*
* CURSO:  2016/2017
*****************************************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <windows.h>
#include "GestionreservaSala.h"
#include "Reserva.h"
#include "CalendarioMes.h"

char LetraAux;
typedef TipoReserva Reservas_acumuladas[100];
GestionReservaSala GesSala;

//inicializar los contadores del programa
bool GestionReservaSala::Inicializar()
{
  bool InicializacionCorrecta;
  //Contadores reservas mes a cero
  contadorMesActual=0;
  contadorMesMasDos=0;
  contadorMesMasTres=0;
  contadorMesMasUno=0;
  InicializacionCorrecta=true;
  //calculo de meses de funcionamiento programa
  mesActual=(MesActual()+1);
  MesActualTipo=TipoMes(fechaHora(1)-1);
  MesMasUnoTipo=MesObjetivo(1);
  MesMasDosTipo=MesObjetivo(2);
  MesMasTresTipo=MesObjetivo(3);
  //calculo de meses(int) programa
  mesMasUno=MesMasUnoTipo;
  mesMasUno=mesMasUno+1;
  mesMasDos=MesMasDosTipo;
  mesMasDos=mesMasDos+1;
  mesMasTres=MesMasTresTipo;
  mesMasTres=mesMasTres+1;
  //inicializar las horas de cada reserva a 0 y las reservas como desactivadas
  for(int i= 0; i<21;i++)
  {
    reservasMesActual[i].Activa=false;
    reservasMesMasUno[i].Activa=false;
    reservasMesMasDos[i].Activa=false;
    reservasMesMasTres[i].Activa=false;
    for(int j=0; j<12;j++)
    {
      reservasMesActual[i].HorasReservadas[j]=0;
      reservasMesMasUno[i].HorasReservadas[j]=0;
      reservasMesMasDos[i].HorasReservadas[j]=0;
      reservasMesMasTres[i].HorasReservadas[j]=0;

    }
  }
  return InicializacionCorrecta;
}

/*
*Funcion para imprimir las reservas de un dia,
*Este metodo Obtiene los valores de mes dia y a�o por medio de consola, y los valida
*con la funcion comprobarFecha(dia,mes,a�o); despues se recorre todas las reservas de todos los mese
*creando un unico vector de tipo HorasReservadas, con las horas que estan reservadas en el dia consultado
*marcadas con un 1 si estan ocupadas, o un 0 si estan libres
*/
void GestionReservaSala::imprimirReservasDia()
{
  int day;
  int month;
  int year;
  int contadorAux;
  bool espacioVacio;
  TipoHorasReservadas horasReservadasDiaConsulta;
  Reservas_acumuladas reservasAimprimir;
  int contadorReservas;
  SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),151);
  printf("ReservasDia:\n");
  SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
  printf("\tDia?");
  scanf("%d",& day);
  printf("\tMes?");
  scanf("%d",& month);
  printf("\tAnio?");
  scanf("%d",& year);
  contadorReservas=0;
  contadorAux=8;
  //inicializar vector trabajo horas
  for(int i=0;i<12;i++)
  {
    horasReservadasDiaConsulta[i]=0;
  }
  if(!comprobarFecha(day,month,year))
  {
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
    printf("\nLa fecha que ha introduciodo no es valida para el sistema\n");
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);

  }
  printf("\n\tReservas del dia: %d/%d/%d\n\n", day, month,year);
  printf("I-------------I--------------------------I\n");
  printf("I Horas:      I08  10  12  14  16  18  20I\n");
  printf("I-------------I--------------------------I\n");
  printf("I Horas:      I  ");
  //rescorrer todas las reservas
  for(int i = 0; i<20; i++)
  {//Comprobar si esta la resserva activa
    if (reservasMesActual[i].Activa)
    {//comprobar si el mes es el adecuado
    if (reservasMesActual[i].Fecha.mes==month)
      {
        if (reservasMesActual[i].Fecha.dia==day)
        {
          contadorAux=0;
          reservasAimprimir[contadorReservas]=reservasMesActual[i];
          contadorReservas++;
          //recorrer las horas reservadas de cada reserva
          for (int j = 8;j<21;j++)
          {
            //por cada hora reservada, se busca su hueco correspondiente en el array
            for (int k = 0;k<12;k++)
              {
                if (reservasMesActual[i].HorasReservadas[k]==j)
                  {
                    //si las horas estan ocupadas ponemos un 1
                    horasReservadasDiaConsulta[contadorAux]=1;

                  }

              }
            contadorAux++;
          }
        }
      }
    }
    //MES MAS UNO
    if (reservasMesMasUno[i].Activa)
    {//comprobar si el mes es el adecuado
    if (reservasMesMasUno[i].Fecha.mes==month)
      {
        if (reservasMesMasUno[i].Fecha.dia==day)
        {
          contadorAux=0;
          reservasAimprimir[contadorReservas]=reservasMesMasUno[i];
          contadorReservas++;
          //recorrer las horas reservadas de cada reserva
          for (int j = 8;j<21;j++)
          {
            //por cada hora reservada, se busca su hueco correspondiente en el array
            for (int k = 0;k<12;k++)
              {
                if (reservasMesMasUno[i].HorasReservadas[k]==j)
                  {
                    //si las horas estan ocupadas ponemos un 1
                    horasReservadasDiaConsulta[contadorAux]=1;

                  }

              }
            contadorAux++;
          }
        }
      }
    }
    //MES MAS DOS
    if (reservasMesMasDos[i].Activa)
    {//comprobar si el mes es el adecuado
    if (reservasMesMasDos[i].Fecha.mes==month)
      {
        if (reservasMesMasDos[i].Fecha.dia==day)
        {
          contadorAux=0;
          reservasAimprimir[contadorReservas]=reservasMesMasDos[i];
          contadorReservas++;
          //recorrer las horas reservadas de cada reserva
          for (int j = 8;j<21;j++)
          {
            //por cada hora reservada, se busca su hueco correspondiente en el array
            for (int k = 0;k<12;k++)
              {
                if (reservasMesMasDos[i].HorasReservadas[k]==j)
                  {
                    //si las horas estan ocupadas ponemos un 1
                    horasReservadasDiaConsulta[contadorAux]=1;

                  }

              }
            contadorAux++;
          }
        }
      }
    }
    //MES MAS TRES
    if (reservasMesMasTres[i].Activa)
    {//comprobar si el mes es el adecuado
    if (reservasMesMasTres[i].Fecha.mes==month)
      {
        if (reservasMesMasTres[i].Fecha.dia==day)
        {
          contadorAux=0;
          reservasAimprimir[contadorReservas]=reservasMesMasTres[i];
          contadorReservas++;
          //recorrer las horas reservadas de cada reserva
          for (int j = 8;j<21;j++)
          {
            //por cada hora reservada, se busca su hueco correspondiente en el array
            for (int k = 0;k<12;k++)
              {
                if (reservasMesMasTres[i].HorasReservadas[k]==j)
                  {
                    //si las horas estan ocupadas ponemos un 1
                    horasReservadasDiaConsulta[contadorAux]=1;

                  }
              }
            contadorAux++;
          }
        }
      }
    }
  }

  for(int i=0; i<12;i++)
  {
     if (horasReservadasDiaConsulta[i]==1)
     {
       SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
       printf("RR");
       SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
     }else
     {
       SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),160);
       printf("  ");
       SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
     }
  }

  printf("I\n");
  printf("I-------------I--------------------------I\n");

  for(int i=0 ;i<=contadorReservas-1;i++)
  {
    reservasAimprimir[i].imprimirReserva();
  }
  SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),128);
  printf("\n\n====================================GESTION TERMINADA=====================================\n\n");
  SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
  printf("Pulse ENTER para continuar...");
  getchar();
  getchar();
}

//Imprime el dia y susreservas con parametros
void GestionReservaSala::imprimirReservasDiaPorFecha(int day, int month, int year)
{

  int contadorAux;
  bool espacioVacio;
  TipoHorasReservadas horasReservadasDiaConsulta;
  Reservas_acumuladas reservasAimprimir;
  int contadorReservas;
  contadorReservas=0;
  contadorAux=8;
  //inicializar vector trabajo horas
  for(int i=0;i<12;i++)
  {
    horasReservadasDiaConsulta[i]=0;
  }
  if(!comprobarFecha(day,month,year))
  {
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
    printf("\nLa fecha que ha introduciodo no es valida para el sistema\n");
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
  }
  printf("\n\tReservas del dia: %d/%d/%d\n\n", day, month,year);
  printf("I-------------I--------------------------I\n");
  printf("I Horas:      I08  10  12  14  16  18  20I\n");
  printf("I-------------I--------------------------I\n");
  printf("I Horas:      I  ");
  //rescorrer todas las reservas
  for(int i = 0; i<20; i++)
  {//Comprobar si esta la resserva activa
    if (reservasMesActual[i].Activa)
    {//comprobar si el mes es el adecuado
    if (reservasMesActual[i].Fecha.mes==month)
      {
        if (reservasMesActual[i].Fecha.dia==day)
        {
          contadorAux=0;
          reservasAimprimir[contadorReservas]=reservasMesActual[i];
          contadorReservas++;
          //recorrer las horas reservadas de cada reserva
          for (int j = 8;j<21;j++)
          {
            //por cada hora reservada, se busca su hueco correspondiente en el array
            for (int k = 0;k<12;k++)
              {
                if (reservasMesActual[i].HorasReservadas[k]==j)
                  {
                    //si las horas estan ocupadas ponemos un 1
                    horasReservadasDiaConsulta[contadorAux]=1;

                  }

              }
            contadorAux++;
          }
        }
      }
    }
    //MES MAS UNO
    if (reservasMesMasUno[i].Activa)
    {//comprobar si el mes es el adecuado
    if (reservasMesMasUno[i].Fecha.mes==month)
      {
        if (reservasMesMasUno[i].Fecha.dia==day)
        {
          contadorAux=0;
          reservasAimprimir[contadorReservas]=reservasMesMasUno[i];
          contadorReservas++;
          //recorrer las horas reservadas de cada reserva
          for (int j = 8;j<21;j++)
          {
            //por cada hora reservada, se busca su hueco correspondiente en el array
            for (int k = 0;k<12;k++)
              {
                if (reservasMesMasUno[i].HorasReservadas[k]==j)
                  {
                    //si las horas estan ocupadas ponemos un 1
                    horasReservadasDiaConsulta[contadorAux]=1;

                  }

              }
            contadorAux++;
          }
        }
      }
    }
    //MES MAS DOS
    if (reservasMesMasDos[i].Activa)
    {//comprobar si el mes es el adecuado
    if (reservasMesMasDos[i].Fecha.mes==month)
      {
        if (reservasMesMasDos[i].Fecha.dia==day)
        {
          contadorAux=0;
          reservasAimprimir[contadorReservas]=reservasMesMasDos[i];
          contadorReservas++;
          //recorrer las horas reservadas de cada reserva
          for (int j = 8;j<21;j++)
          {
            //por cada hora reservada, se busca su hueco correspondiente en el array
            for (int k = 0;k<12;k++)
              {
                if (reservasMesMasDos[i].HorasReservadas[k]==j)
                  {
                    //si las horas estan ocupadas ponemos un 1
                    horasReservadasDiaConsulta[contadorAux]=1;

                  }

              }
            contadorAux++;
          }
        }
      }
    }
    //MES MAS TRES
    if (reservasMesMasTres[i].Activa)
    {//comprobar si el mes es el adecuado
    if (reservasMesMasTres[i].Fecha.mes==month)
      {
        if (reservasMesMasTres[i].Fecha.dia==day)
        {
          contadorAux=0;
          reservasAimprimir[contadorReservas]=reservasMesMasTres[i];
          contadorReservas++;
          //recorrer las horas reservadas de cada reserva
          for (int j = 8;j<21;j++)
          {
            //por cada hora reservada, se busca su hueco correspondiente en el array
            for (int k = 0;k<12;k++)
              {
                if (reservasMesMasTres[i].HorasReservadas[k]==j)
                  {
                    //si las horas estan ocupadas ponemos un 1
                    horasReservadasDiaConsulta[contadorAux]=1;

                  }
              }
            contadorAux++;
          }
        }
      }
    }
  }

  for(int i=0; i<12;i++)
  {
     if (horasReservadasDiaConsulta[i]==1)
     {
       SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
       printf("RR");
       SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
     }else
     {
       SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),160);
       printf("  ");
       SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
     }
  }

  printf("I\n");
  printf("I-------------I--------------------------I\n");

  for(int i=0 ;i<=contadorReservas-1;i++)
  {
    reservasAimprimir[i].imprimirReserva();
  }
}

//Comprueba si alguna de las horas estan ya reservadas
bool GestionReservaSala::comprobarHorasReservadas(TipoMes m, int d, int stTime, int durTime)
{
  bool coincidenciaHoras;
  TipoHorasReservadas horasRes;
  coincidenciaHoras=false;
  for (int g =0 ; g<12; g++)
  {
    horasRes[g]=0;
  }
  for (int t = 0; t<=durTime-1;t++)
  {
    horasRes[t]=stTime+t;
  }
  //Comprobar coincidencia de horas en las reservas
  //comprobar el mes
  if (MesActualTipo==m)
  {
    //recorrer vector de reservas j=indice reserva
    for (int j = 0 ; j < 21 ; j++)
    {
      //si la fecha de la reserva coincide con el dia
      if(reservasMesActual[j].Fecha.dia==d)
      {
        //si la reserva esta activa recorremos el vector de horas reservadas
        if (reservasMesActual[j].Activa)
              {
                //recorrer vector de horas reservadas de la reserva con indice j, i=indice de horas de reserva j
                for(int i = 0 ; i < 12; i++)
                  {
                    //recorrer vector de horas de la reserva a comprobar k = indice de vector a comprobar
                    for(int k= 0; k<12; k++)
                    {
                      //comprobar si coinciden las horas, y que seran distintas de 0, ya que se inizializan a 0
                        if (reservasMesActual[j].HorasReservadas[i]==horasRes[k]&&reservasMesActual[j].HorasReservadas[i]!=0)
                        {
                          coincidenciaHoras=true;
                        }
                      }
                  }
              }
        }
    }

  }else if (MesMasUnoTipo==m)
  {
    for (int j = 0 ; j < 21 ; j++)
    {
      if(reservasMesMasUno[j].Fecha.dia==d)
      {
        if (reservasMesMasUno[j].Activa)
        {
          for(int i = 0 ; i < 12; i++)
            {
              for(int k= 0; k<12; k++)
              {
                if (reservasMesMasUno[j].HorasReservadas[i]==horasRes[k] && reservasMesMasUno[j].HorasReservadas[i]!=0)
                {
                  coincidenciaHoras=true;
                }
              }
            }
        }
      }
    }

  }else if (MesMasDosTipo==m)
  {
    for (int j = 0 ; j < 21 ; j++)
    {
      if(reservasMesMasDos[j].Fecha.dia==d)
      {
        if(reservasMesMasDos[j].Activa)
        {
          for(int i = 0 ; i < 12; i++)
            {
              for(int k= 0; k<12; k++)
              {
                if (reservasMesMasDos[j].HorasReservadas[i]==horasRes[k] && reservasMesMasDos[j].HorasReservadas[i]!=0 )
                {
                  coincidenciaHoras=true;
                }
              }
            }
        }
      }
    }

  }
  else if (MesMasTresTipo==m)
  {
    for (int j = 0 ; j < 21 ; j++)
    {
      if(reservasMesMasTres[j].Fecha.dia==d)
      {
        if(reservasMesMasTres[j].Activa)
        {
          for(int i = 0 ; i < 12; i++)
            {
              for(int k= 0; k<12; k++)
              {
                if (reservasMesMasTres[j].HorasReservadas[i]==horasRes[k] && reservasMesMasTres[j].HorasReservadas[i]!=0)
                {
                  coincidenciaHoras=true;
                }
              }
            }
        }
      }
    }

  }
  return coincidenciaHoras;
}

//Lee los datos para realizar una nueva reserva
//Y los valida, repitiendo la introduccion si estos no son v�lidos
int GestionReservaSala::LeerDatosReserva()
{
  TipoReserva reservaReturn;
  TipoFecha date;
  TipoNombre name1;
  TipoNombre name2;
  int day;
  int month;
  int year;
  int startTime;
  int durationTime;
  bool fechaValida;
  bool horasReservaValida;
  bool reservaCreada;
  SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),151);
  printf("\nNueva Reserva:");
  SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
  printf("\n\tPersona que reserva(Maximo 20 caracteres)?");
  scanf("%s %s",& name1,&name2 );
  strcat(name1," ");
  strcat(name1,name2);
  //leer Nombre
  if (strlen(name1)>20)
      {
        do
        {
          SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
          printf("\nEl nombre que ha introducido es demasiado largo, vuelva a probar con uno que sea inferior a 20 caracteres.");
          SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
          printf("\n\tPersona que reserva(Maximo 20 caracteres)?");
          scanf("%s %s",& name1,&name2 );
          strcat(name1," ");
          strcat(name1,name2);
        }while(strlen(name1)>20);
      }
  //leer fecha
  do
    {
      fechaValida=true;
      printf("\n** Si dedesa cancelar la gestion teclee los valores a 0 (cero). **\n");
      printf("\tDia?");
      scanf("%d",& day);
      printf("\tMes?");
      scanf("%d",& month);
      printf("\tAnio?");
      scanf("%d",& year);
      if(day==0&&month==0&&year==0)
      {
        printf("\nGesti�n Cancelada\n");
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),128);
        printf("\n\n====================================GESTION TERMINADA=====================================\n\n");
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
        printf("Pulse ENTER para continuar...");
        getchar();
        getchar();
        return 0;
      }
      if(!comprobarFecha(day,month,year))
      {
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
        printf("\nLa fecha que ha introduciodo no es valida para el sistema. Vuelve a probar!\n\n");
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
        fechaValida=false;
      }

      //fechaValida=comprobarFecha(day,month,year);
    }
    while(!fechaValida);
  //horas reserva
  do
  {
      horasReservaValida=false;
      printf("\n** Si dedesa cancelar la gestion teclee los valores a 0 (cero). **\n");
      printf("\tHora de comienzo (Hora en punto de 8 a 19)?");
      scanf("%d",& startTime);
      printf("\tDuracion(Horas completas)?");
      scanf("%d",& durationTime);
      if(startTime==0&&durationTime==0)
      {
        printf("\nGesti�n Cancelada\n");
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),128);
        printf("\n\n====================================GESTION TERMINADA=====================================\n\n");
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
        printf("Pulse ENTER para continuar...");
        getchar();
        getchar();
        return 0;
      }
      if(startTime<8 || startTime>20)
      {
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
        printf("\nLa hora tiene que estar comprendida entre las 8 y las 19. Vuelve a probar!\n");
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
        horasReservaValida=true;
      }
      if (durationTime<=0 || durationTime>12)
      {
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
        printf("\nPueden reservarse de 1 a 8 Horas! vuelva a introducir las horas. Vuelve a probar!\n");
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
        horasReservaValida=true;
      }else if((startTime+durationTime)>20)
      {
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
        printf("\nNo se pueden reservar horas despues de las 19 Horas!. Vuelve a probar!\n");
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
        horasReservaValida=true;
      }

      if(day==fechaHora(0)&&month==fechaHora(1)&&fechaHora(3)>startTime)
      {
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
        printf("\nNo se pueden reservar horas pasadas, \nLos viajes en el tiempo aun no se han inventado!\n");
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
        horasReservaValida=true;
      }
      if(comprobarHorasReservadas(TipoMes(month-1),day,startTime,durationTime))
      {
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
        printf("\nLas Horas elegidas Ya estan reservadas. Comprueba los horarios en el siguiente grafico!\n");
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
        imprimirReservasDiaPorFecha(day,month,year);
        horasReservaValida=true;
      }
  }
  while(horasReservaValida);
  date.crearFecha(day,month,year);
  reservaCreada=false;
//comprobar que mes meter la reserva
  if(date.mesTipo==MesActualTipo)
  {
    //comprobamos que no esten ya las 20 reservas
    if (contadorMesActual<=20)
    {
      //recorremos bucle de reservas del mes i= indice de reservas del mes
      for (int i = 0; i<=20; i++)
      {
        //busca una reserva vacia en la lista de reservas
        if (!reservaCreada)
        {
          if(!reservasMesActual[i].Activa)
          {
            reservaReturn.CrearReserva(date,name1,startTime,durationTime);
            reservaReturn.Indice=i;
            reservasMesActual[i]=reservaReturn;
            contadorMesActual=contadorMesActual+1;
            reservaCreada=true;
            SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),160);
            printf("\nReserva creada Con exito!!!!\n");
            SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
            imprimirReservasDiaPorFecha(day,month,year);
            SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),128);
            printf("\n\n====================================GESTION TERMINADA=====================================\n\n");
            SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
            printf("Pulse ENTER para continuar...");
            getchar();
            getchar();
          }
        }
      }
    }
    else
    {
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
        printf("\nLas Reservas de este mes est�n completas!!. Vuelve a probar!\n");
        SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
    }
  }
  else if(date.mesTipo==MesMasUnoTipo)
  {
    //comprobamos que no esten ya las 20 reservas
    if (contadorMesMasUno<20)
    {
      for (int i = 0; i<21; i++)
      {
        //busca una reserva vacia en la lista de reservas
        if (!reservaCreada)
        {
          //busca una reserva vacia en la lista de reservas
          if(!reservasMesMasUno[i].Activa)
          {
            reservaReturn.CrearReserva(date,name1,startTime,durationTime);
            reservaReturn.Indice=i;
            reservasMesMasUno[i]=reservaReturn;
            contadorMesMasUno=contadorMesMasUno+1;
            reservaCreada=true;
            SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),160);
            printf("\nLas Reserva creada con Exito!!\n");
            SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
            imprimirReservasDiaPorFecha(day,month,year);
            SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),128);
            printf("\n\n====================================GESTION TERMINADA=====================================\n\n");
            SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
            printf("Pulse ENTER para continuar...");
            getchar();
            getchar();
          }
        }
      }
    }
    else
    {
      SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
      printf("\nLas Reservas de este mes est�n completas!!. Vuelve a probar!\n");
      SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
    }
  }
  else if(date.mesTipo==MesMasDosTipo)
  {
    //comprobamos que no esten ya las 20 reservas
    if (contadorMesMasDos<=20)
    {
      for (int i = 0; i<=20; i++)
      {
        //busca una reserva vacia en la lista de reservas
        if (!reservaCreada)
        {
          //busca una reserva vacia en la lista de reservas
          if(!reservasMesMasDos[i].Activa)
          {
            reservaReturn.CrearReserva(date,name1,startTime,durationTime);
            reservaReturn.Indice=i;
            reservasMesMasDos[i]=reservaReturn;
            contadorMesMasDos=contadorMesMasDos+1;
            reservaCreada=true;
            SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),160);
            printf("\nLas Reserva creada con Exito!!\n");
            SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
            imprimirReservasDiaPorFecha(day,month,year);
            SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),128);
            printf("\n\n====================================GESTION TERMINADA=====================================\n\n");
            SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
            printf("Pulse ENTER para continuar...");
            getchar();
            getchar();
          }
        }
      }
    }
    else
    {
      SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
      printf("\nLas Reservas de este mes est�n completas!!. Vuelve a probar!\n");
      SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
    }
  }
  else if(date.mesTipo==MesMasTresTipo)
  {
    //comprobamos que no esten ya las 20 reservas
    if (contadorMesMasTres<20)
    {
      for (int i = 0; i<=20; i++)
      {
        //busca una reserva vacia en la lista de reservas
        if (!reservaCreada)
        {
          //busca una reserva vacia en la lista de reservas
          if(!reservasMesMasTres[i].Activa)
          {
            reservaReturn.CrearReserva(date,name1,startTime,durationTime);
            reservaReturn.Indice=i;
            reservasMesMasTres[i]=reservaReturn;
            contadorMesMasTres=contadorMesMasTres+1;
            reservaCreada=true;
            SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),160);
            printf("\nLas Reserva creada con Exito!!\n");
            SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
            imprimirReservasDiaPorFecha(day,month,year);
            SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),128);
            printf("\n\n====================================GESTION TERMINADA=====================================\n\n");
            SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
            printf("\nPulse ENTER para continuar...");
            getchar();
            getchar();
          }
        }
      }
    }
    else
    {
      SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
      printf("\nLas Reservas de este mes est�n completas!!. Vuelve a probar!\n");
      SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
      printf("\nPulse ENTER para continuar...");
            getchar();
            getchar();
    }
  }

}

//Crear una Nueva reserva
bool GestionReservaSala::NuevaReserva()
{
  LeerDatosReserva();
}

//Lee y valida los datos para anular una reserva existente, pide confirmaci�n antes de proceder a la anulacion
void GestionReservaSala::AnularReserva()
{
  bool registroBorrado;
  bool registroEncontrado;
  TipoNombre name1;
  TipoNombre name2;
  int day;
  int month;
  int year;
  int option;
  int indiceAborrar;
  int startTime;
  registroBorrado=false;
  SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),151);
  printf("Anular Reserva:\n");
  SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
  printf("\tPersona que reservo?");
  scanf("%s %s",& name1,&name2 );
  strcat(name1," ");
  strcat(name1,name2);
  printf("\tDia?");
  scanf("%d",& day);
  printf("\tMes?");
  scanf("%d",& month);
  printf("\tA�o?");
  scanf("%d",& year);
  printf("\tHora de Comienzo?");
  scanf("%d",& startTime);
  registroEncontrado=false;
  //Recorrer las reservas
  for(int i = 0; i<=20;i++)
  {//Comprobar si coincide el nombre
    if((strcmp(name1,reservasMesActual[i].Nombre))==0)
    {//comprobar si coincide el dia
      if(day==reservasMesActual[i].Fecha.dia)
      {//comprobar si coincide el mes
        if(month==reservasMesActual[i].Fecha.mes)
        {//comprobar si coincide el a�o
          if(year==reservasMesActual[i].Fecha.anno)
          {//comprobar si coincide la hora de inicio
            if(startTime==reservasMesActual[i].HoraInicio)
            {//si todo se ha cumplido, la reserva coincide, preguntar si se desea borrar
               printf("\nSeguro que desea anular su reserva? (1.si/2.no)");
              scanf("%d",&option);
              if (option==1)
              {//borrar registro de la reserva(poner a 0)
                BorrarRegistro(reservasMesActual[i]);
                contadorMesActual--;
                registroBorrado=true;
                SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),160);
                printf("\nRegistro suprimido del sistema\n");
                SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
                SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),128);
                printf("\n\n====================================GESTION TERMINADA=====================================\n\n");
                SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
                printf("Pulse ENTER para continuar...");
                getchar();
                getchar();
              }
            }

          }
        }
      }
    }
    if ((strcmp(name1,reservasMesMasUno[i].Nombre))==0)
    {
      if(day==reservasMesMasUno[i].Fecha.dia)
      {
        if(month==reservasMesMasUno[i].Fecha.mes)
        {
          if(year==reservasMesMasUno[i].Fecha.anno)
          {
            if(startTime==reservasMesMasUno[i].HoraInicio)
            {
               printf("\nSeguro que desea anular su reserva? (1.si/2.no)");
               scanf("%d",&option);


              if (option==1)
              {
                BorrarRegistro(reservasMesMasUno[i]);
                contadorMesMasUno--;
                registroBorrado=true;
                SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),160);
                printf("\nRegistro suprimido del sistema\n");
                SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
                SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),128);
                printf("\n\n====================================GESTION TERMINADA=====================================\n\n");
                SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
                printf("Pulse ENTER para continuar...");
                getchar();
                getchar();
              }
            }

          }
        }
      }
    }
    if ((strcmp(name1,reservasMesMasDos[i].Nombre))==0)
    {
      if(day==reservasMesMasDos[i].Fecha.dia)
      {
        if(month==reservasMesMasDos[i].Fecha.mes)
        {
          if(year==reservasMesMasDos[i].Fecha.anno)
          {
            if(startTime==reservasMesMasDos[i].HoraInicio)
            {
               printf("\nSeguro que desea anular su reserva? (1.si/2.no)");
               scanf("%d",&option);

              if (option==1)
              {
                BorrarRegistro(reservasMesMasDos[i]);
                contadorMesMasDos--;
                registroBorrado=true;
                SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),160);
                printf("\nRegistro suprimido del sistema\n");
                SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
                SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),128);
                printf("\n\n====================================GESTION TERMINADA=====================================\n\n");
                SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
                printf("Pulse ENTER para continuar...");
                getchar();
                getchar();
              }
            }

          }
        }
      }
    }
    if ((strcmp(name1,reservasMesMasTres[i].Nombre))==0)
    {
      if(day==reservasMesMasTres[i].Fecha.dia)
      {
        if(month==reservasMesMasTres[i].Fecha.mes)
        {
          if(year==reservasMesMasTres[i].Fecha.anno)
          {
            if(startTime==reservasMesMasTres[i].HoraInicio)
            {
               printf("\nSeguro que desea anular su reserva? (1.si/2.no)");
               scanf("%d",&option);

              if (option==1)
              {
                BorrarRegistro(reservasMesMasTres[i]);
                contadorMesMasTres--;
                registroBorrado=true;
                SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),160);
                printf("\nRegistro suprimido del sistema\n");
                SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
                SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),128);
                printf("\n\n====================================GESTION TERMINADA=====================================\n\n");
                SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
                printf("Pulse ENTER para continuar...");
                getchar();
                getchar();
              }
            }

          }
        }
      }
    }

  }

  if (!registroBorrado)
  {
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
    printf("\nLa reserva que esta intentando cancelar, no existe en el sistema. Vuelve a probar!\n");
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
    printf("Pulse ENTER para continuar...");
    getchar();
    getchar();
  }
}

//Se utiliza para anular un registro de una reserva
void GestionReservaSala::BorrarRegistro(TipoReserva &r)
{
  r.Activa=false;
  r.Duracion=0;
  r.Fecha.dia=0;
  r.Fecha.anno=0;
  r.Fecha.mes=0;
  r.HoraInicio=0;
  for(int i = 0; i<12;i++)
  {
    r.HorasReservadas[i]=0;
  }
  strcpy(r.Nombre,"");
}
void limpiarPantalla()
{
  for(int i=0;i<25;i++)
  {
    printf("\n\n");
  }
}

void GestionReservaSala::PrintReservasMes()
{

  int day;
  int day2;
  int month;
  int year;
  bool diaCorrecto;
  diaCorrecto=true;
  day=1;//se introduce un dia siempre v�lido
  SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),151);
  printf("Reservas Mes:\n");
  SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
  printf("\tMes?");
  scanf("%d",& month);
  printf("\tA�o?");
  scanf("%d",& year);
  if(fechaHora(2)<=year&&year<=fechaHora(2)+1&&month>0&&month<13)
  {
      do
    {
      diaCorrecto=false;
      if(day<fechaHora(0)||day>diaFinal(month,year)||diaFecha(day,month,year)==5||diaFecha(day,month,year)==6)
      {
        diaCorrecto=true;
        day=day+1;
      }
    }
    while(diaCorrecto);
    if(comprobarFecha(day, month, year))
    {
      mostrarMesReservas(month,year);
      SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),128);
      printf("\n\n====================================GESTION TERMINADA=====================================\n\n");
      SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
      printf("Pulse ENTER para continuar...");
      getchar();
      getchar();
    }
    else
    {
      /*if(fechaHora(2)+1==(year)) DETECTADO ERROR DE COMPROBACION DE MESES SOLUCION CON COMPROBAR FECHA
      {
      mostrarMesReservas(month,year);
      printf("\n\n====================================GESTION TERMINADA=====================================\n\n");
      printf("Pulse ENTER para continuar...");
      getchar();
      getchar();
      }else{*/
      SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
      printf("\nERROR introduccion de datos");
      SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);

      printf("\nPulse ENTER para continuar...");
      getchar();
      getchar();
    }
  }else
  {
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
    printf("\nFecha Invalida. Vuelve a probar!");
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
    printf("\nPulse ENTER para continuar...");
    getchar();
    getchar();
  }

}

//devuelve true si hay datos en un dia, o false si no los hay
bool GestionReservaSala::comprobarReservasMesDiaDatos(int _dia, int _mes)
{
  int contadorHoras;
  bool datos;
  contadorHoras=0;
  datos = false;
  for(int i=0; i<20;i++)
  {
   if(reservasMesActual[i].Activa)
    {
      if (reservasMesActual[i].Fecha.mes==_mes)
      {
        if (reservasMesActual[i].Fecha.dia==_dia)
        {
          for(int j = 0;j<12;j++)
          {
            if(reservasMesActual[i].HorasReservadas[j]!=0)
            {
              datos=true;
            }
          }
        }
      }
    }
    if(reservasMesMasUno[i].Activa)
    {
      if (reservasMesMasUno[i].Fecha.mes==_mes)
      {
        if (reservasMesMasUno[i].Fecha.dia==_dia)
        {
          for(int j = 0;j<12;j++)
          {
            if(reservasMesMasUno[i].HorasReservadas[j]!=0)
            {
              datos=true;
            }
          }
        }
      }
    }
    if(reservasMesMasDos[i].Activa)
    {
      if (reservasMesMasDos[i].Fecha.mes==_mes)
      {
        if (reservasMesMasDos[i].Fecha.dia==_dia)
        {
          for(int j = 0;j<12;j++)
          {
            if(reservasMesMasDos[i].HorasReservadas[j]!=0)
            {
              datos=true;
            }
          }
        }
      }
    }
    if(reservasMesMasTres[i].Activa)
    {
      if (reservasMesMasTres[i].Fecha.mes==_mes)
      {
        if (reservasMesMasTres[i].Fecha.dia==_dia)
        {
          for(int j = 0;j<12;j++)
          {
            if(reservasMesMasTres[i].HorasReservadas[j]!=0)
            {
              datos=true;
            }
          }
        }
      }
    }
  }
 return datos;
}



//Funcion que improme PA si las reservas del dia indicado son parciales
//TO si son totales
//o simplemente el dia si no hay
void GestionReservaSala::comprobarReservasMesDia(int _dia, int _mes)
{
  int contadorHoras;
  contadorHoras=0;
  for(int i=0; i<20;i++)
  {
   if(reservasMesActual[i].Activa)
    {
      if (reservasMesActual[i].Fecha.mes==_mes)
      {
        if (reservasMesActual[i].Fecha.dia==_dia)
        {
          for(int j = 0;j<12;j++)
          {
            if(reservasMesActual[i].HorasReservadas[j]!=0)
            {
              contadorHoras++;
            }
          }
        }
      }
    }
    if(reservasMesMasUno[i].Activa)
    {
      if (reservasMesMasUno[i].Fecha.mes==_mes)
      {
        if (reservasMesMasUno[i].Fecha.dia==_dia)
        {
          for(int j = 0;j<12;j++)
          {
            if(reservasMesMasUno[i].HorasReservadas[j]!=0)
            {
              contadorHoras++;
            }
          }
        }
      }
    }
    if(reservasMesMasDos[i].Activa)
    {
      if (reservasMesMasDos[i].Fecha.mes==_mes)
      {
        if (reservasMesMasDos[i].Fecha.dia==_dia)
        {
          for(int j = 0;j<12;j++)
          {
            if(reservasMesMasDos[i].HorasReservadas[j]!=0)
            {
              contadorHoras++;
            }
          }
        }
      }
    }
    if(reservasMesMasTres[i].Activa)
    {
      if (reservasMesMasTres[i].Fecha.mes==_mes)
      {
        if (reservasMesMasTres[i].Fecha.dia==_dia)
        {
          for(int j = 0;j<12;j++)
          {
            if(reservasMesMasTres[i].HorasReservadas[j]!=0)
            {
              contadorHoras++;
            }
          }
        }
      }
    }
  }
  if(contadorHoras==0)
  {
    printf("%d",_dia);
  }else if(contadorHoras>0 && contadorHoras<12)
  {
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),224);
    printf("PA");
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
  }else if(contadorHoras==12)
  {
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
    printf("TO");
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
  }else
  {
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),199);
    printf("ER");
    SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
  }

}

void GestionReservaSala::mostrarMesReservas(int mes, int agno)
{
  int diaSem = diaFecha(1,mes, agno);
  int ultimoDia = diaFinal(mes, agno);
  int contadorDia=0;
  int contadorfinal = 6-diaFecha(ultimoDia,mes, agno);

  printf("\n");
  imprimirMes(mes, agno);
  printf("===========================\n");
  printf("LU  MA  MI  JU  VI | SA  DO\n");
  printf("===========================\n");
  for(int x = 0; x < diaSem; x++) //Imprime la tabulacion de los dias vacios inciales del Mes
    {
        if (x==0)
        {
          printf(" . ");
        }
        else if (x==4)
        {
          printf("  . |");
        }
        else if (x==5)
        {
          printf("  . ");
        }
        else
        {
          printf("  . ");
        }
      contadorDia++;

    }

  for(int x = 1; x <= ultimoDia; x++)
        {
            if(contadorDia==4)
            {
                  if (x<=9&&!comprobarReservasMesDiaDatos(x,mes))
                  {
                    printf(" ");
                  }
                  printf(" ");
                  comprobarReservasMesDia(x,mes);
                  printf(" |");
                  diaSem++;
                  contadorDia++;
            }else if (contadorDia==0)
            {
              if (x<=9&&!comprobarReservasMesDiaDatos(x,mes))
                {
                  printf(" ");
                }
                comprobarReservasMesDia(x,mes);
                printf(" ");
                diaSem++;
                contadorDia++;
                if( !( diaSem % 7 ) )//si el modulo de diaSem despues de incrementar es 0 inicia en una nuea linea
                  {
                    printf("\n");
                    contadorDia=0;
                  }
            }
            else{
                if (x<=9&&!comprobarReservasMesDiaDatos(x,mes))
                {
                  printf(" ");
                }
                printf(" ");
                comprobarReservasMesDia(x,mes);
                printf(" ");
                diaSem++;
                contadorDia++;
                if( !( diaSem % 7 ) )//si el modulo de diaSem despues de incrementar es 0 inicia en una nuea linea
                  {
                    printf("\n");
                    contadorDia=0;
                  }
            }
        }
        contadorDia=diaFecha(ultimoDia,mes, agno)+1;
        for (int x=0;x<contadorfinal;x++)
        {

                  if (contadorDia==0)
                  {
                   printf(" . ");
                  }else if (contadorDia==4)
                  {
                    printf("  . |");
                  }
                  else if (contadorDia==5)
                  {
                    printf("  . ");
                  }else
                  {
                     printf("  . ");
                  }
                  contadorDia++;

        }

}
//Imprime por consola el menu principal de la aplicacion
char pintarMenuPrincipal()
{

  char opcion;
  system("color 17"); /*Color de la consola  fondo azul, letra blanca*/
  system("cls");
  fflush(stdin);
  printf("\n\t=========================\n");
    printf("\t#  GESTION DE RESERVAS  #\n");
    printf("\t=========================\n");
    printf("\t#     Menu Principal    #\n");
    printf("\t=========================\n\n");
  printf("Gestion de Reserva Sala\n\t");
  printf("Nueva Reserva\t\t(Pulsar N)\n\t");
  printf("Anular Reserva\t\t(Pulsar A)\n\t");
  printf("Reservas de un Dia\t(Pulsar D)\n\t");
  printf("Reservas de un Mes\t(Pulsar M)\n\t");
  printf("Salir\t\t\t(Pulsar S)\n");
  SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),151);
  printf("Teclear una opcion valida (N|A|D|M|S)?\n");
  SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),23);
  scanf("%c",&opcion);
  return opcion;
}
