/***************************************************************************
*Interfaz de M�dulo CalendarioMes
*
*   Este m�dulo controla la gesti�n de calendario
*
* CURSO:  2016/2017
*****************************************************************************/

#pragma once
#include"Reserva.h"
#include<time.h>


int MesActual();
TipoMes MesObjetivo(int mes);
int diaFinal(int mes, int agno);
bool agnoBisiesto(int anno);
bool comprobarFecha(int dia,int mes, int anno);
bool fechaMenosActual();
int diaFecha(int dia, int mes, int agno);
void mostrarMes(int mes,int agno);
void imprimirMes(int mes, int agno);
int fechaHora(int i);
