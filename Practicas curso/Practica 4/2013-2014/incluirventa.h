/*********************************************************************
* Interfaz de m�dulo: incluirventa
*
* Este m�dulo contiene los elementos para incluir un registro de venta
*
**********************************************************************/
#pragma once
/* ==========================================
    Constantes
   ========================================== */

const int maximo = 999; /* M�ximo de registros que puede tener el programa
                        teniendo en cuenta que empieza el contador en -1*/



/* ==========================================
     Tipos P�blicos
   ========================================== */

/* --Tipos escalares --*/

typedef int TipoProducto;

/* --Arrays --*/

typedef int TipoVector[5]; /* Para almacenar de forma temporal los valores de un solo registro*/
typedef int TipoVectorRegistro[maximo];/* Para almacenar de forma temporal los valores de todos los registros*/

/* -- Enumerado --*/

typedef enum TipoElemento{/*Elementos de un registro de ventas*/
  unidades, producto, dia, mes, anno
  };

/* --Estructuras --*/


typedef struct TipoFecha{/* Struct de la fecha */
  int dia;
  int mes;
  int anno;
};

typedef struct TipoVenta {/* Struct de un registro */
  TipoFecha fecha;
  TipoProducto producto;
  TipoProducto unidades;
};

/* --Arrays --*/

typedef TipoVenta TipoRegistro[maximo];/*Vector para almacenar registros.
                                         Cada parte es un struct con los elementos de un registro*/
typedef int TipoLista[maximo];/*Vector para almacenar registros.
                                         Cada parte escalar y solopuede guardar un elemento int*/

/* ==========================================
     Variables p�blicas
   ========================================== */

extern TipoRegistro codigoVenta; /* Vector con todos los registros de datos */
extern int numeroRegistro;/*Variable que cuenta el n�mero de registros guardado*/
extern int productoAnterior;/*Variable que guarda el �ltimo producto introducido aunque est� repetido*/

/* ==========================================
     Funciones y procedimientos p�blicas
   ========================================== */

void IntroVenta();/*Procedimiento con el submen� y la aplicaci�n de introducir venta*/


