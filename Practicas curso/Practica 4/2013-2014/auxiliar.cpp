/******************************************************************************
* M�dulo: auxiliar
*
* Este m�dulo contiene funciones y procedimientos auxiliares para un uso global
*
*******************************************************************************/

/* ====================================
    M�dulos y librer�as est�ndar usados
   ==================================== */

#include "auxiliar.h"
#include <stdio.h> /* Para prinf() y scanf() */
#include <stdlib.h> /* Para sytem() */
#include <conio.h> /* Para getch() y getche()*/
#include <ctype.h>/* Para isdigit()*/
#include <time.h>/* Para time_t, time() y strftime()*/
#include <string.h> /* Para strlen() */

/* ==========================================
     Variables p�blicas o globales en el m�dulo
   ========================================== */

time_t tiempo = time(NULL);/*Declaramos una variable con el tiempo del sistema en
                             formato time_t (segundos desde 01/01/1970*/
TipoArray nombreMeses = {  /*Declaramos la variable nombreMeses que es un array con los nombres de los meses*/
  " ","enero","febrero","marzo","abril","mayo","junio","julio","agosto","septiembre","octubre","noviembre","diciembre"
};

/* =======================================
    Procedimiento para cambiar
    de color textos aislados
  =======================================*/

void CambiarColor(int color) {
  /* Ponemos en un vector los valores de los colores para poder atribuirles un nombre que est�
  declarado como enum en la interfaz de este m�dulo (ver TipoColorConsola)*/
  TipoVectorColor colores = {
    20,26,27,31,47,55,112,113,114,115,116,121,121,124,159,176,185,207,224,240,249,252
  };

  SetConsoleTextAttribute(GetStdHandle (STD_OUTPUT_HANDLE),colores[TipoColorConsola(color)]);

  /*Establece los atributos de caracteres escritos en el buffer de pantalla
   color = Valor color Fondo * 16 + Valor color Texto
   Valores color: 0 = Negro, 1= Azul, 2 = Verde, 3 = Aguamarina, 4 = Rojo, 5 = Purpura, 6 = Amarillo,
   7 = Blanco, 8 = Gris, 9 = Azul claro A (10) = Verde claro B (11)= Aguamarina claro, C (12)= Rojo claro,
  D (13)= Purpura claro, E (14) = Amarillo claro, F (15) = Blanco brillante */
}
/* ==============================================
    Funcion para posicionar un texto en la pantalla
   ============================================== */

void Posicion(int x, int y) {
  /*Con esta funci�n podemos poner el cursor en cualquier parte de la pantalla utilizando la API de Windows*/
  HANDLE cursor;/*Declaramos variable hcon el tipo que significa tirador en espa�ol. Es una especie de puntero inteligente
                que permite que una referencia pueda ser reubicada en la memoria sin invalidarla
                */
  COORD posicion;/* Declaramos variable posicion. El tipo COORD es un tipo struct con dos variables: X e Y*/
  cursor = GetStdHandle(STD_OUTPUT_HANDLE);/*vinculamos la variable al buffer activo de pantalla*/
  posicion.X = x;/*Damos valor a x (posici�n horizontal)*/
  posicion.Y = y;/*Damos valor a y (posici�n vertical)*/
  SetConsoleCursorPosition(cursor,posicion);/*Selecionamos la posici�n del cursor en la pantalla*/
}
/* ==============================================
    Funciones que devuelven la posicion del cursor
   ============================================== */
/*-- Para calcular la posici�n horizontal --*/
int PosicionAct(TipoPosicion eje) {
  CONSOLE_SCREEN_BUFFER_INFO cursorActual;/*Variable tipo struct cuyas partes debuelven datos de
                                             informaci�n del buffer de pantalla*/
  /*Seleccionamos de la informaci�n del buffer de pantalla la posici�n del cursor*/
  GetConsoleScreenBufferInfo (GetStdHandle(STD_OUTPUT_HANDLE), &cursorActual);
  switch (eje) {
  case X:/* Si eje es X*/
    return cursorActual.dwCursorPosition.X; /*Devuelve la posici�n horizontal*/
    break;
  case Y:/* Si eje es Y*/
    return cursorActual.dwCursorPosition.Y;/*Devuelve la posici�n vertical*/
    break;
  }
}
/* ====================================
    Funci�n que determina si aparece
    unidad si es 1 o unidades si son m�s
   ==================================== */

TipoString Plural(int palabra) {
  if (palabra == 1) {
    return "unidad  "; /* Si el valor es 1 devuelve unidad */
  } else {
    return "unidades"; /* Si es mayor de 1 devuelve unidades */
  }
}

/* ====================================
    Funci�n que determina el numero
    de digitos de un numero
   ====================================
  Divide el n�mero entre 10 hasta que el valor es 0
  En caso de ser el 1.er argumento numDig un n�mero negativo y que el 2.� argumento (signo) se le pase un true
  cuenta en este caso uno m�s para compensar que el signo ocupa un caracter*/

int NumDigitos(int numDig, bool signo) {

  int contDig = 0;/*Declaramos variable contador*/
  if ((numDig < 0)&&(signo)) {
    contDig++;/*Para el byte de signo en los n�meros negativos*/
  }
  while (numDig != 0) {
    contDig++; /* Contamos el n�mero de divisiones que ha efectuado hasta que numDig es = 0 */
    numDig = numDig / 10;
  }

  return contDig;/*Devuelve el n�mero de d�gitos de la variable pasada comovalor*/
}

/* ====================================
    Funci�n que determina el numero
    de digitos que tiene el numero con m�s d�gitos
   ==================================== */

int MaxDigitos(const TipoVectorRegistro numMaxDig, int tamanno) {

  /* Esta funci�n utiliza la anterior que determina el n�mero de d�gitos y compara los resultados
     de las diferentes parts de un vector devolviendo el mayor n�mero de d�gitos
     Como argumentos se le pasan:
       1. Variable tipo vector int que contiene losdiferentes valores a comparar
       2. tama�o de ese vector para que no de problemas
  */

  int maxDig = NumDigitos(numMaxDig[0], true);/* Le damos un valor inicial a la variable maxDig*/
  for (int k = 1; k <= tamanno; k++) { /* Recorremos todo el vector comparando la variable maxDig con el elemente siguiente */
    if (NumDigitos(numMaxDig[k],true) > maxDig) { /* Si el elemento siguiente es mayor ...*/
      maxDig = NumDigitos(numMaxDig[k],true);/* ... la variable toma este nuevo valor*/
    }
  }
  return maxDig; /* Devueve un entero con el n�merom�ximo de d�gitos */
}

/* =======================================
   Procedimiento para usar en lugar de scanf que capta una orden de ESC
===========================================
Esta funci�n se puede emplear en lugar de scanf() y respecto a �sta se diferencia en que:
- Puede leer teclas como ESC y en ese caso sale del procedimiento y devuelve la variable esc con true.
- Se puede asignar un valor por defecto que se introducir� si se pusa intro y no se a�ade otro valor.
- En caso de pulsar una letra o signo devuelve un mensaje de error.
Argumentos:
1: Variable tipo int para el valor. Se pasa como referencia.
2: Valor que devolver� si se da a intro sin dar valor.
3: variable booledana con valor false que en caso de pulsar ESC cambiar� su valor a true. Se pasa como referencia
*/

void Teclado(TipoPuntInt numero,int valorDefecto, TipoPuntBool esc) {

  /* -- Declaramos las variables locales del procedimiento -- */

  TipoCadena cadenaCifra;/* Variable que almacenar� los valores delas teclas pulsadas como un string*/
  int contIndice;/* Variable que cuenta cada caracter introducido*/
  int contDigito;/* Variable que contar� los numeros del vector para revisar si todos son n�meros*/
  bool hayLetra = false; /* Variable cuyo valor cambiar� a true si encuentra un caracter no num�rico*/
  char caracter;/* Variable que contendr� de forma temporal el caracter pulsado queluego pasar� al vector*/
  int posX = PosicionAct(X);
  int posY = PosicionAct(Y);
  /* -- Bloque que se repetir�mientras se detecte que se ha pulsado
  un caracter no num�rico antes del INTRO -- */

  do {
    contIndice = 0;/*Damos valor inicial a la variable del contador que marca el �ndice del vector*/
    contDigito = 0;/*Damos valor inicial a la variable del contador que contar� los
                    caracteres de la cadena hasta encontrar un caracter no num�rico*/
    while ((caracter != 13)  || (hayLetra)) {/*Se dejar�n de a�adir caracters al vector al pulsar la tecal INTRO*/
      hayLetra = false;/*Forzamos que tome el valor false la variable hayLetra */
      fflush(stdin); /* Limpiamos buffer*/
      caracter = getche();
      if (caracter == 27) { /* 27 es la tecla ESC*/
        *esc = true;/* Si se pulsa escape esc se convierte en true para dar la se�al de que se cierre en otros que lo usean*/
        return; /* Se cierra este subprograma */
      }
      /* -- Habilitamos la carga de caracteres con la posibiidad de borrar caracteres con la tecla retroceso --*/

      if ((caracter == 8) && (contIndice > 0)) { /*Si pulsamos a retrorceso el contador se descuenta y se borra un caracter*/
        if (contIndice != 0) { /* Nos aseguramos que el contador no va a bajar de 0 que es su valor inicial*/
          contIndice--; /* Se descuentan el caracter borrador*/
        }
        printf(" \b"); /* Borra caracter y retrocede */
      } else if ((caracter == 8) && (contIndice == 0)) {/*Cuando llega el contador a 0 ya no borra m�s caracteres si no borrar�a la pregunta*/
        printf("%c",char(32));  /* Esto hace que lo que retroceda lo avance*/
      } else if (caracter != 8) { /* Con el resto de caracteres se carga en el vector y avanza el contador que marca el �ndice*/
        cadenaCifra[contIndice] = caracter;/*En cada parte del vector se carga un caracter*/
        contIndice++; /*Contamos el caracter a�adido*/
      }
    }

    while ((isdigit(cadenaCifra[contDigito])) ||(cadenaCifra[contDigito] == '-')||(cadenaCifra[contDigito] == 32)) {
      contDigito++;/*Recorre el vector hasta encontrar un caracter que no es d�gito, el signo de menos o un espacio*/
    }
    contDigito++; /* Subimos uno el contDigito*/
    if (contIndice == 1) {
      *numero = valorDefecto;/*Si no se introduce ning�n valor la variable toma el valor por defecto*/
    } else if (contIndice == contDigito) {/*Si contIndice y contDigito tienen el mismo valor se entiende que no se ha detectado letra*/
      *numero = atoi(cadenaCifra); /*Trasnformamos la cadena de caracteres a entero*/
    } else { /*Si detecta un caracter no num�rico se muestra un error*/
      hayLetra = true;
      Posicion(1, posY - 1);/*Ponemos el cursor encima de la pregunta*/
      printf("%79c",char(32)); /* Borramos si habr�a otro mensaje de error en el mismo sitio*/
      Posicion(1,posY - 1); /* Volvemos a poner el cursor encima de la pregunta*/
      CambiarColor(Error); /* Cambiamos color dela consola a Fondo rojo con texto blanco brillante*/
      printf("\a Error: Caracter no num\202rico ");/*Imprimimos mensaje de error*/
      Posicion(posX, posY);/* Ponemos el cursor en la l�nea de la pregunta y al final de la misma*/
      CambiarColor(Submenu);/* Volvemos al color normal*/
      printf("%*c",80-posX,char(32));/* Borramos el valor que hab�e tras la pregunta*/
      Posicion(posX,posY);/* Volvemos a poner el cursor en la l�nea de la pregunta y al final de la misma*/
    }
  } while (hayLetra);/*Mientras detecta un caracter no num�rico*/

  /* -- Una vez salimos del bucle -- */

  Posicion(1,posY-1); /* Ponemos el cursor encima de la pregunta donde est� el mensaje de error*/
  printf("%79c\n",char(32)); /* Borramos el mensaje de error*/
}
/* ====================================
    Funci�n para buscar en el registro
   ====================================
   Esta funci�n consta de 4 argumentos que en caso de que no se quiera que se tenga en cuenta se
   debe pasar como NULL que equivaldr�a a pasarle una constante con valor 0 al ser un int y no un puntero
   si fuera un char le pasar�amos \0*/

bool Buscar(int buscarProducto,int buscarDia, int buscarMes, int buscarAnno) {
  bool encontrado = false;/*Variable que devolver� la funci�n con el valor inicial false
                            que cambiara a true si la busqueda es positiva*/

  for (int k = 0; k <= numeroRegistro; k++) {/*Recorremos todo el vector de registros
    Se comparan los valores dados en los argumentos con los del registro. Si se ha pasado en el
    argumento NULL la respuesta ser� positiva o true en esa parte*/
    if ( /* Parte 1 producto */
      ((codigoVenta[k].producto == buscarProducto) || (buscarProducto == NULL)) &&
      /* Parte 2 d�a */
      ((codigoVenta[k].fecha.dia == buscarDia) || (buscarDia == NULL)) &&
      /* Parte 3 mes */
      ((codigoVenta[k].fecha.mes == buscarMes) || (buscarMes == NULL)) &&
      /* Parte 4 a�o */
      ((codigoVenta[k].fecha.anno == buscarAnno) || (buscarAnno == NULL))) {
      encontrado = true; /*Si todas las partes dan positivo cambia el valor de la variable encontrado a true*/
    }
  }
  return encontrado;/*La funci�n devuelve la variable encontrado*/
}
/* ====================================
    Procedimiento de control
   ==================================== */

void Control(TipoElemento elemento,int &variable) {
  /* -- Declaramos las variables locales del procedimiento -- */

  bool esc = false;/* Declaramos variable para pasar al procedimiento teclado como referencia
                      con el valor inicial false*/
  bool existeProducto = false;
  bool error;
  int longCad;
  int x = 0;
  int y = PosicionAct(Y);
  /*Pregunta que se imprimir� dependiendo del elemento*/
  TipoArray pregunta = {"","Identificador de Producto ? ","D\241a (1..31)? ","Mes (1..12)? ","A\244o (1600..3000)? "};
  /*Sufijo de mensages de error para elementos tipo fecha (dia mes y a�o)*/
  TipoArray msgError = {"","",
                        "el d\241a tiene que ser ente 1 y 31",
                        "el mes tiene que ser entre 1 y 12",
                        "el a\244o tiene que ser entre 1600 y 3000"
                       };
  TipoCadena fecha;/* Variable para darle valor con el d�a mes o a�o actual como string */
  int valorDefecto;/* Variable para pasar a Teclado() como valor por defecto */
  TipoArray cod = {"","","%d","%m","%Y"};/*C�digos que hay que pasar a strftime() para que aparezca dia, mes
                                           o a�o en el formato que queremos*/

  if (elemento != producto) {/*Si el elemento no es producto el dato ser� de fecha y calculamos
                               el dato de fecha actual*/
    strftime(fecha, 80,cod[elemento], localtime(&tiempo) );/*Con esta funci�n se obtiene el dia mes o a�o actual
                                                  en forma de string*/
    valorDefecto = atoi(fecha);/*Transformamos string en tipo entero (int)*/
  } else {
    valorDefecto = productoAnterior;/*Si es producto el elemento el valor por defecto es el �ltimo introducido*/
  }
  longCad = strlen(pregunta[elemento]);/*Damos el valor de la longitud de la cadena para calcular
                                           posici�n de final de pregunta*/

  /* -- Bloque que se repetir� mientras el valor no este en rango permitido -- */

  do {

    /* 1.� Pedimos el valor para darle a variable */
    error=false;
    CambiarColor(PreguntaSubmenu);/* Mismo color de fondo pero texto azul claro*/
    Posicion(x,y);/*Posici�n de la pregunta*/
    printf("%s",pregunta[elemento]);/* Imprimimos pregunta */
    CambiarColor(Submenu);/* Volvemos a los colores anteriores */
    Teclado(&variable, valorDefecto, &esc);/*Escaneamos la respuesta*/
    if (esc) {/*Si pulsamos ESC volvemos al men�*/
      return;
    }

    /* 2.� Comprobamos que el valor de variable est� dentro del rango permitido */

    /*En caso de ser el elemento producto comprobamos que este exista*/
    if (elemento == producto) {

      for (int k = 0; k <= numeroRegistro; k++) {/*Recorremos todo el vector*/
        if ( variable == codigoVenta[k].producto) {
          /*Comprobamos que el producto existe comparando la variable con los registros del vector*/
          existeProducto = true; /* Si el producto a buscar es encontrado cambia el valor de
                              la variable a true*/
        }
      }
    }

    if ((((variable > 31) || (variable < 1)) && (elemento == dia))||/*Si elemento es d�a*/
        (((variable > 12) || (variable < 1)) && (elemento == mes))||/*Si elemento es mes*/
        (((variable > 3000) || (variable < 1600)) && (elemento == anno))||/*Si elemento es a�o*/
        ((!existeProducto) && elemento == producto)) {/*Si elemento es producto*/

      error = true; /* Si no se cumple algunas de esas condiciones (l�nea) error cambia su valos a true y: */

      Posicion(x+2,y-1);/*Posicionamos el cursor encima de la pregunta con un margen de 2*/
      printf("%80c\n",char(32));/*Borramos l�nea por si habr�a otro mensaje de error*/
      Posicion(x+longCad,y);/* Posicionamos el cursor al final de la pregunta pero en la misma l�nea */
      printf("%*c\n",80-longCad,char(32));/*Borramos valor dado*/
      CambiarColor(Error);/* Color fondo rojo claro texto blanco brilante*/
      Posicion(x+1,y-1);/* Posicionamos el cursor encima de la pregunta con un margen de 2 */
      if (elemento == producto) { /*Mensaje que se imprimir� si el elemento es producto*/
        printf("\a Aviso: El producto %d no est\240 registrado \n",variable);/*Imprimimos mensaje de aviso*/
      } else {/*Mensaje de error para elementos tipo fecha*/
        printf("\a Error: Has introducido %d y %s ",variable, msgError[elemento]);/*Imprimimos mensaje de error*/
      }
      CambiarColor(Submenu);/* Volvemos a los colores anteriores */
    }
  } while (error);

  /*-- Una vez no hay error y salimos del bucle --*/

  Posicion(x,y-1);/* Posicionamos el cursor donde el mensaje de error */
  printf("%80c\n",char(32));/* Borramos la l�nea del mensaje de error */
  Posicion(x+longCad,y);/* Posicionamos el cursor al final de la pregunta pero en la misma l�nea */
  printf("%d", variable);/* Imprimimos a continuaci�n de la pregunta*/
  if (elemento == mes) {/*Si el elemento es mes*/
    printf(" (%s)",nombreMeses[variable]); /*Imprimimos entre par�ntesis el nombre del mes*/
  }
}
/* ==============================================
    Procedimiento para crear una caja con las
    opciones posibles en los submen�s
   ============================================== */

void AyudaSubmenu(TipoElemento elemento) {
  TipoArray ayuda1 = {/*Vector que contiene el complemento de la primera l�nea*/
    "1 unidad","el \243ltimo producto introducido", "el d\241a actual", "el mes actual", "el a\244o actual"
  };
  TipoArray ayuda2 = {/*Vector que contiene el complemento de la segunda l�nea*/
    "cualquiera","cualquiera", "entre 1 y 31", "entre 1 y 12", "entre 1600 y 3000"
  };
  CambiarColor(Ayuda);
  Posicion(1,19);/*Ponemos un margen de un espacio con la misma posici�n vertical*/
  printf("%67c\n",char(32));/*Imprimimos parte superior de la caja*/
  Posicion(1,20);/*Ponemos un margen de un espacio con la misma posici�n vertical*/
  printf(" Presionando solo INTRO introducir\240 %s%*c\n",ayuda1[elemento],66-(35+strlen(ayuda1[elemento])),char(32));
  Posicion(1,21);/*Ponemos un margen de un espacio con la misma posici�n vertical*/
  printf(" Para otro valor presione un n\243mero %s e INTRO%*c\n",ayuda2[elemento],66-(43+strlen(ayuda2[elemento])),char(32));
  Posicion(1,22);/*Ponemos un margen de un espacio con la misma posici�n vertical*/
  printf(" Para volver al men\243 presione ESC%34c\n",char(32));
  Posicion(1,23);/*Ponemos un margen de un espacio con la misma posici�n vertical*/
  printf("%67c",char(32));/*Imprimimos parte inferior de la caja*/
}


