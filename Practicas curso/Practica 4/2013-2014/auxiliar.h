/*****************************************************************************************
* Interfaz de m�dulo:auxiliar
*
* Este m�dulo contiene las funciones auxiliares que tienen en com�n los diferentes m�dulos
*
******************************************************************************************/

#pragma once
#include <windows.h> /* Para SetConsoleTextAttribute(),GetStdHandle(), SetConsoleCursorPosition(),
                        CONSOLE_SCREEN_BUFFER_INFO, GetConsoleScreenBufferInfo()*/
#include "incluirventa.h"/* Para TipoVectorRegistro*/

/* ==========================================
     Tipos P�blicos
   ========================================== */
typedef char TipoCadena[100];/*Tipo para cadenas de texto de hasta 99 caractres*/
typedef char* TipoString;/*Tipo puntero para cadenas de texto. Se usa para las funciones que devuelvn un string */
typedef bool* TipoPuntBool;/* Puntero para pasar argumentos booledanos como referencia en procedimentos */
typedef int* TipoPuntInt;/* Puntero para pasar argumentos int como referencia en procedimentos */
typedef int TipoVectorColor[22];/*Vector para almacenar hasta 22 c�digos de colores*/

/* -- Tipos enumerados -- */

typedef enum TipoColorConsola{ /*Colores que utiliza CambiaColor()*/
  MenuInhabilitado, MenuSalir, OpcionMenu, CMenu, Hecho, PreguntaMenu, Aplicacion, Titulo,
  DatoIntroPositivo, DatoPositivo, DatoIntroNegativo, PreguntaAplicacion,
  Dato, DatoNegativo, Ayuda, Submenu, PreguntaSubmenu, Error, NoHecho, CCalendario,
  NombreMes, MarcaDia
};
typedef enum TipoPosicion{X,Y};/*Posicienes en eje de coordenadas*/
typedef char TipoArray[13][50]; /*Matriz para 12 cadenas de texto con hasta 49 caracteres cada una
                                Se utiliza para los nombres de meses y mensajs de texto */
/* ==========================================
     Variables p�blicas
   ========================================== */

extern TipoArray nombreMeses; /*Nombre de los 12 meses del a�o

/* ==========================================
     Funciones y procedimientos p�blicas
   ========================================== */

void CambiarColor(int color);/*Procedimiento para cambiar el color de un texto aislado*/
void Posicion(int x, int y);/* Procedimiento para situar el cursor en la pantalla*/
int PosicionAct(TipoPosicion eje);/*Funci�n que devuelve la posici�n actual del cursor*/
void Teclado(TipoPuntInt numero,int valorDefecto,TipoPuntBool esc);/*Procedimiento que escanea los valores dados
                        como scanf() pero adem�s lle las pulsaciones de todas las teclas como ESC y se le
                        puede pasar como referencia para cambiar el valor a true si se pulsa
                        y permite poner un valor por defecto si se pulsa a INTRO sin teclear nada*/
TipoString Plural(int palabra);/* Funci�n que devueve unidad o unidades dependiendo de si se le pasa 1
                        u otro n�mero como argumento*/
bool Buscar(int buscarProducto,int buscarDia, int buscarMes, int buscarAnno);/*Funci�n que busca un
                        registro teniendo en cuenta los elementos que se pasa como argumentos que no
                        sean NULL y devuelve true si lo encuentra*/
int NumDigitos(int numDig, bool signo);/*Funci�n que devuelve el n�mero de d�gitos de un n�mero*/
int MaxDigitos(const TipoVectorRegistro numMaxDig, int tamanno);/*Funci�n que devuelve el n�mero m�s alto
                        de d�gitos delos elementos de un vector*/
void Control(TipoElemento elemento,int &variable);/*Procedimiento que encapsula el bloque de pregunta
                        escaneo con  procedimiento Teclado() y aplica el control de rango seg�n sea
                        el elemento*/
void AyudaSubmenu(TipoElemento elemento);/*Procedimiento que crea una caja al final de submen� con las opciones*/


