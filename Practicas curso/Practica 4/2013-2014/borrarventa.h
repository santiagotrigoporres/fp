/*********************************************************************
* Interfaz de m�dulo: borrarventa
*
* Este m�dulo contiene los elementos para borrar un registro de ventas
*
**********************************************************************/

#pragma once
#include "auxiliar.h"

/* ===============================================
        Procedimientos p�blicos
   ===============================================*/

void BorrarElemento(int idProducto, int idDia, int idMes, int idAnno);/*Procedimiento de la aplicaci�n*/
void EliminarVenta();/* Procedimiento del submen�*/


