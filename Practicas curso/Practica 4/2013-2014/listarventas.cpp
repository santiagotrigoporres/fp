/**************************************************************************************************
* M�dulo: listarventas
*
* Este m�dulo contiene los elementos para listar las ventas de un a�o en orden de unidades vendidas
*
***************************************************************************************************/

/* ====================================
    M�dulos y librer�as est�ndar usados
   ==================================== */

#include <stdio.h> /* Para prinf() y scanf() */
#include <stdlib.h> /* Para system() */
#include <conio.h> /* Para getch() */
#include "listarventas.h"

/* ====================================
    Variables globales
   ==================================== */

int contRepetidos = 0;

/* ====================================
   Procedimiento para buscar y listar las ventas
   de un a�o en orden de ventas
   ==================================== */

void ListarVentasAnno(int annoBuscar) {
  /*--Variables locales--*/
  char tecla;
  int aux, nPaginas, lineaPie;
  TipoCadena msgPie1, msgPie2;
  /*--Variables contador con valor inicial--*/
  int contRegAnno = -1;/*Variable que contar� el n�mero de registros que contiene el vector con
                         el valor inicial -1 para que la iteraci�n la empiece de 0*/
  int PosicionRanking = 0;/*Variable que contar� la posici�n en la lista con el valor inicial 0*/
  int pagina = 0;/*Variable que contar� el n�mero de p�gina conel valor inicial 0*/
  /*--Variables vector para copiar los registros--*/
  TipoVectorRegistro productoAnno; /*Para producto*/
  TipoVectorRegistro unidadesAnno; /*Para unidades*/

  /* -- Pasamos los registros de unidades y producto de el a�o dado a dos nuevos vectores --
  Esto lo hacemos para evitar estropear el vector de registros al igualar productos iguales y ordenar*/

  for (int k = 0; k<= numeroRegistro;k++) {/*Recorremos  el vector hasta donde hay registros*/

    if (codigoVenta[k].fecha.anno == annoBuscar) {/*Solo cargamos los registros del a�o dado*/
      contRegAnno++;/*Cada iteraci�n sumamos 1 para saber el tama�o del vector*/
      productoAnno[contRegAnno] = codigoVenta[k].producto;/*Cargamos los productos*/
      unidadesAnno[contRegAnno] = codigoVenta[k].unidades;/*Cargamos las unidades*/
    }
  }

  /* -- Fusionamos productos repetidos --
  Para ello ultizamos dos for ya que con uno solo se comparar�a con el producto del siguiente elemento
  La idea es que haya otras iteracciones combinen las comparaciones de forma que todos los elementos
  del vector sean comparados */

  for (int i = 0; i <= contRegAnno-1; i++) {/*Recorre el vector desde el inicio hasta los registros
                                              contenidos menos uno */
    for (int j = i+1; j <= contRegAnno; j++) {/*Recorre el vector desde el inicio menos uno hasta
                                                los registros contenidos*/
      if (productoAnno[i] == productoAnno[j]) {/*Compara el valor de producto de la iteraci�n de un for
         con la del otro y si son iguales se suman las unidades del 2.� for al 1.� y se anula el
         otro registro d�ndole el valor 0 a producto y unidades*/
        unidadesAnno[i] = unidadesAnno[i] + unidadesAnno[j];/*Sumamos las unidades del producto repetido*/
        unidadesAnno[j] = 0; /* Anulamos unidades */
        productoAnno[j] = 0; /* Anulamos producto */

      }
    }
  }

  /* -- Ordenamos de mayor a menor en funci�n de las unidades vendidas --
  Funciona con la misma idea que el caso anterior con dos for.
  En este caso se comparan todas las unidades registradas y se ordenan utilizando el
  llamado algoridmo de la burbuja*/

  if (contRegAnno > 0) { /* Para que no de fallo si hay solo un registro*/
    for (int i = 0; i <= contRegAnno-1; i++) { /*Recorre el vector desde el inicio hasta los registros
                                              contenidos menos uno */
      for (int j = i+1; j <= contRegAnno; j++) {/*Recorre el vector desde el inicio menos uno hasta
                                                los registros contenidos*/
        if (unidadesAnno[i] < unidadesAnno[j]) {/*Compara el valor de unidades de la iteraci�n de un for
                                                  con la del otro y si es menor la primera:*/
          aux = unidadesAnno[i];/*Se copia el contenido de la 1.� en variable aux*/
          unidadesAnno[i] = unidadesAnno[j];/*Se copia el contenido de la 2.� en la 1.�*/
          unidadesAnno[j] = aux;/*El contenido de aux se compia en la segunda*/

          /*Lo mismo con el vector de productos para que coincidan los �ndices*/
          aux = productoAnno[i];/*Se copia el contenido de la 1.� en variable aux*/
          productoAnno[i] = productoAnno[j];/*Se copia el contenido de la 2.� en la 1.�*/
          productoAnno[j] = aux;/*El contenido de aux se compia en la segunda*/
        }
      }
    }
  }

  /* -- Contamos los registros que contiene el vector --*/

  while (unidadesAnno[contRepetidos] != 0) {
    contRepetidos++;/*cada iteracci�n cuenta uno*/
  }


  /* Calculamos el n�mero de p�ginas que tendr� con 90 registros mostrados por p�gina*/
  if ((contRepetidos%15) == 0) {/*Si el n�mero de registros en m�ltiplo de 90 coincidir� con su divisi�n
                                  entre 90*/
    nPaginas = contRepetidos/15;
  } else {/* Si no habr� una p�gina m�s*/
    nPaginas = (contRepetidos/15)+1;
  }
  /* -- Imprimimos resultados --*/

  for (int k = 0; k <= contRegAnno; k++) {/*Recorre el vector hasta el total de registros incluidos en �l*/

    if (unidadesAnno[k] != 0) { /* Quitamos los que hemos anulado por estar repetidos que le dimos valor 0*/
      PosicionRanking++;/* cada iteraci�n suma 1*/
      /*Imprimimos el orden de registro y la palabra producto*/
      printf("%0*d) Producto ",NumDigitos(contRepetidos,true), PosicionRanking);
      CambiarColor(PreguntaAplicacion);/*Color texto azul claro*/
      /*Imprimimos valor del producto alinado en la izquierda*/
      printf("%0*d: ",MaxDigitos(productoAnno,contRegAnno),productoAnno[k]);
      if (unidadesAnno[k] >= 0) {
        CambiarColor(DatoPositivo);/*Las unidades positivas color aguamarina*/
      } else {
        CambiarColor(DatoIntroNegativo);/*Las unidades negativas color rojo oscuro*/
      }
      printf("%*d",MaxDigitos(unidadesAnno,contRegAnno),unidadesAnno[k]);/*Imprimimos unidades*/
      CambiarColor(Aplicacion);/*Volvemos a los colores por defecto*/
      printf(" %s\n",Plural(unidadesAnno[k]));/*Imprimimos la palabra unidad o unidades seg�n sea 1 o m�s*/


      /*Se imprimen los registros hasta llegar a 15 (o m�ltiplo de)
      Al llegar aqu� se produce una pausa esperando que se pulse una tecla para continuar
      Se muestra un mensaje informativo y depende de la tecla que se pulse se continuar� se volver� para atr�s
      o para el final*/

      if ((PosicionRanking % 15 == 0)|| ((PosicionRanking == contRepetidos) && (contRepetidos>15))) {
        pagina++;
        /*Mensage que se mostrar� al llegar a fin de p�gina (15 registros)*/
        if (pagina == 1) { /*Si la p�gina es la primera*/
          sprintf(msgPie1," Pag: %d/%d, Reg: %d-%d/%d ",
                  pagina,nPaginas,PosicionRanking-14,PosicionRanking,contRepetidos);
          sprintf(msgPie2," INTRO: Siguiente, U: \351ltimas, ESC: Volver al men\243. ");
        } else if (pagina == nPaginas) {/*Si la p�gina es la �ltima*/
          sprintf(msgPie1, " Pag: %d/%d, Reg: %d-%d/%d ",
                  pagina,nPaginas,(PosicionRanking-(PosicionRanking%15))+1,PosicionRanking,contRepetidos);
          sprintf(msgPie2, " A: Anterior, P: Primeras, OTRA TECLA: Volver al men\243 ");
        } else {/*Si es cualquier otra p�gina*/
          sprintf(msgPie1, " Pag: %d/%d, Reg: %d-%d/%d ",
                  pagina,nPaginas,PosicionRanking-14,PosicionRanking,contRepetidos);
          sprintf(msgPie2, " INTRO: Siguiente, A: Anterior, P: Primeras, U: \351ltimas, ESC: Volver al men\243. ");

        }
        if (pagina == nPaginas) {
          lineaPie = (PosicionRanking%15)+4;
        } else {
          lineaPie = (PosicionRanking/pagina)+4;
        }

        CambiarColor(Ayuda);
        Posicion(1,lineaPie);
        printf("%*c",strlen(msgPie2),char(32));/*Imprimimos parte superior de la caja*/
        Posicion(1,lineaPie+1);
        printf("%s%*c",msgPie1,strlen(msgPie2)-strlen(msgPie1),char(32));/*Imprimimos parte mensaje de la caja*/
        Posicion(1,lineaPie+2);
        printf("%s",msgPie2);/*Imprimimos parte mensaje de la caja*/
        Posicion(1,lineaPie+3);
        printf("%*c",strlen(msgPie2),char(32));/*Imprimimosparte inferior de la caja*/
        CambiarColor(Aplicacion);
        printf("\n\n");
        tecla = getch();/*Espera hasta que pulsemos una tecla*/


        switch (tecla) {
        case 'A': /* Si pulsamos la tecla A (P�gina anterior)*/
        case 'a':
          if (pagina == nPaginas) {/*Si adem�s es la �ltima p�gina*/
            system("CLS");/*Limpiamos pantalla*/
            printf("LISTADO DE VENTAS\n\n"); /*Imprimimos cabecera*/
            k = (k-(contRepetidos%15))-15;/*Al contador de iteraci�n se le resta 90 m�s los registros de
                                          la �ltima p�gina*/
            PosicionRanking = (PosicionRanking-(contRepetidos%15))-15;/*Lo mismo el contador de posici�n*/
            pagina = pagina-2;/* Y a p�gina se le descuentan dos ya que volver� a contar uno m�s*/
            /* -- Si es cualquier otra p�gina que no sea la 1.� --*/
          } else if (pagina == 1) {
            system("CLS");/*Limpiamos pantalla*/
            printf("LISTADO DE VENTAS\n\n"); /*Imprimimos cabecera*/
            tecla = 13;/*Como si pulsar�amos intro*/
          } else {
            system("CLS");/*Limpiamos pantalla*/
            printf("LISTADO DE VENTAS\n\n");/*Imprimimos cabecera*/
            k = k-30;/*Al contador de iteraci�n se le restan los 90 de la p�gina actual m�s los de la
                      anterior que volvera a cargar*/
            PosicionRanking = PosicionRanking-30;/*Lo mismo para el contador de posici�n*/
            pagina = pagina-2;/* Y se descuentan 2 p�ginas*/
          }

          break;
        case 'P': /*Si pulsamos P (Primera p�gina)*/
        case'p':
          system("CLS");/*Limpiamos pantalla*/
          printf("LISTADO DE VENTAS\n\n");/*Imprimimos cabecera*/
          /*Ponemos contadores a 0*/
          k = -1;/*Damos al contador de iteraci�n el valor inicial menos 1*/
          PosicionRanking = 0;/*Lo mismo al contador de posici�n*/
          pagina = 0;/* y a p�gina*/
          break;
        case 'u':/*Si pulsamos U (�ltima p�gina)*/
        case 'U':
          system("CLS");/*Limpiamos pantalla*/
          printf("LISTADO DE VENTAS\n\n");/*Imprimimos cabecera*/
          k = (((nPaginas-1)*15)-1);/*Damos al contador de iteraci�n el valor de la �ltima p�gina menos 1*/
          PosicionRanking = ((nPaginas-1)*15);/*Lo mismo a la variable que cuenta la posici�n*/
          pagina = nPaginas-1; /*Damos el valor de la �ltima p�gina (-1 porque se le sumar� 1 al cargarse)*/
          break;
        case 13:/*Si pulsamos INTRO (int 13) y es la �ltima p�gina nos salimos*/
          if (pagina == nPaginas) {
            return;
          } else {/*Si es otra p�gina se limpia la pantalla y se vuelve a imprimir la cabecera
              y continuar�n imprimiendose los registros siguientes*/
            system("CLS");/*Limpiamos pantalla*/
            printf("LISTADO DE VENTAS\n\n");/*Imprimimos cabecera*/
          }
          break;
        case 27:/*Si pulsamos ESC (int 27) nos salimos*/
          return;
          break;
        default:/*Si pulsamos cualquier otra tecla y no es la �ltima p�gina sucede lo mismo que se pulsamos INTRO*/
          if (pagina != nPaginas) {
            system("CLS");/*Limpiamos pantalla*/
            printf("LISTADO DE VENTAS\n\n");
          } else {/*Si estamos en la �ltima p�gina volvemos al men�*/
            return;
          }
        }
      }
    }
  }/*cierre de for*/
  printf("\n"); /*Un espacio en blanco extra al final*/

  if (GetAsyncKeyState(VK_ESCAPE)) {/*Si pulsamos ESC se cierra el subprograma y volvemos al men�*/
    return;
  }
}


/* ====================================
   Procedimiento Dialogo para selecionar
   el a�o en que se quieren listar las ventas
   ==================================== */

void ListaVentas() {
  COORD posicion;
  int annoLista, posicionY;/*Variable que contendr� el a�o dado*/
  system("color B1");/*Color Pantalla. Fondo azul claro, texto negro*/
  printf("LISTADO DE VENTAS\n\n");/*Imprimimos cabecera*/

  /*-- Bloque que se repetir� hasta que se de un a�o en que h habido ventas -- */

  do {
    AyudaSubmenu(anno);
    Posicion(0,3);
    Control(anno,annoLista);/*Preguntamos el a�o y esperamos respuesta*/
    if (GetAsyncKeyState(VK_ESCAPE)) {/*Si pulsamos ESC se cierra el subprograma y volvemos al men�*/
      return;
    }
    if (!Buscar(NULL,NULL,NULL,annoLista)) {/*Buscamos el a�o dado en los registros y si no est�: */
      Posicion(18,2);/*Posici�n del final de la pregunta*/
      printf("%80c\n",char(32));/*Borramos el valor dado*/
      CambiarColor(Error);/*Color fondo rojo texto blanco brillante*/
      Posicion(1,1);/*Posici�n del mensaje de error encima de la pregunta*/
      printf("\a Aviso: En el a\244o %d no ha habido ventas.", annoLista);/*Imprimimos aviso*/
      CambiarColor(Submenu);/*Volvemos a los colores por defecto*/
    }
  } while (!Buscar(NULL,NULL,NULL,annoLista));
  system("CLS");/*Limpiamos pantalla*/
  system("color 70");/*Color Pantalla. Fondo blanco texto negro*/
  printf("LISTADO DE VENTAS\n\n");/*Imprimimos cabecera*/

  /*-- Ejecutamos subprograma -- */

  ListarVentasAnno(annoLista);

  if (contRepetidos < 15) {/*Si hay menos de 90 registros se imprime el texto por defecto*/
    Posicion(1,PosicionAct(Y));/*Ponemos un margen de dos espacios manteniendo la posici�n vertical*/
    CambiarColor(Ayuda);/*Color fondo azul, texto blanco*/
    printf("%46c\n",char(32));/*Imprimimosparte superior de la caja*/
    Posicion(1,PosicionAct(Y));/*Ponemos un margen de dos espacios manteniendo la posici�n vertical*/
    printf(" Presione cualquir tecla para volver al men\243. \n");
    Posicion(1,PosicionAct(Y));/*Ponemos un margen de dos espacios manteniendo la posici�n vertical*/
    printf("%46c",char(32));/*Imprimimimos la parte inferior de la caja*/
    getch();/* Mantiene la pantalla en espera de que pulsemos una tecla */
  }
}

