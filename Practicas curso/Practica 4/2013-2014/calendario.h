/***************************************************************************************************************
* Interfaz de m�dulo: calendario
*
* Este m�dulo contiene las funciones y procedimientos para imprimir una hoja de calendario de un mes y a�o dados
*
****************************************************************************************************************/

#pragma once
#include "auxiliar.h"

/* ==========================================
     Tipos P�blicos
   ========================================== */
/*Nombre de los meses*/
typedef enum TipoMeses {
  Todos,ENERO,FEBRERO,MARZO,ABRIL,MAYO,JUNIO,JULIO,AGOSTO,SEPTIEMBRE,OCTUBRE,NOVIEMBRE,DICIEMBRE
};
/*Tipo para de vector para contener los d�as de venta de un mes*/
typedef int TipoArrayDias[31];


/*Tipo struct que contiene dos funciones que devuelven: */
typedef struct TipoDia {
  int Mes(int meses,int annos);/*El d�a de la semana en que empieza un mes (0 = lunes ..)*/
  int Semana(int meses, int annos);/*Los d�as que tiene un mes*/
};
/*Tipo struct que contiene dos procedimientos*/
typedef struct TipoImprimir{
  void Cabecera(int mes, int anno);/*Imprime cabecera del calendario*/
  void Calendario(int mes, int anno, int productoBusc);/*Imprime la hoja de calendario con cabecera incluida*/
};

/* ==========================================
     Variables p�blicas
   ========================================== */

extern TipoDia diasMes;/*Variable para calculas los d�as de un mes o el d�a en que empieza un mes (0 = lunes)*/

/* ==========================================
     Funciones y procedimientos p�blicas
   ========================================== */
bool Bisiesto(int annos);/*Funci�n que devuelve true si el a�o es bisiesto*/
bool DiaVenta(int numero);/*Funci�n que devuelve true si ese d�a hubo venta*/
void Calendario();/*Submen� para ejecutar el m�dulo*/
