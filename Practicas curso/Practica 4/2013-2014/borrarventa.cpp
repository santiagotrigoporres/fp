/*********************************************************************
* M�dulo: borrarventa
*
* Este m�dulo contiene los elementos para borrar un registro de ventas
*
**********************************************************************/

/* ====================================
    M�dulos y librer�as est�ndar usados
   ==================================== */

#include <stdio.h> /* Para prinf() y scanf() */
#include <stdlib.h> /* Para sytem() */
#include <conio.h> /* Para getch() */
#include "borrarventa.h"


/* ====================================
   Procedimiento para borrar registros
   ==================================== */

void BorrarElemento(int idProducto, int idDia, int idMes, int idAnno) {

  /*-- Variables locales -- */

  char tecla;/*Variable para guardar la tecla pulsada*/
  bool encontradoRegistro = false;/*Variable que cambiara su valor a true si se encuentra un registro*/
  int indiceBorrar;/*Variable para guardar el �ndice del registro encontrado*/
  int idUnidades; /*Variable  para guardar las unidades*/

  /* -- Inicio de apilicaci�n -- */
  for (int k = 0; k <= numeroRegistro; k++) {/*Recorre  el vector hasta el n�mero de registros guardados y
    comprueba si hay una conincidencia entre el registro del vector temporal y el general */
    if ((codigoVenta[k].producto == idProducto) &&
    (codigoVenta[k].fecha.anno == idAnno) &&
    (codigoVenta[k].fecha.mes == idMes) &&
    (codigoVenta[k].fecha.dia == idDia)) {
      /* Si encuentra una coincidencia */
      encontradoRegistro = true;/*Cambiamos a valor true la variable encontradoRegistro*/
      idUnidades = codigoVenta[k].unidades;/*Guardamo las unidades del registro encontradopara usarlas en
                                             el mensaje informativo*/
      indiceBorrar = k;/*Guardamos �ndice del registro encontrado*/
    }
  }
  system("CLS");/*Limpiamos pantalla*/
  system("color 70");/*Color pantalla: Fondo blanco, texto negro*/
  /* -- Si no encuentra el registro --
  Mantenemos esto por si fallan los controles parciales */
  if (!encontradoRegistro) {
    CambiarColor(Error);/*Color fondo rojo claro, texto blanco brillante*/
    printf("\n No se ha encontrado el registro \n");/*Imprimimos mensaje de error*/
    CambiarColor(Aplicacion);/*Restablecemos color fondo blanco, texto negro*/
    printf("\n%02d/%02d/%d, producto %d\n\n",idDia,idMes,idAnno,idProducto);
    /*-- Si encuentra el registro -- */
  } else {
    printf("ELIMINAR VENTA");/*Imprimimos cabecera*/
    CambiarColor(Hecho);/*Color fondo verde, texto blanco brilante*/
    Posicion(1,3);/*Ponemos cursor enla l�nea 4 (3 por debajo de la cabecera)*/
    printf(" Registro encontrado \n\n");/*imprimimos mensaje*/
    CambiarColor(DatoPositivo);/*Color texto azul claro*/
    printf("%d) %02d/%02d/%d, producto %d: %d %s\n\n",indiceBorrar + 1, idDia, idMes, idAnno,idProducto,idUnidades,Plural(idUnidades));
    CambiarColor(PreguntaAplicacion);/*Texto color azul claro*/
    printf("\250Est\240 seguro que quiere eliminarlo? (s/n) ");/*Imprimimos pregunta*/
    CambiarColor(Aplicacion);/*Restablecemos texto negro*/
    fflush(stdin);/*limpiamos buffer*/
    tecla = getch();/* pausa hasta que se pulse una tecla*/
    if (tecla == 13) {
      tecla = 's'; /* INTRO (int 13) equivale a s */
    }
    if (tecla == 's' || tecla  == 'S') {
      /*-- Si se pulsa s, S o INTRO se procede a borrar el registro -- */
      for (int k = indiceBorrar; k <= numeroRegistro; k++) {
        /*Se recorre el vector de Registros (codigoVenta) desde el registro encontrado hasta
        numeroRegistro (registros que hay) y suma a cada indice 1*/
        codigoVenta[k].unidades = codigoVenta[k+1].unidades;
        codigoVenta[k].producto = codigoVenta[k+1].producto;
        codigoVenta[k].fecha.dia = codigoVenta[k+1].fecha.dia;
        codigoVenta[k].fecha.mes = codigoVenta[k+1].fecha.mes;
        codigoVenta[k].fecha.anno = codigoVenta[k+1].fecha.anno;
      }
      Posicion(1,3);/*Ponemos el cursor en la l�nea 4 con un margen de un espacio*/
      printf("%79c\n",char(32));/*Borramos pregunta*/
      Posicion(1,3);/*Ponemos el cursor en la l�nea 4 con un margen de un espacio*/
      CambiarColor(Hecho);/*Color fondo verde texto blanco brillante*/
      printf(" El registro se ha borrado ");/*Imprimimosmensage de confirmaci�n*/
      CambiarColor(Aplicacion);/*Volvemos al color anterior*/
      Posicion(0,7);/*Ponemos el cursor donde la pregunta*/
      printf("%80c\n",char(32));/*Borramos la pregunta*/
      numeroRegistro--;/*Se resta del contador de registros el registro borrado*/
    } else {
      /*-- Si se pulsa cualquier otra tecla se cancela la operaci�n -- */
      Posicion(0,3);/*Ponemos el cursor en la l�nea 4 (3 l�enas debajo de la cabecera)*/
      printf("%80c\n\n",char(32));/*Borramos mensaje registro encontrado y dos saltos de l�nea*/
      printf("%80c\n\n",char(32));/*Borramos informaci�n de registro encontrado y dos saltos de l�nea*/
      printf("%80c",char(32));/*Borramos pregunta*/
      Posicion(1,3);/*Ponemos el cursor en la l�nea 4 con un margen de un espacio*/
      CambiarColor(NoHecho);/*Color fondo amarillo claro, texto negro*/
      printf(" Operaci\242n cancelada \n\n");/*Imprimimosmensaje*/
      CambiarColor(Aplicacion);/*Restablecemoscolores de aplicaci�n fondo blanco y texto negro*/
    }
  }
}

/* ========================================
   Procedimiento Di�logo de eliminar venta
   ========================================= */

void EliminarVenta() {

  /* -- Variables locales -- */

  TipoVector ventaEliminar;/* Vector que contendr� los datos aportados de forma temporal*/

  /* --Inicio de  Submen� -- */

  if (numeroRegistro > -1) {/*Comprobamos que hay registros guardador*/
    system("color B1");/* Color Aguamarina claro con texto negro */
    printf("ELIMINAR VENTA\n\n");/* Imprimimos cabecera*/
    AyudaSubmenu(producto);/*Caja de ayuda con opciones para producto*/
    Posicion(0,3);/*Ponemos el cursor en la l�nea 4*/
    Control(producto, ventaEliminar[1]);/*Di�logo de mes aplicando el control de existencia de producto*/
    if (GetAsyncKeyState(VK_ESCAPE)) { /*Si se pulsa ESC volvemos al men�*/
      return;
    }

    /* - Bloque que se repetir� hasta que se introduzca un dia en que no se vendio el producto --*/

    do {
      AyudaSubmenu(dia);/*Caja de ayuda con opciones para d�a*/
      Posicion(0,5);/*Ponemos el cursor en la l�nea 6*/
      Control(dia,ventaEliminar[3]);/*Di�logo de d�a aplicando el rango v�lido*/
      if (GetAsyncKeyState(VK_ESCAPE)) {/*Si se pulsa ESC volvemos al men�*/
        return;
      }
      /* Comprobamos que ese d�a hubo venta del producto dado*/
      if (!Buscar(ventaEliminar[1],ventaEliminar[3],NULL,NULL)) {
        Posicion(13,5);/* Ponemos el cursor al final de la pregunta d�a donde va el valor dado*/
        printf("%67c\n",char(32));/*Borramos el valor dado*/
        Posicion(1,4);/*Ponemos el cursor encima de la pregunta de d�a*/
        CambiarColor(Error);/*Color rojo claro con texto blanco brillante*/
        printf("\a Aviso: No hay registros del producto %d en d\241a %d \n",ventaEliminar[1],ventaEliminar[3]);
        CambiarColor(Submenu);/* Color Aguamarina claro con texto negro */
      }
    } while (!Buscar(ventaEliminar[1],ventaEliminar[3], NULL, NULL));

    /*-- Una vez se da d�a en que se vendi� el producto dado salimos del while -- */

    Posicion(1,4);/* Ponemos cursor donde elmensaje de error*/
    printf("%79c\n",char(32));/*Borramos mensaje de error*/

    /*-- Bloque que se repetir� hasta que demos un mes en que hubo ventas del producto en el d�a dado -- */

    do {
      AyudaSubmenu(mes);/*Caja de ayuda con opciones para mes*/
      Posicion(0,7);/*Ponemos cursor en la l�nea 8*/
      Control(mes,ventaEliminar[4]);/*Di�logo de mes aplicando el control de rango*/
      if (GetAsyncKeyState(VK_ESCAPE)) {/*Si se pulsa ESC volvemos al men�*/
        return;
      }
      /*Comprobamos que en ese mes ha habido ventas del producto en el d�a dado*/
      if (!Buscar(ventaEliminar[1],ventaEliminar[3],ventaEliminar[4],NULL)) {
        Posicion(13,7);/*Ponemos cursor al final de la pregunta mes donde esta el valor dado*/
        printf("%67c\n",char(32));/*Borramos valor dado a mes*/
        Posicion(1,6);/* Ponemos cursor encima de la pregunta mes*/
        CambiarColor(Error);/* Color fondo rojo letras blanco brilante*/
        printf("\a Aviso: No hay registros del producto %d en d\241a %d de %s \n",ventaEliminar[1], ventaEliminar[3], nombreMeses[ventaEliminar[4]]);
        CambiarColor(Submenu);/* Volvemos a los colores anteriores*/
      }

    } while (!Buscar(ventaEliminar[1],ventaEliminar[3],ventaEliminar[4],NULL));

    /*-- Una vez que damos un mes en que hubo ventas del producto en el d�a dado salimos de while -- */

    Posicion(1,6);/*Ponemos cursor donde el mensaje de error*/
    printf("%79c\n",char(32)); /*Borramos el mensaje de error*/

    /*-- Bloque que se repetir� hasta que demos un a�o en que hubo ventas del producto en el d�a y mes dado-- */

    do {
      AyudaSubmenu(anno);/*Caja de ayuda con opciones para a�o*/
      Posicion(0,9);/* Ponemos cursor en l�nea 10*/
      Control(anno,ventaEliminar[5]);/*Di�logo de a�o aplicando el control de rango*/
      if (GetAsyncKeyState(VK_ESCAPE)) {/*Si pulsamos ESC ceramos subprograma y volvemos al men�*/
        return;
      }
      /*Comprobamos que ese a�o ha habido ventas del producto en el d�a y mes dado */
      if (!Buscar(ventaEliminar[1],ventaEliminar[3],ventaEliminar[4], ventaEliminar[5])) {
        Posicion(18,9);/*Ponemos el cursor al final de la pregunta de a�o donde esta el valor dado*/
        printf("%62c\n",char(32));/*Borramos el valor dado*/
        Posicion(1,8);/*Ponemos el cursor encima de la pregunta de a�o*/
        CambiarColor(Error);/* Color fondo rojo letras blanco brilante*/
        /*Imprimimos mensaje de error*/
        printf("\a Aviso: No hay registros del producto %d en d\241a %d de %s del a\244o %d \n",ventaEliminar[1], ventaEliminar[3], nombreMeses[ventaEliminar[4]],ventaEliminar[5]);
        CambiarColor(Submenu);/* Volvemos a los colores anteriores*/
      }
    } while (!Buscar(ventaEliminar[1],ventaEliminar[3],ventaEliminar[4], ventaEliminar[5]));
    /*-- Una vez se da un a�o en que hubo venta del producto  en el d�a y mes dado salimos del while --*/
    Posicion(1,8);
    printf("%79c\n",char(32));
    BorrarElemento(ventaEliminar[1],ventaEliminar[3],ventaEliminar[4], ventaEliminar[5]);
  } else {/*Dejo una opci�n general por si fallar�an los controles parciales*/
    system("color cf");/*Color pantalla fondo rojo, textoblanco brilante*/
    printf("No hay registros guardados\n");/*Imprimimos mensaje*/
  }
   Posicion(1,8);/*Ponemos un margen de un espacio con la misma posici�n vertical*/
  CambiarColor(Ayuda);/*Restablecemos colores*/
  printf("%46c\n",char(32));/*Imprimimos parte superior de la caja*/
  Posicion(1,PosicionAct(Y));/*Ponemos un margen de un espacio con la misma posici�n vertical*/
  printf(" Presione cualquir tecla para volver al men\243. \n");/*Imprimimos pie de pantalla*/
  Posicion(1,PosicionAct(Y));/*Ponemos un margen de un espacio con la misma posici�n vertical*/
  printf("%46c",char(32));/* Imprimimos parte inferior de la caja*/
  getch();/* Mantiene la pantalla en espera de que pulsemos una tecla */
}




