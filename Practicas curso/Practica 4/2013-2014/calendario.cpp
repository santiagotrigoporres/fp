/****************************************************************************************************
* M�dulo: calendario
*
* Este programa contiene las funciones para imprimir una hoja de calendario de un mes de un a�o dados
*
*****************************************************************************************************/

/*====================================
   M�dulos y librer�as est�ndar usados
  ==================================== */

#include <stdio.h> /* Para prinf() y scanf() */
#include <stdlib.h> /* Para sytem() */
#include <conio.h> /* Para getch() */
#include <string.h> /*Para strlen*/
#include "calendario.h"

/* ==================================
    Variables globales
   ==================================*/

TipoLista diasListados;
TipoLista unidadesListadas;
TipoImprimir imprimir;
int contador = -1;

/*===================================================
 Funci�n para comprobar si un a�o es bisiesto
==================================================
Devuelve true si el a�o correspondiente a la variable anno es bisiesto y false si no lo es*/

bool Bisiesto(int annos) { /* Para que el a�o sea bisiesto tiene que ser m�ltiplo de 4, no serlo de 100 o
                              serlo de 400 */
  if (((annos % 4) == 0) && (((annos % 100) != 0) || ((annos % 400) == 0))) {
    return true;
  } else {
    return false;
  }
}

/* ====================================================
     Funci�n para  averiguar los d�as que tiene un mes
   ====================================================
   Esta funci�n devuelve los d�as que tiene un mes (dado en la variable mes) */

int TipoDia::Mes(int meses,int annos) {

  switch (TipoMeses(meses)) {
  case ABRIL:
  case JUNIO:
  case SEPTIEMBRE:
  case NOVIEMBRE: /* Meses de 30 d�as: abril, junio septiembre y noviembre */
    return 30;
    break;
  case FEBRERO:
    if (Bisiesto(annos)) { /* Caso de febrero que cuando es bisiesto tienes 29 d�as y si no 28 */
      return 29;
    } else {
      return 28;
    }
    break;
  default:  return 31; /* El resto de meses tienen 31 d�as */
  }
}

/* ====================================================
    Funci�n para averiguar el dia de la semana
   ====================================================*/
/* Devuelve el d�a de la semana que corresponde el dia uno del mes del a�o indicado */

int TipoDia::Semana(int mes, int anno) {
  int contadorDias = 0; /* Declaramosla variable contadoDias con el valor inicial 0 */
  int diaSemana;
  TipoDia diaMes;
  /* --Paso 1. Contamos los d�as que trascurren por los a�os pasados desde 1500 (cuyo 1 de enero era lunes) hasta el a�o dado  --*/

  for (int k = 1500; k < anno; k++) {
    if (Bisiesto(k)) {
      contadorDias = contadorDias + 366; /* Si es bisiesto suma 366 d�as */
    } else {
      contadorDias = contadorDias + 365; /* Si no lo es suma 365 d�as */
    }
  }

  /* -- Paso 2. Sumamos al contador los d�as que han trancurrido desde el 1 de enero hasta el 1 del mes
    dado --                                 */

  for (int k = 1; k < mes; k++) {
    contadorDias = contadorDias + diaMes.Mes(k, anno); /* Sumara los d�as correspondientes al mes dado
                                                          usando la funci�n DiaMes*/
  }

  /* -- Paso 3. Obtenemos el resto de dividir la suma de dias totales pasados desde 1601 entre 7*/

  diaSemana = contadorDias % 7;/*Calculamos el resto de dividir el contadorDias entre 7*/
  return diaSemana; /* 0=lunes, 1=martes, 2=mi�rcoles, 3=jueves, 4=viernes, 5=s�bado y 6=domingo. */
}

/* ========================================================
    Procedimiento para selecionar los dias que hubo venta
   ======================================================== */

void BuscarDiaVenta(int buscarMes, int buscarAnno, int buscarProducto) {
  int aux;/*Variable auxiliar que utilizaremospara ordenar un vector*/
  for (int k = 0;k <= numeroRegistro; k++) {/* Buscamos en el registro las vectas del el producto en
                                                       el mes y a�o indicado*/
    if ((codigoVenta[k].fecha.mes == buscarMes) &&
        (codigoVenta[k].fecha.anno == buscarAnno) &&
        (codigoVenta[k].producto == buscarProducto)) {
      contador++; /*cada iteraci�n suma uno a al contador*/
      diasListados[contador] = codigoVenta[k].fecha.dia; /*Introducimos los dias en el vector diasListados*/
      unidadesListadas[contador] = codigoVenta[k].unidades;/*Introducimos la unidades vendidas en el vector
                                                            unidadesListados*/

    }
  }

  /* -- Ordeno por orden de d�a los vectores diasListado y unidadesListadas--
  Paraordenar utilizamos dos for de forma que en la iteraciones (una compara el elemente anterior con el
  posterior y la otra produce un salto para que se compare con el siguiente)se comparen todos los registros del vector
  Se usa el algoridmo de la burbuja para realizar la ordenaci�n de menor a mayor*/

  if (contador >= 0) {
    for (int i = 0;i <= contador-1; i++) {/*Recorre el vector desde el inicio hasta los registros
                                              contenidos menos uno */
      for (int j = i+1; j<= contador; j++) {/*Recorre el vector desde el inicio menos uno hasta
                                                los registros contenidos*/
        if (diasListados[i] > diasListados[j]) {/*Compara el valor de unidades de la iteraci�n de un for
                                                  con la del otro y si es mayor la primera:*/
          aux = diasListados[i];/*Se copia el contenido de la 1.� en variable auxD*/
          diasListados[i] = diasListados[j];/*Se copia el contenido de la 2.� en la 1.�*/
          diasListados[j] = aux;/*El contenido de auxD se compia en la segunda*/
          /*Lo mismo con el vector de unidades para que coincidan los �ndices*/
          aux = unidadesListadas[i];
          unidadesListadas[i] = unidadesListadas[j];
          unidadesListadas[j] = aux;
        }
      }
    }
  }
}

/* ========================================================
    Funci�n para buscar dias a marcar
   ======================================================== */

bool DiaVenta(int numero) {
  bool resultado = false;
  for (int k = 0; k <= contador ; k++) {/*Recorremos el vector hasta elcontador que indica elmaximo deregistrosdeeseproductoenesemesya�o*/
    if (numero == diasListados[k]) {/*Se comprueba si el n�mero coincide con alg�n elemento del vector*/
      resultado = true;
    }
  }
  return resultado;
}

/* ========================================================
    Procedimiento para imprimir la cabecera del calendario
   ======================================================== */

void TipoImprimir::Cabecera(int mes, int anno) {
  TipoCadena tituloMes;
  Posicion(1,PosicionAct(Y));/*Ponemos un margen de un espacio con la misma posici�n vertical*/
  printf("%31c\n",char(32)); /* Un salo de l�nea encima del calendario */

  /* -- Imprimimos el nombre del mes correspondiente y el a�o con los espacios en blanco
        correspondientes entre ambos -- */

  CambiarColor(NombreMes);/*Color fondo blanco brillante, texto azul*/
  for (int k = 0;k<=11;k++) {
    tituloMes[k] = nombreMeses[mes][k];
  }
  Posicion(1,PosicionAct(Y));
  printf("  %s%*d  \n", strupr(tituloMes), 27 - strlen(nombreMeses[mes]), anno);
  /* -- Imprimimos el resto de la cabecera -- */
  CambiarColor(CCalendario);/*Color texto negro*/
  Posicion(1,PosicionAct(Y));/*Ponemos un margen de un espacio con la misma posici�n vertical*/
  printf("  ===========================  \n"); /* Total 27 caracteres + 4 espacios en blanco*/
  Posicion(1,PosicionAct(Y));
  printf("  LU  MA  MI  JU  VI | SA  DO  \n");/* Imprimimos d�as de la semana*/
  Posicion(1,PosicionAct(Y));
  printf("  ===========================  \n");
}

/* ===========================================
    Procedimiento para imprimir el calendario
   =========================================== */

void TipoImprimir::Calendario(int mes, int anno, int productoBusc) {
  TipoImprimir imprimir;
  TipoDia dia;
  int numero;
  int diaMes = dia.Mes(mes,anno);
  int diaSemana = dia.Semana(mes, anno);
  int posicion = 0; /* Declaramos la variable posicion con el valor inicial 0 */

  /* -- Paso 1 Imprimimos la cabecera dada en el procedimiento Cabecera() -- */

  CambiarColor(CCalendario);/*Color fondo blanco brillante, texto negro*/

  imprimir.Cabecera(mes, anno);/*Imprimimos cabecera del calendario*/

  /*Ejecutamos el procedimiento de buscar ventas de un producto en el mes y a�o dado*/
  BuscarDiaVenta(mes,anno,productoBusc);

  /* -- Paso 2 Imprimimos puntos los d�as de la semana que preceden al primer d�a del mes. --
            Creamos la variable posici�n que en cada intineraci�n suma 1 y actuar�
            de cotador fuera de este bucle */
  Posicion(1,PosicionAct(Y));/*Ponemos un margen de un espacio con la misma posici�n vertical*/
  printf("  ");/*Imprimimos dos espacios en blanco delante de la primera casilla del calendario*/
  while (posicion < diaSemana) {
    posicion++; /* Cada iteracci�n posici�n suma uno */
    if (((posicion + 2) % 7) == 0) { /* Para que haya un palito tras el 5� caracter */
      printf(" . | "); /* A la izquierda un espacio y otro a la derecha del palito */
    } else {
      printf(" .  "); /* A la izquierda un espacio y dos a la derecha del punto */
    }
  }

  /* -- Paso 3 Imprimimos los d�as del mes con un salto de linea cuando posici�n sea 7 --
        posicion acumula el valor contado del bucle anterior */

  for (int k = 1; k <= diaMes; k++) {
    posicion++; /* Cada iteracci�n posici�n suma uno */
    if (( posicion % 7 ) == 0) { /* Para que haya un salto de l�nea tras el 7.� caracter */
      if (DiaVenta(k)) {/*Si hubo venta ese d�a*/
        CambiarColor(MarcaDia);/*Ponemos el texto rojo claro*/
        printf(" X  \n");/*Imprimimos una X y un salto de l�nea*/
        Posicion(1,PosicionAct(Y));/*Ponemos un margen de un espacio con la misma posici�n vertical*/
        printf("  ");/*Imprimimos dos espacios en blanco delante de la primera casilla del calendario*/
        CambiarColor(CCalendario);/*Volvemos al color anterior*/
      } else { /*Si no hubo venta ese d�a*/
        printf("%2d  \n", k);/*Imprimimos el n�mero de d�a y un salto de l�nea*/
        Posicion(1,PosicionAct(Y));
        printf("  ");/*Imprimimos dos espacios en blanco delante de la primera casilla del calendario*/
      }
    }

    else if ((( posicion + 2) % 7) == 0) { /* Para que haya un palito tras el 5.� caracter */
      if (DiaVenta(k)) {/*Si hubo venta ese d�a*/
        CambiarColor(MarcaDia);/*Ponemos el texto rojo claro*/
        printf(" X ");/*Imprimimos una X*/
        CambiarColor(CCalendario);/*Volvemos al color anterior*/
        printf("| ");/*Imprimimos el palito*/
      } else {/*Si no hubo venta ese d�a*/
        printf("%2d | ", k); /* imprimimos el n�mero de d�a y el palito*/
      }
    } else {
      if (DiaVenta(k)) { /* Si hubo venta ese d�a*/
        CambiarColor(MarcaDia);/*Ponemos el texto rojo claro*/
        printf(" X  ");/* Imprimimos una X*/
        CambiarColor(CCalendario);/*Volvemos al color anterior*/
      } else {/* Si no hubo venta ese d�a*/
        printf("%2d  ", k); /*Imprimimos el n�mero de d�a*/
      }
    }
  }

  /* -- Paso 4  imprimimos un punto para completar la semama -- */

  while ((posicion % 7) != 0) {
    posicion++;/* Cada iteracci�n posici�n suma uno */
    if (((posicion + 2) % 7) == 0) {/* Si ocupar�a la posici�n 5 en la l�nea*/
      printf(" . | "); /* Imprimimos punto y palito. Todo separado por un espacio*/
    } else if ((posicion %7) == 0) {/*Si ocupar�a la posici�n 7 en la l�nea*/
      printf(" .  \n");/* Imprimimos punto y salto de l�nea*/
      Posicion(1,PosicionAct(Y));/*Ponemos un margen de un espacio con la misma posici�n vertical*/
      printf("  ");/*Imprimimos dos espacios en blanco delante de la primera casilla del calendario*/
    } else { /* Resto de posiciones*/
      printf(" .  ");/*Imprimimos punto*/
    }
  }
  printf("%29c", char(32) );/*Imprimimos la parte inferior de la caja del calendario*/
  CambiarColor(Aplicacion);/*Restablecemoslos colores por defecto*/
  printf("\n\n");/*2 saltos de l�nea debajo de la caja del calendario

         ==================================================
                  Lista de ventas del mes
         ==================================================

 -- Si hay m�s de 11 d�as de venta se imprime en dos columnas y en 3 si hay m�s de 22 --                       */

  for (int k = 0;k <= 10; k++) {
    if (k <= contador) {
      /*Primera columna*/
      printf("- %2d: ", diasListados[k]); /*Imprimimos gui�n y el d�a*/
      if (unidadesListadas[k] >= 0) {
        CambiarColor(DatoPositivo); /*Si las unidades son positivas color texto aguamarina*/
      } else {
        CambiarColor(DatoIntroNegativo); /*Si son negativas color texto rojo oscuro*/
      }
      printf("%*d ",MaxDigitos(unidadesListadas,contador),unidadesListadas[k]);/*Imprimimos las unidades*/
      CambiarColor(Aplicacion);/*Restablecemos los colores anteriores*/
      printf("%s",Plural(unidadesListadas[k]));/*Imprimimos unidad o unidades*/
    }
    /* Segunda columna */
    if (k+11 <= contador) {/*Comprobamos que hay registro que imprimir en la segunda columna*/
      printf(" | - %2d: ", diasListados[k+11]);/*Imprimimos un palito separador, gui�n y el d�a*/
      if (unidadesListadas[k+11] >= 0) {
        CambiarColor(DatoPositivo);/*Si las unidades son positivas color texto aguamarina*/
      } else {
        CambiarColor(DatoIntroNegativo);/*Si son negativas color texto rojo oscuro*/
      }
      printf("%*d ",MaxDigitos(unidadesListadas,contador),unidadesListadas[k+11]);/*Imprimimos las unidades*/
      CambiarColor(Aplicacion);/*Restablecemos los colores anteriores*/
      printf("%s",Plural(unidadesListadas[k+11]));/*Imprimimos palabra unidad o unidades*/
    }
    /* Tercera columna */
    if (k+22 <= contador) {/*Comprobamos que hay registro que imprimir en la segunda columna*/
      printf(" | - %2d: ", diasListados[k+22]);/*Imprimimos un palito separador, gui�n y el d�a*/
      if (unidadesListadas[k+22] >= 0) {
        CambiarColor(DatoPositivo);/*Si las unidades son positivas color texto aguamarina*/
      } else {
        CambiarColor(DatoIntroNegativo);/*Si son negativas color texto rojo oscuro*/
      }
      printf("%*d ",MaxDigitos(unidadesListadas,contador),unidadesListadas[k+21]);/*Imprimimos las unidades*/
      CambiarColor(Aplicacion);/*Restablecemos los colores anteriores*/
      printf("%s",Plural(unidadesListadas[k+22]));/*Imprimimos palabra unidad o unidades*/
    }
    if (k <= contador) {
      printf("\n");/*Un salto de l�nea tras cada iteraci�n*/
    }
  }
  printf("\n\n");/*Imprimimos dos saltos debajo de la lista de registros*/
  contador = -1;/* Ponemos contador temporal en posici�n inicial*/
}

/* ====================================
   Submen� para acceder a Calendario
   ==================================== */

void Calendario() {
  /* -- Variables locales -- */
  TipoVector verMes; /*Vector donde se almacenar�n los valores dados a cada elemento*/
  //char tecla;
  /* -- Inicio de pantalla -- */
  system("color B1"); /* Color pantalla fondo blanco, texto negro */
  printf("MOSTRAR CALENDARIO DE VENTAS\n\n");/* Imprimimos cabecera */
  AyudaSubmenu(producto);
  Posicion(0,3);/*Ponemos cursor al principiode la tercera fila*/
  Control(producto, verMes[1]); /* Ejecutamos procedimiento que pregunta el producto y escanea la respuesta */
  if (GetAsyncKeyState(VK_ESCAPE)) { /* Si se pulsa ESC se cierra el subprograma y se vuelve al men� */
    return;
  }
  /* --Bloque que se repetir� hasta que se de un mes en que se vendi� el producto dado anteriormente --     */

  do {
    AyudaSubmenu(mes);
    Posicion(0,5);/*Posici�n de la pregunta del mes*/
    Control(mes,verMes[4]);/*Ejecutamos procedimiento que pregunta el mes y escanea la respuesta*/
    if (GetAsyncKeyState(VK_ESCAPE)) {/*Si se pulsa ESC se cierra el subprograma y se vuelve al men�*/
      return;
    }
    /* Si no se ha vendido el producto dado en ese mes */
    if (!Buscar(verMes[1], NULL,verMes[4], NULL))  {
      Posicion(13,5);/* Ponemos cursor al final de la pregunta del mes*/
      printf("%67c\n",char(32));/*Se borra el dato aportado*/
      Posicion(1,4);/* Ponemos cursor encima de la pregunta*/
      CambiarColor(Error);/*Color fondo rojo claro, texto blanco brillante*/
      printf("\a Aviso: No existe nig\243n registro de venta del producto %d en %s.\n",
             verMes[1], nombreMeses[verMes[4]]);/*Imprimimos aviso*/
      CambiarColor(Submenu);/*Restablecemos los colores anteriores*/
    }

  } while (!Buscar(verMes[1],NULL,verMes[4],NULL));
  /*Salimos del while cuando el mes es correcto*/
  Posicion(1,4); /* Ponemos cursor en el mensaje de error*/
  printf("%79c\n",char(32)); /*Se borra el mensaje de error

  -- Bloque que se repetir� hasta que se de un a�o en que se vendi� el producto en el mes dado
  anteriormente --                                */

  do {
    AyudaSubmenu(anno);
    Posicion(0,7); /* Posici�n de la pregunta del a�o*/
    Control(anno,verMes[5]);/*Ejecutamos procedimiento que pregunta el a�o y escanea la respuesta*/
    if (GetAsyncKeyState(VK_ESCAPE)) {/*Si se pulsa ESC se cierra el subprograma y se vuelve al men�*/
      return;
    }
    if (!Buscar(verMes[1],NULL, verMes[4], verMes[5])) {
      Posicion(18,7);/* Ponemos cursor al final de la pregunta del a�o*/
      printf("%62c\n",char(32));/*Borramos el valor dado*/
      Posicion(1,6);/* Ponemos cursor encima de la pregunta del a�o*/
      CambiarColor(Error);/*Color fondo rojo claro, texto blanco brillante*/
      printf("\a Aviso: No existe nig\243n registro de venta del producto %d en %s del a\244o %d.\n",
             verMes[1], nombreMeses[verMes[4]],verMes[5]);/*Imprimimos mensaje de aviso */
      CambiarColor(Submenu);/*Restablecemos los colores anteriores*/

    }
  } while (!Buscar(verMes[1],NULL, verMes[4], verMes[5]));

  /*-- Una vez el a�o es correcto, salimos del while y se pasa a la aplicai�n --*/

  system("CLS"); /* Limpiamos pantalla */
  system("color 70"); /* color pantalla: Fondo blanco , texto negro*/
  printf("MOSTRAR CALENDARIO DE VENTAS\n\n");/*Imprimimos cabecera*/

  /*Se ejecuta el subprograma Calendario*/

  imprimir.Calendario(verMes[4],verMes[5],verMes[1]);
  Posicion(1,PosicionAct(Y));/*Ponemos un margen de un espacio con la misma posici�n vertical*/
  CambiarColor(Ayuda);/*Fondo azul, texto blanco*/
  printf("%46c\n",char(32));/*Imprimimos parte superior de la caja*/
  Posicion(1,PosicionAct(Y));/*Ponemos un margen de un espacio con la misma posici�n vertical*/
  printf(" Presione cualquir tecla para volver al men\243. \n");/*Imprimimos pie de pantalla*/
  Posicion(1,PosicionAct(Y));/*Ponemos un margen de un espacio con la misma posici�n vertical*/
  printf("%46c",char(32));/* Imprimimos parte inferior de la caja*/
  CambiarColor(Aplicacion);/*Restablecemos colores*/
  getch();/* Mantiene la pantalla en espera de que pulsemos una tecla */
}


