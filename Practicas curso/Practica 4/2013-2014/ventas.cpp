/*****************************************************************************************
* Programa: ventas
*
* Descripci�n: Este programa archiva, organiza y selecciona datos de ventas de una empresa
*
******************************************************************************************/

/* ====================================
    M�dulos y librer�as est�ndar usados
   ==================================== */

#include <stdio.h> /* Para prinf() y scanf() */
#include <stdlib.h> /* Para system() */
//#include <locale.h> /* Para setlocale() */
#include <conio.h> /* Para getch() */
#include "calendario.h"
#include "borrarventa.h"
#include "listarventas.h"
#include "auxiliar.h"

/* ====================================
    Variables globales
   ==================================== */

char opcion;/*Variable que contendr� la tecla pulsada en el men�*/
bool salir = true;/*Variable para la condici�n de while del programa principal con valor inicial true*/

/*======================================================
   Procedimiento para salir del programa
  ========================================================*/

void Salir() {
  int msgBoxID;/*Variable que contendr� la respuesta del MessageBox*/
  system("color 3F");/*Cambiamos el color de la ventana de men� */
  fflush(stdin);/*Limpiamos buffer*/
  /*Creamos una ventana de mensaje de Windows*/
  msgBoxID = MessageBox ( /* Par�meros: */
               /* 1.Identificador de propiedad de la ventana*/
               NULL, /* No asignamos ninguna propiedad
                  2. Mensaje que aparecer� en la ventana */
               "Si sale del programa perder� los registros introducidos\n�Est� seguro que quiere salir?",
               /* 3. T�tulo de la ventana */
               "Salir de Ventas",
               /* 4. Opciones de la ventana */
               MB_YESNO /*Botones si no*/ + MB_ICONQUESTION /* Icono de pregunta.
    Ambos son de tipo enum y representan los n�mero enteros 4+32*/
             );

  if (msgBoxID == IDYES) { /*Si apretamos el bot�n s� IDYES es un tipo enum que representa el entero 6*/
    system("CLS"); /* Limpiamos pantalla */
    system("color 07");/*Ponemos el color de la pantalla con fondo negro y texto blanco*/
    salir = false; /* Damos valor false a la variable salir para que salgamos del while del programa principal (main)*/
  }
  fflush(stdin);/* Limpiamos buffer */
}

/* ======================================================================
   Funci�n para comprobar que la tcla pulsada en el men� es una
   opci�n v�lidad
   ======================================================================*/

bool TeclaValida(char tecla) {

  /* -- Variables locales de la funci�n -- */

  typedef char ArrayTeclas[8]; /* Vector que contendr� las teclas v�lidas*/
  bool esValida = false;/*Variable booledana con valor inicial false que cambiar� a true si encuentra el caracter
                          como v�lido y que devolver� la funci�n*/
  int opciones; /* Variable que contendr� el numero partes del vector*/

  /* -- Creamos el vector con las teclas v�lidas -- */

  ArrayTeclas teclasValidas = {'5'}; /* Teclas que son v�lidad en cualquier caso*/
  if (numeroRegistro == -1) {
    /* Caso 1: Si no hay ning�n registro a�adimo como v�lida la tecla 1*/
    opciones = 2;/*Total 2 opciones contando las que ya tiene el vector,1 */
    teclasValidas[1] = '1';
  } else if (numeroRegistro == maximo) {
    /* Caso 2: Si hay 1000 registros (maximo) a�adimos como v�lidas las teclas 2,3 y 4*/
    teclasValidas[1] = '3';
    teclasValidas[2] = '2';
    teclasValidas[3] = '4';
    opciones = 4;/*Total 4 opciones contando las que ya tiene el vector, 3*/
  } else {
    /* Caso 3: En el resto de casos a�adimos las teclas 1,2,3 y 4 */
    opciones = 5;/*Total 5 opciones contando las que ya tiene el vector, 3*/
    teclasValidas[1] = '1';
    teclasValidas[2] = '3';
    teclasValidas[3] = '2';
    teclasValidas[4] = '4';
  }

  /* -- Buscamos si est� la tecla introducida entre las teclas v�lidas -- */

  for (int k = 0; k <= opciones; k++) { /* Recorremos el vector hasta opciones */
    if (tecla == teclasValidas[k]) { /* Comparamos el caracter introducido con los del vector */
      esValida = true;/* Si hay coincidencia la variable esValida cambia su valor a true */
    }
  }
  return esValida; /*Devuelve la variable esValida*/
}

/*=====================================================
  Procedimiento que seleciona las opciones del men�
====================================================*/

void Opcion() {

  switch (opcion) {
  case '1': /* Si pulsamos 1 */
    system("CLS");/*Limpiamos pantalla*/
    IntroVenta();/*Se ejecuta el subprograma IntroVenta()*/
    break;
  case '2':/* Si pulsamos 2 */
    if (numeroRegistro != -1) {/*Comprobamos que hay registros guardados*/
      system("CLS");/*Limpiamos pantalla*/
      EliminarVenta();/*Se ejecuta el subprograma EliminarVenta()*/
    }
    break;
  case '3':/* Si pulsamos 3 */
    system("CLS");/*Limpiamos pantalla*/
    Calendario();/*Se ejecuta el subprograma Calendario()*/
    break;
  case '4':/* Si pulsamos 4 */
    if (numeroRegistro != -1) {/*Comprobamos que hay registros guardados*/
      system("CLS");/*Limpiamos pantalla*/
      ListaVentas();/*Se ejecuta el subprograma ListaVentas()*/
    }
    break;
  case '5':/* Si pulsamos 5 */
    Salir();/*Se ejecuta el subprograma Salir()*/
    break;
  }
}
/*=====================================================
  Procedimiento del Men� de inicio
====================================================*/

void Menu () {
  system("color 3F"); /* Color de la pantalla del men� */
  Posicion(3,1); /* Posici�n de la cabecera de la pantalla*/
  printf("ANALIZADOR DE VENTAS"); /* Imprimimos cabecera */
  CambiarColor(OpcionMenu); /* Cambiamos el color para caja de opciones a fondo azul y texto aguamarina claro*/
  Posicion(2,3); /* Posici�n de la caja de opciones */
  printf("%41c\n",char(32)); /*Imprimimos parte superior de la caja de opciones*/
  Posicion(2,4); /* Posici�n opci�n 1*/
  if (numeroRegistro == maximo) {
    /*Si hay m�s de 1000 registros (maximo) aparecer� el texto en color rojo oscuro*/
    CambiarColor(MenuInhabilitado);
  }
  printf(" 1. Introducir venta%21c", char(32)); /* Imprimimos opci�n 1*/
  CambiarColor(OpcionMenu);/*Restablemos color texto verde claro*/
  Posicion(2,5);/* Posici�n opci�n 2*/
  if (numeroRegistro == -1) {
    /*Si no hay  registros guardados aparecer� en un color rojo oscuro*/
    CambiarColor(MenuInhabilitado);
  }
  printf(" 2. Eliminar venta%23c", char(32));/* Imprimimos opci�n 2*/

  Posicion(2,6); /* Posici�n opci�n 3*/
  printf(" 3. Listar ventas de un mes%14c", char(32));/* Imprimimos opci�n 3*/
  Posicion(2,7); /* Posici�n opci�n 4*/
  printf(" 4. Listar productos por orden de ventas%c", char(32));/* Imprimimos opci�n 4 */
  Posicion(2,8); /* Posici�n parte inferior de la caja de opciones*/
  printf("%41c\n",char(32));/* Imprimimos una l�nea sin texto en caja de opciones*/
  Posicion(2,9); /* Posici�n opci�n 5*/
  CambiarColor(MenuSalir); /*Cambiamos el color a texto verde claro con el mismo fondo */
  printf(" 5. Salir%32c", char(32)); /* Imprimimos opci�n 5 salir */
  Posicion(2,10); /* Posici�n de la parte inferior de la caja de opciones*/
  printf("%41c\n",char(32)); /* Imprimimos la parte inferior de la caja de opciones */
  CambiarColor(PreguntaMenu);/* Cambiamos el color a fondo aguamarina oscuro y texto blanco */
  Posicion(2,12); /* Posici�n pregunta: Opci�n?*/
  printf("Opci\242n ? ");/*Imprimimos la pregunta Opci�n?*/
  fflush(stdin); /* Limpiamos buffer*/
  opcion = getche(); /* Pedimos la pulsaci�n de una tecla y guardamos lo pulsado en la variable opci�n*/
  if (opcion == 13) {
    /*Convertimos la tecla INTRO (int 13) en 1 */
    opcion = '1';
  } else if (opcion == 27) {
    /*Convertimos la tecla ESC (int 27) en 5 */
    opcion = '5';
  }


  while ((!TeclaValida(opcion))&&(opcion != 27)) {/*Si se pulsa una tecla no v�lida mostrar� un
                                                   mensaje de error mientras no se pulse una tecla v�lida
                                                   del mensaje se sale con ESC (int 27)*/
    CambiarColor(Error); /*Fondo color rojo claro y texto blanco brilante */
    Posicion(2,12); /* Posici�n de la parte superior de la caja de error*/
    printf("%60c", char(32));/*Imprimimos la parte de arriba de la caja del mensaje de error (l�nea vac�a)*/
    Posicion(2,13); /* Posici�n 1.� l�nea del mensaje de error */
    /*Mensaje que se imprimir� cuando se pulsa teclas 2, 3 o 4 y no hay registros guardado*/
    if (((numeroRegistro == -1)) && ( (opcion == '3') || (opcion == '4') || (opcion == '2'))) {
      printf(" No hay registros guardados%33c", char(32));
      Posicion(2,14);/*Posici�n 2.� l�nea del mensaje de error*/
      printf(" Pulse 1 para introducir un registro%24c",char(32));
      /*Mensaje que se imprimir� cuando se pulsa tecla 1 y hay 1000 registros */
    } else if ((numeroRegistro == maximo) && (opcion == '1')) {
      printf(" Se han completado los 1000 registros permitidos%12c", char(32));
      Posicion(2,14);/*Posici�n 2.� l�nea del mensaje de error*/
      printf(" Antes de introducir m\240s debe borrar alguno%17c",char(32));
    } else {
      /*Mensaje que se imprimir� si se pulsa una tecla no v�lida*/
      printf(" ERROR: Selecci\242n no v\240lida%33c", char(32));
      Posicion(2,14);/*Posici�n 2.� l�nea del mensaje de error/*

      /*Mensaje de error que se imprimir� si pulsamos una tecla incorrecta dependiendo de*/
      if (numeroRegistro == -1) {
      /* 1. Si no hay registros guardados*/
        printf(" Ha pulsado la tecla %c y tiene que pulsar: 1, o 5%11c",opcion,char(32));
      } else if (numeroRegistro == maximo) {
       /* 2. Si hay entre 1 y 999 registros*/

        printf(" Ha pulsado la tecla %c y tiene que pulsar: 2, 3, 4, o 5%5c",opcion,char(32));
      } else {
       /* 3. En el resto de casos*/
        printf(" Ha pulsado la tecla %c y tiene que pulsar: 1, 2, 3, 4, o 5  ",opcion);
      }
    }
    Posicion(2,15);/*Posici�n de la parte inferior del mensaje de eror*/
    printf("%60c", char(32)); /* Imprimimos la parte inferior de la caja del mensaje de error*/
    CambiarColor(PreguntaMenu);
    Posicion(2,17);
    printf("Opci\242n ? ");/*Se vueve a imprimir la pregunta opci�n? debajo de la caja del mensaje de error*/
    fflush(stdin); /* Limp�amos buffer por si acaso */
    opcion= getch();/*Se vuelve a preguntar por la opci�n*/
    if (opcion == 13) {/*Se le vuelve a dar valor 1 a INTRO*/
      opcion = '1';
    }
  }

  /*-- Una vez que salimos del bucle --*/

  Posicion(2,12);/*Posici�n inicial del mensaje de error*/
  for (int k = 1; k <= 6;k++) { /* Limpiamos mensaje de error y la pregunta opci�n.
                                    Esto es para que no coincida si despu�s de un error le damos a salir*/
    printf("%60c\n", char(32)); /* Se borra una l�nea en cada iteraci�n hasta 6 */
  }

  /* -- Se ejecuta el procedimiento Opci�n() que abrira el subprograma correspondiente a la opci�n
       o saldremos del programa -- */

  Opcion();
}

/* ====================================
    Parte principal del programa
   ==================================== */

int main() {
  //setlocale(LC_ALL, "spanish"); /* Ponemos la configuraci�n de idioma espa�ol */
  system("MODE CON cols=85 lines=32");/*Ajustamos tama�o de la pantalla*/
  system("TITLE Ventas"); /*Ponemos que aparezca ventas como nombre de la ventana*/

  /*-- Bloque que se ejecutar� hasta que salir sea false -- */
  do {
    system("CLS");/*Limpiamos pantalla*/
    Menu();/*Ejecutamos el subprograma men�*/
  } while (salir);
}
/*Cuando salir sea igual a false salimos del programa*/
