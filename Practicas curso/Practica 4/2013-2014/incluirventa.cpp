/*********************************************************************
* M�dulo: incluirventa
*
* Este m�dulo contiene los elementos para incluir un registro de venta
*
**********************************************************************/

/* ====================================
    M�dulos y librer�as est�ndar usados
   ==================================== */

#include <stdio.h> /* Para prinf() y scanf() */
#include <stdlib.h> /* Para sytem() */
#include <conio.h> /* Para getch() */
#include "incluirventa.h"
#include "auxiliar.h"
#include "calendario.h"

/* ====================================
    Variables globales
   ==================================== */

int numeroRegistro = -1;/* Damos valor inicial a la variable que contara el n�mero de registros*/
TipoRegistro codigoVenta;/* Vector que contendr� los registros introducidos*/
int productoAnterior = 1;/* Variable que contendr� el �ltimo producto introducido. Le damos valor inicial 1 que
                           ser� el valor que tome en el primer registro que introduzcamos*/
TipoDia diasMes;
/* ====================================
   Procedimiento para introducir una venta
   ==================================== */

void IntroVenta() {
/*-- Variables locales --*/
  TipoVector IntroDatos; /* Vector para guardar temporalmente los datos introducidos*/
  bool existe = false; /* variable boledana que utilizaremos al final para comprovar si existe un registro del mismo dia y produzto*/
  int regEncontrado, regTotal, codigo, digUnidades, opcion,rep, index;
  char tecla;/*Variable que contendr� la tecla pulsada en la funci�n getch()*/
  bool esc = false;/*Variable que se p�sar� comoreferencia al procedimiento teclado que cambiar�
                     su valor a true si se pulsa ESC*/

  /*-- Iniciamos pantalla --*/

  system("color B1"); /* Color Aguamarina claro con texto negro */
  printf("INTRODUCIR VENTA"); /* Imprimimos cabecera */

  /*-- Bloque que se repetir� mientras no se introduzca una fecha correcta --*/

  do {
    AyudaSubmenu(dia);/*Caja de ayuda con opciones para dia*/
    Posicion(0,3);/*Ponemos cusor en l�nea 4*/
    Control(dia,IntroDatos[3]);/*Ejecutamos procedimiento que pregunta el d�a y escanea la respuesta*/
    if (GetAsyncKeyState(VK_ESCAPE)) {/*Si se pulsa ESC se cierra el subprograma y se vuelve al men�*/
      return;
    }
    AyudaSubmenu(mes);/*Caja de ayuda con opciones para mes*/
    Posicion(0,5);/*Ponemos cusor en l�nea 6*/
    Control(mes,IntroDatos[4]);/*Ejecutamos procedimiento que pregunta el mes y escanea la respuesta*/
    if (GetAsyncKeyState(VK_ESCAPE)) {/*Si se pulsa ESC se cierra el subprograma y se vuelve al men�*/
      return;
    }
    AyudaSubmenu(anno);/*Caja de ayuda con opciones para a�o*/
    Posicion(0,7);/*Ponemos cusor en l�nea 8*/
    Control(anno,IntroDatos[5]);/*Ejecutamos procedimiento que pregunta el a�o y escanea la respuesta*/
    if (GetAsyncKeyState(VK_ESCAPE)) {/* Si se pulsa ESC se cierra el subprograma y se vuelve al men�*/
      return;
    }
    /*-- Comprobamos que la fecha es correcta --*/

    if (diasMes.Mes(IntroDatos[4],IntroDatos[5])< IntroDatos[3]) {
      /* Si no es correcta la fecha */
      CambiarColor(Error); /* Color fondo rojo texto blanco brillante */
      Posicion(1,9);/*Ponemos cursor en la posici�n donde debe ir el mensajede error*/
      printf("\a \250%02d/%02d/%d? %s tiene %d d\241as \n",IntroDatos[3],IntroDatos[4],IntroDatos[5],nombreMeses[IntroDatos[4]],diasMes.Mes(IntroDatos[4],IntroDatos[5]));
      Posicion(1,10);/*Ponemos cursor en la siguiente l�nea con 1 espacio*/
      printf(" ERROR: la fecha no es correcta.    \n\n");
      CambiarColor(Submenu); /* Volvemos al color anterior */
      CambiarColor(Ayuda);/*Color fondo azul, texto blanco*/
      Posicion(0,19);/*Misma posici�n que la caja de ayuda*/
      printf("%67c\n",char(32));/*imprimimos parte superior de la caja*/
      printf("%67c\n",char(32));/* otra l�nea para que coincida con la caja de ayuda*/
      printf(" Presiona cualquier tecla para volver a introducir la fecha    \n");/*Imprimimos mensaje*/
      printf("%67c\n",char(32));/* otra l�nea para que coincida con la caja de ayuda*/
      printf("%67c\n",char(32));/*imprimimos parte inferior de la caja*/
      CambiarColor(Submenu); /* Volvemos al color anterior */
      getch(); /* Ponemos una pausa hasta que se pulse un tecla */
      if (GetAsyncKeyState(VK_ESCAPE)) {/* Si se pulsa ESC se cierra el subprograma y se vuelve al men�*/
      return;
      }
      system("CLS");/* Limpiamos pantalla */
      Posicion(0,0);/*Ponemos el cursor al principio de la pantalla*/
      printf("INTRODUCIR VENTA");/*Imprimimos cabecera*/
    }
  } while (diasMes.Mes(IntroDatos[4],IntroDatos[5]) < IntroDatos[3]);
  /*Cuando introducimos una fecha correcta salimos de while*/
  AyudaSubmenu(producto);/*Caja de ayuda con opciones para producto*/
  CambiarColor(PreguntaSubmenu);/*Color texto azul*/
  Posicion(0,9);/*Ponemos el cursor donde debe ir la pregunta de producto*/
  printf("Identificador de Producto ? ");/* Imprimimos pregunta*/
  CambiarColor(Submenu);/*Restablecemos color texto negro*/
  Teclado(&IntroDatos[2],productoAnterior,&esc);/* Escaneamos la respuesta */
  Posicion(28,9); /* Ponemos el cursor al final de la pregunta y antes del valor dado */
  printf("%80c",char(32));/* Borramos el valor dado */
  Posicion(28,9);/*Volvemos a ponemos el cursor al final de la pregunta */
  printf("%d",IntroDatos[2]); /*Imprimimos el valor de nuevo pero con esto nos aseguramos que esta en su sitio y ordenado*/
  if (esc) {/*Si se pulsa ESC se cierra el subprograma y se vuelve al men�*/
    return;
  }
  AyudaSubmenu(unidades);/*Caja de ayuda con opciones para unidades*/
  CambiarColor(PreguntaSubmenu);/*Cambiamos color texto a azul claro*/
  Posicion(0,11);/*Ponemos cursor en el sitio de pregunta de unidades*/
  printf("Unidades ? ");/*Imprimimos pregunta*/
  CambiarColor(Submenu);/*Restablecemos color texto negro*/
  Teclado(&IntroDatos[1],1,&esc);/* Escaneamos la respuesta */
  if (esc) {/*Si pulsamos ESC cerramos el subprograma y volvemos al men�*/
    return;
  }

  /* --Buscamos los datos introducidos en el registro por si habria uno del mismo dia y producto -- */


  for (int k = 0; k<= numeroRegistro; k++) { /* Recorremos el vector hasta el contador de registros*/
    if ((IntroDatos[2] == codigoVenta[k].producto) &&(IntroDatos[3] == codigoVenta[k].fecha.dia) && (IntroDatos[4] == codigoVenta[k].fecha.mes) && (IntroDatos[5] == codigoVenta[k].fecha.anno)) {
      regEncontrado = codigoVenta[k].unidades; /* Si encuentra un produsto guardamos las unidades para el mensaje*/
      index = k;
      regTotal = codigoVenta[k].unidades+IntroDatos[1]; /* Guardamos el total para el mensaje */
      existe = true; /* Damos el valor verdadero a la variable existe*/
    }
  }

  /*-- Cambio de pantalla --*/

  system("CLS"); /* Limpiamos la pantalla */
  system("color 70"); /* Fondo blanco texto negro */
  printf("INTRODUCIR VENTA\n\n");/* Imprimimos cabecera en la siguiente pantalla*/

  /* -- Averiguamos la cifra con m�s d�gitos --*/

  if (NumDigitos(regEncontrado,true) < (NumDigitos(IntroDatos[1],false)+1)) {
    digUnidades = NumDigitos(IntroDatos[1],false)+1;/*Si tiene m�s d�gitos el valor que hab�a*/
  } else  {
    digUnidades = NumDigitos(regEncontrado,true);/*Si tiene m�s d�gitos el valor nuevo*/
  }
  if (NumDigitos(regEncontrado+IntroDatos[1],true) > digUnidades) {
    digUnidades = NumDigitos(regEncontrado+IntroDatos[1],true); /* Si tiene m�s d�gitos la suma de ambos */
  }

  /* -- Mensaje si existe un registro del mismo producto y el mismo d�a --*/

  if (existe) {
    Posicion(1,PosicionAct(Y)+1);/*Ponemos un margen de un espacio con la misma posici�n vertical*/
    CambiarColor(Hecho);/* Color texto blanco fondo verde */
    printf(" Registro para fusionar \n\n");
    CambiarColor(Aplicacion);/* Color texto negro fondo gris claro*/
    printf("\nExiste un registro de ventas de ese producto para el mismo d\241a\nal que se sumar\240n las unidades introducidas.\n\n",Plural(IntroDatos[1]));
    CambiarColor(PreguntaAplicacion); /* Color texto azul oscuro fondo gris claro*/
    printf(" * Registro existente:   %02d/%02d/%d, producto %d: %*d %s\n",IntroDatos[3],IntroDatos[4],IntroDatos[5], IntroDatos[2],digUnidades, regEncontrado,Plural(regEncontrado));
    if (IntroDatos[1] > 0) {
      CambiarColor(DatoPositivo);/*Si la unidadades introducidas son positivas texto aguamarina*/
    } else {
      CambiarColor(DatoNegativo);/*Si la unidadades introducidas son negativas texto rojo claro*/
    }
    printf(" * Registro introducido: %02d/%02d/%d, producto %d: %+*d %s\n",IntroDatos[3],IntroDatos[4],IntroDatos[5], IntroDatos[2],digUnidades, IntroDatos[1],Plural(IntroDatos[1]) );
    CambiarColor(Aplicacion); /* Color texto negro fondo gris claro*/
    printf("%3c",char(32));/*Espacio en blanco delante de la l�nea de guiones*/
    rep = 1;
    while (rep != (56 + digUnidades + NumDigitos(IntroDatos[2],true))) {
      printf("%c",char(45));/* Repetimos un gui�n seg�n sean las cifras de largas*/
      rep++;
    }
    printf("\n");
    if (regTotal > 0) {
      CambiarColor(DatoIntroPositivo);/* Si la suma sale positiva texto color verde*/
    } else {
      CambiarColor(DatoIntroNegativo);/* Si la suma sale negativa texto color rojo*/
    }
    printf(" * Registro a guardar:   %02d/%02d/%d, producto %d: %*d %s\n\n",IntroDatos[3],IntroDatos[4],IntroDatos[5], IntroDatos[2],digUnidades, regTotal, Plural(regTotal));
    CambiarColor(PreguntaAplicacion);
    do {
      Posicion(0,15);
      printf("\250Guardar cambios? (s/n) ");
      fflush(stdin);
      CambiarColor(PreguntaAplicacion);
      tecla = getch();
      if (tecla == 13) {
        tecla = 's';/* Para que si se pulsa intro no se estropee el mensaje ya que intro borra */
      }
      CambiarColor(Aplicacion);
      if ((tecla == 's') || (tecla == 'S')) {
        codigoVenta[index].unidades = codigoVenta[index].unidades + IntroDatos[1];
        productoAnterior = IntroDatos[2];
        system("CLS");
        printf("INTRODUCIR VENTA\n\n");
        Posicion(1,2);
        CambiarColor(Hecho);
        printf(" Registro fusionado \n");
        CambiarColor(Aplicacion); /* Color texto negro fondo blanco*/
        printf("\nSe ha guardado el siguiente registro:\n\n");
        if (regTotal > 0) {
          CambiarColor(DatoIntroPositivo);/*Si las unidades son positivas texto color verde*/
        } else {
          CambiarColor(DatoIntroNegativo);/*Si las unidades con negativa texto color rojo*/
        }
        printf("%02d/%02d/%d, producto %d: %d %s\n\n",IntroDatos[3],IntroDatos[4],IntroDatos[5],IntroDatos[2], regTotal, Plural(regTotal));

      } else if ((tecla == 'n') || (tecla == 'N')) {
        Posicion(0,2);
        system("CLS");
        printf("INTRODUCIR VENTA\n\n");
        CambiarColor(NoHecho);
        Posicion(1,2);
        CambiarColor(NoHecho);/* Color texto negro fondo amarillo claro*/
        printf("\n Operaci\242n cancelada \n\n");
        CambiarColor(Aplicacion);/* Color texto negro fondo blanco*/
      } else {
        CambiarColor(Error);/* Color texto blanco brillante fondo rojo claro*/
        Posicion(1,14);
        printf("\a Ha pulsado %c y tiene que pulsar s o n\n",tecla);
        CambiarColor(PreguntaAplicacion);
        printf("%50c\n",char(32));
      }
    } while (!((tecla == 'n') || (tecla == 'N') || (tecla == 's') || (tecla == 'S')));
  } else {

    /* -- Mensaje si no existe un registro del mismo producto y el mismo d�a --*/

    CambiarColor(Titulo);/* Color texto azul fondo blanco */
    printf("\nRegistro introdudido \n\n");
    if (IntroDatos[1] > 0) {
      CambiarColor(DatoIntroPositivo);/*Si las unidades son positivas texto color verde*/
    } else {
      CambiarColor(DatoIntroNegativo);/*Si las unidades con negativa texto color rojo*/
    }
    printf("%02d/%02d/%d, producto %d: %d %s\n\n",IntroDatos[3],IntroDatos[4],IntroDatos[5],IntroDatos[2],IntroDatos[1],Plural(IntroDatos[1]));
    CambiarColor(PreguntaAplicacion);
    /*-- Importamos los valores de la variable temporal al registro -- */
    do {
      Posicion(0,8);
      printf("\250Guardar? (s/n) ");
      CambiarColor(Aplicacion);
      fflush(stdin);
      tecla = getch();
      if (tecla == 13) {
        tecla = 's' ;/* INTRO (int 13) equivale a s */
      }
      Posicion(0,7);
      printf("%50c\n",char(32));
      printf("%50c\n",char(32));
      if ((tecla == 's') || (tecla == 'S')) {
        codigo = numeroRegistro +1;/*El codigo del registro guardado sera el contador de registros m�s 1 */
        numeroRegistro++; /* Sumamos 1 al contador de registros */
        codigoVenta[codigo].unidades = IntroDatos[1];
        codigoVenta[codigo].producto = IntroDatos[2];
        codigoVenta[codigo].fecha.dia = IntroDatos[3];
        codigoVenta[codigo].fecha.mes = IntroDatos[4];
        codigoVenta[codigo].fecha.anno= IntroDatos[5];
        productoAnterior = 0 + IntroDatos[2];
        Posicion(0,3);
        printf("%1c",char(32));
        Posicion(1,3);
        CambiarColor(Hecho);
        printf(" Registro guardado ");
        CambiarColor(Aplicacion);
        printf("%2c",char(32));
      } else if ((tecla == 'n') || (tecla == 'N')) {
        /* Color texto negro fondo amarillo claro*/
        Posicion(0,2);
        for (int k = 1;k<=4;k++) {
          printf("%80c\n",char(32));
        }
        CambiarColor(NoHecho);/*Color fondo amarilla, texto negro*/
        Posicion(1,2);
        printf(" Operaci\242n cancelada \n\n");
        CambiarColor(Aplicacion);/* Color texto negro fondo blanco*/
      } else {
        CambiarColor(Error);/* Color texto blanco brillante fondo rojo claro*/
        Posicion(1,7);
        printf("\a Ha pulsado %c y tiene que pulsar s o n \n",tecla);
        CambiarColor(PreguntaAplicacion);
        printf("%50c\n",char(32));
        CambiarColor(PreguntaAplicacion);
      }
    } while (!((tecla == 'n') || (tecla == 'N') || (tecla == 's') || (tecla == 'S')));

  }
  Posicion(1,8);/*Ponemos un margen de un espacio con la misma posici�n vertical*/
  CambiarColor(Ayuda);/*Color fondo azul texto blanco*/
  printf("%46c\n",char(32));/*Imprimimos parte superior de la caja*/
  Posicion(1,PosicionAct(Y));/*Ponemos un margen de un espacio con la misma posici�n vertical*/
  printf(" Presione cualquir tecla para volver al men\243. \n");/*Imprimimos pie de pantalla*/
  Posicion(1,PosicionAct(Y));/*Ponemos un margen de un espacio con la misma posici�n vertical*/
  printf("%46c",char(32));/* Imprimimos parte inferior de la caja*/
  getch();/* Mantiene la pantalla en espera de que pulsemos una tecla */
}



