/***************************************
* Interfaz de m�dulo: CalendarioAnual

Este m�dulo contiene los elementos para
una matriz 80x80 con el calendario anual mediante
la funcion iniciar ue crea una matriz 80x80 vac�a y
la funcion cargar datos que utilizando el modulo calendariomensual
crea la matriz 80x80 con el calendario anual.
*************************************************/

#pragma once

typedef char TipoMatriz[80][80];
typedef struct Pantalla{
  TipoMatriz matriz;
  void Iniciar();/**Se crea el tipo de dato pantalla con caracteres en blanco**/
  void CargarCalendario(int anno);/**Se crea el tipo de dato pantalla con el calendario anual**/
};


