/***************************************
* M�dulo: CalendarioAnual

Este m�dulo contiene los elementos para
generar un mes en el tipo de datos pantalla
Variables de entrada, mes y a�o. Variable de salida pantalla
*************************************************/

#include <stdio.h>
#include <string.h>
#include "CalendarioMensual.h"

int CM_NumCol=22;
int CM_NumFil=10;


int bisiesto(int ano){
  /*Devuelve 0 si no es bisiesto y 1 si es bisiesto*/
  int aux=0;
  if (ano % 4 == 0){
    aux=1;
    if (ano % 100 == 0){
      aux=0;
    }
    if (ano % 400 == 0){
        aux=1;
      }
    }
  return aux;
}

int diasmes (int mes, int anno){
  /*Devuelve el numero de d�as de cada mes, en febrero tiene en cuenta si es bisiesto*/
  int num_dias;
  num_dias=0;
  if (mes==1 || mes==3 || mes==5 || mes==7 || mes==8 || mes== 10 || mes==12){
    num_dias=31;
  }
  else if (mes==2){
    if (bisiesto(anno)==0){
      num_dias=28;
    }
    else{
      num_dias=29;
    }
  }
  else{
    num_dias=30;
  }
  return num_dias;
}

int totaldias(int mes, int anno){
  /*Devuelve el n�mero de dias transcurridos desde el 1/1/0000, si es el mes 12/2014, contabiliza los 31 d�as de noviembre del 2014 */
  int dias=0;
  for (int i=0; i<(anno); i++){
    if (bisiesto(i)==0){
      dias=dias+365;
    }
    else{
      dias=dias+366;
    }
  }
  for (int j=1; j<(mes); j++){
    dias=dias+diasmes(j,anno);
  }
  return dias;
}

int diasemana(int mes, int anno){
  /*Devuelve 1 si es lunes,2 si es marte, 3 mierc...7 domingo*/
  int dia_domingo, dia_uno, dif_dias_pasado, dif_dias_futuro, aux;
  /*fijamos un d�a que sabemos que fue domingo, este 16 de noviembre, la fecha introducida la compararemos con esta*/
  dia_domingo=totaldias(11,2014)+16;
  /*calculamos el numero de dias del d�a 1 del mes y a�o introducido*/
  dia_uno=totaldias(mes,anno)+1;
  if (dia_uno>=dia_domingo){
    /*Si el dia introducido es mayor que el domingo utilizado como comparador calculamos la diferencia de dias.
    calculando el m�dulo entre 7 sabemos si es LU=1,MA=2...DO=7*/
    dif_dias_futuro=dia_uno-dia_domingo;
    aux=dif_dias_futuro % 7;
    if (aux==0){
      aux=7;
    }
    return (aux);
  }
  else{
    /*Si el dia introducido es menor que el domingo utilizado como comparador calculamos la diferencia de dias.
    calculando la resta de 7 menos el m�dulo entre 7 sabemos si es LU=1,MA=2...DO=7*/
    dif_dias_pasado=dia_domingo-dia_uno;
    aux=7-(dif_dias_pasado % 7);
    if (aux==0){
      aux=7;
    }
    return (aux);
  }
}

void PantallaMes::IniciarMes(){
  /**Se crea el tipo de dato pantalla con caracteres en blanco**/
  for (int i=0; i<(CM_NumFil); i++){
    for (int j=0; j<(CM_NumCol); j++){
      matriz[i][j]=' ';
    }
  }
}
void PantallaMes::CargarMes(int mes, int anno){
  /**Se cargan los datos del mes, partiendo del a�o y mes introducido**/
  typedef char cadena[22];
  int dia_uno,i,n;
  cadena TextoMes,TextoAno,TextoDias,TextoMarco,TextoComienzo,TextoNum;
  switch(mes){
    case 1:strcpy(TextoMes,"ENERO             ");break;
    case 2:strcpy(TextoMes,"FEBRERO           ");break;
    case 3:strcpy(TextoMes,"MARZO             ");break;
    case 4:strcpy(TextoMes,"ABRIL             ");break;
    case 5:strcpy(TextoMes,"MAYO              ");break;
    case 6:strcpy(TextoMes,"JUNIO             ");break;
    case 7:strcpy(TextoMes,"JULIO             ");break;
    case 8:strcpy(TextoMes,"AGOSTO            ");break;
    case 9:strcpy(TextoMes,"SEPTIEMBRE        ");break;
    case 10:strcpy(TextoMes,"OCTUBRE           ");break;
    case 11:strcpy(TextoMes,"NOVIEMBRE         ");break;
    case 12:strcpy(TextoMes,"DICIEMBRE         ");break;
  }
  sprintf(TextoAno,"%d",anno);
  strcat(TextoMes, TextoAno);
  for (int j=0; j<22; j++){
    matriz[0][j]=TextoMes[j];
  }
  strcpy(TextoMarco,"======================");
  for (int j=0; j<22; j++){
    matriz[1][j]=TextoMarco[j];
    matriz[3][j]=TextoMarco[j];
  }
  strcpy(TextoDias,"LU MA MI JU VI | SA DO");
  for (int j=0; j<22; j++){
    matriz[2][j]=TextoDias[j];
  }
  dia_uno=diasemana(mes,anno);
  i=1;
  n=4;
  while (i<dia_uno){
    if (i==1){
      strcpy(TextoComienzo," . ");
    }
    else if (i==5){
      strcat( TextoComienzo ," . | ");
    }
    else if (i==7){
      strcat( TextoComienzo ," .");
    }
    else{
      strcat( TextoComienzo ," . ");
    }
    i++;
  }
  for (int j=1; j<=(diasmes(mes , anno)); j++){
    if (i==1){
      sprintf(TextoNum,"%2i",j);
      strcpy(TextoComienzo,TextoNum);
      strcat( TextoComienzo ," ");
    }
    else if (i==5){
      sprintf(TextoNum,"%2i",j);
      strcat( TextoComienzo ,TextoNum);
      strcat( TextoComienzo ," | ");
    }
    else if (i==7){
      sprintf(TextoNum,"%2i",j);
      strcat( TextoComienzo ,TextoNum);
      i=0;
      for (int i=0; i<22; i++){
        matriz[n][i]=TextoComienzo[i];
      }
      n=n+1;
    }
    else {
      sprintf(TextoNum,"%2i",j);
      strcat( TextoComienzo ,TextoNum);
      strcat( TextoComienzo ," ");
    }
    i++;
  }
  if (i>1){
    while (i<=7){
      if (i==5){
        strcat( TextoComienzo ," . | ");
      }
      else if (i==7){
        strcat( TextoComienzo ," .");
      }
      else{
        strcat( TextoComienzo ," . ");
      }
      i++;
    }
  }
  if (i>1){
    for (int i=0; i<22; i++){
      matriz[n][i]=TextoComienzo[i];
    }
  }

}






