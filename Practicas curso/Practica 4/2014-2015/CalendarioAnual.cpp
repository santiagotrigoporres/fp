/***************************************
* M�dulo: CalendarioAnual

Este m�dulo contiene los elementos para
una matriz 80x80 con el calendario anual mediante
la funcion iniciar ue crea una matriz 80x80 vac�a y
la funcion cargar datos que utilizando el modulo calendariomensual
crea la matriz 80x80 con el calendario anual.
*************************************************/

#include <stdio.h>
#include <string.h>
#include "CalendarioAnual.h"
#include "CalendarioMensual.h"

int CA_NumCol=80;
int CA_NumFil=80;
int n,m,PuntoUltimaLinea;


void Pantalla::Iniciar(){
  /**Se crea el tipo de dato pantalla con caracteres en blanco**/
  for (int i=0; i<(CA_NumFil); i++){
    for (int j=0; j<(CA_NumCol); j++){
      matriz[i][j]=' ';
    }
  }
}
void Pantalla::CargarCalendario(int anno){
  /**Se crea el tipo de dato pantalla con el calendario anual**/
  PantallaMes pantalla_mes;
  Pantalla pantalla_anual;
  n =0;
  m=0;
  PuntoUltimaLinea=0;
  for (int NumMes=1; NumMes<=12; NumMes++){
    pantalla_mes.IniciarMes();
    pantalla_mes.CargarMes(NumMes,anno);
    if (((NumMes==3) || (NumMes==6)) || (NumMes==9)) {
      for (int i=0; i<10; i++){
        for (int j=0; j<22; j++){
          matriz[i+m][j+n]=pantalla_mes.matriz[i][j];
        }
      }
      n=0;
      for (int j=0; j<80; j++){
        if (matriz[m+9][j]=='.'){
          PuntoUltimaLinea=1;
        }
      }
      if (PuntoUltimaLinea==1){
          m=m+11;
        }
      else {
        m=m+10;
      }
    }

    else {
      for (int i=0; i<10; i++){
        for (int j=0; j<22; j++){
          matriz[i+m][j+n]=pantalla_mes.matriz[i][j];
        }
      }
      n=n+25;
      PuntoUltimaLinea=0;
    }
  }

}







