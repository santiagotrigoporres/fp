/***************************************
* Interfaz de m�dulo: Pantalla

Este m�dulo contiene los elementos para volcar los datos de una matriz 80x80
e imprimirlos en pantalla
*************************************************/

#pragma once

typedef char TipoMatrizImprimir[80][80];
typedef struct Pantalla_Imprimir{
  TipoMatrizImprimir matriz;
  void Almacenar(TipoMatrizImprimir pantalla);/*Vuelca los datos de la matriz del calendarioanual a esta*/
  void ImprimirEnPantalla();/*Imprime en pantalla el calendario introducido en la matriz*/
};

