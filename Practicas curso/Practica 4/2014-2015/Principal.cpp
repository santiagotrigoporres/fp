/***************************************
* Principal

Este es el programa Principal del proyecto P4.cbd
Se crea el calendario Anual con el m�dulo de su nombre
y se imprime con el m�dulo pantalla
*************************************************/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include "CalendarioAnual.h"
#include "Pantalla.h"

int main(){

  Pantalla pantalla;
  Pantalla_Imprimir pantalla_i;
  int anno;
  anno=2000;
  while (anno !=0){
      system("cls"); /*Limpiamos pantalla*/
      printf("\xA8"); printf("A\xA4o (1601..3001)? (0 para salir):");
      scanf("%d",&anno);
      if (anno>=1601 && anno<=3000) {
        printf("\n");
        pantalla.Iniciar();
        pantalla.CargarCalendario(anno);
        pantalla_i.Almacenar(pantalla.matriz);
        pantalla_i.ImprimirEnPantalla();
      }
      else if (anno==0) {
        system("PAUSE");
      }
      else {
        printf("\n");
        printf("No has introducido un numero entero entre el intervalo indicado (1601..3001)");
        printf("\n");
        system("PAUSE");
      }
  }
}


