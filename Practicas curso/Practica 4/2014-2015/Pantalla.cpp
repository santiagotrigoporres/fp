/***************************************
* M�dulo: Pantalla

Este m�dulo contiene los elementos para volcar los datos de una matriz 80x80
e imprimirlos en pantalla
*************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Pantalla.h"
#include "CalendarioAnual.h"

void Pantalla_Imprimir::Almacenar(TipoMatrizImprimir pantalla){
  /*Vuelca los datos de la matriz del calendarioanual a esta*/
  for (int i=0; i<(80); i++){
      for (int j=0; j<(80); j++){
        matriz[i][j]=pantalla[i][j];
      }
  }
}
void Pantalla_Imprimir::ImprimirEnPantalla(){
  /*Imprime en pantalla el calendario introducido en la matriz*/
  int LineaVacia=1;
  for (int i=0; i<(80); i++){
      /*A continuaci�n hacemos un barrido buscando en la fila que estamos y en la siguiente carcacteres que no sean ' '
      imprimiendo solo una linea de espacio entre meses y evitando imprimir laparte final de la matriz que esta vacia
      */
      for (int j=0; j<(80); j++){
        if ((matriz[i][j]!=' ') || (matriz[i+1][j]!=' ')){/*Se chequea tambien la linea siguiente para no borrar lineas vacias entre meses*/
          LineaVacia=0;
        }
      }
      if (LineaVacia==0) {
        /*Si no es la Linea Vacia*/
        for (int j=0; j<(80); j++){
          printf("%c",matriz[i][j]);
        }
      }
      LineaVacia=1;
  }
  system("PAUSE");
}



