/*****************************************
*  Interfaz de Modulo: Calendario
*****************************************/

#pragma once

#include <stdio.h>
#include <string.h>
#include "pelicula.h" /*usamos la interfaz de pelicula para poder imprimir
                        el calendario*/

typedef enum TipoDia{
    Lunes, Martes, Miercoles, Jueves,
    Viernes, Sabado, Domingo
};

typedef enum TipoMes{
    Enero, Febrero, Marzo, Abril,
    Mayo, Junio, Julio, Agosto,
    Septiembre, Octubre, Noviembre, Diciembre
};

typedef struct TipoFecha{
    int dia;
    TipoMes mes;
    int ano;
};

typedef char NombreMes[20];

int GetDiaDeLaSemana (TipoFecha fecha);
void GetNombreMes( TipoFecha fecha, NombreMes &mes);
int GetCantidadDiasMes(TipoMes mes);
void PrintCabecera(TipoFecha fecha);
void PrintCalendario(int comienzo, TipoMes mes, int ano, Peliculas peli);
bool FechaValida(int dia, int mes, int ano);
