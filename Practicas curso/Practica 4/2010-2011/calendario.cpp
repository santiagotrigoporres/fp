#include "calendario.h"

bool _esBisiesto;

bool AnoBisiesto(int _ano){
  if ((_ano % 4 == 0) && (_ano % 100 != 0) || (_ano % 400 == 0)){
    return true;
  }else{
    return false;
  }
}

/* Devuelve el n�mero de dia que comienza el mes */
int GetDiaDeLaSemana (TipoFecha _fecha){
   int _dia, _mes, _ano, _mesAux, _res, _res1, _comienzaMes;
   _dia = _fecha.dia;
   _ano = _fecha.ano;

  _esBisiesto = AnoBisiesto(_ano);

  _mes = int(_fecha.mes) + 1;
  _mesAux = _mes;
  if (_mes == 1){
    _mesAux = 13;
    _ano = _ano - 1;
  }
  if (_mes == 2){
    _mesAux = 14;
    _ano = _ano - 1;
  }
  _res = _dia + (_mesAux * 2) + ((_mesAux + 1)*3/5) + _ano + (_ano/4) - (_ano/100) + (_ano/400);
  _res1 = _res / 7;
  _comienzaMes = _res - (_res1 * 7);

  return _comienzaMes;
}

/* Segun el tipo de mes devuelve el nombre del mes */
void GetNombreMes(TipoFecha _fecha, NombreMes &_mes ){
  switch (_fecha.mes){
      case Enero:
        strcpy(_mes,"ENERO");
        break;
      case Febrero:
        strcpy(_mes,"FEBRERO");
        break;
     case Marzo:
        strcpy(_mes,"MARZO");
        break;
     case Abril:
        strcpy(_mes, "ABRIL");
        break;
     case Mayo:
        strcpy(_mes, "MAYO");
        break;
      case Junio:
        strcpy(_mes,"JUNIO");
        break;
      case Julio:
        strcpy(_mes,"JULIO");
        break;
      case Agosto:
        strcpy(_mes, "AGOSTO");
        break;
      case Septiembre:
        strcpy(_mes,"SEPTIEMBRE");
        break;
      case Octubre:
        strcpy(_mes,"OCTUBRE");
        break;
      case Noviembre:
        strcpy(_mes,"NOVIEMBRE");
        break;
      case Diciembre:
        strcpy(_mes,"DICIEMBRE");
        break;
  }
}

/* Devuelve la cantidad de dias del mes */
int GetCantidadDiasMes(TipoMes _mes){
    switch(_mes){
      case Enero:
      case Marzo:
      case Mayo:
      case Julio:
      case Agosto:
      case Octubre:
      case Diciembre:
        return 31;
        break;
      case Febrero:
        if(_esBisiesto){return 29;}
        else{return 28;}
        break;
      case Abril:
      case Junio:
      case Septiembre:
      case Noviembre:
        return 30;
        break;
      default:
      ;
  }
}

/*Imprime el a�o y mes introducido por el usuario
  y la cabecera del calendario*/
void PrintCabecera(TipoFecha _fecha){
  NombreMes _nomMes;
  int _contMes;
  GetNombreMes(_fecha, _nomMes);
  printf("\n%s", _nomMes);

  switch (_fecha.mes){
      case Mayo:
        _contMes=4;
        break;
      case Enero:
      case Marzo:
      case Abril:
      case Junio:
      case Julio:
        _contMes = 5;
        break;
      case Agosto:
        _contMes = 6;
        break;
      case Febrero:
      case Octubre:
        _contMes = 7;
        break;
      case Noviembre:
      case Diciembre:
        _contMes = 9;
        break;
      case Septiembre:
        _contMes = 10;
        break;
    }

  for(int i=0; i<28-_contMes-5; i++){
        printf(" ");
  }
  printf("%d", _fecha.ano);
  printf("\n===========================");
  printf("\nLU  MA  MI  JU  VI | SA  DO");
  printf("\n===========================\n");

}

/* Imprime el calendario */
void PrintCalendario(int _comienzo, TipoMes _mes, int _ano, Peliculas _peli){
  int _cantidad, _cont, _numeroDia;
  char _res;
  _cont = 0;
  _numeroDia = 1;
  _cantidad = GetCantidadDiasMes(_mes);
  for(int i=1; i<_cantidad +1;i++){
    _res=GetFechaEnCalendario(_numeroDia,int(_mes+1),_ano);
    if(i > _comienzo){
      switch(_res){
      case 'X':printf("XX");break;
      case 'V':printf("VV");break;
      case 'N':
        if (_numeroDia< 10){printf(" %d",_numeroDia);}
        else {printf("%d",_numeroDia);}
        break;
      }
        _numeroDia++;
      }else{
          printf(" .");
      }
      if (_numeroDia==2){
        _cantidad = _cantidad + _cont;
      }
      _cont++;
    if((i!=1 )&& (i % 7 == 0)){
      printf("\n");
      _cont = 0;
    }else{
        if(_cont == 5){
          printf(" | ");
        }else{
          printf("  ");
        }
        if(_numeroDia > GetCantidadDiasMes(_mes)){
          for(int j=0; j<7 -_cont;j++){
              if ((7-_cont) - j==2){
                if(_cont == 5){
                  printf(" .");
                }else{
                  printf("| ");
                  printf(" .");
                }
              }else{
                printf(" .");
              }
              if ((7-_cont) - (j+1)==2){
                printf(" ");
              }else{
            printf("  ");
          }
        }
      }
    }
  }
}

bool FechaValida(int dia, int mes, int ano){
  TipoMes _mes;
  _mes = TipoMes(mes-1);
  switch(_mes){
      case Febrero:
        _esBisiesto=AnoBisiesto(ano);
        if(_esBisiesto && dia>29){
          return false;
          }
        if(!_esBisiesto && dia>28){
          return false;
          }
        break;
      case Abril:
      case Junio:
      case Septiembre:
      case Noviembre:
        if(dia>30){return false;}
        break;
      default: /*Entrara para los meses que tiene 31 y ya se controla
                anteriormente que no se introduzca un numero mayor a 31*/
      return true;
    }
  return true;
}
