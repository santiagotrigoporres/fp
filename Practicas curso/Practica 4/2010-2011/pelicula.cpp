#include <ctype.h>
#include "pelicula.h"
#include "global.h"

Peliculas _pelicula;
ListaGenero Generos = {"COMEDIA", "DRAMA", "TERROR","BELICA"};
int _len, _cont;

/*Funcion que devuelve si hay o no peliculas*/
bool GetPeliculas(){
  _cont=0;
  for(int i=0; i<numPeli; i++){
    _len = strlen(_pelicula[_cont].titulo);
    if (_len>0){
      _cont++;
      }
    }
  if (_cont<numPeli){ return true;}
  else{return false;}
  }

/*Devuelve la posicion de la ultima pelicula*/
int UltimaPelicula(){
  bool _seguir = true;
  _cont=0;
  do{
    _len = strlen(_pelicula[_cont].titulo);
    if (_len>0){
      _seguir=true;
      _cont++;
      }
    else {_seguir=false;}
  }while(_seguir);
  return _cont;
  }

/*A�ade una pelicula al array*/
void AddPelicula(Peli _peli){
    int _numPeli;
    _numPeli = UltimaPelicula();
    ConvertCadenaMayuscula(_peli.titulo);
    ConvertCadenaMayuscula(_peli.hora);
    ConvertCadenaMayuscula(_peli.genero);
    strcpy(_pelicula[_numPeli].titulo, _peli.titulo);
    strcpy(_pelicula[_numPeli].hora, _peli.hora);
    strcpy(_pelicula[_numPeli].genero,_peli.genero);
    _pelicula[_numPeli].dia = _peli.dia;
    _pelicula[_numPeli].mes = _peli.mes;
    _pelicula[_numPeli].anno = _peli.anno;
}

/*Devuelve la posicion (en el array) una pelicula en concreto segun su titulo*/
int GetPelicula(TipoString _titulo){
  int _sonIguales;
  ConvertCadenaMayuscula(_titulo);
  for (int i=0; i<=numPeli-1; i++){
    _sonIguales = strcmp(_pelicula[i].titulo, _titulo);
    if (_sonIguales==0){
      return i;
    }
  }
  return -1;
}

/*Devuelve las entradas disponibles en una pelicula en concreto*/
int GetEntradasDisponibles(TipoString _titulo){
  return _pelicula[GetPelicula(_titulo)].entradas;
}

/*Modifica las entradas de una pelicula en concreto*/
void SetEntrada(TipoString _titulo, int _entradas){
   int _posPeli = GetPelicula(_titulo);
   _pelicula[_posPeli].entradas =_pelicula[_posPeli].entradas + _entradas;
  }

/*Imprime por pantalla la pelicula. Esta funcion ademas de
  recivir en num de la pelicula recibe el orden para imprimir
  los datos segun la funcion que le llame
  0--> Para imprimir Listado segun genero
  1--> Para imprimir Calendario*/
void PrintPelicula(int _posicionPeli, int _orden){

  if(_orden==0){
    printf("\n\n%s%s",_pelicula[_posicionPeli].genero,". ");
    printf("%s%s",_pelicula[_posicionPeli].titulo,". ");
    printf("%d%s",_pelicula[_posicionPeli].dia,"-");
    printf("%d%s",_pelicula[_posicionPeli].mes,"-");
    printf("%d%s",_pelicula[_posicionPeli].anno,". ");
    }
  else{
    printf("\n\n%d%s",_pelicula[_posicionPeli].dia,"-");
    printf("%d%s",_pelicula[_posicionPeli].mes,"-");
    printf("%d%s",_pelicula[_posicionPeli].anno,". ");
    printf("%s%s",_pelicula[_posicionPeli].titulo,". ");
    printf("%s%s",_pelicula[_posicionPeli].genero,". ");
    }

  printf("%s%s",_pelicula[_posicionPeli].hora, " HORAS. ");
  if (_pelicula[_posicionPeli].entradas==20){
      printf("AFORO COMPLETO.\n\n");
    }
  else{
      printf("%d%s",_pelicula[_posicionPeli].entradas, " ENTRADAS VENDIDAS.\n\n");
    }
}

/*Imprime las peliculas segun genero*/
void PrintPeliculas(TipoString _genero){
  int _sonIguales, _cont;
  bool _existeGen=true;
  _cont=0;
  for (int i=0; i<=numPeli-1; i++){
    if (GetGenero(_genero)){
       _sonIguales = strcmp(_pelicula[i].genero, _genero);
       if(_sonIguales==0){
         PrintPelicula(i,0);
         }
        else{
          _cont++;
          }
      }
      else{
        printf("\n|| ERROR ||\nGENERO INEXISTENTE \n\n");
        return;/*Sale de la funci�n GetPeliculas*/
        }
   }
   if (_cont==numPeli){
    printf("\n\n|| ATENCION ||\nNO SE ENCUENTRAN PELICULAS DEL GENERO %s\n\n", _genero);
    }
  }

/*Imprime laS peliculas segun fecha*/
void PrintPeliculas(int _mes, int _ano){
  _cont=0;
  for (int i=0; i<=numPeli-1; i++){
    if ((_pelicula[i].mes==_mes) && (_pelicula[i].anno==_ano)){
        PrintPelicula(i,-1);
    }
  }
}

/*Funcion que devuelve true/false si hay pelicula en esa fecha o no*/
bool GetFecha(int _dia, int _mes, int _ano){
  for (int i=0; i<=numPeli-1; i++){
    if ((_pelicula[i].dia==_dia) && (_pelicula[i].mes==_mes) && (_pelicula[i].anno==_ano)){
      return true;
    }
  }
  return false;
}

/*Funcion que devuelve 3 caracteres distintos segun la fecha
  'V'--> Hay Pelicula ese dia
  'X'--> Ademas de lo anterior si se han vendido todas las entradas
  'N'--> No hay Pelicula ese dia*/
char GetFechaEnCalendario(int _dia, int _mes, int _ano){
  for (int i=0; i<=numPeli-1; i++){
    if ((_pelicula[i].dia==_dia) && (_pelicula[i].mes==_mes) && (_pelicula[i].anno==_ano)){
      if(_pelicula[i].entradas==20){return 'X';}
      else{return 'V';}
    }
  }
  return 'N';
}

/*Comprueba que el genero existe*/
bool GetGenero(TipoString _genero){
  int _res;
  ConvertCadenaMayuscula(_genero);
  for(int i=0; i<4; i++){
    _res = strcmp(Generos[i], _genero);
    if (_res == 0){
      return true;
    }
  }
  return false;
  }
