#include <ctype.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
typedef char TipoString[25];

/*Convierte un carcter en mauscula*/
void ConvertCaracterMayuscula(char &_caracter){
  _caracter = toupper(_caracter);
}

/*Convierte una cadena a mayuscula*/
void ConvertCadenaMayuscula(TipoString _cad){
  int _cont=0;
  while(_cont!=strlen(_cad)){
    ConvertCaracterMayuscula(_cad[_cont]);
    _cont++;
  }
}

void ClearPantalla(){
  system("cls");
  }

void Continuar(){
  char _car;
  printf("\n\nPulse INTRO para continuar...");
  fflush(stdin);
  scanf("%c", &_car);
  ClearPantalla();
  }

