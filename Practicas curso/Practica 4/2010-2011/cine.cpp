/*=================================
      DIRECTIVAS DE EJECUCION
==================================*/

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include "pelicula.h"
#include "calendario.h"
#include "global.h"

Peliculas pel;
bool _existeTitulo,_existeFecha, _existeGenero;
TipoString _titulo, _genero, _hora;
int _dia, _mes, _ano, _res;

void ComprobarTitulo(){
  _existeTitulo=false;
  do{
    printf("\nT%ctulo de la Pel%ccula..:",161,161);
    scanf("%s", &_titulo);
    if (GetPelicula(_titulo) > -1){
      printf("\n||ERROR||\nLA PELICULA YA EXISTE\n\n");
      _existeTitulo=true;
    }
    else{_existeTitulo=false;}
  }while(_existeTitulo);
}

void ComprobarDia(){
  do{
    printf("\nD%ca de la Pel%ccula.....:",161,161);
    fflush(stdin);
    _res = scanf("%d", &_dia);
    if (_dia<1 || _dia>31 || _res==0){
      printf("\n||ERROR||\nEL DIA TIENE QUE SER MAYOR/IGUAL A 1 Y MENOR/IGUAL DE 31");
      printf("\n");
    }
  }while (_dia<1 || _dia>31 || _res==0);
}

void ComprobarMes(TipoString _mensaje){
  do{
    printf(_mensaje);
    fflush(stdin);
    _res = scanf("%d", &_mes);
    if (_mes<1 || _mes>12 || _res==0){
      printf("\n||ERROR||\nEL MES TIENE QUE SER MAYOR/IGUAL A 1 Y MENOR/IGUAL DE 12");
      printf("\n");
    }
  }while (_mes<1 || _mes>12 || _res==0);
}

void ComprobarAno(TipoString _mensaje){
  do{
    printf(_mensaje);
    fflush(stdin);
    _res=scanf("%d", &_ano);
    if (_ano<1601 || _ano>3000 || _res==0){
      printf("\n||ERROR||\nEN EL ANO");
      printf("\n");
    }
  }while (_ano<1601 || _ano>3000 || _res==0);
}

void ComprobarFecha(){
  TipoString _msjMes, _msjAno;
  bool _error=false;
  strcpy(_msjMes, "\nMes de la Pelicula.....:");
  strcpy(_msjAno, "\nAnno de la Pelicula....:");
  do{
    ComprobarDia();
    ComprobarMes(_msjMes);
    ComprobarAno(_msjAno);
    /*Controlamos que no existe esa fecha*/
    if (GetFecha(_dia, _mes, _ano)){
      _existeFecha = true;
      _error=true;
      printf("\n||ERROR||\nEN ESTA FECHA YA HAY PELICULA\n");
    }
    else{
      _existeFecha=false;
      _error=false;
    }
    if(!_error){
      if (!FechaValida(_dia, _mes, _ano)){
        _existeFecha = true;
        printf("\n||ERROR||\nFECHA INCORRECTA PARA MES O ANO\n");
        }
    }
  }while(_existeFecha);
}

void ComprobarGenero(){
  do{
    printf("\nGenero de la Pel%ccula..:",161);
    scanf("%s", &_genero);
    _existeGenero=GetGenero(_genero);
    if (!_existeGenero){
      printf("\n||ERROR||\nGENERO INEXISTENTE\n");
      }
  }while(!_existeGenero);
}

void MainAlta(){
    ClearPantalla();
    _existeFecha=false;
    _existeGenero= false;
    if (GetPeliculas()){
    printf("\n         ALTA DE PELICULAS");
    printf("\n   =============================\n");
    ComprobarTitulo();
    ComprobarFecha();
    printf("\nHora de la Pel%ccula....:",161);
    scanf("%s", &_hora);
    ComprobarGenero();
    strcpy(pel[0].titulo, _titulo);
    strcpy(pel[0].hora, _hora);
    strcpy(pel[0].genero, _genero);
    pel[0].dia = _dia;
    pel[0].mes = _mes;
    pel[0].anno = _ano;
    AddPelicula(pel[0]);
    }
    else{
      printf("\n||ERROR||\nNO SE PUEDEN AGREGAR MAS PELICULAS.\n");
      }
    Continuar();
  }

void MainVenderEntradas(){
    int _numEntradas, _entradasVendidas, _totEntradas;
    _existeTitulo=false;
    ClearPantalla();
    printf("\n         VENDER ENTRADAS");
    printf("\n   =============================\n");
    do{
      printf("\nT%ctulo de la Pel%ccula..:",161,161);
      scanf("%s", &_titulo);
      if (GetPelicula(_titulo)==-1){
        printf("\n||ERROR||\nLA PELICULA NO EXISTE\n\n");
        _existeTitulo=false;
      }
      else{_existeTitulo=true;}
    }while(!_existeTitulo);
    do{
      printf("\nN%cmero de Entradas a Vender...:",163);
      fflush(stdin);
      _res = scanf("%d", &_numEntradas);
      if(_res!=0){
        _entradasVendidas = GetEntradasDisponibles(_titulo);
        _totEntradas = _entradasVendidas + _numEntradas;
        if (_entradasVendidas == 20 ){
          printf("\n||ERROR||\nNO SE PUEDEN VENDER MAS ENTRADAS\n\n");
        }
        else if(_totEntradas>20){
          printf("\n||ERROR||\nCOMO MAXIMO SE PUEDEN VENDER %d ENTRADAS \n\n", 20-_entradasVendidas);
        }
        else{/*Vendemos las entradas*/
          SetEntrada(_titulo, _numEntradas);
        }
      }
      else{printf("\n||ERROR||\nNUMERO DE ENTRADAS INCORRECTO");}
    }while(_res==0);
    Continuar();
  }

void MainListadoPeliculas(){
    TipoString _genero;
    ClearPantalla();
    printf("\n        LISTADO DE PELICULAS");
    printf("\n   =============================\n");
    printf("Genero de la Pel%ccula..:",161);
    scanf("%s", &_genero);
    PrintPeliculas(_genero);
    Continuar();
  }

/*Menu del calendario */
void MainCalendarioPeliculas(){
    int _comienzo;
    TipoString _msjMes, _msjAno;
    TipoMes _mesActual;
    TipoFecha _fecha;
    ClearPantalla();
    printf("\n       CALENDARIO PELICULAS");
    printf("\n   =============================\n");
    fflush(stdin);
    strcpy(_msjMes,"\nMes..:");
    strcpy(_msjAno,"\nAnno..:");
    ComprobarMes(_msjMes); /*Llamos a comprobar mes y le pasamos el mensaje*/
    ComprobarAno(_msjAno); /*Llamos a comprobar a�o y le pasamos el mensaje*/
    _fecha.dia = 1;
    _fecha.mes = TipoMes(_mes - 1);
    _fecha.ano = _ano;
    _comienzo = GetDiaDeLaSemana(_fecha);
    PrintCabecera (_fecha);
    PrintCalendario(_comienzo, _fecha.mes, _ano, pel);
    PrintPeliculas(_mes,_ano);
    Continuar();
  }

/*Menu principal que ademas llama a cada sub menu*/
void MainPrincipal(){
    char _valor;
    do{
      ClearPantalla();
      printf("\n        GESTION DE PELICULAS");
      printf("\n   =============================");
      printf("\nAlta de Pel%ccula                   <Pulsar A>",161);
      printf("\nVender Entradas                    <Pulsar V>");
      printf("\nListado de Pel%cculas               <Pulsar L>",161);
      printf("\nMostrar Calendario de Pel%cculas    <Pulsar M>",161);
      printf("\nSalir                              <Pulsar S>");
      printf("\n============================");
      printf("\n   INTRODUZCA UNA OPCION VALIDA <A|V|L|M|S>");
      fflush(stdin);
      scanf("%c", &_valor);
      ConvertCaracterMayuscula(_valor);
      switch (_valor){
        case 'A':
          MainAlta();
          break;
        case 'V':
          MainVenderEntradas();
          break;
        case 'L':
          MainListadoPeliculas();
          break;
        case 'M':
          MainCalendarioPeliculas();
          break;
        case 'S':
          break;
        default:
          printf("\n||ERROR||\nOPCION INCORRECTA\n");
          Continuar();
        }
    }while (_valor!='S');
  }

int main(){
  MainPrincipal();
  }
