/*****************************************
*  Interfaz de Modulo: Pelicula
*****************************************/

#pragma once
#include <stdio.h>
#include <string.h>

/*Variables*/
typedef char TipoString[25];

typedef struct Peli{
    char titulo[20];
    int dia;
    int mes;
    int anno;
    char hora[6];
    char genero[20];
    int entradas;
  };

typedef TipoString ListaGenero[4];
const int numPeli = 30; /*3*/
typedef Peli Peliculas[numPeli];

/*Procedimientos*/
void AddPelicula(Peli p);
bool GetFecha(int dia, int mes, int ano);
char GetFechaEnCalendario(int dia, int mes, int ano);
int GetEntradasDisponibles(TipoString titulo);
void SetEntrada(TipoString titulo, int entradas);
int GetPelicula(TipoString titulo);
void PrintPeliculas(TipoString genero);
void PrintPeliculas(int ano, int mes);
bool GetPeliculas();
bool GetGenero(TipoString genero);

