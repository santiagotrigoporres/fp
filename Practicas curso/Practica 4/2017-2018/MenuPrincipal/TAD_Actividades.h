#pragma once

/********************************************************************************
T_AlmacenarActividades es un vector de 10 espacios de 1000 caracteres cada uno.
********************************************************************************/
typedef char T_CaracteresMaximos[1001];
typedef T_CaracteresMaximos T_AlmacenarActividades[10];

/********************************************************************************
T_CarreraDescanso es un enumerado de 10 espacios cuyo contenido puede ser C o D.
********************************************************************************/
typedef enum T_CoD {C,D};
typedef T_CoD T_CarreraDescanso[10];

typedef struct T_Actividades{
  /**************
    VARIABLES
  ***************/
  T_AlmacenarActividades GuardarDatos; //Variable de 10 espacios de 1000 caracteres cada uno para almacenar el plan de entrenamiento.
  T_CarreraDescanso TipoCD; //Variable de 10 espacios de tipo enum con los valores posibles C y D.
  bool PlanGuardado; //Almacena true=si se introdujeron datos -- False=si no se introdujeron.
  /**************************************
  FUNCIONES PARA LA OPCI�N 1 Y LA 2.
  ***************************************/
  void InicializarPlan();  //inicializa el plan por si se da el caso de mostrar datos antes de introducirlos.
  void IntroducirPlan();   //nos pide el plan de entrenamiento para 10 dias.
  void ListarPlan();       //nos lista el plan de entrenamiento de 10 dias.
};
