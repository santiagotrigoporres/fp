#include <stdio.h>
#include <stdlib.h> //Para poder usar system("cls")
#include <ctype.h>  //Para poder usar Toupper

#include "TAD_MenuPrincipal.h"

/*=============================================================================
              FUNCIONES DEL STRUCT T_MenuPrincipal
===============================================================================*/

/*******************************************************************************
Procedimientos principales del TAD
********************************************************************************/
/*-----------------------------------------------------------------------------*/
void T_MenuPrincipal::InicializarValores(){
  PlanDeActividades.InicializarPlan();  //Llama a InicializarPlan de TAD_Actividades
  PlanDeActividades.PlanGuardado=false; //True=Se ha introducido un plan. False=No se ha introducido ningun plan.
  Fecha.FechaGuardada=false;            //True=Se introducido una fecha. False=No se introducido ninguna fecha.
}
/*-----------------------------------------------------------------------------*/
void T_MenuPrincipal::ElegirOpcion(){
  system("cls");
  printf("          ELIGE UNA OPCION\n");
  printf("1. Introducir plan de entrenamiento\n");
  printf("2. Listar plan de entrenamiento\n");
  printf("3. Introducir fecha de inicio del plan\n");
  printf("4. Mostrar plan en el calendario\n");
  printf("5. Salir del programa\n");
  //Solo vamos a aceptar validos valores entre 1 y 5, sino volveremos a pedir una opcion.
  do{
    printf("\n>>> ");
    fflush(stdin);
    scanf("%d",&opcion);
    if((opcion<1)||(opcion>5)){
      printf("Error: Opcion elegida no valida");
    }
  }while((opcion<1)||(opcion>5));
}
/*-----------------------------------------------------------------------------*/
void T_MenuPrincipal::RealizarAccion(){
  switch (opcion){
    case 1: system("cls"); fflush(stdin);
            PlanDeActividades.IntroducirPlan();
            break;
    case 2: system("cls");
            PlanDeActividades.ListarPlan();
            break;
    case 3: system("cls");
            Fecha.InicioPlan();
            break;
    case 4: system("cls");
            Fecha.MostrarPlan(PlanDeActividades.TipoCD,PlanDeActividades.PlanGuardado);
            break; //Pasamos a MostrarPlan la variable que contiene si es C o D, y la variable que conoce si hemos introducido un plan o no.
    case 5: break;

  }
}
/*-----------------------------------------------------------------------------*/
void T_MenuPrincipal::Repetir(){
  fflush(stdin);
  if(opcion!=5){  //No mostraremos la opcion de repetir si la opcion introducida fue un 5.
    do{
      printf("\nVolver al Menu Principal? s/n: ");
      scanf("%s",&respuesta);
      respuesta=toupper(respuesta); //Convertimos cualquier respuesta introducida a mayuscula, asi nos da igual "s,S,n,N"
    }while(respuesta!='S' && respuesta!='N'); //Solo aceptaremos como respuestas validas "S" o "N".
  }
}
/*-----------------------------------------------------------------------------*/
