#include <stdio.h>
#include <stdlib.h>
#include <string.h> //para poder usar el strlen
#include <ctype.h> //para poder usar el toupper

#include "TAD_Actividades.h"

/*=============================================================================
              FUNCIONES DEL STRUCT T_Actividades
=============================================================================*/
/*******************************************************************************
Funciones y procedimientos principales de la TAD
********************************************************************************/
/*-----------------------------------------------------------------------------*/
/*Este procedimiento se hace pensando en el caso de que queramos mostrar los datos del plan
sin haberlos introducido previamente*/
void T_Actividades::InicializarPlan(){
  for(int i=0;i<10;i++){
    strcpy(GuardarDatos[i],"No se han introducido datos");
  }
}
/*-----------------------------------------------------------------------------*/
void T_Actividades::IntroducirPlan(){

T_CaracteresMaximos descanso="D"; //Compararemos los datos introducidos con esta variable para saber si es Dia de descanso o no.

  for(int i=0;i<10;i++){
    do{
      printf("Actividad del dia %2d?(D mayuscula para dia de descanso):\n",i+1);
      printf(">>> ");
      gets(GuardarDatos[i]);
      system("cls");
      if (strlen(GuardarDatos[i])>1000){
        system("cls");
        printf("Error...has puesto mas de 1000 caracteres\n");
      }
    }while(strlen(GuardarDatos[i])>1000); //Volveremos a pedir los datos siempre que nos pasemos de 1000 caracteres.

    if(strcmp(GuardarDatos[i],descanso)==0){    //si los datos introducidos son iguales a "D"
     TipoCD[i]=D;                               //Guardaremos en la variable tipoCD una "D" y
     strcpy(GuardarDatos[i],"Dia de descanso"); //Ademas sustituiremos esa "D" por el texto "Dia de descanso"
    }else{
     TipoCD[i]=C;                               //Sino guardaremos en la variable tipoCD una "C"
    }
  }
  PlanGuardado=true; //Como hemos almacenado un plan de entrenamiento PlanGuardado pasara a True.

  printf("Todos los datos se han almacenado correctamente.\n");

}
/*-----------------------------------------------------------------------------*/
void T_Actividades::ListarPlan(){

  for(int j=0;j<10;j++){ //Mostramos los datos introducidos de cada dia.
    printf("Entrenamiento del dia %2d\n",j+1);
    printf("%s\n\n",GuardarDatos[j]);
  }
}
/*-----------------------------------------------------------------------------*/
