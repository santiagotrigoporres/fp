#include <stdio.h>
#include <stdlib.h> //Para poder usar system("cls")
#include <conio.h> //Para usar el getch()

#include "TAD_MenuPrincipal.h"

int main(){

  T_MenuPrincipal Menu;

  Menu.InicializarValores(); //Llamamos al procedimiento que inicializa los valores del plan.
                             //De esta forma sabremos cuando hemos introducido valores y cuando no.
  do{
    Menu.ElegirOpcion();     //Nos muestra el menu principal y limita la Opcion introducida a valores entre 1 y 5.
    Menu.RealizarAccion();   //Contiene un Switch que llama a otras funciones segun la Opcion introducida.
    Menu.Repetir();          //Damos la opcion al usuario de decir si quiere realizar otra accion.
  }while((Menu.respuesta=='S') && (Menu.opcion!=5)); //Bucle mientras no introduzcamos un "5" en Opcion o digamos que no queremos repetir.

  system("cls");
  printf("Has elegido salir del programa.");
  getch(); //su funcion es que si ejecutas directamente el .exe, no se cierre sin dar tiempo a ver el mensaje de Despedida.
}
