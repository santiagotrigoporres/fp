/*************************************************************************************************************************
*   Este m�dulo contiene procedimientos para verificar la correcta carga de los datos
*************************************************************************************************************************/
#pragma once
#include "cargaDatos.h"
/*parametros n�meros de sala*/
const int sala_min=1;
const int sala_max=3;
/*par�metro a�os de selecci�n*/
const int anno_menor=1600;
const int anno_mayor=3000;
/*param�tros para la selecci�n de mes (tal vez trivial)*/
const int mes_menor=1;
const int mes_mayor=12;
/*param�tros correspondiente a los d�as de mes elegibles*/
const int dia_menor=1;

/*funciones*/
bool sala_ok(int sala_sel);
bool anno_ok(int anno_sel);
bool anno_ok_r(int anno_sel,int anno_men_r, int anno_may_r);
bool mes_ok(int mes_sel);
int f_dia_mayor(int anno_sel,int mes_sel);
bool dia_ok(int anno_sel, int mes_sel, int dia_sel);
bool nombre_ok(char nombre_sel[]);
bool sala_libre(int sala_s, int anno_s, int mes_s, int dia_s,registros registro);
bool hay_registros(int sala_s, int anno_s, int mes_s);
bool dia_reservado(int sala_r, int anno_r, int mes_r, int dia_r);
bool reserva_sala(int sala_r);
bool reserva_s_a(int sala_r, int anno_r);
bool mes_ok_r(int mes_sel,int mes_men_r, int mes_may_r);
bool reserva_s_a_m(int sala, int anno, int mes);
bool reserva_s_a_m_d(int sala, int anno, int mes, int dia);
bool sala_ok_r(int sala_sel, int mini, int maxi);
