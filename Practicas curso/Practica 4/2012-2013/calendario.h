/*************************************************************************************************************************
*   Interface para el m�dulo de calendario.
*   Este m�dulo mostrar� el calendario para las sala, a�o y mes seleccionado. Solamente permitir� seleccionar como valores
*   v�lidos aquellos que tengan reservas anotada.
**************************************************************************************************************************/
#pragma once
#include "cargaDatos.h"

void mostrar_calendario_reservas();
void mostrar_calendario(int mes_p, int anno_p);
int diaSemanaZeller(int mesz, int annoz);
