/*******************************************************************************************************************************
**    APLICACI�N PARA LA GESTI�N DE LA RESERVA DE SALAS DE REUNIONES DE VULCANO TELECOM. ES POSIBLE RESERVAR CUALQUIERA DE LAS
**    TRES SALAS CON LAS QUE CUENTA LA EMPRESA. LA RESERVA SER� PARA D�A COMPLETO. SE PODR�N GESTIONAR 100 RESERVAS. EL RANGO DE
**    FECHAS EN LAS QUE SE PUEDE RESERVAR VA DESDE EL 01/01/1600 A 31/12/3000. NO SE PODR� RESERVAR LOS S�BADOS NI DOMINGOS. SER�
**    POSIBLE LISTAR LAS RESERVAS YA REALIZADAS, AGREGAR RESERVAS NUEVAS Y ELIMINAR LAS YA EXISTENTE
********************************************************************************************************************************/
#include<stdio.h>
#include <conio.h>
#include <stdlib.h>
#include "calendario.h"
#include "cargaDatos.h"
#include "baja.h"
/************variables***************/
registros registro; /*asigno al elemento registro la definicion de registros*/
int id_registro=0;
/***************************************/
void pintar_menu(){/*MUESTRA EL MEN� PRINCIPAL CON SUS OPCIONES*/
printf("GESTOR DE RESERVAS DE SALAS DE REUNIONES\n\n");
printf("1.  Listar reservas.\n");
printf("2.  Introducir reserva.\n");
printf("3.  Eliminar reserva.\n");
printf("4.  Salir.\n\n");
printf("Opci%cn?",162);
}

int main(){
char tecla;
inicializar_registros();
/*tecla=getche();*/
/*compruebo que la tecla pulsada corresponda al rango 1-4 en caso contrario pido de nuevo pulsar opci�n v�lida*/
tecla='0'; /*doy este valor para asegurarme de que es distinto a 4*/
  system("CLS"); /*limpia la pantalla*/
  pintar_menu();
while (tecla != '4'){
  tecla=getche();
if (tecla=='1'){
  if (id_registro==0){
  system ("CLS");
  printf("No existen reservas en el sistema.\n");
  } else{
    mostrar_calendario_reservas();
  }
}
if (tecla=='2'){
 /*printf("Muestro introducci�n reservas");*/
 recoger_datos();
}
if (tecla=='3'){
  if (id_registro==0){
  system ("CLS");
  printf("No existen reservas en el sistema.\n");
  } else{
    eliminar_r();
  }
}
    if(tecla!='4' && tecla!='1'){/*Necesito cargar de nuevo el men� principal cuando la opci�n seleccionada sea disinta de 4 (dento de 1-4)*/
      printf("\n");
      system("PAUSE");/*Pido al usuario que pulse tecla para continuar*/
      system("CLS");/*Para limpiar la pantalla*/
      pintar_menu();
}
/*continuo seg�n elecci�n del usuario*/
}/* fin del while*/
}
