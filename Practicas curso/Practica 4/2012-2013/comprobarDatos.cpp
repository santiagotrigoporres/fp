#include "comprobarDatos.h"
#include "calendario.h"
#include "cargaDatos.h"
#include "baja.h"
#include <stdio.h>
#include <string.h>
#include <ctype.h>
int diaSemanaZeller2(int mesz, int annoz, int diaz){
int v_a=0;
int v_b=0;
if (mesz<=2){
  mesz=mesz+10;
  annoz=annoz-1;
}else{
  mesz=mesz-2;
}
v_a=annoz%100;
v_b=annoz/100;
return(700+((mesz*26-2)/10)+diaz+v_a+(v_a/4)+(v_b/4)-(v_b*2))%7;
}

bool sala_ok(int sala_sel){ /*Se asegura de que la sala sea un valor comprendido entre sala_min y sala_max*/
bool sala_ind;
sala_ind=false;
if (sala_sel>=sala_min && sala_sel<=sala_max){
sala_ind=true;
}
return sala_ind;
}
bool sala_ok_r(int sala_sel, int mini, int maxi){ /*Se asegura de que la sala sea un valor comprendido entre sala_min y sala_max*/
bool sala_ind;
sala_ind=false;
if (sala_sel>=mini && sala_sel<=maxi){
sala_ind=true;
}
return sala_ind;
}
bool anno_ok(int anno_sel){ /* Se asegura de que el a�o este comprendido entre el valor menor y el mayor*/
bool anno_ind;
anno_ind=false;
if (anno_sel>=anno_menor && anno_sel<=anno_mayor){
    anno_ind=true;
}
return anno_ind;
}
bool mes_ok(int mes_sel){ /* Se asegura que el valor introducido en el par�metro mes est� entre los elegibles*/
bool mes_ind;
mes_ind=false;
if (mes_sel>=mes_menor && mes_sel<=mes_mayor){
mes_ind=true;
}
return mes_ind;
}
int f_dia_mayor(int anno_sel,int mes_sel){/*en funcion del a�o y mes devuelve el d�a mayor de ese mes*/
int dia_v=1;
switch(mes_sel){
case 1:
case 3:
case 5:
case 7:
case 8:
case 10:
case 12:
  dia_v=31;
  break;
case 4:
case 6:
case 9:
case 11:
  dia_v=30;
  break;
default: /*hay que ver si es o no bisiesto*/
  if(anno_sel%4==0){ /*multipol de cuatro*/
      if(!((anno_sel%100==0) && !(anno_sel%400==0)) ){
        dia_v=29;
      } else{
        dia_v=28;
      }
    } else{
      dia_v=28;
    }
  }
return dia_v;
}

bool dia_ok(int anno_sel, int mes_sel, int dia_sel){ /*verifico que el d�a seleccionado no corresponda a s�bado o domingo
                                                      y que est� dentro del rango*/
bool dia_ind=false;
int dia_in_mes=1;
if (dia_sel>=dia_menor && dia_sel<=f_dia_mayor(anno_sel,mes_sel)){ /*verifico si est� en el rango correcto*/
  /*tengo que comprobar si se trata de un s�bado o domingo*/
  dia_in_mes=diaSemanaZeller2(mes_sel,anno_sel, dia_sel); /*me da su posici�n 0-domingo 6-s�bado*/
  if (!(dia_in_mes==0 || dia_in_mes==6)){
    dia_ind=true;
  }
}
return dia_ind;
}
bool nombre_ok(char nombre_sel[]){ /* compruebo que la cadena sea del tipo palabra espacio palabra*/
bool nombre_ind=false;
int blancos=0;
int largo_p=0;
largo_p=strlen(nombre_sel);/* la funci�n da la longitud de la cadena*/
for (int i=0; i<largo_p;i++){
  if (isspace(nombre_sel[i])){/*en caso de ser un espacio...*/
    blancos=blancos+1;
  }
}
if (blancos>1 && largo_p>=12){
  nombre_ind=true;
}
return nombre_ind;
}
bool sala_libre(int sala_s, int anno_s, int mes_s, int dia_s, registros registro){ /*verifica si la sala est� libre en esta fecha*/
bool sala_ind=false;
bool coincide=false;
int combi_pr;
int k=0;
combi_pr=(anno_s*100000)+(mes_s*1000)+(dia_s*10)+sala_s;
if (id_registro==0){/*== si el vector no tiene ning�n dato est� libre==*/
  sala_ind=true;
}else{/*==tengo que recorrer el vector para buscar coincidencias de sala y fecha. Si no las encuentro estar� libre==*/
 while (!coincide && (k<id_registro+1)){/*no necesito recorrelo todo. si encuentr uno repetido paro y marco falso*/
    if(registro[k].combi_f ==combi_pr){
    sala_ind=false;
    coincide=true;/*para salir del bucle*/
    }else{
    sala_ind=true;
    }
  k++;
  }
}
return sala_ind;
  }
bool hay_registros(int sala_s, int anno_s, int mes_s){ /*verifica si hay reservas para esta sala, a�o y mes*/
bool registros_ind=false;
int i=0;
while (!registros_ind && i<id_registro+1){/*se repite hasta que el valor de registro_ind sea verdadero o llegue al limite de rgistros*/
    if (registro[i].sala==sala_s&&registro[i].anno==anno_s&&registro[i].mes==mes_s){
      registros_ind=true;
    }
    i++;
}
return registros_ind;
}

bool dia_reservado(int sala_r, int anno_r, int mes_r, int dia_r){ /*si hay reserva para esta sala y fecha verdadero*/
bool reservado_ind=false;
int valor_buscado;
int i=0;
valor_buscado=(anno_r*100000)+(mes_r*1000)+(dia_r*10)+sala_r;
while(!reservado_ind && i<id_registro+1){
  if(registro[i].combi_f==valor_buscado){
    reservado_ind=true;
  }
  i++;
}
return reservado_ind;
}
bool anno_ok_r(int anno_sel,int anno_men_r, int anno_may_r){ /* Se asegura de que el a�o este comprendido entre el valor menor y el mayor*/
bool anno_ind;
anno_ind=false;
if (anno_sel>=anno_men_r && anno_sel<=anno_may_r){
    anno_ind=true;
}
return anno_ind;
}

bool reserva_sala(int sala_r){/*compruebo si existe alguna reserva para la sala dada*/
bool rs_ind=false;
int k=0;
while (!rs_ind && k<id_registro){
  if (sala_r==registro[k].sala){
    rs_ind=true;
  }/*fin if comprobacion sala*/
  k++;
}/*fin del while*/
return rs_ind;
}
bool reserva_s_a(int sala_r, int anno_r){/*compruebo si existe reserva para la sala y a�o seleccionado*/
  bool rs_s_a=false;
  int k=0;
  while (!rs_s_a && k<id_registro){
    if(sala_r==registro[k].sala && anno_r==registro[k].anno){
      rs_s_a=true;
    }
    k++;
  }/* fin comprocaci�n sala y a�o*/
return rs_s_a;
}
bool mes_ok_r(int mes_sel,int mes_men_r, int mes_may_r){ /* Se asegura de que el a�o este comprendido entre el valor menor y el mayor*/
bool mes_ind;
mes_ind=false;
if (mes_sel>=mes_men_r && mes_sel<=mes_may_r){
    mes_ind=true;
}
return mes_ind;
}
bool reserva_s_a_m(int sala, int anno, int mes){/*verifico que existe reserva para la sala en el a�o y mes dados*/
bool rs_s_a_m=false;
  int k=0;
  while (!rs_s_a_m && k<id_registro){
    if(sala==registro[k].sala && anno==registro[k].anno && mes==registro[k].mes ){
      rs_s_a_m=true;
    }
    k++;
  }/* fin comprocaci�n sala y a�o*/
return rs_s_a_m;
}
bool reserva_s_a_m_d(int sala, int anno, int mes, int dia){
bool rs_s_a_m_d=false;
  int k=0;
  while (!rs_s_a_m_d && k<id_registro){
    if(sala==registro[k].sala && anno==registro[k].anno && mes==registro[k].mes && dia==registro[k].dia){
      rs_s_a_m_d=true;
    }
    k++;
  }/* fin comprocaci�n sala y a�o*/
return rs_s_a_m_d;
}
