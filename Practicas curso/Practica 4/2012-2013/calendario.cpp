#include <stdio.h>
#include<string.h>
#include "Calendario.h"
#include "cargaDatos.h"
#include "comprobarDatos.h"
#include "baja.h" /*para poder emplear funciones y variables sobre las salas ya reservadas*/
#include <stdlib.h>

void pintar_cabecera(int mes_n, int fech_anno){
/* asigno a cada mes su nombre*/
const char mes1[]="ENERO";
const char mes2[]="FEBRERO";
const char mes3[]="MARZO";
const char mes4[]="ABRIL";
const char mes5[]="MAYO";
const char mes6[]="JUNIO";
const char mes7[]="JULIO";
const char mes8[]="AGOSTO";
const char mes9[]="SEPTIEMBRE";
const char mes10[]="OCTUBRE";
const char mes11[]="NOVIEMRE";
const char mes12[]="DICIEMBRE";
/*en funci�n del n�mero del mes tomo su nombre, relleno con espacios hasta 27-4-longitud nombre mes y agrego el a�o en digitos*/
switch (mes_n){
  case 1:
  printf ("%s",mes1);
  for (int i=1;i<23-strlen(mes1)+1;i++){
    printf(" ");
  }
  printf("%4d",fech_anno);
  break;
  case 2:
  printf ("%s",mes2);
  for (int i=1;i<23-strlen(mes2)+1;i++){
    printf(" ");
  }
  printf("%4d",fech_anno);
  break;
  case 3:
  printf ("%s",mes3);
  for (int i=1;i<23-strlen(mes3)+1;i++){
    printf(" ");
  }
  printf("%4d",fech_anno);
  break;
  case 4:
  printf ("%s",mes4);
  for (int i=1;i<23-strlen(mes4)+1;i++){
    printf(" ");
  }
  printf("%4d",fech_anno);
  break;
  case 5:
  printf ("%s",mes5);
  for (int i=1;i<23-strlen(mes5)+1;i++){
    printf(" ");
  }
  printf("%4d",fech_anno);
  break;
  case 6:
  printf ("%s",mes6);
  for (int i=1;i<23-strlen(mes6)+1;i++){
    printf(" ");
  }
  printf("%4d",fech_anno);
  break;
  case 7:
  printf ("%s",mes7);
  for (int i=1;i<23-strlen(mes7)+1;i++){
    printf(" ");
  }
  printf("%4d",fech_anno);
  break;
  case 8:
  printf ("%s",mes8);
  for (int i=1;i<23-strlen(mes8)+1;i++){
    printf(" ");
  }
  printf("%4d",fech_anno);
  break;
  case 9:
  printf ("%s",mes9);
  for (int i=1;i<23-strlen(mes9)+1;i++){
    printf(" ");
  }
  printf("%4d",fech_anno);
  break;
  case 10:
    printf ("%s",mes10);
  for (int i=1;i<(23-strlen(mes10)+1);i++){
  printf(" ");
  }
  printf("%4d",fech_anno);
  break;
  case 11:
    printf ("%s",mes11);
  for (int i=1;i<23-strlen(mes11)+1;i++){
  printf(" ");
  }
  printf("%4d",fech_anno);
  break;
  case 12:
  printf ("%s",mes12);
  for (int i=1;i<23-strlen(mes12)+1;i++){
    printf(" ");
  }
  printf("%4d",fech_anno);
  break;
}
printf("\n");
/*pinto el encabezado que aparecer� sobre la tabla del calendario*/
printf ("===========================\n");
printf ("LU  MA  MI  JU  VI | SA  DO\n");
printf ("===========================\n");
}
void pintar_mes(int num_dias, int pos_comienzo, int sala_p, int anno_p, int mes_p){
/*pinto los d�as del mes desde 1 al num_dias. El valor pos_comienzo me dice donde comenzar. Si 0 es domingo.*/
int num_lineas=0;
const char punto[]=" .";
int dia_actual=1;
if (pos_comienzo==0) {/* si el primer valor va en la posici�n cero la convierto en 7*/
  pos_comienzo=7;
}
/*calculo el n�mero de l�neas a pintar*/
num_lineas=(num_dias+pos_comienzo-1)/7;
if (((num_dias+pos_comienzo-1)%7)>0){
  num_lineas=num_lineas+1;
}
/*bucle de pintado del cuadro*/
for (int i=1;i<num_lineas+1;i++){/*bucle a repetir por cada l�nea*/
  for (int k=1; k<8;k++){/*bucle desde el lunes al domingo*/
    if (k>=pos_comienzo){
      if (dia_actual<=num_dias){
        /*printf("%2d",dia_actual);*/
        /*compruebo si el d�a DD del mes MM del a�o YYYY para la sala Z est� ocupado. Si lo est� pinto X, en otro caso pinto
        * el valor correspondiente al d�a del mes*/
        if (dia_reservado(sala_p, anno_p, mes_p, dia_actual)){
          printf(" X");
        }else{
          printf("%2d",dia_actual);
        }
        dia_actual=dia_actual+1;
        pos_comienzo=1;
      } else{
        printf(" .");
      }
    } else{
      printf(" .");
    }
      switch(k){
        case 7:
        printf("\n");
        break;
        case 5:
        printf(" | ");
        break;
        default:
        printf("  ");
      }
    }
  }
}
int total_dias(int mes, int anno){
/*en funci�n del mes y a�o da el n�mero de d�as de dicho mes*/
switch(mes){
case 11:
case 4:
case 6:
case 9:
  return(30);
  break;
case 2:
  /*hay qe comprobar si es bisiesto o no*/
  if(anno%4==0){/*multiplo de cuatro*/
        if(!((anno%100==0) && !(anno%400==0)) ){
          return(29);
    }else{
      return(28);
    }
  }else{
  return(28);
  }
  break;
default:
return(31);

}
}
int diaSemanaZeller(int mesz, int annoz){
int v_a=0;
int v_b=0;
if (mesz<=2){
  mesz=mesz+10;
  annoz=annoz-1;
}else{
  mesz=mesz-2;
}
v_a=annoz%100;
v_b=annoz/100;
return(700+((mesz*26-2)/10)+1+v_a+(v_a/4)+(v_b/4)-(v_b*2))%7;
}
bool reserva_s_ok(int sala_m){/*verdadero si existe reserva para esta sala seleccionada*/
bool indicador=false;
int k=0;
while (!indicador && k<id_registro){
  if (registro[k].sala==sala_m){
    indicador=true;
  }
  k++;
}/*fin del while*/
return indicador;
}
bool reserva_s_a_ok(int sala_m,int anno_m){/*verdadero si existe reserva para esta sala y a�o seleccionado*/
bool indicador=false;
int k=0;
while (!indicador && k<id_registro){
  if (registro[k].sala==sala_m && registro[k].anno== anno_m ){
    indicador=true;
  }
  k++;
}/*fin del while*/
return indicador;
}
bool reserva_s_a_m_ok(int sala_m,int anno_m,int mes_m){/*verdadero si hay al menos una reserva para la sala, a�o y mes*/
bool indicador=false;
int k=0;
while (!indicador && k<id_registro){
  if (registro[k].sala==sala_m && registro[k].anno== anno_m && registro[k].mes== mes_m){
    indicador=true;
  }
  k++;
}
  return indicador;
}

void mostrar_calendario(int mes_p, int anno_p){
int aux=0;
if (mes_p>0 && mes_p<13 && anno_p>1600 && anno_p<3001){
  printf("\n");
/*aux=diaSemana(mes_p,anno_p);*/
aux=diaSemanaZeller(mes_p,anno_p);
pintar_cabecera(mes_p,anno_p);
/*printf ("mes %d, a�o %d, valor %d",mes_p, anno_p,total_dias(mes_p,anno_p));*/
/*pintar_mes(total_dias(mes_p,anno_p),aux);*/
} else{
printf("Los datos introducidos no se encuentran entre los l�mites. Int�ntelo de nuevo");
}
}
void listar_usuarios(int sala_l, int anno_l, int mes_l){/*lista las reservas de la sala mes y a�o indicados*/
for (int i=0; i<id_registro+1;i++){
  if(registro[i].sala==sala_l && registro[i].anno==anno_l && registro[i].mes==mes_l){
  /* lo imprimo*/
  printf("D%ca %2d, reservado por %s\n", 161, registro[i].dia, registro[i].solicitante);
  }
}
}

void mostrar_calendario_reservas(){
int sala_m=0;
int anno_m=0;
int mes_m=0;
int aux=0;
bool seguir=true;
/*recojo los datos del calendario a mostrar*/
system("CLS"); /*limpio la pantalla*/
limites_salas();/*carga los l�mites de salas elegibles*/
do{ /*pregunto la sala a mostrar, si la respuesta es erronea (fuera l�mietes) repito pregunta*/
  printf("Indique la sala a mostrar %d-%d:", sala_men_r,sala_may_r);
  fflush(stdin);
  scanf("%d", &sala_m);
  } while (!sala_ok_r(sala_m, sala_men_r, sala_may_r)|| !reserva_s_ok(sala_m));
limites_annos(sala_m); /*para que cargue las variables relativas a a�o menor y mayor*/
  do{/*== hago lo mismo con el a�o, debe de ser valor entre anno_menor y anno_mayor*/
      printf("\nIndique a%co de la reserva %4d-%4d: ", -92,anno_men_r,anno_may_r);
      fflush(stdin);
      scanf("%d", &anno_m); /* Verificar formato de a�o v�lido*/
  } while(!anno_ok_r(anno_m, anno_men_r, anno_may_r)|| !reserva_s_a_ok(sala_m, anno_m));
  /*} while(!anno_ok(anno_m));*/
limites_mes(sala_m, anno_m);
  do{/*compruebo que el mes est� comprendido en el periodo elegible*/
      printf("\nIndique el mes de la reserva %2d-%2d: ",mes_men_r,mes_may_r);
      fflush(stdin);
      scanf("%2d", &mes_m);
    } while(!mes_ok_r(mes_m, mes_men_r, mes_may_r)|| !reserva_s_a_m_ok(sala_m, anno_m, mes_m));
/* la sala est� entre los l�mites elegibles pero debo de asegurarme de que exista reserva en este mes*/
/*====================================================================================
== las condiciones anteriores, y de acceso a este m�dulo obligan a la existencia de registros
== por lo que no es necesario comprobar de nuevo esta situaci�n.
======================================================================================*/
  printf("\nReservas para la Sala %d, a%co %d, mes %d\n\n", sala_m, -92, anno_m, mes_m);
  aux=diaSemanaZeller(mes_m,anno_m);
  pintar_cabecera(mes_m,anno_m);
  pintar_mes(total_dias(mes_m,anno_m), aux, sala_m, anno_m, mes_m);
  printf ("\n");
  listar_usuarios(sala_m, anno_m, mes_m);

/*system ("PAUSE");*/
}
