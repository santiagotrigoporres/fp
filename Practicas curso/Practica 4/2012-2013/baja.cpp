#include <stdio.h>
#include <stdlib.h>
#include <conio.h>
#include<string.h>
#include "cargaDatos.h"
#include "comprobarDatos.h"
int sala_men_r=0;
int sala_may_r=0;
int anno_men_r=0;
int anno_may_r=0;
int mes_men_r=0;
int mes_may_r=0;
int dia_men_r=0;
int dia_may_r=0;
void limites_salas(){/*busco las salas que son elegibles, esto es, tienen reservas*/
sala_men_r=registro[0].sala;
sala_may_r=sala_men_r;
for (int i=0;i<id_registro;i++){/*recorrer� todo el vector buscando la sala m�s baja y la m�s alta*/
  if (sala_men_r>=registro[i].sala){
    sala_men_r=registro[i].sala;
  }
  if (sala_may_r<=registro[i].sala){
    sala_may_r=registro[i].sala;
  }
}/*fin del for*/
}
void limites_annos(int sala){
/*  Este procedimiento obtenr� el menor de los a�os en los que la sala seleccionda est� reservada y el mayor    *
*   asignar� los valores recogidos en las variables anno_men_r y anno_may_r. Para hacerlo recorrer� el vector ya*
*   ordenado y almacenr� el menor valor y el mayor siempre que la sala tenga reserva en dichos a�os            */
anno_men_r=0;
anno_may_r=0;
for (int i=0;i<id_registro;i++){
  if (registro[i].sala==sala ){ /*solo valoro si es de esta sala*/
    if (anno_men_r==0){/*el primer valor ser� el menor*/
      anno_men_r=registro[i].anno;
    }
    if (registro[i].anno>=anno_may_r){
      anno_may_r=registro[i].anno;
    }
    }/* fin i comprobacion*/
  }/*fin del for*/
  }
void limites_mes(int sala, int anno){/*busca el mes menor y mayor para el a�o y sala dada en el que existe reserva*/
mes_men_r=0;
mes_may_r=0;
for (int i=0; i<id_registro;i++){
  if (registro[i].sala==sala && registro[i].anno==anno){
    if (mes_men_r==0){
      mes_men_r=registro[i].mes;
    }
    if (registro[i].mes>=mes_may_r){
      mes_may_r=registro[i].mes;
    }
  }/*fin del if si sala y a�o*/
}/*fin del for*/
}
void limites_dia(int sala, int anno, int mes){/*calculo el d�a del mes seleccionado menor y mayor elegibles*/
  dia_men_r=0;
  dia_may_r=0;
for (int i=0;i<id_registro;i++){
  if (registro[i].sala==sala && registro[i].anno==anno && registro[i].mes==mes){
    if (dia_men_r==0){
      dia_men_r=registro[i].dia;
    }
    if (registro[i].dia>=dia_may_r){
      dia_may_r=registro[i].dia;
    }
  }
}/*fin for*/
}
void borrar_registro(int sala, int anno, int mes, int dia){/*elimina el registro seleccionado y reorganiza el vector para mantener orden*/
bool encontrado=false;
int combi=0;
if (id_registro>1){
  combi=(anno*100000)+(mes*1000)+(dia*10)+sala;
  for (int i=0; i<id_registro; i++){/*tengo que recorrer el vector completo*/
    if (registro[i].combi_f==combi){
      encontrado=true;
    }
    if (encontrado){/*como ya ha sido encontrado muevo el registro de la posici�n i+1 a la posicion de i*/
      registro[i].sala=registro[i+1].sala;
      registro[i].anno=registro[i+1].anno;
      registro[i].mes=registro[i+1].mes;
      registro[i].dia=registro[i+1].dia;
      registro[i].combi_f=registro[i+1].combi_f;
      strcpy(registro[i].solicitante,registro[i+1].solicitante);
    }
  } /*fin del for*/
} else{/*solamente est� el registro 0*/
  registro[0].sala=0;
  registro[0].anno=0;
  registro[0].mes=0;
  registro[0].dia=0;
  registro[0].combi_f=0;
  /*strcpy(" ",registro[0].solicitante);*/
}
id_registro=id_registro-1; /*descuento el registro borrado*/
}


void eliminar_r(){
/*declaraci�n de variables*/
int sala_b=0;
int anno_b=0;
int mes_b=0;
int dia_b=0;
bool seguir=true;
char tecla;
bool tecla_correcta=false;
/*fin declaraci�n de variables*/
/*recopila los datos para proceder a la baja*/
system ("CLS");
limites_salas();
  do{ /*== pregunto por la sala y me aseguro de que el valor sea comprendido entre m�nimo y maximo entre los posibles*/
      printf("Indique la ala entre los valores elegibles (%1d-%1d):", sala_men_r, sala_may_r);
      fflush(stdin);
      scanf("%d", &sala_b);
  } while (!sala_ok_r(sala_b, sala_men_r, sala_may_r));
if (!reserva_sala(sala_b)){
  printf("No hay ninguna reserva en el sistema para la sala seleccionada");
  seguir=false;
}
/* IDEA si no hay reserva para esta sala entre todas las reservas lo aviso y vuelvo al men� principa*/
if (seguir){/*compruebo que la prueba anterior ha sido correcta, es decir que hay reservas para esta sala*/
  limites_annos(sala_b); /*para que cargue las variables que quiero mostrar a continuaci�n*/
    do{/*== hago lo mismo con el a�o, debe de ser valor entre a�o "menor"  y a�o "mayor" que tenga reserva*/
        printf("\nIndique a%co de la reserva entre los valores (%4d-%4d): ", -92,anno_men_r, anno_may_r);
        fflush(stdin);
        scanf("%d", &anno_b); /* Verificar formato de a�o v�lido*/
      } while(!anno_ok_r(anno_b, anno_men_r, anno_may_r));
  /*verifico que exita reserva para el a�o indicado*/
  if (!reserva_s_a(sala_b,anno_b)){
    printf("No hay ninguna reserva para esta sala en el a%co %4d", -92, anno_b);
    seguir=false;
  }
  if (seguir){/*he comprobado que sala y a�o son correctos continuo con el mes*/
  limites_mes(sala_b, anno_b);
    do{/*== hago lo mismo con el mes, debe de ser valor entre mes "menor"  y mes "mayor" que tenga reserva para esa sala y a�o*/
        printf("\nIndique el mes de la reserva entre los valores (%2d-%2d): ", mes_men_r, mes_may_r);
        fflush(stdin);
        scanf("%d", &mes_b); /* Verificar formato de a�o v�lido*/
      } while(!mes_ok_r(mes_b, mes_men_r, mes_may_r));
if (!reserva_s_a_m(sala_b, anno_b, mes_b)){
  printf ("No hay ninguna reserva para la sala %d, en el a%co %4d y mes %2d", sala_b, -92, anno_b, mes_b);
  seguir=false;
  }
  if (seguir){/*paso a comprobar el d�a*/
  limites_dia(sala_b, anno_b, mes_b);
  do{/*compruebo que el d�a introducido est� entre los l�mites para esa sala, a�o, y mes*/
        printf("\nIndique el d%ca de la reserva a eliminar entre los valores (%2d-%2d): ", 161, dia_men_r, dia_may_r);
        fflush(stdin);
        scanf("%d", &dia_b); /* Verificar formato de a�o v�lido*/
  }while (!mes_ok_r(dia_b,dia_men_r, dia_may_r));
if (!reserva_s_a_m_d(sala_b, anno_b, mes_b, dia_b)){
  printf ("No hay ninguna reserva para la sala %d, en el a%co %4d,  mes %2d d%ca, %2d", sala_b, -92, anno_b, mes_b, 161, dia_b);
  seguir=false;
}
  }/*cierre del tercer seguir (sala, a�o y mes correctos)*/
  }/*cierre del segundo seguir (la sala y a�o son correctos)*/
}/*cierre del 1er seguir de la pantalla*/
if (seguir){

  while (!tecla_correcta){ /*compruebo si el usuario pulsa las teclas s � S � n � N para ejecutar la acci�n en caso afirmativo o salir en negativo*/
    system ("CLS");
    printf("Est%c seguro de que va a dar de baja la reserva para la sala %2d, a%co %4d, mes %2d y d%ca %2d S/N", 160,  sala_b, -92, anno_b, mes_b,161, dia_b);
    tecla=getche();
  if (tecla=='s' || tecla=='S'){/*caso afirmativo, ejecuto el borrado*/
    borrar_registro(sala_b, anno_b, mes_b, dia_b);
    tecla_correcta=true;
  }
  if (tecla=='n' || tecla=='N'){/*caso negativo, salgo sin hacer m�s*/
    tecla_correcta=true;
  }
  }/*fin del while*/
}
}
