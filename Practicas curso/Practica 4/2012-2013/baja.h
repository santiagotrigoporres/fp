/*************************************************************************************************************************
*   Este m�dulo contiene procedimientos para la eliminaci�n de la reserva seleccionada por el usuario.
*   de la misma forma que el m�dulo de calenario solamente admite como v�lidos los datos que correspondan a reservas para
*   la sala dada, este m�dulo hace lo propio. Recopila la selecci�n del usuario y antes de ejecutarla pide confirmaci�n.
*
*************************************************************************************************************************/
#pragma once
extern int sala_men_r;
extern int sala_may_r;
extern int anno_men_r;
extern int anno_may_r;
extern int mes_men_r;
extern int mes_may_r;
/*procedimientos comunes con la carga del calendario*/
void limites_salas();
void limites_annos(int sala);
void limites_mes(int sala, int anno);
/*procedimientos propios llamado desde el m�dulo principal*/
void eliminar_r();
