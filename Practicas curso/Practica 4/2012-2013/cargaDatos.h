/**********************************************************************************************************************
*   Interface para el m�dulo de carga de datos.
*   Mostrar� al usuario la pantalla para introducir la sala a seleccionar, a�o, mes y dia as� como el nombre del solicitante
*   para guardar los datos obliga al usuario a que estos s�an v�lidos. No admite como fecha v�lida para la reserva los
*   s�bados o domingos ni fechas tipo "30 de febrero".
************************************************************************************************************************/
#pragma once
/*definici�n de variables*/
const int largo_sol=100;/*longitud m�xima para el campo solicitante*/
typedef struct t_dato_originales{
  int sala;
  int anno;
  int mes;
  int dia;
  char solicitante[largo_sol];
  int combi_f; /*campo para combinar yyyymmdds A�oMesDiaSala y poder ordenar*/
};
const int maximo_registros=100; /*define el m�ximo de registros que la aplicaci�n puede manejar*/
typedef t_dato_originales registros[maximo_registros];
extern registros registro; /*asigno al elemento registro la definicion de registros*/
extern int id_registro;
/*funciones*/
int diaSemanaZeller2(int mesz, int annoz, int diaz);
/*procedimientos*/
void inicializar_registros();
void recoger_datos();/*== Esta funci�n cargar� los datos facilitados por el usuario en el vector de datos registrados ==*/

