#include "cargaDatos.h"
#include "comprobarDatos.h"
#include <stdio.h>
#include<string.h>
#include <stdlib.h>
void inicializar_registros(){/*inicializa el vector registros*/
  for (int i=0; i <id_registro;i++){/*para recorrer todo el vector*/
    registro[i].sala=0;
    registro[i].anno=0;
    registro[i].mes=0;
    registro[i].dia=0;
    registro[i].combi_f=0;
    strcpy(" ", registro[i].solicitante);
  }
id_registro=0;
}

void insertar_registro(int sala_r, int anno_r, int mes_r, int dia_r, char solicitante_r[], int & id_registro){
/* este procedimiento auxiliar guarda el registro en su posici�n correspondiente en funci�n del a�o, mes, dia*/
int combi_n=0;
int pos_vector=0;
int k=id_registro;
int i=0;
bool saltar_for=false;
combi_n=(anno_r*100000)+(mes_r*1000)+(dia_r*10)+sala_r; /*valor para posicionar el registro*/
if (id_registro==0){ /*para el primer registro de la carga*/
    registro[0].sala=sala_r;
    registro[0].anno=anno_r;
    registro[0].mes=mes_r;
    registro[0].dia=dia_r;
    strcpy(registro[0].solicitante, solicitante_r);
    registro[0].combi_f=combi_n;
    id_registro=id_registro+1; /* he cargado el primer registro y modificado el valor del id_registro*/
}else { /*nos encontramos en un caso posterior a la primera carga. Habr� que localizar su posicion*/
  while (!saltar_for){ /* repito el bucle hasta que el valor sea verdadero*/
  /* puede sucecer que el combi_n sea mayor, igual o menor que el combi_ra
  *  si es menor o igual tendr� que mover el actual y siguientes una posici�n hac�a "arriba" y en su
  *  hueco insertarlo. Si es mayor y no he llegado al l�mite de registros cargados lo dejo para la
  *  siguiente prueba. Si he llegado al l�mite y es mayor lo insertar� en la nueva posici�n*/
   if (combi_n<=registro[i].combi_f){
      /*muevo el registro actual y siguientes una posici�n "arriba" empezando por el �ltimo*/
      k=id_registro;
      while (k>i){ /*muevo los registros ya cargados*/
        registro[k].sala=registro[k-1].sala;
        registro[k].anno=registro[k-1].anno;
        registro[k].mes=registro[k-1].mes;
        registro[k].dia=registro[k-1].dia;
        strcpy(registro[k].solicitante,registro[k-1].solicitante);
        registro[k].combi_f=registro[k-1].combi_f;
        k--;
      }
      pos_vector=i;
      saltar_for=true;
      k=0;
      }else if(id_registro==i) {
        pos_vector=id_registro;
        saltar_for=true;
        k=0;
      }
      i++;
  }/*cierre del while */
  /*grabo el registro en la posici�n pos_vector*/
  registro[pos_vector].sala=sala_r;
  registro[pos_vector].anno=anno_r;
  registro[pos_vector].mes=mes_r;
  registro[pos_vector].dia=dia_r;
  strcpy(registro[pos_vector].solicitante, solicitante_r);
  registro[pos_vector].combi_f=combi_n;
  id_registro++;
 } /*cierra el elseque se ejecuta en el caso de haber ya un registro*/
}

/*==  Este procedimiento se encargar de pedir al usuario los datos relativos a la reserva de la sala. Los
*     cargar� en el caso de ser "correctos" en el vector de forma ordenada                     ==*/
void recoger_datos(){
/*variables que intervienen*/
int dia_mayor=1;
int sala=0;
int anno=0;
int mes=0;
int dia=0;
typedef char cadena_sol[largo_sol];
cadena_sol solicitante;
/* ejecuci�n*/
system("CLS");
if (id_registro >=maximo_registros){ /*Compruebo que no tenga m�s registros de los permitido*/
  printf("El sistema ha registrado ya %2d solicitudes de sala. No es posible agregar m%cs.\nBorre alguna para continuar.", id_registro,160);
} else{
  do{ /*== pregunto por la sala y me aseguro de que el valor sea comprendido entre m�nimo y maximo*/
      printf("Indique la sala a reservar %1d-%1d: ",sala_min, sala_max);
      fflush(stdin);
      scanf("%d", &sala);
  } while (!sala_ok(sala));
  do{/*== hago lo mismo con el a�o, debe de ser valor entre anno_menor y anno_mayor*/
      printf("\nIndique a%co de la reserva %4d-%4d: ", -92,anno_menor,anno_mayor);
      fflush(stdin);
      scanf("%d", &anno); /* Verificar formato de a�o v�lido*/
  } while(!anno_ok(anno));
  do{/*compruebo que el mes est� comprendido en el periodo elegible*/
      printf("\nIndique el mes de la reserva %2d-%2d: ",mes_menor,mes_mayor);
      fflush(stdin);
      scanf("%d", &mes);
  } while (!mes_ok(mes));
  /*obtengo el d�a mayor del mes*/
  dia_mayor=f_dia_mayor(anno,mes);
  do{/*compruebo que el d�a seleccionado sea correcto en funci�n del mes a�o y s�bados y domingos*/
      printf("\nIndique el d%ca a reservar, debe de estar comprendido entre %2d-%2d y no ser s%cbado o domingo: ",161, dia_menor, dia_mayor,160);
      fflush(stdin);
      scanf("%d", &dia);
  }while (!dia_ok(anno, mes,dia));
  /*trato de comprobar que el nombre de usuario este formado por palabra espacio palabra*/
  do{
  printf("\nIntroduzca su nombre y dos apellidos: ");
  fflush(stdin);
  gets(solicitante);
  }while (!nombre_ok(solicitante));
  /*si ha llegado a este punto es porque todos los valores introducidos son v�lidos. Hay que comprobar si la sala est� libre
  el d�a seleccionado. Si lo est� habr� que guardar el registro en su posici�n correcta del vector.*/
  if (sala_libre(sala,anno,mes,dia, registro)){
    /*al estar libre la sala guardo los datos mediante el procedimiento de ordenaci�n*/
    insertar_registro(sala,anno,mes,dia,solicitante, id_registro);
    }else{
    printf("La sala est%c reservada para este d%ca, repita el proceso de nuevo.",160,161);
    }
  }
}
