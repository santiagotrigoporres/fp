
/**************************
         LIBRERIAS
 **************************/

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <ctype.h>

    #include "OfertasMes.h"



 /**************************
 FUNCIONES Y PROCEDIMIENTOS
 **************************/


   /*=========================
            PROGRAMA
   ==========================*/

      /*OFERTAS DEL MES*/
        void ordenarVector(tipoVector vector, tipoVector orden){                                                /**ORDENA LOS VUELOS POR PRECIOS**/

          int aux1 = 0;
          int aux2 = 0;


          for (int j = 0; j < GestionVuelosMesGlobal.longitudVectorVuelos; j++){
            for (int m = 0; m < GestionVuelosMesGlobal.longitudVectorVuelos; m++){
              if (vector[m] > vector[m+1]){
                aux1 = vector[m+1];
                vector[m+1] = vector[m];
                vector[m] = aux1;

                aux2 = orden[m+1];
                orden[m+1] = orden[m];
                orden[m] = aux2;
              }
            }
          }

          if (vector[0] < 0){                                                                                   /**ESTA PARTE ARREGLA UN BUG DE MEMORIA DEL QUE NO PUDE ENCONTRAR EXPLICACION**/
            for (int n = 0; n < 6; n++){
              orden[n] = orden[n+1];
              vector[n] = vector[n+1];
            }
          }

        }

        void mostrarOM(int mes, int anno){                                                                      /**DA FORMATO A LA INFORMACION DE LOS VUELOS**/

          int posicion = 0;
          int aux;
          tipoVector precios;
          tipoVector posicionPrecios = {0,0,0,0,0};


          for (int i = 0; i < GestionVuelosMesGlobal.longitudVectorVuelos; i++){
            precios[i] = GestionVuelosMesGlobal.listaVuelos[i].precioActual;
            posicionPrecios[i] = i;

          }


          ordenarVector(precios, posicionPrecios);


          printf("\t%s   %s   %s   \t%s   \t    %s\n", formatoVuelosOM[0], formatoVuelosOM[1],
                  formatoVuelosOM[2], formatoVuelosOM[3], formatoVuelosOM[4]);

         for(int j = 0; j < 5; j++){

            printf("\t%d", j+1);
            posicion = posicionPrecios[j];

            printf("\t%d", GestionVuelosMesGlobal.listaVuelos[posicion].precioActual);

            formatoVuelosMes(GestionVuelosMesGlobal.listaVuelos[posicion].precioActual);
            printf("%s", GestionVuelosMesGlobal.listaVuelos[posicion].codigo);

            aux = strlen(GestionVuelosMesGlobal.listaVuelos[posicion].codigo);
            aux = 15-aux;
            for (int k = 1; k <= aux; k++){
              printf(" ");
            }
            printf("%d", GestionVuelosMesGlobal.listaVuelos[posicion].dia);

            printf("   ");

            formatoVuelosMes(GestionVuelosMesGlobal.listaVuelos[posicion].dia);
            printf("%d:00\n", GestionVuelosMesGlobal.listaVuelos[posicion].hora);
         }




        }

        void responderOM(){                                                                                     /**DA FORMATO A LA INFORMACION DE LOS VUELOS**/

          tipoVector datos;
            tipoVector vectorAuxiliar;
            tipoTexto variable;
            tipoTexto auxiliarString;
            tipoTexto texto1;
            tipoTexto texto2;
            int auxiliarInt;
            int min;
            int max;

            try{

              for(int i = 0; i < 2; i++) {
                  min = limitesVM[i*2];
                  max = limitesVM[i*2 + 1];
                  strcpy(variable, listaOpcionesVM[i]);
                  datos[i] = respuestaLimites(listaOpcionesVM[i], min, max);
              }

              imprimirLinea();

              mostrarOM(datos[0], datos[1]);

            } catch(int error){
                  switch(error){
                    case (0):
                      imprimirLinea();
                      opcionError(textoError[4]);
                      break;
                    case (1):
                      imprimirLinea();
                      sprintf(texto1, "%d", max);
                      sprintf(texto2, "%d", min);
                      escribirErrorLimites(variable, texto1, texto2);
                      break;
                  }
            }

          }

        void ofertasMes(){                                                                                      /**LLAMA A LAS FUNCIONES**/

          imprimirLinea();


          tituloMenu(textoOfertasMes);

          responderOM();


          imprimirLinea();

        }
