
/**************************
         LIBRERIAS
 **************************/

    #pragma once



/**************************
      TIPOS Y CONSTANTES
 **************************/

    extern bool salir;

    const int anchoMaximo = 60;                                                                                       /**CONTROLA EL ANCHO DEL PROGRAMA. MINIMO EN 60. SOLO MULTIPLOS DE 10**/
    const int tabulacionesIzquierda = 8;                                                                              /**SON LAS TUBULACIONES DE UNA PREGUNTA**/
    const char caracterLinea = '-';                                                                                   /**EL CARACTER QUE SE USA PARA ESCRIBIR LA LINEA**/
    const char caracterError = '=';                                                                                   /**EL CARACTER QUE SE USA PARA ESCRIBIR EL ERROR**/


    typedef char tipoTexto[100];

        extern tipoTexto textoGestionVuelos;
        extern tipoTexto textoAltaNuevoVuelo;
        extern tipoTexto textoComprarPlazas;
        extern tipoTexto textoVuelosMes;
        extern tipoTexto textoOfertasMes;
        extern tipoTexto textoTeclearOpcionValida;

    typedef int tipoVector[100];

        extern tipoVector limitesGlobales;

        extern tipoVector diasMes;

        extern tipoVector limitesCP;

        extern tipoVector limitesVM;



    typedef tipoTexto tipoTextoMatriz[100];

        extern tipoTextoMatriz avion;

        extern tipoTextoMatriz textoError;

        extern tipoTextoMatriz listaOpcionesMP;

        extern tipoTextoMatriz listaOpcionesANV;

        extern tipoTextoMatriz listaOpcionesCP;
        extern tipoTextoMatriz formatoVuelosCP;
        extern tipoTextoMatriz listaComprarCP;

        extern tipoTextoMatriz listaOpcionesVM;

        extern tipoTextoMatriz listaOpcionesOM;
        extern tipoTextoMatriz formatoVuelosOM;

    typedef char tipoTextoLista[100];

        extern tipoTextoLista listaOpcionesRespuestaMP;

    typedef struct FormatoVuelo{                                                                                      /**EL TAD CON EL FORMATO DE LOS VUELOS**/

        tipoTexto codigo;
        int dia;
        int mes;
        int anno;
        int hora;
        int plazasIniciales;
        int precioInicial;
        int plazasLibres;
        int precioActual;

    };

    typedef FormatoVuelo vectorVuelo[1200];                                                                           /**EL VECTOR QUE GUARDA LOS VUELOS**/


    typedef struct GestionVuelosMes {                                                                                 /**EL TAD QUE GESTIONA LOS VUELOS**/

      vectorVuelo listaVuelos;
      int longitudVectorVuelos;

      void nuevoVuelo(FormatoVuelo vuelo);
      void recalcularPrecios(FormatoVuelo vuelo, int posicion);
      void buscarVuelo(tipoVector datos, int &numeroVuelos, tipoVector vectorAuxiliar);
      void auxiliar();

    };

    extern GestionVuelosMes GestionVuelosMesGlobal;






/**************************
 FUNCIONES Y PROCEDIMIENTOS
 **************************/

  /*=========================
            GLOBALES
   ==========================*/


    /*ESCRITURA CONTINUADA DE CARACTERES*/
      void escrituraCaracter(char caracter, int repeticiones);


    /*IMPRIMIR LINEAS MENU*/
      void imprimirLinea();


    /*TITULO MENU*/
      void tituloMenu(tipoTexto texto);



    /*ESCRIBIR TABULACIONES*/
      void tabulaciones(tipoTexto texto, int extras);


    /*CENTRAR TEXTO*/
      void centrarTexto(tipoTexto texto, char caracter);


    /*OPCION ERRONEA*/
      void opcionError(tipoTexto texto);

      void escribirErrorLimites(tipoTexto variable, tipoTexto limiteMax, tipoTexto limiteMin);


    /*PEDIR RESPUESTA*/
      void pedirRespuesta(bool menu, tipoTexto respuesta);


    /*FORMATO PREGUNTAS*/
      void formatoPreguntas(tipoTexto texto);


    /*OPCIONES PREGUNTAS*/
      void pedirRespuesta(bool menu, bool string, tipoTexto respuesta);


    /*TRANSFORMAR STRING EN INT*/
      int transformarString(tipoTexto texto);


    /*COMPROBAR LIMITES*/
      void comprobarLimites(int respuesta, int min, int max);


    /*CABECERA*/
      void cabecera();


    /*RESPUESTA CON LIMITES*/
      int respuestaLimites(tipoTexto opcion, int min, int max);


    /*FORMATO PARA MOSTRAR VUELOS*/
      void formatoVuelosMes(int numero);


    /*COMPROBAR ANNO BISIESTO*/
      bool bisiesto(int year);

