
 /**************************
         LIBRERIAS
 **************************/

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <ctype.h>

    #include "menuPrincipal.h"



 /**************************
 FUNCIONES Y PROCEDIMIENTOS
 **************************/


   /*=========================
            PROGRAMA
   ==========================*/


    /*MENU PRINCIPAL*/

      void formatoMP(tipoTexto texto, char respuesta){                                                              /**DA FORMATO A LAS PREGUNTAS**/

        printf("\t");
        printf("~ %s", texto);
        tabulaciones(texto, 2);
        printf("(Pulsar %c)", respuesta);
        printf("\n");

      }

      void opcionesMP(){                                                                                            /**IMPRIME LAS OPCIONES**/

        int posicionTexto = 0;
        int posicionOpciones = 1;


        while(listaOpcionesMP[posicionTexto][0] != '\0') {
            formatoMP(listaOpcionesMP[posicionTexto], listaOpcionesRespuestaMP[posicionOpciones]);
            posicionOpciones = posicionOpciones + 2;
            posicionTexto++;
        }

        printf("\n");
        printf("   %s (%c|%c|%c|%c|%c)", textoTeclearOpcionValida,
                listaOpcionesRespuestaMP[1], listaOpcionesRespuestaMP[3], listaOpcionesRespuestaMP[5],
                listaOpcionesRespuestaMP[7], listaOpcionesRespuestaMP[9]);
        printf("\n");

      }

      void comprobarRespuestaMP(char respuesta, tipoTexto longitudRespuesta){                                       /**COMPRUEBA LAS RESPUESTAS**/

        int longitud = strlen(listaOpcionesRespuestaMP);
        int posicion = -1;
        tipoTexto textoError;


        if(longitudRespuesta[0] == '\0' || longitudRespuesta[1] != '\0') {
          strcpy(textoError, "La opcion no existe y es demasiado larga");
          opcionError(textoError);
          return;
        }

        for ( int i = 0 ; i < longitud ; i++){
          if (respuesta == listaOpcionesRespuestaMP[i]){
            posicion = i;
          }
        }

        switch (posicion){
          case 0:
          case 1:
            altaNuevoVuelo();
            break;
          case 2:
          case 3:
            ofertasMes();
            break;
          case 4:
          case 5:
            comprarPlazas();
            break;
          case 6:
          case 7:
            vuelosMes();
            break;
          case 8:
          case 9:
            salir = true;
            break;
          default:
            strcpy(textoError, "La opcion no existe");
            opcionError(textoError);
            return;
        }

      }

      void menuPrincipal(){                                                                                         /**LLAMA A LAS FUNCIONES**/


        tipoTexto respuesta;


        imprimirLinea();


        tituloMenu(textoGestionVuelos);

        opcionesMP();


        imprimirLinea();

        pedirRespuesta(true, respuesta);

        comprobarRespuestaMP(respuesta[0], respuesta);

      }


