
/**************************
         LIBRERIAS
 **************************/

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <ctype.h>

    #include "VuelosPredefinidos.h"

/**************************
      TIPOS Y CONSTANTES
 **************************/


      tipoTextoMatriz VP_codigos      = {"UNED-0001", "UNED-0002", "UNED-0003", "UNED-0004", "UNED-0005",
                                         "UNED-0006", "UNED-0007", "UNED-0008", "UNED-0009", "UNED-0010",
                                         "UNED-0011", "UNED-0012", "UNED-0013", "UNED-0014", "UNED-0015"};

      tipoVector VP_dias              =  {    25,         13,           12,         7,            7,
                                              18,         15,           11,         1,            3,
                                              27,         13,           25,         21,           21};

      tipoVector VP_meses             = {      5,         3,            2,           2,           2,
                                               7,         12,           10,          8,           1,
                                               1,          1,            1,          1,           1};

      tipoVector VP_annos             = {limitesGlobales[4],limitesGlobales[4],limitesGlobales[4],limitesGlobales[4],limitesGlobales[4],
                                         limitesGlobales[5],limitesGlobales[5],limitesGlobales[5],limitesGlobales[5],limitesGlobales[5],
                                         limitesGlobales[5],limitesGlobales[5],limitesGlobales[5],limitesGlobales[5],limitesGlobales[5]};

      tipoVector VP_horas             = {       4,         12,           5,          14,          16,
                                               23,         20,           6,          21,           8,
                                                3,          8,          14,          16,          20};

      tipoVector VP_plazasIniciales   = {     150,        100,          90,          75,          110,
                                              200,        185,          85,          60,           50,
                                              160,        140,         135,          35,          165};

      tipoVector VP_precioInicial     = {     150,        100,          90,          75,          110,
                                              200,        185,          85,          60,           50,
                                              160,        140,         135,          35,          165};

      tipoVector VP_plazasLibres      = {     150,        100,          90,          75,          110,
                                              200,        185,          85,          60,           50,
                                              160,        140,         135,          35,          165};

      tipoVector VP_precioActual      = {     150,        100,          90,          75,          110,
                                              200,        185,          85,          60,           50,
                                              160,        140,         135,          35,          165};


 /**************************
 FUNCIONES Y PROCEDIMIENTOS
 **************************/

   /*=========================
             VUELOS
   ==========================*/

      void rellenarVuelos(){

        FormatoVuelo vuelo;


        for (int i = 0; i < 15; i++){

          strcpy(vuelo.codigo, VP_codigos[i]);
          vuelo.dia = VP_dias[i];
          vuelo.mes = VP_meses[i];
          vuelo.anno = VP_annos[i];
          vuelo.hora = VP_horas[i];
          vuelo.plazasIniciales = VP_plazasIniciales[i];
          vuelo.precioInicial = VP_precioInicial[i];
          vuelo.plazasLibres = VP_plazasLibres[i];
          vuelo.precioActual = VP_precioActual[i];

          GestionVuelosMesGlobal.nuevoVuelo(vuelo);

        }

      }
