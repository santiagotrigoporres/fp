
/**************************
         LIBRERIAS
 **************************/

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <ctype.h>

    #include "VuelosMes.h"



 /**************************
      TIPOS Y CONSTANTES
 **************************/


  VM_Vector VM_LETRAS_MES = {5, 7, 5, 5, 4, 5, 5, 6, 10, 7, 9, 9};                                      /**VECTOR CON LAS LETRAS DE CADA MES**/

  CalendarioMes CalendarioMesGlobal;                                                                    /**EL TAD*/

  int VM_ESPACIOS = 24;                                                                                 /**ESPACIOS PARA CALCULOS**/





 /**************************
 FUNCIONES Y PROCEDIMIENTOS
 **************************/


   /*=========================
            PROGRAMA
   ==========================*/

      /*VUELOS DEL MES*/

          void CalendarioMes::auxiliarVM(){                                                             /**ASIGNA EL A�O MINIMO PARA EL CALCULO**/
            VM_MIN_YEAR = 1601;

          }

          void CalendarioMes::leerMes(VM_Mes mes) {                                                     /**IMPRIME EL MES**/
            switch (mes) {
              case ENERO:
                printf("Enero");
                break;
              case FEBRERO:
                printf("Febrero");
                break;
              case MARZO:
                printf("Marzo");
                break;
              case ABRIL:
                printf("Abril");
                break;
              case MAYO:
                printf("Mayo");
                break;
              case JUNIO:
                printf("Junio");
                break;
              case JULIO:
                printf("Julio");
                break;
              case AGOSTO:
                printf("Agosto");
                break;
              case SEPTIEMBRE:
                printf("Septiembre");
                break;
              case OCTUBRE:
                printf("Octubre");
                break;
              case NOVIEMBRE:
                printf("Noviembre");
                break;
              case DICIEMBRE:
                printf("Diciembre");
                break;
            }
          }

          int CalendarioMes::calculoDiaSemana(int year, int mes) {                                      /**CALCULA EN QUE DIA DE LA SEMANA EMPIEZA EL MES**/
                int numDias = 0;
                int numDiasMeses = 0;

                for (int yearActual = CalendarioMesGlobal.VM_MIN_YEAR; yearActual < year; yearActual++) {
                  if (bisiesto(yearActual)) {
                    numDias = numDias + 366;
                  }
                  else {
                    numDias = numDias + 365;
                  }
                  numDias = numDias % 7;
                }

                if (bisiesto(year)) {
                  diasMes[1] = 29;
                }

                for (int mesActual = 0; mesActual < mes; mesActual++) {
                  numDiasMeses = numDiasMeses + diasMes[mesActual];

                }

                numDias = (numDias + numDiasMeses) % 7;

                return numDias;

              }

          void CalendarioMes::escribirBarra(int diaSemana, int diaActual) {                             /**ESCRIBE LA BARRA QUE SEPARA LOS DIAS**/
            if (diaSemana == 4) {
                printf(" | ");
            }
            else {
              printf("  ");
            }
          }

          void CalendarioMes::escribirEspacios(int diaSemana) {                                         /**ESCRIBE LOS ESPACIOS DEL PRINCIPIO**/
            int aux = 0;


            printf("\t");
            if (diaSemana >= 1) {
              for (int diaVacio = 0; diaVacio < diaSemana; diaVacio++) {
                printf("  ");
                escribirBarra(diaVacio, aux);
              }
            }
          }

          void CalendarioMes::escribirCabecera(VM_Mes mes, int espacios, int year) {                    /**ESCRIBE LA CABECERA**/
            printf("\t");
            CalendarioMesGlobal.leerMes(mes);
              for (int i=1; i<=espacios-VM_LETRAS_MES[int(mes)]; i++) {
            printf(" ");
            }

            printf("%d", year);
            printf("\n\t");
            printf("____________________________\n");
            printf("\t");
            printf(" L   M   M   J   V |  S   D");
            printf("\n");

         }

          void CalendarioMes::escribirDias(int mes, int diaSemana, int year) {                          /**ESCRIBE LOS DIAS DEL CALENDARIO**/

            tipoVector datos;
            tipoVector vectorAuxiliar;
            int auxiliarInt = 0;


            datos[0] = 0;
            datos[1] = mes+1;
            datos[2] = year;

            for (int diaActual = 1; diaActual <= diasMes[mes]; diaActual++) {

              datos[0] = diaActual;

              try{
                  GestionVuelosMesGlobal.buscarVuelo(datos, auxiliarInt, vectorAuxiliar);
                  } catch (int Error){}

              if(auxiliarInt > 0){
                if (diaActual < 10){
                  printf("0%d", diaActual);
                }else{
                  printf("%d", diaActual);
                }
              }else{
                printf("--");
              }

              CalendarioMesGlobal.escribirBarra(diaSemana, diaActual);

              if (diaSemana == 6) {
                printf("\n");
                printf("\t");
                diaSemana = 0;
              }
              else {
                diaSemana = diaSemana+1;
              }
            }

          }

          void CalendarioMes::calendario(VM_Mes mes, int espacios, int year) {                          /**LLAMA A LAS FUNCIONES QUE ESCRIBEN EL CALENDARIO*/
          int diaSemana = 0;

          printf("\n");

          CalendarioMesGlobal.escribirCabecera(mes, espacios, year);

          diaSemana = CalendarioMesGlobal.calculoDiaSemana(year, int (mes));

          CalendarioMesGlobal.escribirEspacios(diaSemana);

          CalendarioMesGlobal.escribirDias(mes, diaSemana, year);

          printf("\n\n");

          }



          void responderVM(){                                                                           /**SE ENCARGA DE RECOGER LAS RESPUESTAS**/

            tipoVector datos;
            tipoVector vectorAuxiliar;
            tipoTexto variable;
            tipoTexto auxiliarString;
            tipoTexto texto1;
            tipoTexto texto2;
            int auxiliarInt;
            int min;
            int max;

            try{

              for(int i = 0; i < 2; i++) {
                  min = limitesVM[i*2];
                  max = limitesVM[i*2 + 1];
                  strcpy(variable, listaOpcionesVM[i]);
                  datos[i] = respuestaLimites(listaOpcionesVM[i], min, max);
              }

              imprimirLinea();

              CalendarioMesGlobal.calendario(VM_Mes((datos[0]-1)), VM_ESPACIOS, (datos[1]));

            } catch(int error){
                  switch(error){
                    case (0):
                      imprimirLinea();
                      opcionError(textoError[4]);
                      break;
                    case (1):
                      imprimirLinea();
                      sprintf(texto1, "%d", max);
                      sprintf(texto2, "%d", min);
                      escribirErrorLimites(variable, texto1, texto2);
                      break;
                  }
            }

          }

          void vuelosMes(){                                                                             /**LLAMA LAS FUNCIONES**/


            imprimirLinea();

            CalendarioMesGlobal.auxiliarVM();

            tituloMenu(textoVuelosMes);

            responderVM();

            diasMes[1] = 28;


            imprimirLinea();

          }


