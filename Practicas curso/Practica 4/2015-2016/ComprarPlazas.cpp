
/**************************
         LIBRERIAS
 **************************/

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <ctype.h>

    #include "ComprarPlazas.h"



 /**************************
 FUNCIONES Y PROCEDIMIENTOS
 **************************/


   /*=========================
            PROGRAMA
   ==========================*/


      /*COMPRAR PLAZAS*/


        void elegirVuelo(tipoVector vuelos, int numeroVuelos, int min, int max){                                  /**CONTROLA QUE VUELO SE ELIGE**/

         int posicion;
         int numVuelo;
         int numPlazas;

         tipoVector datos;
         FormatoVuelo vuelo;
         tipoTexto variable;
         tipoTexto auxiliarString;
         tipoTexto texto1;
         tipoTexto texto2;
         int auxiliarInt;

         imprimirLinea();

        try{
         printf("\t%s   %s       %s   %s   %s\n", formatoVuelosCP[0], formatoVuelosCP[1],                         /**IMPRIME LA CABECERA DE LOS VUELOS**/
                  formatoVuelosCP[2], formatoVuelosCP[3], formatoVuelosCP[4]);

         for(int i = 1; i <= numeroVuelos; i++){                                                                  /**DA FORMATO A LA INFORMACION DE LOS VUELOS**/
            posicion = vuelos[i-1];

            printf("\t%d", i);

            printf("\t%s", GestionVuelosMesGlobal.listaVuelos[posicion].codigo);

            auxiliarInt = strlen(GestionVuelosMesGlobal.listaVuelos[posicion].codigo);
            auxiliarInt = 13-auxiliarInt;
            for (int i = 1; i <= auxiliarInt; i++){
              printf(" ");
            }
            printf("%d:00", GestionVuelosMesGlobal.listaVuelos[posicion].hora);

            printf("   ");
            printf("%d", GestionVuelosMesGlobal.listaVuelos[posicion].plazasLibres);

            formatoVuelosMes(GestionVuelosMesGlobal.listaVuelos[posicion].plazasLibres);
            printf("%d\n", GestionVuelosMesGlobal.listaVuelos[posicion].precioActual);
         }

         imprimirLinea();

            min = 1;                                                                                              /**RESPUESTA DEL NUMERO DE VUELO**/
            max = numeroVuelos;
            strcpy(variable, listaComprarCP[0]);
            datos[0] = respuestaLimites(listaComprarCP[0], min, max);


            if (GestionVuelosMesGlobal.listaVuelos[vuelos[datos[0]-1]].plazasLibres == 0){                        /**COMPRUEBA SI EXISTE DICHO NUMERO**/
              throw 10;
            }

            min = 1;                                                                                              /**RESPUETA DE NUMERO DE PLAZAS**/
            max = 5;
            strcpy(variable, listaComprarCP[1]);
            datos[1] = respuestaLimites(listaComprarCP[1], min, max);

            if (GestionVuelosMesGlobal.listaVuelos[vuelos[datos[0]-1]].plazasLibres - datos[1] < 0 ){             /**COMPRUEBA QUE LAS PLAZAS PASEN DE CERO**/
              throw 11;
            }

                                                                                                                  /**RESTA LAS PLAZAS AL VUELO**/
            GestionVuelosMesGlobal.listaVuelos[vuelos[datos[0]-1]].plazasLibres =
            GestionVuelosMesGlobal.listaVuelos[vuelos[datos[0]-1]].plazasLibres - datos[1];
                                                                                                                  /**RECALCULA EL PRECIO DEL VUELO**/
            GestionVuelosMesGlobal.recalcularPrecios(GestionVuelosMesGlobal.listaVuelos[vuelos[datos[0]-1]],
            vuelos[datos[0]-1]);

        } catch(int error){
                switch(error){
                  case (0):
                    imprimirLinea();
                    opcionError(textoError[4]);
                    break;
                  case (1):
                    imprimirLinea();
                    sprintf(texto1, "%d", max);
                    sprintf(texto2, "%d", min);
                    escribirErrorLimites(variable, texto1, texto2);
                    break;
                  case (10):
                    imprimirLinea();
                    opcionError(textoError[7]);
                    break;
                  case (11):
                    imprimirLinea();
                    opcionError(textoError[8]);
                    break;

                }
          }

        }

        void responderCP(){                                                                                       /**CONTROLA LAS RESPUESTAS**/

          tipoVector datos;
          tipoVector vectorAuxiliar;
          tipoTexto variable;
          tipoTexto auxiliarString;
          tipoTexto texto1;
          tipoTexto texto2;
          int auxiliarInt;
          int min;
          int max;




          try{

            for(int i = 0; i < 3; i++) {                                                                          /**CONTROLA LAS RESPUESTAS DEL USUARIO*/
              min = limitesCP[i*2];
              max = limitesCP[i*2 + 1];
              strcpy(variable, listaOpcionesCP[i]);
              datos[i] = respuestaLimites(listaOpcionesCP[i], min, max);
            }

            GestionVuelosMesGlobal.buscarVuelo(datos, auxiliarInt, vectorAuxiliar);                               /**BUSCA EL VUELO INTRODUCIDO**/

            elegirVuelo(vectorAuxiliar, auxiliarInt, min, max);                                                   /**LLAMA A LA FUNCION DE ELEGIR**/

          } catch(int error){
                switch(error){
                  case (0):
                    imprimirLinea();
                    opcionError(textoError[4]);
                    break;
                  case (1):
                    imprimirLinea();
                    sprintf(texto1, "%d", max);
                    sprintf(texto2, "%d", min);
                    escribirErrorLimites(variable, texto1, texto2);
                    break;
                  case (2):
                    imprimirLinea();
                    opcionError(textoError[5]);
                    break;
                  case (3):
                    imprimirLinea();
                    opcionError(textoError[6]);
                    break;
                }
            }

        }

        void comprarPlazas(){                                                                                     /**LLAMA A LAS FUNCIONES**/

          imprimirLinea();


          tituloMenu(textoComprarPlazas);

          responderCP();


          imprimirLinea();

        }

