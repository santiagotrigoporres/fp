
/**************************
         LIBRERIAS
 **************************/

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <ctype.h>

    #include "AltaNuevoVuelo.h"



 /**************************
 FUNCIONES Y PROCEDIMIENTOS
 **************************/


   /*=========================
            PROGRAMA
   ==========================*/


      /*ALTA NUEVO VUELO*/

        void responderANV(){                                                                                  /**CONTROLA EL ALTA DE UN NUEVO VUELO**/

          tipoVector datos;
          FormatoVuelo vuelo;
          tipoTexto variable;
          tipoTexto auxiliarString;
          tipoTexto texto1;
          tipoTexto texto2;
          int auxiliarInt;
          int min;
          int max;


          try{
                  strcpy(variable, listaOpcionesANV[0]);                                                      /**CONTROLA LAS RESPUESTAS**/
                  formatoPreguntas(variable);
                  pedirRespuesta(false, vuelo.codigo);

                  if(strlen(vuelo.codigo) > 10){
                    throw 50;
                  }

                  for(int i = 0; i < 6; i++) {
                    min = limitesGlobales[i*2];
                    max = limitesGlobales[i*2 + 1];
                    strcpy(variable, listaOpcionesANV[i+1]);
                    datos[i] = respuestaLimites(listaOpcionesANV[i+1], min, max);
                  }

                  if(bisiesto(datos[2])){                                                                     /**COMPRUEBA SI EL A�O ES BISIESTO**/
                    diasMes[1] = 29;
                  }

                  if(datos[0] > diasMes[datos[1]-1]){                                                         /**COMPRUEBA QUE LA RESPUESTA DIA ESTE ENTRE SUS DIAS DEL MES**/
                    min = limitesGlobales[2];
                    strcpy(variable, listaOpcionesANV[1]);
                    max = diasMes[datos[1]-1];
                    throw 20;
                  }

                  auxiliarInt = 0;                                                                            /**CUENTA VUELOS PARA DAR UN ERROR SI SE PASAN DE 50**/
                  for (int v = 0; v < GestionVuelosMesGlobal.longitudVectorVuelos; v++){
                    if (GestionVuelosMesGlobal.listaVuelos[v].mes == datos[1] && GestionVuelosMesGlobal.listaVuelos[v].anno == datos[2]){
                       auxiliarInt++;
                    }
                  }

                  if (auxiliarInt == 50){
                    throw 100;
                  }
                  auxiliarInt = 0;


                  for (int v = 0; v < GestionVuelosMesGlobal.longitudVectorVuelos; v++){                      /**COMPRUEBA SI EL VUELO YA EXISTE**/
                    if (GestionVuelosMesGlobal.listaVuelos[v].dia == datos[0] && GestionVuelosMesGlobal.listaVuelos[v].mes == datos[1]
                        && GestionVuelosMesGlobal.listaVuelos[v].anno == datos[2]
                        && GestionVuelosMesGlobal.listaVuelos[v].hora == datos[3]){
                          throw 120;
                    }
                  }


                  vuelo.dia = datos[0];                                                                       /**RELLENA EL VUELO CON LOS DATOS INTRODUCIDOS**/
                  vuelo.mes = datos[1];
                  vuelo.anno = datos[2];
                  vuelo.hora = datos[3];
                  vuelo.plazasIniciales = datos[4];
                  vuelo.precioInicial = datos[5];
                  vuelo.plazasLibres = datos[4];
                  vuelo.precioActual = datos[5];


                  GestionVuelosMesGlobal.nuevoVuelo(vuelo);                                                   /**INDICA SI EL VUELO SE HA CREADO**/
                  printf("\n");
                  imprimirLinea();
                  centrarTexto(listaOpcionesANV[7], ' ');
                  printf("\n");

            } catch(int error){
                switch(error){
                  case (0):
                    imprimirLinea();
                    opcionError(textoError[4]);
                    break;
                  case (1):
                    imprimirLinea();
                    sprintf(texto1, "%d", max);
                    sprintf(texto2, "%d", min);
                    escribirErrorLimites(variable, texto1, texto2);
                    break;
                  case (20):
                    imprimirLinea();
                    sprintf(texto1, "%d", max);
                    sprintf(texto2, "%d", min);
                    escribirErrorLimites(variable, texto1, texto2);
                    break;
                  case (50):
                    imprimirLinea();
                    opcionError(textoError[9]);
                    break;
                  case (100):
                    imprimirLinea();
                    opcionError(textoError[10]);
                    break;
                  case (120):
                    imprimirLinea();
                    opcionError(textoError[11]);
                    break;
                }
            }

        }

        void altaNuevoVuelo(){                                                                                /**LLAMA A LAS FUNCIONES**/

          imprimirLinea();


          tituloMenu(textoAltaNuevoVuelo);

          responderANV();

          diasMes[1] = 28;


          imprimirLinea();

        }


