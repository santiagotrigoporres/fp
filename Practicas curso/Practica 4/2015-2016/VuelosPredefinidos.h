
/**************************
         LIBRERIAS
 **************************/

    #pragma once

    #include "Global.h"


/**************************
      TIPOS Y CONSTANTES
 **************************/


      extern tipoTextoMatriz VP_codigos;
      extern tipoVector VP_dias;
      extern tipoVector VP_meses;
      extern tipoVector VP_annos;
      extern tipoVector VP_horas;
      extern tipoVector VP_plazasIniciales;
      extern tipoVector VP_precioInicial;
      extern tipoVector VP_plazasLibres;
      extern tipoVector VP_precioActual;

 /**************************
 FUNCIONES Y PROCEDIMIENTOS
 **************************/

   /*=========================
             VUELOS
   ==========================*/

      void rellenarVuelos();
