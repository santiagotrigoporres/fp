
/**************************
         LIBRERIAS
 **************************/

    #pragma once

    #include "Global.h"
    #include "AltaNuevoVuelo.h"
    #include "OfertasMes.h"
    #include "ComprarPlazas.h"
    #include "VuelosMes.h"



 /**************************
 FUNCIONES Y PROCEDIMIENTOS
 **************************/


   /*=========================
            PROGRAMA
   ==========================*/


    /*MENU PRINCIPAL*/

      void formatoMP(tipoTexto texto, char respuesta);

      void opcionesMP();

      void comprobarRespuestaMP(char respuesta, tipoTexto longitudRespuesta);

      void menuPrincipal();
