
/**************************
         LIBRERIAS
 **************************/

    #include <stdio.h>
    #include <stdlib.h>
    #include <string.h>
    #include <ctype.h>

    #include "Global.h"



/**************************
      TIPOS Y CONSTANTES
 **************************/

    bool salir;                                                                                                       /**CONTROLA SI FINALIZA EL PROGRAMA**/


    tipoTexto textoGestionVuelos = "Gestion de Vuelos";                                                               /**SON EL TITULO DE CADA MENU**/
    tipoTexto textoAltaNuevoVuelo = "Nuevo Vuelo";
    tipoTexto textoComprarPlazas = "Comprar Plazas";
    tipoTexto textoVuelosMes = "Vuelos del Mes";
    tipoTexto textoOfertasMes = "Ofertas del Mes";
    tipoTexto textoTeclearOpcionValida = "Teclear una opcion valida";

                                                                                                                      /**CONTROLA LOS LIMITES DE DIAS, MESES, A�OS HORAS, PLAZAS Y PRECIO**/
    tipoVector limitesGlobales =  { 1 ,   31,
                                    1 ,   12,
                                    2015, 2016,
                                    0,    24,
                                    1,    200,
                                    100 , 1000};

    tipoVector diasMes = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};                                            /**INDICA LOS DIAS QUE TIENE CADA MES**/

                                                                                                                      /**LOS LIMITES DE LAS RESPUESTAS DE COMPRAR PLAZAS**/
    tipoVector limitesCP = { limitesGlobales[0] ,   limitesGlobales[1],
                             limitesGlobales[2],    limitesGlobales[3],
                             limitesGlobales[4],    limitesGlobales[5]};
                                                                                                                      /**LOS LIMITES DE LAS RESPUESTAS DE VUELOS DEL MES**/
    tipoVector limitesVM = { limitesGlobales[2],    limitesGlobales[3],
                             limitesGlobales[4],    limitesGlobales[5]};

                                                                                                                      /**AVION DE LA CABECERA**/
    tipoTextoMatriz avion       = {               "     _      .---"  ,
                                    "******** _________/ |____'@/   "    ,
                                       " AIR (__ UNED /__|_____%~ ",
                                    ""}  ;
                                                                                                                      /**ALMACENA LOS DISTINTOS TEXTOS DE ERROR**/
    tipoTextoMatriz textoError       = {"ERROR: ", " no existe", " debe estar entre ", " y ",
                                        "La respuesta no es un numero", "no hay vuelos para ese dia",
                                        "no existen vuelos", "no hay plazas libres en el vuelo",
                                        "no se puede superar las plazas existentes",
                                        "el Codigo no puede superar los 10 digitos",
                                        "alcanzado el maximo de 50 vuelos en el mes",
                                        "Ya hay un vuelo en esa fecha y hora" , "\0"};
                                                                                                                      /**LAS DISTINTAS OPCIONES DEL MENU PRINCIPAL**/
    tipoTextoMatriz listaOpcionesMP  = {"Alta Nuevo Vuelo", "Ofertas del Mes", "Comprar Plazas",
                                        "Vuelos del Mes", "Salir", "\0"};
                                                                                                                      /**LAS PREGUNTAS EN ALTA NUEVO VUELO**/
    tipoTextoMatriz listaOpcionesANV = {"Codigo de vuelo", "Dia", "Mes", "Anno", "Hora", "Plazas del Vuelo",
                                        "Precio Inicial", "Se ha creado el vuelo",  "\0"};
                                                                                                                      /**LAS OPCIONES EN COMPRAR PLAZAS**/
    tipoTextoMatriz listaOpcionesCP  = {"Dia", "Mes", "Anno", "\0"};
    tipoTextoMatriz formatoVuelosCP  = {"Vuelo", "Codigo", "Hora", "Plazas", "Precio","\0"};
    tipoTextoMatriz listaComprarCP   = {"Elegir Vuelo", "Numero de Plazas", "\0"};
                                                                                                                      /**LAS OPCIONES DE LOS VUELOS DEL MES**/
    tipoTextoMatriz listaOpcionesVM  = {"Mes", "Anno", "\0"};
                                                                                                                      /**LAS OPCIONES DE LAS OFERTAS DEL MES**/
    tipoTextoMatriz listaOpcionesOM  = {"Mes", "\0"};
    tipoTextoMatriz formatoVuelosOM  = {"Vuelo", "Precio", "Codigo", "Dia", "Hora", "\0"};


    tipoTextoLista listaOpcionesRespuestaMP = { 'a', 'A', 'o', 'O', 'c', 'C', 'v', 'V', 's', 'S' };                   /**LAS POSIBLES RESPUESTAS DEL MENU PRINCIPAL**/


    GestionVuelosMes GestionVuelosMesGlobal;                                                                          /**EL TAD EN EL QUE SE GUARDAN LOS VUELOS**/






/**************************
 FUNCIONES Y PROCEDIMIENTOS
 **************************/

  /*=========================
            GLOBALES
   ==========================*/

    /*PROCEDIMIENTOS GESTION VUELOS MES*/                                                                             /**INICIALIZA LA VARIABLE LONGITUD A CERO**/
      void GestionVuelosMes::auxiliar() {
        longitudVectorVuelos = 0;
      }
                                                                                                                      /**A�ADE EL VUELO QUE SE LE META AL VECTOR CON LOS VUELOS**/
      void GestionVuelosMes::nuevoVuelo(FormatoVuelo vuelo) {
        listaVuelos[longitudVectorVuelos] = vuelo;
        longitudVectorVuelos++;
      }
                                                                                                                      /**RECALCULA EL PRECIO ACTUAL DEL VUELO**/
      void GestionVuelosMes::recalcularPrecios(FormatoVuelo vuelo, int posicion){
        if (vuelo.plazasLibres >= vuelo.plazasIniciales*0.5){
          vuelo.precioActual = vuelo.precioInicial;
        } else if (vuelo.plazasLibres >= vuelo.plazasIniciales*0.3){
          vuelo.precioActual = vuelo.precioInicial*2;
        } else if (vuelo.plazasLibres >= vuelo.plazasIniciales*0.15){
          vuelo.precioActual = vuelo.precioInicial*3;
        } else if (vuelo.plazasLibres >= vuelo.plazasIniciales*0.05){
          vuelo.precioActual = vuelo.precioInicial*5;
        }

        GestionVuelosMesGlobal.listaVuelos[posicion].precioActual = vuelo.precioActual;

      }
                                                                                                                      /**BUSCA UN VUELO EN FUNCION DEL DIA, MES Y A�O**/
      void GestionVuelosMes::buscarVuelo(tipoVector datos, int &numeroVuelos, tipoVector vectorAuxiliar){

          int posicion = 0;
          int longitud = GestionVuelosMesGlobal.longitudVectorVuelos;
          bool end;

          numeroVuelos = 0;

          while (posicion < longitud){
            if (GestionVuelosMesGlobal.listaVuelos[posicion].dia == datos[0] && GestionVuelosMesGlobal.listaVuelos[posicion].mes == datos[1]
                && GestionVuelosMesGlobal.listaVuelos[posicion].anno == datos[2]){
                vectorAuxiliar[numeroVuelos] = posicion;
                numeroVuelos++;
                end = true;
            }
            posicion++;
         }


         if (longitud == 0){
            throw 3;
         }else if (end != true && longitud != 0 && posicion == longitud && GestionVuelosMesGlobal.listaVuelos[posicion].dia != datos[0]){
            throw 2;
         }

        }


    /*ESCRITURA CONTINUADA DE CARACTERES*/                                                                            /**ESCRIBE UNA SUCESION DE CARACTERES**/
      void escrituraCaracter(char caracter, int repeticiones){

        for (int i = 1; i <= repeticiones; i++){
          printf("%c", caracter);
        }

      }


    /*IMPRIMIR LINEAS MENU*/                                                                                          /**IIMPRIME UNA RECTA CON LONGITUD EL ANCHO DEL PROGRAMA**/
      void imprimirLinea(){
        /**/escrituraCaracter(caracterLinea, anchoMaximo);
        printf("\n");

      }


    /*TITULO MENU*/                                                                                                   /**INICIALIZA LA VARIABLE LONGITUD A CERO**/
      void tituloMenu(tipoTexto texto){

        printf(" + %s:", texto);
        printf("\n\n");

      }


    /*ESCRIBIR TABULACIONES*/                                                                                         /**ESCRIBE LOS ESPACIOS QUE HAY ENTRE PREGUNTA Y RESPUESTA**/
      void tabulaciones(tipoTexto texto, int extras){

        int tabulaciones = 10;
        char espacio = ' ';


        tabulaciones = anchoMaximo - tabulacionesIzquierda - tabulaciones - strlen(texto) - extras;
        if(tabulaciones < 1) {
          printf("%c", espacio);
        } else {
          escrituraCaracter(espacio, tabulaciones);
        }

      }


    /*CENTRAR TEXTO*/                                                                                                 /**CENTRA EL TEXTO QUE SE LE INTRODUZCA**/
      void centrarTexto(tipoTexto texto, char caracter){

        int longitud = strlen(texto);
        int tabulaciones = anchoMaximo - longitud;


        if(tabulaciones > 0) {
          escrituraCaracter(caracter, tabulaciones/2);
          printf(texto);
          escrituraCaracter(caracter, tabulaciones/2 +1);
        } else {
          printf(texto);
        }

      }


    /*OPCION ERRONEA*/                                                                                                /**IMPRIME UN TEXTO EN FORMA DE ERROR**/
      void opcionError(tipoTexto texto){

        tipoTexto textoTotal;


        escrituraCaracter(caracterError, anchoMaximo);
        printf("\n");

        strcpy(textoTotal, textoError[0]);
        strcat(textoTotal, texto);

        centrarTexto(textoTotal, ' ');

        printf("\n");
        escrituraCaracter(caracterError, anchoMaximo);
        printf("\n");

      }

      void escribirErrorLimites(tipoTexto variable, tipoTexto limiteMax, tipoTexto limiteMin){                        /**JUNTA UN TEXTO Y SUS LIMITES EN UN MISMO STRING**/

        tipoTexto texto;


        strcpy(texto, variable);
        strcat(texto, textoError[2]);


        strcat(texto, limiteMin);
        strcat(texto, textoError[3]);
        strcat(texto, limiteMax);


        opcionError(texto);

      }


    /*PEDIR RESPUESTA*/                                                                                               /**ES LA FUNCION QUE CONTROLA LA RESPUESTA DEL USUARIO**/
      void pedirRespuesta(bool menu, tipoTexto respuesta){

        if (menu){
          printf(" ====");
        }

        printf("=> ");
        scanf("%s", respuesta);

      }


    /*FORMATO PREGUNTAS*/                                                                                             /**DA UN FORMATO A LA PREGUNTA QUE SE LE INTRODUZCA**/
      void formatoPreguntas(tipoTexto texto){

        printf("\t");
        printf("~ %s?", texto);

        tabulaciones(texto, 6);

      }


    /*TRANSFORMAR STRING EN INT*/                                                                                     /**TRANSFORMA UN NUMERO EN STRING A INT**/
      int transformarString(tipoTexto texto){

        int longitud = strlen(texto);
        int numero;


          for (int posicion = 0 ; posicion < longitud; posicion++){
            if (isdigit(texto[posicion]) == false){
              throw 0;
            }
          }

          return atoi(texto);

      }


    /*COMPROBAR LIMITES*/                                                                                             /**COMPRUEBA SI LA RESPUESTA ESTA ENTRE SUS LIMITES**/
      void comprobarLimites(int respuesta, int min, int max){
        if (respuesta < min || respuesta > max){
          throw 1;
        }
      }


    /*CABECERA*/
      void cabecera(){                                                                                                /**ESCRIBE LA CABECERA DEL PROGRAMA**/

        printf("      ");
        centrarTexto(avion[0], ' ');


        printf("\n");
        centrarTexto(avion[1], '*');

        printf("\n");
        centrarTexto(avion[2], '=');

        printf("\n");
        escrituraCaracter('*', anchoMaximo);
        printf("\n");

      }


    /*RESPUESTA CON LIMITES*/                                                                                         /**CONTROLA LA RESPUESTAS CON LIMITES DEL USUARIO**/
      int respuestaLimites(tipoTexto opcion, int min, int max){

        tipoTexto variable;
        tipoTexto auxiliarString;
        tipoTexto texto1;
        tipoTexto texto2;
        int auxiliarInt;


          strcpy(variable, opcion);
                formatoPreguntas(variable);
                pedirRespuesta(false, auxiliarString);
                auxiliarInt = transformarString(auxiliarString);
                comprobarLimites(auxiliarInt, min, max);
                return auxiliarInt;

      }


    /*FORMATO PARA MOSTRAR VUELOS*/                                                                                   /**IMPRIME ESPACIOS EN FUNCION DE LA LONGITUD DE UN TEXTO**/
      void formatoVuelosMes(int numero){

          int longitud;

          if (numero < 10){
            longitud = 1;
          }else if(numero < 100){
            longitud = 2;
          }else if(numero < 1000){
            longitud = 3;
          }else if(numero < 10000){
            longitud = 4;
          }

          longitud = 9-longitud;
          for (int i = 1; i <= longitud; i++){
              printf(" ");
          }

        }


    /*COMPROBAR ANNO BISIESTO*/                                                                                       /**COMPRUEBA SI UN A�O ES BISIESTO**/
      bool bisiesto(int year) {
        return (year % 4 == 0 && (year % 400 == 0 || year % 100 != 0));
      }
