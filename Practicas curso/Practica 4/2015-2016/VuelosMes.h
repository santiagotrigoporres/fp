
/**************************
         LIBRERIAS
 **************************/

    #pragma once

    #include "Global.h"



 /**************************
      TIPOS Y CONSTANTES
 **************************/

  typedef enum VM_Mes {ENERO, FEBRERO, MARZO, ABRIL, MAYO, JUNIO, JULIO,
                        AGOSTO, SEPTIEMBRE, OCTUBRE, NOVIEMBRE, DICIEMBRE};

  typedef int VM_Vector[12];
  extern VM_Vector VM_LETRAS_MES;
  extern int VM_ESPACIOS;

  typedef struct CalendarioMes{                                                                     /**EL TAD DE LOS MESES**/

    int VM_MIN_YEAR;



    void auxiliarVM();

    void leerMes(VM_Mes mes);

    int calculoDiaSemana(int year, int mes);

    void escribirBarra(int diaSemana, int diaActual);

    void escribirEspacios(int diaSemana);

    void escribirCabecera(VM_Mes mes, int espacios, int year);

    void escribirDias(int mes, int diaSemana, int year);

    void calendario(VM_Mes mes, int espacios, int year);




};


 /**************************
 FUNCIONES Y PROCEDIMIENTOS
 **************************/


   /*=========================
            PROGRAMA
   ==========================*/

      /*VUELOS DEL MES*/

          void responderVM();

          void vuelosMes();







