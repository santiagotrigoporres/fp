#include "TipoListaDeFechasElegidas.h"
#include "TipoFechaElegida.h"
#include "TipoListaDePreferencias.h"
#include <stdio.h>
#include <stdlib.h>

void ObtenerFechasElegidasAPartirDeLasPreferencias(TipoListaDePreferencias preferencias, int contpre, TipoListaDeFechasElegidas & fechasElegidas, int & contfech, int & contprele){
  /**Modifica una variable TipoListaDePreferencias en una variable TipoListaDeFechasElegidas*/
  TipoFechaElegida fecha;
  bool repe; /**Si la fecha esta ya almacenada tiene TRUE sino FALSE*/

  for(int i=contprele ;i<contpre ;i++){/**Para recorrer todas las preferencias almacenadas hasta el momento*/
    repe = false;

    /**Verifica si la fecha ya existe, y en caso de que lo haga, le a�ade uno al contador de veces seleccionada*/
    for (int j=0 ;j<contfech ;j++){
      if (preferencias[i].anno == fechasElegidas[j].anno && preferencias[i].mes == fechasElegidas[j].mes && preferencias[i].dia == fechasElegidas[j].dia){
        fechasElegidas[j].veces ++;
        j=contfech;
        repe = true;
      }
    }

    /**A�adir una nueva fecha a la lista, cuando aun no existe*/
    if (repe == false){
      fechasElegidas[contfech].anno = preferencias[i].anno;
      fechasElegidas[contfech].mes = preferencias[i].mes;
      fechasElegidas[contfech].dia = preferencias[i].dia;
      fechasElegidas[contfech].veces = 1;
      contfech++;
    }
  }
  contprele = contpre; /**Para que la siguiente vez que entre en este codigo, comience a mirar solamente los que no se han mirado antes*/
}

void OrdenarFechasElegidas(TipoListaDeFechasElegidas & fechasElegidas, int contfech){
  /**Ordena las variables TipoListaDeFechasElegidas, segun el n�mero de veces que han sido seleccionadas
  de mayor a menor
  Usar metodo de insercion directa explicado en el capitulo 12 del libro*/
  TipoFechaElegida fecha;
  int j;

  for (int i=0; i<contfech; i++){
   fecha = fechasElegidas[i];
   j=i;
   while (j>0 && fecha.veces>fechasElegidas[j-1].veces){
     fechasElegidas[j]=fechasElegidas[j-1];
     j--;
   }
   fechasElegidas[j]=fecha;
  }
}

void ImprimirFechasElegidas(TipoListaDeFechasElegidas fechasElegidas, int contfech){
  /**Imprimir preferencias*/

  fflush(stdin);/*Para limpiar el buffer del teclado, porque sino no guarda bien el siguiente dato*/
  system("cls");/*Para limpiar la pantalla*/

  printf("\nLISTADO DE FECHAS ELEGIDAS:\n\n");

  for(int i=0; i<contfech; i++){
    ImprimirFecha(fechasElegidas[i]);
  }


}
