/***********************************************************************
* Interfaz del modulo: calendario
*
* Este modulo define la abstraccion funcional ImprimirCalendario,
* capaz de imprimir en pamptalla calendario, tal y
* como se especidica en el capitulo 8 del libro:
* "Practica de programacion de C+-", de
* Jose A. Cerrada, Manuel Collado, Ismael Abad y Ruben Haradio.
* Editorial Centro de Estucios Ramon Areces
**********************************************************************/

#pragma once

const int Aini = 2011; /* anno minimo para proponer una reunion */
const int Afin = 3000; /* anno maximo para proponer una reunion */


/**ImprimirCalendario:
* - imprime el calendario correspondiente al mes y anno
* - devuelve el numero total de dias del mes que puede ser:
*   * 28,29 30 o 31 si es el mes y el anno son correctos
*   * 0 si el mes o el anno son incorrectos
*/

int ImprimirCalendario(int Mes, int Anno);

