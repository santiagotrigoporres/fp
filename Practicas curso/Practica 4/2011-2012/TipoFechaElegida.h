#pragma once

typedef struct TipoFechaElegida {
 int dia;
 int mes;
 int anno;
 int veces;
};

void AnnadirFecha ();
/**Verifica si la fecha no esta almacenada, y si no esta la guarda, y si esta suma uno al numero de veces*/

void ImprimirFecha (TipoFechaElegida fecha);
/**Imprime una fecha con el formato
DIA-MES-A�O ha sido elegida VECES veces*/
