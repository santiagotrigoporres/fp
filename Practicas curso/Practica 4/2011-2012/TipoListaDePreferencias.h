#pragma once
#include "TipoPreferencia.h"

/**Almacena las preferencias de los usuarios*/

typedef TipoPreferencia TipoListaDePreferencias[50];

void InicializarPreferencias();

void AnnadirPreferencia(TipoListaDePreferencias & preferencias, int & contpre,int & contfech);

void ImprimirPreferencias(TipoListaDePreferencias preferencias, int contpre);
