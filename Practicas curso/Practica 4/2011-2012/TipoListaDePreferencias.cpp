#include "TipoListaDePreferencias.h"
#include "TipoPreferencia.h"
#include <stdio.h>
#include <stdlib.h>

void InicializarPreferencias(){

  /**TipoListaDePreferencias preferencias;

  for (int i=0; i<50; i++){
    preferencias[i].mes=0;
  }*/

}

void AnnadirPreferencia(TipoListaDePreferencias & preferencias, int & contpre, int & contfech){
  bool bien; /**Si es correcto TRUE si no es correcto FALSE*/

  TipoPreferencia preferencia;

  /** bien si contiene true, es porque en el proceso de alta todos los datos son correctos y se pueden almacenar */
  bien = PreguntarPreferenciaAlUsuario(preferencia);/**TipoPreferencia.h*/

  if (bien == true){
    preferencias[contpre]=preferencia;
    contpre++;
  }
}

void ImprimirPreferencias(TipoListaDePreferencias preferencias, int contpre){

  fflush(stdin);/*Para limpiar el buffer del teclado, porque sino no guarda bien el siguiente dato*/
  system("cls");/*Para limpiar la pantalla*/

  printf("\nLISTADO DE PREFERENCIAS:\n\n");
  for(int i=0;i<contpre;i++){
    ImprimirPreferencia(preferencias[i]);
  }
}
