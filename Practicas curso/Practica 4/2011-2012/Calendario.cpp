/**
* Programa, al que le pasas un mes y un a�o, y te muestra el calendario para
* ese mes de ese a�o.
**/

#include "Calendario.h"
#include <stdio.h>
#include <stdlib.h>

/** Imprimir calendario **/
int ImprimirCalendario(int Mes, int Anno){
  int a,y,m,dia,numdias;
  int contador;/**ire modificando para saber en que dia de la semana me encuentro*/

  if(Mes>0 && Mes<=12 && Anno>=Aini&&Anno<=Afin){

  /**Calcula el d�a de la semana que corresponde al d�a uno del mes*/
  a = (14-Mes)/12;
  y = Anno - a;
  m = Mes + 12 * a -2;
  dia = (1 + y + y/4 - y/100 + y/400 + (31*m)/12) % 7;  /**Almacena el numero 0 si es domingo 6 si es sabado*/

  /**Calcula el numero de dias del mes, para ese a�o*/
    numdias=28;
    if(Mes==1 || Mes==3 || Mes==5 || Mes==7 || Mes==8 || Mes==10 || Mes==12){
      numdias = numdias + 3;
    }else if(Mes==2){
      if((Anno%4==0)&&(Anno%100!=0 || Anno%400==0)){/**Este if lo hago, para controlar que si es bisiesto me ponga en febrero un dia mas*/
        numdias = numdias+1;
      }/**else{
        numdias;
      }*/
    } else {
      numdias = numdias+2;
    }

    /**Escribir el mes en letra*/
      switch (Mes){
       case 1: printf("\nENERO     ");
      break;
       case 2: printf("\nFEBRERO   ");
      break;
       case 3: printf("\nMARZO     ");
      break;
       case 4: printf("\nABRIL     ");
      break;
       case 5: printf("\nMAYO      ");
      break;
       case 6: printf("\nJUNIO     ");
      break;
       case 7: printf("\nJULIO     ");
      break;
       case 8: printf("\nAGOSTO    ");
      break;
       case 9: printf("\nSEPTIEMBRE");
      break;
       case 10:printf("\nOCTUBRE   ");
      break;
       case 11:printf("\nNOVIEMBRE ");
      break;
       case 12:printf("\nDICIEMBRE ");
      break;
      }

      /**Imprimir cabecera del calendario*/
      printf("%17d\n",Anno);
      printf("===========================\n");
      printf("LU  MA  MI  JU  VI | SA  DO\n");
      printf("===========================\n");

      /**Imprime los d�as*/
      contador = dia;

        /**Fragmento de codigo que imprime los espacios y puntos del principio*/
        if(dia==0){
          printf(" .   .   .   .   . |  .");
        }else{
          for(int i=1;i<dia ;i++){
            if(i == 1){
              printf(" .");
            }else if(i == 6){
              printf(" |  .");
            }else{
              printf("   .");
            }
          }
        }

        /**Fragmento de codigo que imprime los numeros del calendario*/
       for(int i=1;i<=numdias;i++){
         if(contador==6){
           printf(" |%3d",i);
           contador=0;
         }else if (contador==1){
           printf("%2d",i);
           contador++;
         }else if(contador==0){
           printf("%4d\n",i);
           contador++;
         }else{
           printf("%4d",i);
           contador++;
         }
       }

       /**Fragmento de codigo que imprime los puntos del final del calendario*/
       if(contador!=1){
         if(contador!=0){
           for(int i=contador;i<8;i++){
             if(i == 6){
                printf(" |  .");
              }else{
                printf("   .");
              }
           }
         }else{
           printf("   .");
         }
       }

       printf("\n");

       return numdias;
  }else{
    return 0;
  }
}
