#include "TipoPreferencia.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Calendario.h"

bool PreguntarPreferenciaAlUsuario(TipoPreferencia & preferencia){
  int dias; /**Almacena el numero de dias del mes*/
  TipoPreferencia teclado;

  fflush(stdin);/*Para limpiar el buffer del teclado, porque sino no guarda bien el siguiente dato*/
  system("cls");/*Para limpiar la pantalla*/

  printf("\nINTRODUCIR LAS PREFERENCIAS DE UN USUARIO\n\n");
  printf("\xa8Nombre? ");
  fflush(stdin);/**Para limpiar el buffer del teclado si se queda algun valor*/

  /**Guardo el nombre introducido por teclado en teclado.nombre, que es una variable temporal
  Como cojo 50 caracteres, en caso de meter por teclado menos, coje tambien el salto de l�nea
  En caso de introducir menos de 50, copio a la cadena definitiva todos los caracteres menos el �ltimo
  Como no he cogido el caracter de fin de cadena, que es barra 0, se lo a�ado*/
  fgets(teclado.nombre,50,stdin);
  if(strlen(teclado.nombre)<50){
    strncpy(preferencia.nombre,teclado.nombre,(strlen(teclado.nombre)-1));
    preferencia.nombre[strlen(teclado.nombre)-1]='\0';
  }

  printf("\xa8Mes (1..12)? ");
  fflush(stdin);/**Para limpiar el buffer del teclado si se queda algun valor*/
  scanf("%d",&preferencia.mes);

  /** Verifica si las opciones introducidas ent�n dentro del rango esperado, y si no lo esta/an
  muestra un error descriptivo del error.
  De tal forma, que solo imprima el calendario, cuando los datos introducidos son los correctos. */

  if(preferencia.mes<1 || preferencia.mes>12){
    printf("\n<<ERROR: El numero de mes no esta dentro del rango>>\n");
  }else{

      printf("\xa8\A\xA4o (2011..3000)? "); /** barra de la izquierda del uno xA4 para la �*/
      fflush(stdin);/**Para limpiar el buffer del teclado si se queda algun valor*/
      scanf("%d",&preferencia.anno);

    if(preferencia.anno<Aini || preferencia.anno>Afin){
      printf("\n<<ERROR: El numero de a\xA4o no esta dentro del rango>>\n");
    }else {
      dias = ImprimirCalendario(preferencia.mes , preferencia.anno);/**Calendario.h*/

      printf("\n\xa8\Dia? ");
      fflush(stdin);/**Para limpiar el buffer del teclado si se queda algun valor*/
      scanf("%d",&preferencia.dia);

      /** TipoListaDePreferencias retorna, true si todo ha ido bien o false, si se ha producido algun error, porque los datos no son correctos */
      if(preferencia.dia<1 || preferencia.dia>dias){
        printf("<<ERROR: El maximo numero de dias para ese mes es %d>>\n",dias);
        return false;
      }else{
        return true;
      }
    }
  }
}


void ImprimirPreferencia(TipoPreferencia preferencia){
  printf("%s prefiere que la reunion sea el %d-%d-%d\n",preferencia.nombre,preferencia.dia,preferencia.mes,preferencia.anno);
}
