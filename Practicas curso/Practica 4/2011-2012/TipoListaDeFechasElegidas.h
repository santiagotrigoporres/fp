#pragma once
#include "TipoListaDePreferencias.h"
#include "TipoFechaElegida.h"



typedef TipoFechaElegida TipoListaDeFechasElegidas[50];

void ObtenerFechasElegidasAPartirDeLasPreferencias(TipoListaDePreferencias preferencias, int contpre, TipoListaDeFechasElegidas & fechasElegidas, int & contfech, int & contprele);
  /**Modifica una variable TipoListaDePreferencias en una variable TipoListaDeFechasElegidas*/

void OrdenarFechasElegidas(TipoListaDeFechasElegidas & fechasElegidas, int contfech);
  /**Ordena las variables TipoListaDeFechasElegidas, segun el n�mero de veces que han sido seleccionadas
  de mayor a menor
  Usar metodo de insercion directa explicado en el capitulo 12 del libro*/

void ImprimirFechasElegidas(TipoListaDeFechasElegidas fechasElegidas, int contfech);
  /**Imprimir preferencias*/





