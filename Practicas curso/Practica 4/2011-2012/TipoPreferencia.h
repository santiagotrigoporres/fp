#pragma once
#include "Calendario.h"

typedef struct TipoPreferencia{
  char nombre[50];
  int dia;
  int mes;
  int anno;
};

bool PreguntarPreferenciaAlUsuario(TipoPreferencia & preferencia);
/**Pregunta al usuario su preferencia
Imprime el calendario para el mes y el a�o especificado
Guarda el resultado en una variable TipoPreferencia
Devuelve true si los datos son correctos false, si no lo son*/

void ImprimirPreferencia(TipoPreferencia preferencia);
/**Imprime la preferencia con el formato
NOMBRE prefiere que la reunion sea el DIA-MES-A�O*/

