#include <stdio.h>
#include <stdlib.h>/*Para que funcione el system*/
#include "TipoListaDeFechasElegidas.h"
#include "TipoListaDePreferencias.h"

TipoPreferencia preferencia;
TipoListaDePreferencias preferencias;
TipoListaDeFechasElegidas fechasElegidas;

int contpre; /**Almacena el numero de preferencias almacenadas*/
int contfech; /**Almacena el numero de fechas diferentes almacenadas*/
int contprele; /**Almacena el numero de preferencias que han sido leidas y pasadas a fechas,
 para que de esa forma no vuelva a leer los mismos regisros de preferencias varias veces */

int main(){
  char opcion;
  contprele = 0;/**Inicializo el valor para que no de problemas en el for de ObtenerValores*/

  InicializarPreferencias();
  opcion =0;

  while (opcion != '4'){
    printf("\n\nPLANIFICADOR DE EVENTOS PARA REUNIONES\n\n");
    printf("1. Introducir las preferencias de un usuario\n");
    printf("2. Listar las preferencias de todos los usuarios\n");
    printf("3. Calcular la fecha optima para la reunion\n");
    printf("4. Salir \n");

    printf("\nIntroduzca una opcion valida (1-4): ");
    fflush(stdin);/**Para limpiar el buffer del teclado si se queda algun valor*/
    scanf("%c",&opcion);

    if ((opcion < '1')||(opcion >'4')){
      printf("\n<<ERROR: opcion incorrecta>>\n");
    }
    if (opcion == '1'){
      if (contpre < 50){
        AnnadirPreferencia(preferencias,contpre,contfech); /**TipoListaDePreferencias.h*/
      }else{
        printf("Se ha alcanzado el numero maximo de preferencias admitidas \n");
      }
    }
    if (opcion == '2'){
      ImprimirPreferencias(preferencias,contpre); /**TipoListaDePreferencias.h*/
    }
    if (opcion == '3'){
      ObtenerFechasElegidasAPartirDeLasPreferencias(preferencias,contpre,fechasElegidas,contfech,contprele); /**TipoListaDeFechasElegidas.h*/
      OrdenarFechasElegidas(fechasElegidas,contfech); /**TipoListaDeFechasElegidas.h*/
      ImprimirFechasElegidas(fechasElegidas,contfech); /**TipoListaDeFechasElegidas.h*/
    }

    if(opcion!='4'){/**Para que limpie la pantalla solamente cuando no sea la opcion 4*/
      printf("\n");
      system("pause");/*Pausa la ejecucion para que se pueda ver la frase*/
      system("cls");/*Para limpiar la pantalla*/
    }
  }
}

