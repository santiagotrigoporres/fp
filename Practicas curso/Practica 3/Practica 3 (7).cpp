#include <stdio.h>

const int numElementos = 12;

typedef char cadena[20];
typedef int diasMeses[numElementos];

int main()
{
   /**************************************
  * NOMBRE: #Isa�as#
  * PRIMER APELLIDO: #Guerrero#
  * SEGUNDO APELLIDO: #Monge#
  * DNI: #47208689#
  * EMAIL: #iguerrero47@alumno.uned.es#
  ***************************************/

  int mes;
  int anyo;
  bool repetir=true;
  bool esBisiesto=false;
  cadena mesLetras = "";
  int diaMes = 1;
  int dia = 1;
  int diaSemana;
  int totalDiasMes = 0;

  diasMeses diasMes = {31,28,31,30,31,30,31,31,30,31,30,31};

  int a;
  int b;
  int mesx, val4, val5, val6, val7, val8, val9;

    printf("�Mes (1..12)?");
    scanf("%i", &mes);

    if(mes<=0 || mes >12)
    {
        printf("N�mero de mes incorrecto\n");
        return 0;
    }

    printf("�A�o (1601..3000)?");
    scanf("%i", &anyo);

    if(anyo<1601 || anyo >3000)
    {
        printf("N�mero de anyo fuera de rango\n");
        return 0;
    }

  printf("\n");
  switch(mes)
  {
    case 1:
      printf("ENERO");
      break;
    case 2:
      printf("FEBRERO");
      if(anyo%4 == 0)
      {
        if(anyo%100==0 && anyo%400!=0)
        {
          esBisiesto=false;
        }
        else
        {
          esBisiesto=true;
        }
      }
      else
      {
        esBisiesto=false;
      }

      if(esBisiesto)
      {
        totalDiasMes = 29;
      }
      else
      {
        totalDiasMes = 28;
      }
      break;
    case 3:
      printf("MARZO");
      break;
    case 4:
      printf("ABRIL");
      break;
    case 5:
      printf("MAYO");
      break;
    case 6:
      printf("JUNIO");
      break;
    case 7:
      printf("JULIO");
      break;
    case 8:
      printf("AGOSTO");
      break;
    case 9:
      printf("SEPTIEMBRE");
      break;
    case 10:
      printf("OCTUBRE");
      break;
    case 11:
      printf("NOVIEMBRE");
      break;
    case 12:
      printf("DICIEMBRE");
      break;
  }
  printf("\t\t\t             %i\n", anyo);

  printf("===========================\n");
  printf("LU  MA  MI  JU  VI | SA  DO\n");
  printf("===========================\n");

  /*Comprobar en que d�a de la semana cae el 1 del mes
  seleccionado para el a�o seleccionado*/

  mesx = mes;
  if(mes==1)
  {
    mesx = 13;
    anyo = anyo-1;
  }
  else if(mes==2)
  {
    mesx = 14;
    anyo = anyo-1;
  }

  val4 = ((mesx+1)*3)/5;
  val5 = anyo/4;
  val6 = anyo/100;
  val7 = anyo/400;
  val8 = dia + (mesx*2)+val4+anyo+val5-val6+val7+2;
  val9 = val8/7;
  diaSemana = (val8-(val9*7));

  if(diaSemana == 0)
  {
    diaSemana = 6;
  }
  else if(diaSemana == 1)
  {
    diaSemana = 7;
  }
  else
  {
    diaSemana = diaSemana - 2;
  }

  for(int i=1;i<=7;i++)
  {
    if(i<10)
    {
      printf(" ");
    }
    if((i-1)<diaSemana)
    {
      if(i==5)
      {
        printf(". ");
      }
      else if(i!=7)
      {
        printf(".  ");
      }
      else
      {
        printf(".");
      }
    }
    else
    {
      if(i==5)
      {
        printf("%i ", diaMes);
      }
      else if(i!=7)
      {
        printf("%i  ", diaMes);
      }
      else
      {
        printf("%i", diaMes);
      }
      diaMes++;
    }
    if(i==5)
    {
      printf("| ");
    }
    if(i==7)
    {
      printf("\n");
    }
  }

  if(mes!=2)
  {
    totalDiasMes = diasMes[mes-1];
  }

  while(diaMes<=totalDiasMes)
  {
    if(diaMes<10)
    {
      printf(" ");
    }
    if(dia==5)
    {
      printf("%i ", diaMes);
    }
    else if(dia!=7)
    {
      printf("%i  ", diaMes);
    }
    else
    {
      printf("%i", diaMes);
    }
    diaMes++;
    if(dia == 5)
    {
      printf("| ");
    }
    if(dia==7)
    {
      printf("\n");
      dia = 0;
    }
    dia++;
  }

  while(dia <=7)
  {
    if(dia==7)
    {
      printf("   .");
    }
    else if(dia == 5 || dia == 6)
    {
      printf(" .");
    }
    else
    {
      printf(" .  ");
    }
    if(dia == 5)
    {
      printf(" | ");
    }
    dia++;
  }
  printf("\n");

  return 0;
}
