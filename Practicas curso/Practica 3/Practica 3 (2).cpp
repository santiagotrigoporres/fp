#include <stdio.h>

typedef enum TipoDia {Lunes, Martes, Miercoles, Jueves, Viernes, Sabado, Domingo};
typedef enum TipoMes {Enero, Febrero, Marzo, Abril, Mayo, Junio, Julio, Agosto, Septiembre, Octubre, Noviembre, Diciembre};

/***************************************
========================================
* Funciones y procedimientos************
========================================
***************************************/

/* Procedimiento al que se le pasa el numero del mes y lo devuelve en letra */

void EscribirMes(int m) {
  printf("\n");
  switch (m) {
  case 1:
    printf("ENERO        ");
    break;
  case 2:
    printf("FEBRERO      ");
    break;
  case 3:
    printf("MARZO         ");
    break;
  case 4:
    printf("ABRIL         ");
    break;
  case 5:
    printf("MAYO          ");
    break;
  case 6:
    printf("JUNIO         ");
    break;
  case 7:
    printf("JULIO         ");
    break;
  case 8:
    printf("AGOSTO        ");
    break;
  case 9:
    printf("SEPTIEMBRE    ");
    break;
  case 10:
    printf("OCTUBRE       ");
    break;
  case 11:
    printf("NOVIEMBRE     ");
    break;
  case 12:
    printf("DICIEMBRE     ");
    break;
  default:
    ;
  }
}

/* Funcion que devuelve TRUE si el a�o "a" es bisiesto */
bool EsAnhoBisiesto(int a) {
  if (((a%4==0)&&(a%100!=0))||((a%400)==0)) {
    return true;
  } else {
    return false;
  }
}

/* Funci�n que determina el n�mero de dias del mes m del a�o a */

int determinarNumeroDiasDelMes( int m, int a) {
  switch (m) {

  case 1:      /* Enero     */
  case 3:      /* Marzo     */
  case 5:      /* Mayo      */
  case 7:      /* Julio     */
  case 8:      /* Agosto    */
  case 10:     /* Octubre   */
  case 12:     /* Diciembre */
    return 31;
    break;
  case 4:     /* Abril      */
  case 6:     /* Junio      */
  case 9:     /* Septiembre */
  case 11:    /* Noviembre  */
    return 30;
    break;
  case 2:
    if (EsAnhoBisiesto(a)) {
      return 29;
    } else {
      return 30;
    }
    break;
  }
}

/* Funci�n que determina el dia de la semana en que cae el dia de a�o nuevo en el a�o a */

int determinarDiaAnhoNuevo(int a) {
  int diaAnhoNuevo;

  for ( int i=1601; i<=(a-1);i++) {
    if (EsAnhoBisiesto(a)) {
      diaAnhoNuevo=diaAnhoNuevo+2;
    } else {
      diaAnhoNuevo=diaAnhoNuevo+1;
    }
  }
  return diaAnhoNuevo%7;
}

/* Funci�n que determina el dia de la semana en que cae el primer dia del mes m del a�o a*/

int determinarPrimeroDelMes(int m, int a) {
  int diaPrimeroMes;
  diaPrimeroMes= determinarDiaAnhoNuevo(a);

  for (int i=1;i<=m-1;i++) {
    if ((i==1)||(i==3)||(i==5)||(i==7)||(i==8)||(i==10)) {
      diaPrimeroMes=diaPrimeroMes+3;
    } else if ((i==4)||(i==6)||(i==9)||(i==11)) {
      diaPrimeroMes=diaPrimeroMes+2;
    } else if (i==2) {
      if (EsAnhoBisiesto(a)) {
        diaPrimeroMes=diaPrimeroMes+1;
      } else {
        diaPrimeroMes=diaPrimeroMes+0;
      }
    }
  }
  return diaPrimeroMes%7;
}

/* Funci�n que determina el dia de la semana en que cae el �ltimo dia del mes m del a�o a*/

int determinarUltimoDelMes(int m, int a) {
  int diaUltimoDelMes;
  diaUltimoDelMes= determinarPrimeroDelMes(m,a) + (determinarNumeroDiasDelMes(m,a)-1);

  return diaUltimoDelMes%7;
}

/*Procedimiento que imprime por pantalla la cabecera del calendario */

void Cabecera() {
  printf ( "===========================\n" );
  printf ( "LU  MA  MI  JU  VI | SA  DO\n" );
  printf ( "===========================\n" );
}


/***************************************
========================================
*Programa Principal ********************
========================================
***************************************/

int main() {
  int mes, anno;
  int diaPrMes, diaUltMes;
  int numDiasDelMes;
  int contador;

  printf("\xa8Mes (1..12)? ");
  scanf("%d", &mes);
  printf("\xa8 A�o (1601..3000)? ");
  scanf("%d", &anno);

  diaPrMes= determinarPrimeroDelMes(mes,anno);
  diaUltMes= determinarUltimoDelMes(mes,anno);
  numDiasDelMes= determinarNumeroDiasDelMes(mes,anno);

  if ((mes >= 1 && mes <= 12) && (anno >= 1601 && anno <= 3000)) {

    contador=1;

    /*Imprimir cabecera*/

    if ((mes==2)||(mes==10)) {
      EscribirMes(mes);
      printf("%20d\n",anno);
      Cabecera();

    } else if ((mes==1)||(mes==3)||(mes==4)||(mes==6)||(mes==7)) {
      EscribirMes(mes);
      printf("%20d\n",anno);
      Cabecera();

    } else if (mes==5) {
      EscribirMes(mes);
      printf("%23d\n",anno);
      Cabecera();

    } else if (mes==8) {
      EscribirMes(mes);
      printf("%21d\n",anno);
      Cabecera();

    } else if (mes==9) {
      EscribirMes(mes);
      printf("%17d\n",anno);
      Cabecera();

    } else if ((mes==11)||(mes==12)) {
      EscribirMes(mes);
      printf("%18d\n",anno);
      Cabecera();
    }

    /*Imprimir los primeros puntos */

    for (int i=1; i< diaPrMes;i++) {
      printf(" .");
      printf(" ");
      if (contador%7==5) {
        printf("|");
      }
      printf(" ");
      contador++;
    }

    /*Imprimir los dias del mes */

    for (int i=1;i<=numDiasDelMes;i++) {

      printf( "%2d", i);
      if ( contador % 7 == 0 ) {
        printf( "\n" );
      } else if ( contador % 7 == 5 ) {
        printf( " | " );
      } else {
        printf( "  " );
      }
      contador++;
    }
    /*Imprimir los ultimos puntos */

    while ( contador % 7 != 1 ) {
      printf( " ." );
      printf(" ");
      if ( contador % 7 == 5 ) {
        printf( "|" );
      }
      printf(" ");
      contador++;
    }
    printf("\n");
  }
}
