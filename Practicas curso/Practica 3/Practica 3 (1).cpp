/*Programa que escribe en pantalla el
calendario de un mes comprendido entre
el a�o 1601 y el a�o 3000, dados como
datos el a�o y el mes */

#include <stdio.h>

int mes, anno;
/* Programa que escribe la cabecera del calendario*/
void escribirCabeceraCalendario (int m, int a){
  /* Aqui vamos a ajustar la impresion del mes a la
  derecha y el a�o a la izquierda del calendario*/
  switch (m) {
    case 1: printf("ENERO");printf("%22d",a);break;
    case 2: printf("FEBRERO");printf("%20d",a);break;
    case 3: printf("MARZO"); printf("%22d",a);break;
    case 4: printf("ABRIL"); printf("%22d",a);break;
    case 5: printf("MAYO"); printf("%23d",a);break;
    case 6: printf("JUNIO"); printf("%22d",a);break;
    case 7: printf("JULIO"); printf("%22d",a);break;
    case 8: printf("AGOSTO"); printf("%21d",a);break;
    case 9: printf("SEPTIEMBRE"); printf("%17d",a);break;
    case 10: printf("OCTUBRE"); printf("%20d", a); break;
    case 11: printf("NOVIEMBRE"); printf("%18d", a); break;
    case 12: printf("DICIEMBRE"); printf("%18d",a); break;
  }
  printf("\n===========================");
  printf("\nLU  MA  MI  JU  VI   SA  DO");
  printf("\n===========================");
  printf("\n");
}
            /*Comprobado que esta todo bien hasta aqu�*/

  /*Funcio que determina el n�mero de d�as que tiene un mes*/
    /* Primero calculamos si un a�o es bisiesto o no con las
    int a
    return true o false*/

  bool EsAnnoBisiesto (int a) {
   return (((a%4==0)&&!(a%100==0)) || ((a%400==0)));
  }
    /*Ahora determinados el n�mero de d�as que tiene el mes*/
  int determinarNumeroDiasMes (int m, int a) {
    switch (m) {
      case 1:
      case 3:
      case 5:
      case 7:
      case 8:
      case 10:
      case 12: return 31;break;
      case 4:
      case 6:
      case 9:
      case 11: return 30;break;
      case 2: if (EsAnnoBisiesto(a)){
        return 29;
      } else {
        return 28;
      } break;
    }
  }
      /*Compilador todo bien*/
    /*Funcion que determina en que d�a cae el primero
    del mes*/
      /*Primero determinamos en que cae el dia de a�o
      nuevo del a�o que interesa
      int a
      return un valor de 0 a 6*/
  int determinarDiaAnnoNuevo (int a) {
    int diaAnnoNuevo=0;
    for (int i=1601; i<=(a-1); i++) {
      if (EsAnnoBisiesto (i)){
        diaAnnoNuevo=diaAnnoNuevo+2;
      } else {
        diaAnnoNuevo=diaAnnoNuevo+1;
      }

    }
    diaAnnoNuevo=diaAnnoNuevo%7;
    return diaAnnoNuevo;
  }
/* TODO OK*/

      /*Despu�s calculamos el d�a en que cae el primero de mes
      dado el d�a de a�o nuevo*/

  int determinarPrimeroMes (int m, int a) {
    int diaPrimeroMes=determinarDiaAnnoNuevo(a);
    for (int i=1; i<=(m-1); i++) {
      switch (i){
      case 1: case 3: case 5: case 7: case 8: case 10: case 12: diaPrimeroMes=diaPrimeroMes+3; break;
      case 4: case 6: case 9: case 11: diaPrimeroMes=diaPrimeroMes+2; break;
      case 2: if (EsAnnoBisiesto (a)) {
        diaPrimeroMes=diaPrimeroMes+1;
      } else {
        diaPrimeroMes=diaPrimeroMes;
      }break;
    }
  }
  diaPrimeroMes=diaPrimeroMes%7;
  return diaPrimeroMes;
}
/* TODO OK*/

  /*Desterminar cuando cae el �ltimo d�a del mes*/

  int determinarUltimoDiaMes (int m, int a) {
  int diaUltimoMes;
  diaUltimoMes=(determinarPrimeroMes (m,a)+determinarNumeroDiasMes (m,a)-1)%7;
    return diaUltimoMes;
  }
/* TODO OK+/

  /*Impresi�n del calendario*/

  void imprimirCalendario (int m, int a) {

   int diaPrMes=determinarPrimeroMes (m,a);
   int diaUltMes=determinarUltimoDiaMes (m,a);
   int NumeroDiasMes=determinarNumeroDiasMes (m,a);
   int diaSemana;
   for (int diaSemana=1; diaSemana<=diaPrMes; diaSemana++) {
     if (diaSemana==6) {
       printf("|  .  ");
     } else if (diaSemana==5){
       printf(" . | ");
       } else {
       printf(" .  ");
     }

   }
   for (int i=1; i<=NumeroDiasMes; i++) {
     printf("%2d",i);
     if (diaPrMes>=7) {
      diaPrMes=0;
     }
     if (diaPrMes==4){
       printf (" | ");
     } else if (diaPrMes==6) {
        printf ("\n");
     } else {
       printf("  ");
     }
     diaPrMes=diaPrMes+1;
   }

   for(int i = diaUltMes+1; i<7; i++){

    if(i==4){

      printf(" . | ");

    }else if (i==6){
      printf(" .");
      }else {

      printf(" .  ");

    }

  }
}

int main () {
 int m, a;
 printf("�Mes (1..12)?");
 scanf("%d", &m);
 printf("\nA�o (1601..3000)?");
 scanf("%d", &a);
 printf("\n");
  if ((a>=1601 && a<=3000) && (m>=1 && m<=12)) {
  escribirCabeceraCalendario (m,a);
  imprimirCalendario (m,a);
  } else {
   printf("\n");
 }

}
