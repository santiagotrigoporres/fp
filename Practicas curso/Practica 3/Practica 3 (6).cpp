#include <stdio.h>

/***********************************************
==PROCEDIMIENTO PARA ESCRIBIR EL MES Y EL A�O Y=
==ASIGNAR LOS DIAS QUE TIENE CADA MES===========
***********************************************/
void EscribirMes(int mes, int anno, int & diasMes){
      if (mes == 1){
        diasMes = 31;
        printf( "\n\nENERO%22d", anno);
      }else if ( mes == 2){
          if ((anno % 4 == 0) && (anno % 100 != 0) || (anno % 400 == 0)){
              diasMes = 29;  /*A�os bisiestos*/
          }else{
              diasMes = 28;
              }
        printf( "\n\nFEBRERO%20d", anno);
      }else if ( mes == 3){
        diasMes = 31;
        printf( "\n\nMARZO%22d", anno);
      }else if ( mes == 4){
        diasMes = 30;
        printf( "\n\nABRIL%22d", anno);
      }else if ( mes == 5){
        diasMes = 31;
        printf( "\n\nMAYO%23d", anno);
      }else if ( mes == 6){
        diasMes = 30;
        printf( "\n\nJUNIO%22d", anno);
      }else if ( mes == 7){
        diasMes = 31;
        printf( "\n\nJULIO%22d", anno);
      }else if ( mes == 8){
        diasMes = 31;
        printf( "\n\nAGOSTO%21d", anno);
      }else if ( mes == 9){
        diasMes = 30;
        printf( "\n\nSEPTIEMBRE%17d", anno);
      }else if ( mes == 10){
        diasMes = 31;
        printf( "\n\nOCTUBRE%20d", anno);
      }else if ( mes == 11){
        diasMes = 30;
        printf( "\n\nNOVIEMBRE%18d", anno);
      }else if ( mes == 12){
        diasMes = 31;
        printf( "\n\nDICIEMBRE%18d", anno);
    }
}

/***********************************************
==FUNCION DEL 'ALGORITMO DE ZELLER', TE DEVUELVE
EL D�A DE LA SEMANA CORRESPONDE UNA FECHA. EL
DOMINGO ES EL 0 Y EL SABADO EL 6 ===============
***********************************************/
int Zeller(int anno, int mes){
  int a = 0;
  int b = 0;
  int resultado = 0;
  int d = 0;
  if (mes <= 2){
    mes = mes + 10;
    anno = anno - 1;
  }else{
    mes = mes - 2;
    }
    a = anno % 100;
    b = anno / 100;
    d = (700 + ((26 * mes - 2) / 10) + dia + a + a / 4 + b / 4 - 2 * b) % 7;

    if (d == 0){/*Cambiamos el numero asignado al domingo  0 por 7*/
    d = 7;
    }
    return d ;
}

/***********************************************
==PROCEDIMIENTO PARA ESCRIBIR EL CALENDARIO=====
***********************************************/
void ImprimirCalendario (int diaSemana, int diasMes){
int contador = 1;
  printf ( "\n===========================\n" );
  printf ( "LU  MA  MI  JU  VI | SA  DO\n" );
  printf ( "===========================\n" );


/*Imprime los puntos iniciales*/
  for (int i = 1; i < diaSemana; i++){
    printf (" .");
    printf (" ");
    if (contador % 7 == 5){
      printf ("|");
    }
    printf (" ");
    contador++;
  }

/*Imprime los numeros del calendario*/
    for (int i = 1; i <= diasMes; i++){
      printf ("%2d",i);
      if (contador % 7 == 0){
        printf( "\n");
      }else if(contador % 7 == 5){
        printf (" | ");
      }else{
        printf("  ");
      }
      contador++;
    }

/*Imprime los puntos finales*/
    while (contador % 7 != 1){
      printf (" .");
      printf (" ");
      if (contador % 7 == 5){
      printf ("|");
    }
    printf (" ");
    contador++;
  }
  printf ("\n");
}



/***********************************************
==PROGRAMA PRINCIPAL============================
***********************************************/
int main(){
  int anno = 0;
  int mes = 0;
  int dia = 1;/*Se inicializa a 1 para que
              diaSemana te muestre el dia de la
              semana que corresponde al dia 1 del mes*/
  int diasMes = 0;
  int diaSemana = 0;
  int d = 0;

  printf ("\xa8Mes (1..12)?");
  scanf ("%d", &mes);
  printf ("A�o (1601..3000)?");
  scanf ("%d", &anno);

  if ((anno >= 1601) && (anno <= 3000) && (mes >= 1) && (mes <= 12)){
    EscribirMes(mes, anno, diasMes);
    diaSemana = Zeller(anno, mes, dia);
    ImprimirCalendario(diaSemana, diasMes);
  }
}
