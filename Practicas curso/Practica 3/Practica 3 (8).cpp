#include<stdio.h>
#include<stdlib.h>
#include<ctype.h>

typedef enum tipomes {CERO,ENERO, FEBRERO, MARZO, ABRIL, MAYO, JUNIO, JULIO, AGOSTO, SEPTIEMBRE, OCTUBRE, NOVIEMBRE, DICIEMBRE};
typedef enum tipodia {Lunes, Martes, Miercoles, Jueves, Viernes, Sabado, Domingo};

tipomes mes;
tipodia inicio_dia;

int ano = 0;
int dias_mes = 31;
bool es_bisiesto;


bool ano_bisiesto(int aux_ano)
{
  bool bisiesto;
  if (((aux_ano % 4 == 0)&&(aux_ano % 100 != 0)) || (aux_ano % 400 == 0))
  {
      bisiesto = true;
  }
  else
  {
      bisiesto = false;
  }
  return bisiesto;
}

tipodia inicio_mes()
{
  /* 52 semanas 364 dias. A�o normal avanza 1 d�a de la semana, a�o bisiesto 2*/
  int avance_mes = 0;
  int avance_ano = 0;
  for (int i = 1601; i <ano; i++)
  {
      if (ano_bisiesto(i))
      {
        avance_ano = avance_ano +2;
        printf("\r\n bisiesto");
      }
      else
      {
        avance_ano ++;
        printf("\r\n nobisiesto");
      }
      printf (" %d", i);
  }
  printf("%d", avance_ano);
  /*Calcular avance del mes requerido*/
  avance_mes = (avance_ano % 7);
  for (int i = 1; i < int(mes); i++)
  {
    if ((i == int (ENERO)) || (i == int (MARZO)) || (i == int (MAYO)) || (i == int (JULIO)) || (i == int (AGOSTO)) || (i == int (OCTUBRE)) || (i == int (DICIEMBRE)))
    {
        avance_mes = avance_mes + 31;
    }
    else if ( i == int (FEBRERO))
    {
        if (ano_bisiesto(ano))
        {
          avance_mes = avance_mes + 29;
        }
        else
        {
          avance_mes = avance_mes + 28;
        }
    }
    else
    {
      avance_mes = avance_mes + 30;
    }
  }
  printf("\r\n%d", avance_mes%7);
  return (tipodia(avance_mes%7));
}

int imprimir_mes()
{
  printf("\r\n");
  /* Imprimir Nombre Mes*/
  switch(mes)
  {
    case 1:
      printf ("ENERO                  ");
      dias_mes = 31;
      break;
    case 2:
      printf ("FEBRERO                ");
      if (es_bisiesto)
      {
        dias_mes = 29;
      }
      else
      {
        dias_mes = 28;
      }
      break;
    case 3:
      printf ("MARZO                  ");
      dias_mes = 31;
      break;
    case 4:
      printf ("ABRIL                  ");
      dias_mes = 30;
      break;
    case 5:
      printf ("MAYO                   ");
      dias_mes = 31;
      break;
    case 6:
      printf ("JUNIO                  ");
      dias_mes = 30;
      break;
    case 7:
      printf ("JULIO                  ");
      dias_mes = 31;
      break;
    case 8:
      printf ("AGOSTO                 ");
      dias_mes = 31;
      break;
    case 9:
      printf ("SEPTIEMBRE             ");
      dias_mes = 30;
      break;
    case 10:
      printf ("OCTUBRE                ");
      dias_mes = 31;
      break;
    case 11:
      printf ("NOVIEMBRE              ");
      dias_mes = 30;
      break;
    case 12:
      printf ("DICIEMBRE              ");
      dias_mes = 31;
      break;
  }
  printf("%4d",ano);
  printf("\r\n");
  /* separador*/
  for(int i =0; i<27;i++)
  {
    printf("=");
  }
  printf("\r\n");
  printf("LU  MA  MI  JU  VI | SA  DO");
  printf("\r\n");
  /* separador*/
  for(int i =0; i<27;i++)
  {
    printf("=");
  }
  printf("\r\n");

  switch(inicio_dia)
  {
    case 0:
      printf (" 1   2   3   4   5 |  6   7\r\n");
      printf (" 8   9  10  11  12 | 13  14\r\n");
      printf ("15  16  17  18  19 | 20  21\r\n");
      printf ("22  23  24  25  26 | 27  28\r\n");
      switch (dias_mes)
      {
          case 31:
            printf ("29  30  31   .   . |  .   .\r\n");
            break;
          case 30:
            printf ("29  30   .   .   . |  .   .\r\n");
            break;
          case 28:
            break;
          default:
            printf ("29   .   .   .   . |  .   .\r\n");
      }
      break;
    case 1:
      printf (" .   1   2   3   4 |  5   6\r\n");
      printf (" 7   8   9  10  11 | 12  13\r\n");
      printf ("14  15  16  17  18 | 19  20\r\n");
      printf ("21  22  23  24  25 | 26  27\r\n");
      switch (dias_mes)
      {
          case 31:
            printf ("28  29  30  31   . |  .   .\r\n");
            break;
          case 30:
            printf ("28  29   30   .   . |  .   .\r\n");
            break;
          case 28:
            printf ("28   .   .   .   . |  .   .\r\n");
            break;
          default:
            printf ("28  29   .   .   . |  .   .\r\n");
      }
      break;
    case 2:
      printf (" .   .   1   2   3 |  4   5\r\n");
      printf (" 6   7   8   9  10 | 11  12\r\n");
      printf ("13  14  15  16  17 | 18  19\r\n");
      printf ("20  21  22  23  24 | 25  26\r\n");
      switch (dias_mes)
      {
          case 31:
            printf ("27  28  29  30  31 |  .   .\r\n");
            break;
          case 30:
            printf ("27  28  29  30   . |  .   .\r\n");
            break;
          case 28:
            printf ("27  28   .   .   . |  .   .\r\n");
            break;
          default:
            printf ("27  28  29   .   . |  .   .\r\n");
      }
      break;
    case 3:
      printf (" .   .   .   1   2 |  3   4\r\n");
      printf (" 5   6   7   8   9 | 10  11\r\n");
      printf ("12  13  14  15  16 | 17  18\r\n");
      printf ("19  20  21  22  23 | 24  25\r\n");
      switch (dias_mes)
      {
          case 31:
            printf ("26  27  28  29  30 | 31   .\r\n");
            break;
          case 30:
            printf ("26  27  28  29  30 |  .   .\r\n");
            break;
          case 28:
            printf ("26  27  28   .   . |  .   .\r\n");
            break;
          default:
            printf ("26  27  28  29   . |  .   .\r\n");
      }
      break;
    case 4:
      printf (" .   .   .   .   1 |  2   3\r\n");
      printf (" 4   5   6   7   8 |  9  10\r\n");
      printf ("11  12  13  14  15 | 16  17\r\n");
      printf ("18  19  20  21  22 | 23  24\r\n");
      switch (dias_mes)
      {
          case 31:
            printf ("25  26  27  28  29 | 30  31\r\n");
            break;
          case 30:
            printf ("25  26  27  28  29 | 30   .\r\n");
            break;
          case 28:
            printf ("25  26  27  28   . |  .   .\r\n");
            break;
          default:
            printf ("25  26  27  28  29 |  .   .\r\n");
      }
      break;
    case 5:
      printf (" .   .   .   .   . |  1   2\r\n");
      printf (" 3   4   5   6   7 |  8   9\r\n");
      printf ("10  11  12  13  14 | 15  16\r\n");
      printf ("17  18  19  20  21 | 22  23\r\n");
      switch (dias_mes)
      {
          case 31:
            printf ("24  25  26  27  28 | 29  30\r\n");
            printf ("31   .   .   .   . |  .   .\r\n");
            break;
          case 30:
            printf ("24  25  26  27  28 | 29  30\r\n");
            break;
          case 28:
            printf ("24  25  26  27  28 |  .   .\r\n");
            break;
          default:
            printf ("24  25  26  27  28 | 29   .\r\n");
      }
      break;
    case 6:
      printf (" .   .   .   .   . |  .   1\r\n");
      printf (" 2   3   4   5   6 |  7   8\r\n");
      printf (" 9  10  11  12  13 | 14  15\r\n");
      printf ("16  17  18  19  20 | 21  22\r\n");
      switch (dias_mes)
      {
          case 31:
            printf ("23  24  25  26  27 | 28  29\r\n");
            printf ("30  31   .   .   . |  .   .\r\n");
            break;
          case 30:
            printf ("23  24  25  26  27 | 28  29\r\n");
            printf ("30   .   .   .   . |  .   .\r\n");
            break;
          case 28:
            printf ("23  24  25  26  27 | 28   .\r\n");
            break;
          default:
            printf ("23  24  25  26  27 | 28  29\r\n");
      }
      break;
  }


}

int main ()
{

  printf("Mes (1..12)? ");
  scanf ("%d", &mes);

  if ((mes<1) || (mes > 12))
  {
    printf("Mes entre 1 y 12");
  }

  else
  {
    printf("Anno (1601...3000)? ");
    scanf ("%d", &ano);
    if ((ano<1601) || (ano > 3000))
    {
      printf("Ano entre 1601 y 3000");
    }
    else
    {
      es_bisiesto = ano_bisiesto(ano);
      inicio_dia = inicio_mes();
      printf("\r\n%d", inicio_dia);
      imprimir_mes();
    }
  }


}



