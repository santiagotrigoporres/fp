/*************************************************************************************************
* Programa: Calendario
*
* Descripci�n:
* Programa que muestra por pantalla la hoja de calendario de cualquier mes y a�o comprendido entre
* los a�os 1601 y 3000. Para a�os fuera de este rango no imprime nada. El programa pide mes y a�o
* solo una vez.
*************************************************************************************************/
#include <stdio.h>
/*============================================================================================
Global, Tipo enumerado con los dias de la semana
==============================================================================================*/
typedef enum TipoDia {Lunes, Martes, Miercoles, Jueves, Viernes, Sabado, Domingo} ;

/*============================================================================================
Funcion para calcular el dia de la semana en que comienza un mes y a�o dados, devuelve un tipo
enumerado correspondiente al dia de la semana.
TipoDia (Lunes, Martes, Miercoles, Jueves, Viernes, Sabado, Domingo ).
==============================================================================================*/
TipoDia DiaSemanaComienzoDeMes (int mes, int anho) {
  /*El algoritmo utiliza la Congruencia de Zeller para calcular el d�a de la semana de
  comienzo de la mes, estaf�rmula tiene en cuenta los a�os bisiestos*/
  const int dia=1;
  int casoDia;
  TipoDia diaSemana;
  int a=(14-mes)/12;        /*Congruencia de Zeller*/
  int y=anho-a;
  int m=mes+(12*a)-2;
  casoDia=(dia+y+(y/4)-(y/100)+(y/400)+(31*m)/12)%7;
  if (casoDia==0) { /*Cuando el resto de la division es 0 es Domingo.*/
    casoDia=7;
  }
  diaSemana=TipoDia (casoDia);
  return diaSemana;
}

/*============================================================================================
Funcion para calcular si un a�o es bisiesto. Devuelve true si el ano es bisisesto.
==============================================================================================*/
bool EsBisiesto(int anio) {
  /* Son a�os bisiestos los m�ltiplos de 4, excepto los que son multiplos de 100 queno son m�ltiplos de 400*/
  if (anio%4==0 && !(anio%100==0 && anio%400!=0)) {
    return true;
  }
  return false;
}

/*============================================================================================
Procedimiento para imprimir el calendario correspondiente a un mes y ano dados.
==============================================================================================*/
void ImprimirCalendario(int mes, int anio) {
  typedef char string[26];                          /*Longitud de cada campo de la cabecera (meses)*/
  typedef string vectorMeses[12];                   /*Vector con cadenas correspondientes a los meses */
  typedef int numeroDiasMeses[12];                  /*Vector con la cantidad de d�as de cada mes*/
  TipoDia diaSemana,comienzoSemana;                 /*Variables del tipo enumerado global*/
  int i, diaMes;                                    /*Variables enteras auxiliares usadas como indices*/

  /*============================ INICIALIZACION DE VARIABLES ===============================*/
  vectorMeses meses = {"ENERO                  ",
                       "FEBRERO                ",
                       "MARZO                  ",
                       "ABRIL                  ",
                       "MAYO                   ",
                       "JUNIO                  ",
                       "JULIO                  ",
                       "AGOSTO                 ",
                       "SEPTIEMBRE             ",
                       "OCTUBRE                ",
                       "NOVIEMBRE              ",
                       "DICIEMBRE              "
                      };                            /*Inicializaci�n Meses con las cadenas de cada mes*/
  numeroDiasMeses diasMeses= {31, 28, 31, 30, 31,
                              30, 31, 31, 30, 31,
                              30, 31
                             };              /*Inicializaci�n DiasMeses con los dias de cada mes*/
  if (EsBisiesto(anio)) {                           /*Febrero tiene 29 dias si el ano es bisiesto*/
    diasMeses[1]=29;
  }
  comienzoSemana = DiaSemanaComienzoDeMes (mes, anio);/*Inicializaci�n del dia de la semana en que comienza un mes*/

  /*============================ CABECERA DEL CALENDARIO ===================================*/
  printf("%s%d\n",meses[mes-1],anio);
  printf("%s\n","===========================");
  printf("%s\n","LU  MA  MI  JU  VI | SA  DO");
  printf("%s\n","===========================");

  /*============================ CUERPO DEL CALENDARIO ===================================*/
  /*Imprime puntos enlos d�as de la semana en los que el mes a�n no ha comenzado*/
  for (int diaSemana=Lunes;diaSemana<TipoDia(comienzoSemana-1);diaSemana++) {
    if (diaSemana == Domingo) {
      printf("%2s",".");
    } else {
      if (diaSemana==Viernes) {
        printf("%2s%1s",".","");
      } else {
        printf("%2s%2s",".","");
      }
    }
    if (diaSemana==Viernes) {
      printf("%s ","|");
    }
    if (diaSemana==Domingo) {
      printf("\n");
    }
  }
  /*Imprime los d�as de mes para cada d�a de la semana del mes*/
  for (int diaMes=1;diaMes<=int(diasMeses[mes-1]);diaMes++) {
    diaSemana = TipoDia(comienzoSemana-1);
    if (diaSemana == Domingo) {
      printf("%2d",diaMes);
    } else {
      if (diaSemana==Viernes) {
        printf("%2d ",diaMes);
      } else {
        printf("%2d%2s",diaMes,"");
      }
    }

    if (diaSemana==Viernes) {
      printf("%s%1s","|","");
    }
    if (diaSemana==Domingo) {
      comienzoSemana=Lunes;
      printf("\n");
    }
    comienzoSemana=TipoDia(comienzoSemana+1);
  }
  /*Imprime puntos en los d�as de la semana en los que el mes ya termin�*/
  if (diaSemana!=Domingo) {
    for (int diaSemana=TipoDia(comienzoSemana-1);diaSemana<=Domingo;diaSemana++) {
      if (diaSemana == Domingo) {
        printf("%2s",".");
      } else {
        if (diaSemana==Viernes) {
          printf("%2s%1s",".","");
        } else {
          printf("%2s%2s",".","");
        }
      }

      if (diaSemana==Viernes) {
        printf("%s ","|");
      }
    }
  }
  printf("\r\n");
}

/*============================================================================================
Procedimiento que pide los datos de entrada. Ano y mes para el que se calcula el calendario.
==============================================================================================*/
void PedirDatos(int &anio,int &mes) {
  printf("%c%s%c",168,"Mes (1..12)?");
  scanf ("%d",&mes);
  printf("%c%c%c%s",168,char('A'),164,"o (1601..3000)?");
  scanf ("%d",&anio);
  printf("\n");
}

/*============================================================================================
Programa principal
==============================================================================================*/
int main() {
  int anio,mes;
  PedirDatos(anio,mes);

  if ((anio >= int(1601) && anio <=int(3000)) && (mes>=int(1) && mes<=int(12))) {
    ImprimirCalendario(mes,anio);
  }
}
