
/* Practica 3, pasar la cabecera */

# include <stdio.h>
# include <ctype.h>

typedef enum TipoMes {
ENERO, FEBRERO, MARZO, ABRIL, MAYO, JUNIO, JULIO, AGOSTO, SEPTIEMBRE, OCTUBRE, NOVIEMBRE, DICIEMBRE
};
TipoMes Mes;

typedef enum TipoDia{
LUNES, MARTES, MIERCOLES, JUEVES, VIERNES, SABADO, DOMINGO
};
TipoDia dia;



/***************
Funcion para sumar dias de la semana ciclicamente
****************/

TipoDia SumarDias (TipoDia dia, int n){
const int DiasSemana=7;
int aux;

aux = (int(dia)+n)%DiasSemana;
return TipoDia(aux);
}

/**************
Funcion que calcula el dia de la semana que corresponde el inicio de un mes
***************/

TipoDia DiaDeLaSemana (TipoMes M, int A, bool bisiesto){


TipoDia UnoEnero1601= LUNES;

int IncreBisis, IncreAnnos, IncreDias;

if (M==ENERO){
  IncreDias=0;
  }else if (M==FEBRERO){
  IncreDias=3;
  }else if (M==MARZO){
  IncreDias=3;
  }else if (M==ABRIL){
  IncreDias=6;
  }else if(M==MAYO){
  IncreDias=1;
  }else if (M==JUNIO){
  IncreDias=4;
  }else if (M==JULIO){
  IncreDias=6;
  }else if (M==AGOSTO){
  IncreDias=2;
  }else if(M==SEPTIEMBRE){
  IncreDias=5;
  }else if (M==OCTUBRE){
  IncreDias=0;
  }else if (M==NOVIEMBRE){
  IncreDias=3;
  }else{
  IncreDias=5;
}


IncreAnnos=A-1601;
IncreBisis=IncreAnnos/4-IncreAnnos/100+IncreAnnos/400;
IncreDias=IncreDias+IncreAnnos+IncreBisis;

if ((bisiesto==true)&&(M>FEBRERO)){
IncreDias++;
}

return SumarDias(UnoEnero1601,IncreDias);
}
/********
FUNCION QUE SACA EL MES POR PANTALLA
*/


void EscribirCalendario(TipoDia D,TipoMes M, bool bisiesto, int anno){

const int LENERO=31;
const int LFEBRERONORMAL=28;
const int LFEBREROBISIESTO=29;
const int LMARZO=31;
const int LABRIL=30;
const int LMAYO=31;
const int LJUNIO=30;
const int LJULIO=31;
const int LAGOSTO=31;
const int LSEPTIEMBRE=30;
const int LOCTUBRE=31;
const int LNOVIEMBRE=30;
const int LDICIEMBRE=31;

TipoDia ContadorDia=SumarDias(D,0);

int longmes=0;

if (M==ENERO){
  longmes=LENERO;
  printf("ENERO");
  for (int i=1;i<19;i++){
    printf(" ");
    }
  }

  else if (M==FEBRERO&&(bisiesto==true)){
    longmes=LFEBREROBISIESTO;
   printf("FEBRERO");
   for (int i=1;i<17;i++){
    printf(" ");
    }
  }
     else if (M==(FEBRERO&&(bisiesto==false))){
    longmes=LFEBRERONORMAL;
   printf("FEBRERO");
   for (int i=1;i<17;i++){
    printf(" ");
    }
    }
    else if (M==MARZO){
      longmes=LMARZO;
    printf("MARZO");
    for (int i=1;i<19;i++){
    printf(" ");
    }
    }else if (M==ABRIL){
      longmes=LABRIL;
      printf("ABRIL");
      for (int i=1;i<19;i++){
    printf(" ");
    }
      }else if (M==MAYO){
        longmes=LMAYO;
        printf("MAYO");
        for (int i=1;i<20;i++){
    printf(" ");
    }
        }else if (M==JUNIO){
          longmes=LJUNIO;
          printf ("JUNIO");
          for (int i=1;i<19;i++){
    printf(" ");
    }
          }else if (M==JULIO){
            longmes=LJULIO;
           printf ("JULIO");
           for (int i=1;i<19;i++){
    printf(" ");
    }
            }else if (M==AGOSTO){
              longmes=LAGOSTO;
              printf("AGOSTO");
              for (int i=1;i<18;i++){
    printf(" ");
    }
              }else if (M==SEPTIEMBRE){
                longmes=LSEPTIEMBRE;
                printf("SEPTIEMBRE");
                for (int i=1;i<14;i++){
    printf(" ");
    }
                }else if (M== OCTUBRE){
                  longmes=LOCTUBRE;
                  printf ("OCTUBRE");
                  for (int i=1;i<17;i++){
    printf(" ");
    }
                  }else if (M==NOVIEMBRE){
                    longmes=LNOVIEMBRE;
                    printf ("NOVIEMBRE");
                    for (int i=1;i<15;i++){
    printf(" ");
    }
                    }else{
                      longmes=LDICIEMBRE;
                      printf ("DICIEMBRE");
                      for (int i=1;i<15;i++){
    printf(" ");
    }
                      }

      printf ("%d\n",anno);


  for (int i=1;i<28;i++){
    printf("=");
    }
    printf("\n");
  printf("LU  MA  MI  JU  VI | SA  DO\n");
  for (int i=1;i<28;i++){
    printf("=");
    }
    printf("\n");


  for (int j=0;j<D;j++){
    if (j==4){
      printf(" . | ");
      }else{
    printf(" .  ");
      }

    }

for (int l=1;l<=longmes;l++){

  if(l<=9){
  if (ContadorDia==4){
    printf(" %d | ",l);
    }else if(ContadorDia==6){
      printf (" %d\n",l);
      }else {
        printf (" %d  ",l);
        }

  ContadorDia=SumarDias(ContadorDia,1);
  }else{
    if (ContadorDia==4){
    printf("%d | ",l);
    }else if(ContadorDia==6){
      printf ("%d\n",l);
      }else {
        printf ("%d  ",l);
        }

  ContadorDia=SumarDias(ContadorDia,1);

  }
  }
if (ContadorDia>0){
  for (int j=ContadorDia;j<7;j++){

       if (ContadorDia==4){
      printf(" . | ");
      }else{
    printf(" .  ");
      }
    ContadorDia=SumarDias(ContadorDia,1);
    }
    printf("\n");

  }
}



int main(){

int mes, anno;
bool bisiesto=false;

printf("�Mes (1..12)?\n");
scanf("%d",&mes);

printf ("�A�o  (1601..3000)?\n");
scanf("%d", &anno);

if (anno%4==0){
if(!(anno%100==0)||(anno%400==0)){
bisiesto=true;
  }
}else{
  bisiesto=false;
  }

if (mes>=1&&mes<=12){
  if (anno>=1601&&anno<3001){
    printf("\n");

    Mes=TipoMes(mes-1);

    dia= DiaDeLaSemana(Mes,anno,bisiesto);

    EscribirCalendario(dia,Mes, bisiesto, anno);


  }
}

}
