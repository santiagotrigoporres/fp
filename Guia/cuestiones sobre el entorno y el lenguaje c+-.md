En relación a diversas cuestiones relacionadas con el entorno de desarrollo, el lenguaje C+/- y los conceptos básicos de la asignatura, comentar:

- Sobre el error 'undefined reference to _WinMain@16'WINMAIN16

En relación al error anterior se genera en los casos en los que se compila y ejecuta un programa en C/C++ que no tiene función “main”.

Tal y como se indica en el punto 2.8.2 del libro de la asignatura la estructura formal de un programa completo es: { Include} int main() Bloque

Si se inicia desde el entorno un comando de “compilación y ejecución” es porque el programa que está compilando dispone de esta función “main” que sirve como punto de entrada de la ejecución del programa para el sistema operativo.

- Comentarios

En el lenguaje C+/-, los comentarios siempre van entre los delimitadores "/*" y "*/" (al estilo C).

En C+/-, no existe el delimitador de comentario "//" (al estilo C++) para el inicio de un comentario de una línea que existe en las versiones modernas de C/C++. En estos casos el comentario se inicia con "//" y acaba con el fin de linea (el caracter \n").
En el entorno de C+/- pueden usarse por compatibilidad léxica con C++ el comentario "//".

- Int o float

Uno de los elementos fundamentales respecto al uso de uno u otro tipo de datos reside en que el tipo entero tiene un carácter "exacto" frente al caracter "no exacto" de los tipos reales, según la forma que tienen los computadores de almacenar y representar los datos. Este tema se comenta en los apartados 2.5.1 y 2.5.2 del libro de la asignatura. 
