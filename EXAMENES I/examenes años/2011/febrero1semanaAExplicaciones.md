# Respuesta de examenes

1. En el lenguaje C±,la sentencia catch:
  A. Agrupa las sentencias para el tratamiento de la excepción.

La sentencia try agrupa el bloque de código en el que se programa el algoritmo problema a resolver sin tener en cuenta las posibles excepciones que se podrian producir. A continuación, la sentencia catch agrupa el código para tratamiento de la excepción que se declara entre parentesis.

2. ¿Qué imprime la siguiente sentencia escrita en C±?:
  printf("Descuento: %5.2f%c\n",12.5,'%');
  C. Descuento: 12.50%

3. Si a = true y b = false, la expresión:
  !(!(a || b) && !(a))
  D. !(!a || b) || !b **Dificil importante repasar**
4. La sentencia del lenguaje C±:
  scanf("mes%2d",&mes)
  A. Se ejecuta correctamente si se lee el dato: mes3
5. 