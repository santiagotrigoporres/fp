/*
  Realizar un tipo abstracto de datos para el juego de las TRES EN RAYA.
 El tablero es de 3x3 y cualquier cuadricula podra estar vacia u ocupada
solo por una ficha blanca o negra. Las operaciones son:
  1.- Poner una ficha en una cuadricula que no esta ocupada
  2.- Quitar una ficha de una cuadricula
  3.- Comprobar si se produce tres en raya e indicar si es de blancas o
    de negras
*/

#include "threeRow.h"

char ThreeRow::put(int posX, FileType t){
   for (int i=6;i>0;i--){
      if (board[posX][i]==empty){
         board[i][0] = t;
         
         return 'OK';
      }
   }
   return 'false';
}

FileType ThreeRow::threeRow(){

  for (int i=0;i<3;i++){
   if ((board[i][0]==board[i][1]) && (board[i][0]==board[i][2])){
      return board[i][0];
   }
  }

  for (int i=0;i<3;i++){
   if ((board[0][i]==board[1][i]) && (board[0][i]==board[2][i])){
      return board[0][i];
   }
  }

   if ((board[0][0]==board[1][1]) && (board[2][2]==board[0][0])){
      return board[0][0];

   }

   if ((board[2][0]==board[1][1]) && (board[2][0]==board[0][2])){
      return board[2][0];
   }

   return empty;

}
