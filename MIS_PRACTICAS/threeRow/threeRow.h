#pragma once

typedef enum FileType { empty, white, black };

typedef struct ThreeRow{
  FileType board[6][6];

  char put(int posX, FileType t);
  FileType threeRow();
};
