#include <stdio.h>

  /**********************
   * NOMBRE: #Santiago#
   * PRIMER APELLIDO: #Trigo#
   * SEGUNDO APELLIDO: #Porres#
   * DNI: #50984264A#
   * EMAIL: #santiagotrigoporres@gmail.com#
   ****************/
int main(){

  const char *nombre = "Santiago";
  const char *primerApellido = "Trigo";
  const char *segundoApellido = "Porres";
  const char *dni = "50984264A";
  const char *email = "santiagotrigoporres@gmail.com";

  printf("NOMBRE:%9s \n", nombre);
  printf("PRIMER APELLIDO:%6s \n", primerApellido);
  printf("SEGUNDO APELLIDO:%6s \n", segundoApellido);
  printf("DNI:%9s \n", dni);
  printf("EMAIL:%30s ", email);

}