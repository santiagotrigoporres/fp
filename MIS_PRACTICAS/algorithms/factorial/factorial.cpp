#include <stdio.h>

int main(){
  int number;
  int result = 1;

  printf("Enter the number for calculate factorial");
  scanf("%d", &number);

  for(int i = 1; i <= number; i++){
    result = result * i;
  }

  printf("Result is %d", result);
}