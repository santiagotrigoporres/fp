#include "atm.h"
#include <stdio.h>
#include <time.h>

void menu(){
  printf("Menu:\n");
  printf("1. meter dinero\n");
  printf("2. consultar saldo\n");
  printf("3. sacar dinero\n");
  printf("0. Exit\n");
  printf("Choose: 0,1,2?: ");
}

int main(){
  functionsOperations x;
   //Test t;
  float ingress;
  float remove;
  float result;
  int take;
  char command;

  do{
    menu();
    scanf(" %c", &command);
    switch(command) {
      case '0': printf("nos vemos"); break;
      case '1':
        printf("Cantidad que quieres ingresar \n");
        scanf(" %f", &ingress);
        x.load(ingress);
        break; 
      case '2':
        result = x.consult();
        printf("your balance is %.2f € \n", result);
        break;
      case '3':
        printf("Cantidad que quieres retirar \n");
        scanf(" %f", &remove);
        x.takeOut(remove);
        break;
    }
  }while(command != '0');
}