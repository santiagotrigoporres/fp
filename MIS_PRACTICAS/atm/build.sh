RED='\033[0;31m'
GREEN='\033[0;32m'
YELLOW='\033[1;33m'
NC='\033[0m' # No Color
echo ${RED}'compile cpp files..........'${NC}
g++ -c main.cpp atm.cpp
echo ${YELLOW}'generate assembly code........'${NC}
g++ -o atm atm.o main.o
echo ${GREEN}'running......'${NC}
./atm