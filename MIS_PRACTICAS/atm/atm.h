/*
  Crear un cajero automatico.
  Se podra:
    1. consultar el saldo
    2. realizar transferencias
    3. añadir dinero
    4. consultar ultimos movimientos
*/

#pragma once
/* Operaciones que se pueden realizar sacar, meter y consultar saldo */
typedef enum operations {load, takeOut, consult};


typedef struct Date {
  int day;
  int month;
  int year;
};

typedef struct functionsOperations {
  float balance = 0;
  float quantity;
  float consult();
  float load(float q);
  float takeOut(float q);
};

