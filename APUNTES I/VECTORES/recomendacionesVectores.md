En relación a las diferentes cuestiones sobre el uso de vectores (declaración de vectores, operaciones, paso de argumentos, ..) se recomienda repasar con detalle los apartados correspondientes del libro, en particular el apartado 9.6.

- No se admite en C+/-, el uso de tipos anónimos (apartado 9.6.1):
char mivariable[5]; int misenteros[10];

- Y es necesario la declaración explicita del tipo:
typedef char TipoCincoCar[5]; typedef int TipoDiezEnteros[10];
TipoCincoCar mivariable; TipoDiezEnteros misenteros;

- También se debe recordar que, como se indica en el apartado (9.6.5), el modo por defecto de paso de argumentos de tipo formación es por referencia (sin necesidad de usar el & como se indicaba en el apartado 7.4.2). Se usará la palabra reservada "const" si lo se quiere es NO modificar el contenido de una formación en un subprograma. Esta norma aplica a las cadenas (strings) como vectores de caracteres que son.