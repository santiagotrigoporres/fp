# 2 Elementos básicos de programación.

El objetivo que trata de alcanzar este tema es permitir el desarrollo de programas completos desde el principio. Estos programas se podrán realizar como prácticas con el computador de manera inmediata y directa utilizando un compilador de C/C++.

## 2.1 Lenguaje C+-

Los ficheros C+- tendra una extensión **.cpp** como C++. La presentación de cada nuevo elemento de C+- se realizará formalmente mediante la ***notación BNF***. 

## 2.2 Notación BNF

Un lenguaje de programación sigue reglas gramaticales ismilares a las de cualquier idioma humano, pero más estrictas. Estas reglas sobre cómo han de escribirse los elementos del lenguaje en forma de simbolos utilizan a su vez otros simbolos, que se denominan ***metasimbolos***. Son los siguientes: (vease foto 1).

También se emplearán distintos estilos de letra para distinguir los elementos simbólicos siguientes:

- Elemento_no_terminal: Este estilo se emplea para escribir el nombre de un elemento gramatical que habrá de ser definido por alguna regla. Cualquier elemento a la izquierda del metasímbolo **::=** será no terminal y aparecerá con este estilo.

- Elemento_terminal: Este estilo se emplea para representar los elementos que forman parte del lenguaje C±, es decir, que constituyen el texto de un programa. Si aparecen en una regla deberán escribirse exactamente como se indica.

## 2.3 Valores y tipos

Un **DATO** es un elemento de información que puede tomar un valor entre varios posibles.  Si un datto tiene siempre necesariamente un valor fijo, diremos que es una **CONSTANTE**.

En programación a las distintas clases de valores se les denomina **tipos**. Un dato tiene asociado un tipo, que representa la clase de valores que puede tomar. Es importante destacar que el concepto de tipo es algo abstracto, e independiente de los símbolos concretos que se empleen para representar los valores.

## 2.4 Representación de valores constantes

Uno de los objetivos de los lenguajes de programación es evitar las ambigüedades o imprecisiones que existen en los lenguajes humanos. 

### 2.4.1 Valores numéricos enteros 

Los valores enteros representan un número eexacto de unidades, y no pueden tener parte fraccionaria. Un valor entero se escribe mediante una secuencia de uno o mas digitos de 0 al 9 sin separadores de ninguna clase entre ellos y precedidos de opcionalmente de los simbolos más o menos.

El lenguaje C+- como derivado de C/C++ considera que cuando el valor entero comienza por un primer dígito O se está escribiendo en base 8 (octal) en lugar de en base 10 (decimal). Así, el valor númerico 020 es un número octal que equivale a 16 en decimal. El compilador de C/C++ da un error si al escribir un valor octal se utilizan los dígitos 8 ó 9.

### 2.4.2 Valores numéricos reales

Los valores númericos reales permiten expresar cualquier cantodad, incluyendo fracciones de unidad.

En la notación decimal habitual un valor real se escribe con una parte entera terminada siempre por un punto (.), y seguida opcionalmente por una secuencia de dígitos que constituyen la parte fraccionaria decimal.

En la notación científica un número real se escribe como una mantisa, que es un número real en la notación decimal habitual, seguida de un factor de escala que se escribe como la letra E seguida del exponente entero de una potencia de 10 por la que se multiplica la mantisa. Ejemplo: -23.2E+12 equivalente a -23.2x10 elevado a 12.

### 2.4.3 Caracteres 

Los lenguajes de programación nos deben permitir representar valores correspondientes a los caracteres de un texto, y que están disponibles en cualquier teclado, pantalla o impresora.

- El espacio en blanco (' ') ess un carácter valido como los demas.
- Hay que distinguir entre el valor entero de un digito (7) y el caracter correspondiente ('7').
- '\n' Salto al comienzo de una nueva línea de escritura
- '\r' Retorno al comienzo de la misma línea de escritura
- '\t' Tabulación
- '\ '' Apóstrofo
- '\\' Barra inclinada
- '\f' Salto a una nueva página o borrado de pantalla

### 2.4.4 Cadena de caracteres (string)

Un string se escribe como una secuencia de crcateres incluidos entre comillas ('').

- Si una cadena incluye comillas en su interior se escribirá mediante \ "
- No hay que confundir un valor de tipo carácter ('x') con una cadena del mismo único carácter ("x"). La distinción se produce por el delimitador utiliazado comillas (") para una cadena y apóstrofo (') para un carácter 
- Es posible definir una cadena vacía que no contenga ningún carácter ("").

## 2.5 Tipos predefinidos

Una misma clase de valores pueden distinguirse varios tipos diferentes, tanto a nivel de tipos predefinidos en el lenguaje, como de tipos definidos por el programador.

Un tipo de datos define:

1. Una colección de valores posibles.
2. Las operaciones significativas sobre ellos.

### 2.5.1 El tipo entero (int)

Los valores de este tipo son los numéricos enteros positivos y negativos. Coincide con el concepto matemático de los numeros enteros, sin embargo debido al caracter fisico de los computadores el rango nunca podra ser infinito. Los rangos mas comunes (según combinación de procesador, sistema operativo y compilador) son: (vease foto2).

Estos raangos son dados en que los computadores suelen emplear codificación en base 2 de valores enteros. Para el signo del numero se utiliza un bit, quedando por tanto 15, 31 ó 63 para el valor absoluto.

> NOTA: Para facilitar la escritura de programas que tengan en cuenta la limitación particular de rango existente en cada caso, C y C++ permiten hacer referencia al valor mínimo mediante el nombre simbólico INT_MIN, y al valor máximo mediante INT_MAX. El rango admisible será, por tanlo: INT_MIN ... O ... INT_MAX. Estos nombres están definidos en el módulo limits de la librería estándar de C (cabecera <limits.h»>)

Asociadas a este tipo hay operaciones qque se pueden realizar con los valores. (+,-,*,/,% resto de division, + identidad de un entero, - cambio de signo a un entero).

Cuando se realiza una operación con enteros se debe tener en cuenta el rango de valores disponible en la plataforma que se esta utilizando. Si se produce un resultado fuera del rango disponible se producirá un error. En algunos casos este tipo de errores no se indica y puede ser difícil su detección.

### 2.5.2 El tipo real (float)

Con el tipo float se trata de representar en el computador los valores numéricos reales positivos y negativos. Sin embargo, al contrario que en caso del tipo int, esta representación puede no ser exacta.

Los valores reales se suelen representar internamente de forma equivalente a la notación científica, con una mantisa y un factor de escala. El rango de valores representahles está limitado tanto para valores grandes como pequeños. Los valores más pequeños que un límite dado se confunden con el cero.

Asociadas al tipo float están las operaciones que se pueden realizar con el (+,-,*,/,% resto de division, + identidad de un entero, - cambio de signo a un entero).

### 2.5.3 El tipo carácter (char)

Cada carácter no se representa internamente como un dibujo, sino como un valor numérico entero que es su código. La colección concreta de caracteres y sus códigos numéricos se establecen en una tabla (charset) que asocia a cada carácter el código numérico (codepoint) que le corresponde.

> Tablas de caracteres mas habituales: (ver foto 3)

En C+- (como en C¡C++) los valores del tipo char ocupan 8 bits e incluyen el repertorio ASCII. Además incluyen otros caracteres no--ASCII que dependen de la tabla de caracteres establecida.

Además conviene saber que la tabla ASCII posee las siguientes características:

- Los caracteres correspondientes a las letras mayúsculas de la 'A' a la 'Z' están ordenados en posiciones consecutivAS y crecientes según el orden alfabético.

- Los caracteres correspondientes a las letras minúsculas de la 'a' a la 'z' están ordenados en posiciones consecutivas y crecientes según el orden alfabético.

- Los caracteres correspondientes a los dígitos del 'O' al '9' están ordenados en posiciones consecutivas y crecientes.

Esto facilita el obtener por cálculo el valor numérico equivalente al carácter de un dígito decimal, o la letra mayúscula correspondiente a una minúscula o viceversa.

En (y en C+-) se puede usar también el módulo de librería ctype (cabecera <ctype.h>), que facilita el manejo de diferentes clases de caracteres. Este módulo incluye funciones tales como:

- isalpha( e ) Indica si e es una letra
- isascii( e ) Indica si e es un carácter ASCII
- isblank( e ) Indica si e es un carácter de espacio o tabulación
- iscntrl( e ) Indica si e es un carácter de control
- isdigit( e ) Indica si e es un dígito decimal (0-9)
- i.slower( e ) Indica si e es una letra rninúscula
- isspace( e ) Indica si e es espacio en blanco o salto de línea o página.
- isupper( e ) Indica si e es una letra mayúscula
- tolower ( e ) Devuelve la minúscula correspondiente a e
- toupper( e ) Devuelve la mayúscula correspondiente a e

## 2.6 Expresiones aritméticas

Una expresión aritmética representa un cálculo a realizar con valores numéricos. Una expresión aritmética es una combinación de operandos y operadores.

Los paréntesis ayudan a indeicar el orden en el que se efectuaran las operaciones. Si no este dependera de la jerarquía de los operadores. 1-Operadores multiplicativos, 2-Operadores aditivos.

> El lenguaje C+- permite la ambigüedad que supone la mezcla de tipos de datos diferentes en la misma expresion sin exigir una conversión explícita. Para salvar dicha ambigüedad, se convierte de manera automatica todos los valos de una misma expresion al tipo del valor con mayor rango y precision. Para evitar esta situación es obligatorio que se realice siempre una conversión explícita de tipos. 

## 2.7 Operaciones de escritura simples

Operaciones de escritura simple son las acciones que envían resultados al exterior.

### 2.7.1 El procedimiento printf

Este procedimiento pertenece al módulo stdio (cabecera <stdio.h>). La
forma más sencilla de invocarlo es escribir:

> printf(cadena-de-cameteres);

NOTA: Esta forma sencilla sólo es válida si la cadena de caracteres a escribir no contiene el carácter %.

Una cadena de caracteres con formatos deberá incluir en su interior una especificación de formato por cada valor que se quiera insertar. Usando el carácter fijo % seguido de una letra de código que indica el tipo de formato a aplicar. Algunos códigos de formato habituales son:

| Código | Nemotécnico | Tipo de valor|
|--------|-------------|--------------|
|   d    | decimal     | entero       |
|   f    | fixed point | real         |
|   e    | exponential | real con notación exponencial|
|   g    | general     | real con/sin notación exponencial|
|   c    | character   | un carácter  |
|   s    | string      | una cadena de caracteres|

Para conseguir espacios en los resultados se indica cuántos caracteres debe ocupar el valor de cada dato escrito. Esto se hace poniendo el número de caracteres entre el simbolo de % y el código del formato (%5d). Cuando se utiliza un formato f, e ó g se puede especificar el número de decimales (%5.2g).

Para escribir resultados en varias líneas de texto habrá que recordar que dentro de una cadena se pueden incluir caracteres especiales mediante secuencias de escape. Más concretamente, si queremos dar por terminada una línea de resultados y pasar a escribir en la siguiente bastará con incluir en el punto adecuado la secuencia de escape **\n**. 

> printf( "%10.4f\n", 24.45 )

## 2.8 Estructura de un programa completo

Una línea precedida del símbolo # comienzan lo que se llaman directivas para el compilador. En concreto con la directiva #include se indica al compilador que utilice el módulo de librería stdio (cabecera <stdio.h>) para las operaciones de escritura que se realizarán en el programa. De hecho la directiva #include será la única que se usará en C±.

> Todos los programas en C± se deben guardar en un fichero con el nombre del programa y la extensión **.cpp** (hola.cpp).

### 2.8.1 Uso de comentarios

Todos los lenguajes permiten incluir dentro del texto del programa comentarios que faciliten su comprensión. Estos comentarios sirven sólo como documentación del programa fuente, y son ignorados por el compilador, en el sentido de que no pasan a formar parte del código objeto al que se traducirá el programa. En C± los comentarios se incluyen dentro de los símbolos /* y */. 

> /* jOjo! Esto es un comentario */

### 2.8.2 Descripción formal de la estructura de un programa

Cada directiva debe ocupar una línea del programa ella sola. En los lcnguajes C y C++ hay una gran variedad de directivas, pero en C± se utilizará casi exclusivamente la directiva **#include**, que sirve para indicar que el programa utilizará un determinado módulo de librería. El parámetro Nombre_módulo corresponde en realidad al nombre del fichero de cabecera (header) del módulo.
