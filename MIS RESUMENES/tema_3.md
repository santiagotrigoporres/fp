# 3 Constantes y variables 

## 3.1 Identificadores

Se llaman identificadores a los nombres usados para identificar cada elemento del programa. En C± los identificadores son una palabra formada con caracteres alfabéticos o numéricos seguidos, sin espacios en blanco ni signos de puntuación intercalados, y que debe comenzar por una letra. Pueden usarse las 52 letras mayúsculas y minúsculas del alfabeto inglés, el guión bajo (_), y los dígitos decimales del O al 9.

En general, se debe tener cuidado al utilizar identificadores que difieran en pocas letras y en particular que difieran sólo en el uso de los letras mayúsculas o minúsculas para distinguir identificadores diferentes. En estos casos, es difícil distinguir entre un cambio de alguna letra por error y una utilización correcta de identificadores distintos.

A la hora de inventar nombres conviene seguir unas reglas de estilo uniformes que faciliten la lectura del programa. En el Manual de Estilo de C± se sugieren. entre otras, las siguientes:

- Por defecto, escribir todo en minúsculas.

- Escribir en mayúsculas o empezando por mayúsculas los nombres de constantes que sean globales o que sean parámetros generales del pro- grama.

- Usar guiones o mayúsculas intermedias para los nombres compuestos.

## 3.2 El vocabulario de C+-

Las palabras clave, sirven para delimitar determinadas construcciones del lenguaje de programación. Son elementos fijos del lenguaje. Este conjunto de de elementos fijos se denominan palabras reservadas, y no pueden ser redefinidas por el programador.

## 3.3 Constantes

### 3.3.1 Concepto de constante

Una constante es un valor fijo que se utiliza en un programa. El valor no puede cambiar de una ejecución a otra. Son ejemplos de constantes el número de meses del año, el número de días de una semana...

### 3.3.2 Declaración de cosntantes con nombre.

La declaración de un valor constante con nombre consiste en asociar un identificador a dicho valor constante. La declaración de la constante especifica su monbre y tipo y el valor asociado. 

> const float Pi =3.141S926S;

Las constantes con nombre han de ser declaradas en el programa antes de ser utilizadas. Una vez definida la constante se puede utilizar su nombre exactamente igual que si fuera su valor explícito.

## 3.4 Variables 

### 3.4.1 Concepto de variable

En programación el concepto de variable está asociado a la memoria del computador. Esta memoria permite almacenar información para ser usada posteriormente. La función de la memoria es mantener dicho valor todo el tiempo que sea necesario para usarlo tantas veces como se necesite.

Los valores almacenados en la memoria pueden ser modificados cuantas veces se desee. Al almacenar un valor en un elemento determinado de la memoria, dicho valor se mantiene de ahí en adelante, pero sólo hasta que se almacene en dicho elemento un nuevo valor diferente.

Una variable representa un valor almacenado que se puede conservar indefinidamente para ser usado tantas veces como se desee. El valor de una variable se puede modificar en cualquier momento, y será el nuevo valor el que estará almacenado en ella a partir de entonces.

Las variables de un programa se designan mediante nombres o identificadores. El identificador de una variable representa el valor almacenado en dicha variable.

### 3.4.2 Declaración de variables

Cada variable en un programa en C± debe tener asociado un tipo de valor determinado. Esto quiere decir que si una variable tiene asociado el tipo int, por ejemplo, sólo podrá almacenar valores de este tipo, pero no valores de tipo float u otro diferente.

La declaración consiste simplemente en escribir el tipo y el nombre de la variable. La declaración termina con punto y coma (;).

Si varias variables tienen el mismo tipo, se pueden declarar todas conjunta- mente, escribiendo sus nombres seguidos, separados por el carácter coma (.) detrás del tipo comun a todas ellas. 

> Por ejemplo: int dia, mes, año;

### 3.4.3 Uso de variables. Inicialización.

El valor almacenado en una variable puede utilizarse usando la variable como operando en una expresión aritmética. El tipo declarado para cada una de las variables determina las operaciones que posteriormente se podrán realizar con ella.

Para usar una variable de manera correcta es necesario inicializarla antes de usar su valor en ningún cálculo. Inicializar una variable es simplemente darle un valor determinado por primera vez. Si no se especifica el valor inicial al declarar la variable, entonces deberá ser inicializada en el momento adecuado asignándole valor de alguna manera durante la ejecución del programa, antes de usar el valor almacenado para operar con él.

## 3.5 Sentencia de asignacion

Una forma de conseguir que una variable guarde un determinado valor es mediante una sentencia de asignación. Esta sentencia permite inicializar una variable o modificar el valor que tenía hasta el momento. Mediante asignaciones podremos dar valores iniciales a las variables, o guardar en ellas los resultados intermedios o finales
de cualquier programa.

El signo igual (=) es el **operador de asignación**. Este operador indica que el resultado de la expresión a su derecha debe ser asignado a la variable cuyo identificador está a su izquierda.

### 3.5.1 Sentencias de autoincremento y autodecremento

Se dispone de una sentencia especial de autoincremento que utiliza el símbolo ++.

> variable++;

De forma semejante cuando se necesita decrementar una variable en una unidad, sentencia especial de autodecremento que utiliza el símbolo y que se escribe:

> variable--;

### 3.5.2 Compatibilidad de tipos

Una sentencia de asignación puede resultar confusa si el tipo de la variable y el del resultado de la expresión son diferentes. Se convierte previamente de manera automática el valor a asignar al tipo del valor de la variable.

En C± es obligatorio que se realice siempre una conversión explícita de tipos en estos casos. Ejemplo:

> int saldo; float gastos; saldo = int(gastos);

## 3.6 Operaciones de lectura simple

Las operaciones de lectura, al igual que las de escritura, pueden presentar grandes diferencias dependiendo del dispositivo utilizado. Pero, también en este caso, existe en C± un conjunto de procedimientos generales para la lectura de datos, que son invocados siempre de la misma manera, con independencia del dispositivo de entrada utilizado. Por defecto, el dispositivo de entrada suele estar asociado al teclado del terminal por el que se accede al computador. Estos procedimientos están incluidos también en el módulo de librería stdio.

### 3.6.1 El procedimiento scanf

El procedimiento scanf pertenece al módulo de librería stdio. Para leer datosde entrada y almacenarlos en determinadas variables se escribirá:

> scanf(cadena-con-formatos, &variablel, &variable2. ... &variableN);

Debe contener un formato de conversión (%x) por cada variable a leer. Al ejecutarse el procedimiento scanf se van tomando caracteres del dispositivo de entrada, se ponen en correspondencia con los formatos indicados, y se extraen los valores correspondientes a cada formato de conversión para asignarlos a cada variable de la lista, respectivamente.

- Un formato numérico (%d, %f, %g, ...) hace que se salten los siguientes caracteres de espacio en blanco en la entrada, si los hay. A continuación se leen los caracteres no blancos que formen una representación válida de un valor numérico del tipo correspondiente al formato, y el valor nu- mérico obtenido se asigna al siguiente argumento de la lista de variables a leer.

- Un formato %c hace que se lea exactamente el siguiente carácter de la entrada, sea o no espacio en blanco, y se asigne a la siguiente variable a leer.

- Un carácter de espacio en blanco en la cadena con formatos hace que se salten los siguientes caracteres de espacio en blanco en el texto de entrada, si los hay.

- Un carácter no blanco en la cadena con formatos hace que se lea (y se salte) el siguiente carácter de la entrada, que debe coincidir exactamente con el carácter del formato.

Si alguna de las acciones anteriores no puede realizarse, porque el texto de entrada no contiene los caracteres apropiados, la ejecución de **scanf** se interrumpe en ese momento, y no se lee más texto de entrada ni se asignan valores a las variables que falten por leer.

A diferencia de los formatos para **printf**, en que ese tamaño era el mínimo número de caracteres a escribir, en scanf significa el tamaño máximo del dato de entrada a leer.

### 3.6.2 Lectura interactiva

Cuando un programa se comunica con el usuario mediante un terminal de texto se suele programar cada operación de lectura inmediatamente después de una escritura en la que se indica qué dato es el que se solicita en cada momento. 

Por ejemplo:
> float saldo

> printf( "¿Cantidad Pendiente? " );

> scanf( "%f", &saldo );

Tras ejecutar la escritura del texto de petición, en la pantalla se verá:

> ¿Cantidad Pendiente?_

## 3.7  Estructura de un programa con declaraciones

El contenido de un bloque se organizará en dos partes. La primera de ellas contendrá todas las declaraciones de constantes, variables, etc., y la segunda incluirá las sentencia.., ejecutables correspondientes a las acciones a realizar. Las declaraciones pueden hacerse en el orden que se quiera, con la limitación de que cada nombre debe ser declarado antes de ser usado. Las sentencias ejecutables deben escribirse exactamente en el orden en que han de ser ejecu- tadas.