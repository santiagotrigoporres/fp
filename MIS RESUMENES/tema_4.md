# Metodología de desarrollo de programas (I)

## 4.1  La programación como resolución de problemas

La labor de programación puede considerarse como un caso particular de la resolución de problemas. Resolver un problema consiste esencialmente en encontrar una estrategia a seguir para conseguir la solución.

Un programa en el modelo de programación imperativa, se expresa como una serie de instrucciones u órdenes que gobiernan el funcionamiento de una máquina. La máquina va ejecutando dichas instrucciones en el orden preciso que se indique.

Todo programa puede considerarse, de alguna forma, como la solución de un problema determinado, consistente en obtener una cierta información de salida a partir de unos determinados datos de entrada. La tarea de desarrollar dicho programa equivale, por tanto, a la de expresar la estrategia de resolución del problema en los términos del lenguaje de programación utilizado.

## 4.2 Descomposición en subproblemas

Cualquier problema de cierta complejidad necesitará una labor de desarrollo para expresar la solución. El método más general de resolución de problemas no triviales consiste en descomponer el problema original en subproblemas más sencillos,continuando el proceso hasta llegar a subproblemas que puedan ser resueltos de forma directa.

Según esta idea, para desarrollar la estrategia de resolución,habrá que ir identificando subproblemas que se resolverán ejecutando acciones cada vez más simples.

## 4.3 Desarrollo por refinamientos sucesivos

Consiste en expresar inicialmente el programa a desarrollar como una accion global, si es necesario se ira descomponiendo en acciones más sencillas, que pueden ser expresadas directamente como sentencias del lenguaje de programación.

Esta descomposición exige:

- Identificar las acciones componentes.

- Identificar la manera de combinar las acciones componentes para conseguir el efecto global.



