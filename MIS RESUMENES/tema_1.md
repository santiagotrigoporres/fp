# 1. Máquinas y programas

***Maquina*** es un dispositivo o instrumento físico capaz de realizar un cierto trabajo u operación.

---

## 1.1 Maquinas programables

***Las máquinas no automáticas***, o de control manual, son gobernadas por un operador o agente externo que desencadena unas determinadas operaciones en cada momento (Por ejemplo: Maquina de escribir).

***Las máquinas automáticas*** actúan por sí solas, sin necesidad de operador, aunque pueden responder a estímulos externos (Por ejemplo: un ascensor automatico gobierna por si mismo los movimientos de subida y bajada incluyendo cambios de velocidad, apertura y cierre de puertas)

***Una máquina programable*** se puede concebir como una máquina base, de comportamiento fijo, que se completa con una parte modificable que describe el funcionamiento de la máquina base. Esta parte modificable se denomina programa. Una máquina programable se comporta, de manera diferentes en función del programa utilizado.

---

## 1.2 Concepto de cómputo

> Cómputo: Determinación indirecta de una cantidad mediante el cálculo de ciertos datos.

En informática y de una forma general puede identificarse el concepto de cómputo con el de tratamiento de la información. Ejemplo de cómputo: 34x5+8x7.

Un cómputo se concibe también como un proceso a lo largo del cual se van realizando operaciones o cálculos elementales hasta conseguir el resultado final.

- Producto de 34 por 5, obteniendo 170.
- Product.o de 8 por 7, obtcllienno 56.
- Suma de los resultados anteriores, obteniendo 226

---

## 1.3 Concepto de computador

Un computador se define como una máquina programable para tratamiento de la información. Un computador posee unos elementos fijos (máquina base) y otros modificables (programa). De forma simplificada podemos asociar los elementos fijos a los dispositivas físicos del computador, que constituyen el ***hardware***, y los elementos modificables, a las representaciones de los programas en sentido amplio, que constituyen el ***software***.

Los computadores actuales son máquinas de progmma almacenado. En estas máquinas la modificación del programa no implica un cambio de componentes físicos, estas máquinas poseen una memoria en la cual se puede almacenar información de cualquier tipo, debidamente codificada, y esta información incluye tanto los datos con los que opera la máquina como la representación codificada del programa. El programa es, por tanto, pura información, no algo material.

---

# 2. Programación e ingeniería de software

## 2.1 Programación

Este término se suele reservar para designar las tareas de desarrollo de programas en pequeña escala, es decir, realizadas por una sola persona. El desarrollo de programas complejos, exige un equipo más o menos numeroso de personas que debe trabajar de manera organizada. Las técnicas para desarrollo de software a gran escala constituyen la ***ingeniería de software***.

El desarrollo de software en gran escala consiste esencialmente en descomponer el trabajo total de programación en partes independientes que pueden ser desarrolladas por miembros individuales del equipo. La ingeniería de software se limita a añadir técnicas o estrategias organizativas a las técnicas básicas de programación. El trabajo en equipo es, en último extremo, la suma de los trabajos realizados por los individuos.


## 2.2 Objetivos de la programación

Entre los objetivos particulares de la programación podemos reconocer los siguientes:


- Corrección: Es evidente que un programa no debe producir resultados erróneos. Esto tiene una consecuencia inmediata que no siempre se considera evidente: antes de desarrollar un programa debe especificarse con toda claridad cuál es el funcionamiento esperado. Sin dicha especificación es inútil hablar de funcionamiento correcto.

- Claridad: Prácticamente todos los programas han de ser modificados después de haber sido desarrollados inicialmente. Por esta razón es fundamental que sus descripciones sean claras y fácilmente inteligibles por otras personas distintas de su autor, o incluso por el mismo autor al cabo de un cierto tiempo, cuando ya ha olvidado los detalles del programa.

- Eficencia: Una tarea de tratamiento de información puede ser programada de muy diferentes maneras sobre un computador determinado, es decir, habrá muchos programas distintos que producirán los resultados deseados. Algunos de estos programas serán más eficientes que otros. Los programas eficientes aprovecharán mejor los recursos disponibles y, por tanto, su empleo será más económico en algún sentido.

Si se trata de establecer una importancia relativa entre los distintos objetivos, habría que considerar como prioritaria la corrección. A continuación debe perseguirse la claridad, que como ya se ha indicado es necesaria para poder realizar modificaciones, o simplemente para poder certificar que el programa es correcto.
Finalmente ha de atenderse a la eficiencia. Este objetivo, aunque importante, sólo suele ser decisivo en determinados casos.

## 2.3 Lenguajes de programción

La forma de codificar programas de una máquina en particular se dice que es su **código de máquina o lenguaje de máquina**. La palabra "lenguaje" es en realidad, una transcripción directa del término inglés "language", cuyo significado correcto es "idioma".

## 2.4 Compiladores e Intérpretes

Un programa escrito en un lenguaje de programación simbólico puede ser ejecutado en máquinas muy diferentes. Pero para ello se necesita disponer de los mecanismos adecuados para transformar ese programa simbólico en un programa en el lenguaje particular de cada máquina.

Un compilador es un programa que traduce prgrarnas de un lenguaje de programación simbólico a código de máquina. A la representación del programa en lenguaje simbólico se le llama programa fuente, y su representación en código de máquina se le llama programa objeto. Análogamente al lenguaje simbólico y al lenguaje máquina se les llama también lenguaje fuente y lenguaje objeto, respectivamente.

La ejecución del programa mediante compilador exige al menos dos etapas separadas. En la primera de ellas se traduce el programa simbólico a código de máquina mediante el programa compilador. En la segunda etapa se ejecuta ya directamente el programa en código de máquina, y se procesan los datos y resultados particulares. La compilación del programa ha de hacerse sólo una vez, quedando el programa en código de máquina disponible para ser utilizado de forma inmediata tantas veces como se desee.

Un intérprete es un programa que analiza directamente la descripción simbolica del programa fuente y realiza las operaciones oportunas.

## 2.5 Modelos abstractos de computo

Los lenguajes de programación permiten describir programas o cómputos de manera formal, y por tanto simbólica y rigurosa. 

> **Modelo abstracto** de cómputo recoge los elementos básicos y formas de combinación de una manera abstracta,prescindiendo de la notación concreta usada en cada lenguaje de programación para representarlos.

Existen diversos modelos abstractos de cómputo, o modelos de programación, que subyacen en los lenguajes de programación actuales. Entre ellos:

### 2.5.1 Modelo funcional

Se basa casi exclusivamente en el empleo de funciones. Una función es una aplicación, que hace corresponder un elemento de un conjunto de destino (resutado) a cada elemento de un conjunto de partida (argumento) para el que la función esté definida.

Para describir cómputos complejos, las funciones pueden combinarse unas con otras, de manera que el resultado obtenido en una función use como argumento para otra. De esta manera un cómputo tal como
> 34x5+8 x 7

puede representarse de manera funcional de la forma:

> Suma( Producto( 34, 5 ), Producto( 8, 7 ) )

El proceso de cómputo, llamado **reducción**, se basa en reemplazar progresivamente cada función por el resultado de la misma.

La programación funcional permite la definición de nuevas funciones a partir de las ya existentes. Utilizando de manera convencional el símbolo **::=** para indicar definición, podremos crear una nueva función, Cuadrado, para obtener el cuadrado de un número basándose en el uso del producto de dos números.
> Cuadrado( x ) ::= Producto( x, x )

Cuando en un cómputo intervienen funciones definidas, la evaluación se sigue haciendo por sustitución. El proceso, llamado **reescritura**, consiste en reemplazar una función por su definición, sustituyendo los argumentos simbólicos en la definición por los argumentos reales en el cómputo.

### 2.5.2 Modelo de flujo de datos.

Un operador espera hasta tener los valores presentes en sus entradas y entonces se activa el solo, consume dichos valores, calcula el resultado y lo envia a la salida. Puede organizarse de manera iterativa, obteniendo una serie de resultados. Añadiendo operadores de bifurcacion se puede conseguir que se detenga al llegar a un resultado adecuado.

### 2.5.3 Modelo de programación lógica 

Un programa consiste en plantear de manera formal un problema a base de declarar una serie de elementos conocidos y luego preguntar por el resultado. dejandp que la maquina decida como obtener el resultado. En programación lógica los elementos conocidos que pueden declararse son hechos y reglas. 

- Hecho: es una relación entre objetos concretos.
- Regla: es una relación general entre objetos que cumplen ciertas propiedades. 

Una relación entre objetos la ponemos como el nombre de dicha relacion y luegos los objetos relacionados entre parentesis. **Hijo(Juan, Luis)**.
Juan es hijo de Luis.

### 2.5.4 Modelo imperativo 

El modelo de programación imperativa responde a la escritura interna habitual de un computador, que se denomina **Von Nuemann**. El nombre de programación imperativa deriva en el hecho de que un programa aparece como una lista de ordenes a cumplir. El orden de ejecución puede alterarse en caso necesario mediante el uso de instruciones de control. Se consigue ejeecutar, o no, o repetir. La capacidad de se representa con el uso de ***variables***. Una variable representa un dato almacenado bajo el nombnre dado. Ese valor puede sser usado o modificado tantas veces como se desee.

## 2.6 Elementos de la programación imperativa.

### 2.6.1 Procesador, entorno, acciones 

Con el modelo de programacion imperativa se define un **programa** como una lista de órdenes o instrucciones que han de ir siendo ejecutadas por la maquina en el orden preciso que se indique.
El **procesador** es todo agente capaz de entender las ordenes del programa y ejecutarlas. Es un elemento de control. Para ejecutar las instrucciones empleara los recursos necesarios. Todos los elementos disponibles para ser utilizados pro el procesador constituyen su entorno. Un programa imperativo aparece así corno la descripción de una serie de acciones a realizar en un orden preciso. Las acciones son la idea abstracta equivalente a las instrucciones de un programa real.

### 2.6,2 Acciones primitivas. Acciones compuestas

**Acciones primitivas** son las que pueden ser realizadas por el processador, son bastante sencillas. **Acción Compuesta** son un elemento de descripción que facilita la compresión del programa en su conjunto, o de partes importantes de él.

### 2.6.3 Esquema de acciones 

La manera en la que varias acciones sencillas se combinan para realizar una acción complicada se denomina esquema de la acción compuesta. Una buena metodología de programación exige usar esquemas sencillos y fá- ciles de entender a la hora de desarrollar acciones compuestas.

## 2.7 Evolución de la programación 

### 2.7.1 Evolución comparativa Hardware/Software

Los primeros computadores eran máquinas extraordinariamente costosas, y con una capacidad ridicula. Hacían posibles determinados trabajos de cálculo inabordables hasta entonces.

Los programas se escribían directamente en el lenguaje de maquina o en lenguaje ensamblador en que cada instrucción de máquina se representaba mediantee un codigo memotecnico para facilirar la lectura del programa.

Se consideraba que el mejor programa era el que reealizaba el trabajo en menos tiempo y usando el minimo de recursos de máquina. Dada la limitación de capacidad de las máquinas, los programas eran necesariamente sencillos, en términos relativos, y se consideraba que podían llegar a depurarse de errores mediante ensayos o pruebas en número suficiente.

### 2.7.2 Necesidad de metodología y buenas prácticas

Es necesario aplicar técnicas de desarrollo muy precisas para controlar el producto obtenido. Las técnicas aplicables a proyectos desarrollados en equipo constituyen la **ingeniería de software**. Para aplicaciones grandes la claridad se convierte en un objetivo prioritario, ya que resulta imposible analizar y modificar un programa si no se comprende suficientemente su funcionamiento.

Para facilitar la obtención de programas correctos, sin fallos, se pueden emplear técnicas formales, que permitan en lo posible garantizar la corrección del programa mediante demostraciones lógico-matemáticas y no mediante ensayos en busca de posibles errores.
